<?php
return [ 'ROLES' => array('manager' => 'Manager','assistant_manager' =>'Assistant Manager','team_lead'=>'Team Lead','tele_caller'=>'Tele Caller','Verifier Head'=>'Verifier Head','Verifier'=>'Verifier','Ftl'=>'Ftl','Fo'=>'Fo','Voc'=>'Voc'),
	'url'=>'http://www.cherishgold.com/crm/api/',
	'BpoCrmKey'=>['call'=>'callstatusandsubstatus','bpo'=>'bpo','state'=>'state','city'=>'city'],
	'timecache'=>240,
	'urlimage'=>'http://localhost/crmnew/sp-laravel/public/',
	'userhierarchy'=>['Manager'=>'Admin','Assistant Manager'=>'Manager','Team Lead'=>'Assistant Manager','Tele Caller'=>'Team Lead','Verifier Head'=>'Admin','Verifier'=>'Verifier Head','Spoc'=>'Admin','Ftl'=>'Admin','Fo'=>'Ftl','Voc'=>'Admin'],
	'selfurl'=>'localhost/cherishgoldwebsite/',
	'FoPayment'=>[1=>'Online',4=>'Cash Received',2=>'Cheque Received',10=>'Paytm',6=>'NEFT/RTGS'],
	'niremarks'=>['financialissue'=>'Financial Issue','productunsatisfied'=>'Product Unsatisfied','unconvinced'=>'Unconvinced','other'=>'Others'],
	'Met'=>['saledone'=>'Sale Done','followup'=>'Follow Up','callback'=>'Call Back','notint'=>'Not Interested'],
	'Notmet'=>['notcontactable'=>'Not Contactable','fieldissue'=>'Postpond By field'],
	'Metkey'=>['saledone','followup','callback','notint'],
];
?>
