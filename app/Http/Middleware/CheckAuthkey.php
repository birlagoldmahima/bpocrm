<?php

namespace App\Http\Middleware;

use Closure;
use App\BpoDetail;
use Session;

class CheckAuthkey
{	
	 public function handle($request, Closure $next){
        $token=getallheaders()['api_token'];
        
        //$arrvalue = explode("__",$token);
        $dbdata=BpoDetail::where('api_key',$token)
                    ->first();
        if((empty($dbdata))){
								
            $array=array('success'=>false,'message'=>'Invalid Token');
            return response($array,422);
        }
        Session::put('api_key',$token);
        $request->merge(['bpo_id'=>$dbdata->id]);
        return $next($request);
    }
}
