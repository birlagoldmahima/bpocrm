<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use DB;

class DashboardController extends Controller {

    public function dashboard() {
        $received = 'received';
        $bpo_id = Auth::user()->bpo_id;
        $report_manager = Auth::user()->id;
        $role = Auth::user()->role;
        
        $datasales = DB::table("leads")
                        ->leftJoin("users", "leads.rel_manager_id", '=', "users.id")
                        ->where('leads.bpo_id', $bpo_id)
                        ->where('leads.status', 'Converted')
                        ->where('leads.customer_status','Verified')
                        ->where(function($query) use ($role, $report_manager) {
                            if ($role == 'Team Lead') {
                                $query = $query->where('users.reporting_manager', '=', " $report_manager");
                            } else if ($role == 'Tele Caller') {
                                $query = $query->where('users.id', '=', " $report_manager");
                            } elseif ($role == 'Assistant Manager') {
                                $query->whereIn("rel_manager_id", $this->assitance_manager_to_tl());
                            }
                        })
                        ->select(DB::Raw("cast(sum(goldallocate) as char) as gram"),DB::Raw("sum(emiamount) as emi"),DB::raw('count(reference_id) as totalconverted') )
                        ->first();
        $resultsales = json_decode(json_encode($datasales), 1);
        $data['totalsales'] = empty($resultsales['emi']) ? 0 : $resultsales['emi'];
        $data['totalsalestola'] = empty($resultsales['gram'])? 0 : $resultsales['gram'];
        $data['converted'] = $resultsales['totalconverted'];
        
        $datareceive = DB::table("leads")
                         ->leftJoin("users", "leads.rel_manager_id", '=', "users.id")
                         ->where('leads.bpo_id', $bpo_id)
                         ->where('leads.status', 'Received')
                         ->where('leads.customer_status', 'Request')
                         ->where(function($query) use ($role, $report_manager) {
                            if ($role == 'Team Lead') {
                                $query = $query->where('users.reporting_manager', '=', " $report_manager");
                            } else if ($role == 'Tele Caller') {
                                $query = $query->where('users.id', '=', " $report_manager");
                            } elseif ($role == 'Assistant Manager') {
                                $query->whereIn("rel_manager_id", $this->assitance_manager_to_tl());
                            }   
                       })
                       ->select(DB::raw('count(leads.reference_id) as totallead'))
                       ->first();
        $resultreceive = json_decode(json_encode($datareceive), 1);
        $data['leadreceive'] = $resultreceive['totallead'];
        
        $dataverification = DB::table("leads")
                              ->leftJoin("users", "leads.rel_manager_id", '=', "users.id")
                              ->where('leads.bpo_id', $bpo_id)
                              ->where('leads.status', 'Converted')
                              ->where('leads.customer_status','Active')
                              ->where(function($query) use ($role, $report_manager) {
                                if ($role == 'Team Lead') {
                                    $query = $query->where('users.reporting_manager', '=', " $report_manager");
                                } else if ($role == 'Tele Caller') {
                                   $query = $query->where('users.id', '=', " $report_manager");
                                } elseif ($role == 'Assistant Manager') {
                                    $query->whereIn("rel_manager_id", $this->assitance_manager_to_tl());
                                }
                              })
                              ->select(DB::Raw("count(reference_id) as cntref"))
                              ->first();
        $resultverification = json_decode(json_encode($dataverification), 1);
        $data['verification'] = $resultverification['cntref'];
        
        $datalead = DB::table("leads")
                      ->leftJoin("users", "leads.rel_manager_id", '=', "users.id")
                      ->where('leads.bpo_id',$bpo_id)
                      ->where('leads.status', 'In Progress')
                      ->where(function($query) use ($role, $report_manager) {
                            if ($role == 'Team Lead') {
                                $query = $query->where('users.reporting_manager', '=', " $report_manager");
                            } else if ($role == 'Tele Caller') {
                                $query = $query->where('users.id', '=', " $report_manager");
                            } elseif ($role == 'Assistant Manager') {
                                $query->whereIn("rel_manager_id", $this->assitance_manager_to_tl());
                            }
                      })
                      ->select(DB::raw('count(leads.reference_id) as totallead'))
                        ->first();
        $resultlead = json_decode(json_encode($datalead), 1);
        $data['lead'] = $resultlead['totallead'];
        
        $datanotintersted = DB::table("leads")
                              ->leftJoin("users", "leads.rel_manager_id", '=', "users.id")
                              ->where('leads.bpo_id',$bpo_id)
                              ->where('leads.status', 'Not Interested')
                              ->where(function($query) use ($role, $report_manager) {
                                if ($role == 'Team Lead') {
                                    $query = $query->where('users.reporting_manager', '=', "$report_manager");
                                } else if ($role == 'Tele Caller') {
                                    $query = $query->where('users.id', '=', " $report_manager");
                                } elseif ($role == 'Assistant Manager') {
                                    $query->whereIn("rel_manager_id", $this->assitance_manager_to_tl());
                                }
                            })
                            ->select(DB::raw('count(leads.reference_id) as totallead'))
                            ->first();
        $resultnotintersted = json_decode(json_encode($datanotintersted), 1);
        $data['notintersted'] = $resultnotintersted['totallead'];

        
        $datacallback = $this->callback_detail()
                              ->select(DB::raw('count(callback_alerts.id) as callbackdata'))
                              ->first();
        $resultcallback = json_decode(json_encode($datacallback), 1);
        $data['callback'] = $resultcallback['callbackdata'];
        $data['controller'] = "dashboard";
        $data['action'] = "";
        return view('dashboard',$data);
    }

    public function callback_detail() {
        $bpo_id = Auth::user()->bpo_id;
        $report_manager = Auth::user()->id;
        $role = Auth::user()->role;
        return DB::table("callback_alerts")
                  ->Join("leads", "leads.reference_id",'=',"callback_alerts.customer_id")
                  ->Join("users", "leads.rel_manager_id",'=',"users.id")
                  ->where('leads.bpo_id',$bpo_id)
                  ->where('leads.status','In Progress')
                  ->where(DB::raw('date(callbacktime)'),'<=',DB::raw("date(now())"))
                  ->where(function($query) use ($role, $report_manager) {
                        if ($role == 'Team Lead') {
                            $query = $query->where('users.reporting_manager', '=', " $report_manager");
                        } else if ($role == 'Tele Caller') {
                            $query = $query->where('users.id', '=', " $report_manager");
                        } elseif ($role == 'Assistant Manager') {
                            $query->whereIn("rel_manager_id", $this->assitance_manager_to_tl());
                        }
                   });
    }

    public function callBackData(){
        $result = $this->callback_detail()
                       ->select('leads.name as leadname','reference_id','users.name as username','callbacktime','leads.mobile as leadmobile','leads.email as leademail')
                       ->paginate(50);
        $data['result'] = $result;
        $data['controller'] = "";
        $data['action'] = "";
        return view('call-back',$data);
    }
    

}
