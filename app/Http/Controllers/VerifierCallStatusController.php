<?php

    namespace App\Http\Controllers;

    use Illuminate\Http\Request;
    use Illuminate\Http\Response;
    use App\VerifierCallStatus;
    use Illuminate\Pagination\Paginator;
    use Illuminate\Pagination\LengthAwarePaginator;

    class VerifierCallStatusController extends Controller {

        protected function validator(array $data) {
            return Validator::make($data, [
                        'name' => 'required|max:255',
                        'order' => 'required|max:50',
                        'status' => 'required',
            ]);
        }

        public function listVerifierCallStatus(Request $request) {
            $search = $request->input('q');
            $search_option = $request->input('search_option');
            $arraySearch = array('criteria' => $search_option, 'value' => $search);

            $data['results'] = VerifierCallStatus::where(function ($query) use($arraySearch) {
                                if (!empty($arraySearch['criteria'])) {
                                    $search = $arraySearch['value'];
                                    if ($arraySearch['criteria'] == 'name') {
                                        $query = $query->where('name', 'LIKE', "%$search%");
                                    }
                                    if ($arraySearch['criteria'] == 'order') {
                                        $query = $query->where('order', 'LIKE', "%$search%");
                                    }
                                    if ($arraySearch['criteria'] == 'status') {
                                        $query = $query->where('status', 'LIKE', "%$search%");
                                    }
                                }
                            })->orderBy('name', 'asc')
                            ->paginate(20)->appends(['q' => $search, 'search_option' => $search_option]);

            $data['controller'] = 'verifierCallStatus';
            $data['action'] = 'verifierCallStatusList';

            return view('verifierCallStatus/listVerifierCallStatus', $data)->with([
                        'search' => $search,
                        'search_option' => $search_option,
                        'selectOption' => $arraySearch['criteria'],
            ]);
        }

        public function createVerifierCallStatus(Request $request) {
            $data['controller'] = 'verifierCallStatus';
            $data['action'] = 'createVerifierCallStatus';

            return view('verifierCallStatus/createVerifierCallStatus', $data);
        }

        public function storeVerifierCallStatus(Request $request) {
            $this->validate($request, [
                'name' => 'required',
                'order' => 'required',
                'status' => 'required'
            ]);

            $verifierCallStatus = new VerifierCallStatus;

            $verifierCallStatus->name = $request['name'];
            $verifierCallStatus->order = $request['order'];
            $verifierCallStatus->status = $request['status'];

            $verifierCallStatus->save();

            return redirect('listVerifierCallStatus');
        }

        public function editVerifierCallStatus(Request $request, $id) {
            $data['verifierCallStatus'] = VerifierCallStatus::findOrFail($id);

            $data['controller'] = 'verifierCallStatus';
            $data['action'] = 'editVerifierCallStatus';

            return view('verifierCallStatus/editVerifierCallStatus', $data);
        }

        public function updateVerifierCallStatus(Request $request) {
            $this->validate($request, [
                'name' => 'required',
                'order' => 'required',
                'status' => 'required'
            ]);

            $id = $request->id;

            $post = VerifierCallStatus::findOrFail($id);

            $post->name = $request['name'];
            $post->order = $request['order'];
            $post->status = $request['status'];

            $post->save();

            return redirect('listVerifierCallStatus')->withMessage('message', 'Call status update successfully');
        }

        public function activateVerifierCallStatus(Request $request, $id) {
            $verifier = VerifierCallStatus::find($id);
            $verifier->status = 'active';
            $verifier->save();

            return redirect()->back();
        }

        public function deactivateVerifierCallStatus(Request $request, $id) {
            $verifier = VerifierCallStatus::find($id);
            $verifier->status = 'inactive';

            $verifier->save();

            return redirect()->back();
        }

    }
    