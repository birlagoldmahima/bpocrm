<?php
namespace App\Http\Controllers;

use Mail;
use App\Interactions;
use App\User;
use App\Lead;
use App\CallbackAlerts;
use DateTime;
use Validator;
use Excel;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Pagination\Paginator;
use App\VerifierInteraction;
use DB;
use Session;
use App\EmailTemplate;

class LeadController extends Controller {

    public function index() {
        $leadsourcevalue = DB::table('lead_source')
                              ->select("leadsource_id", "leadsource_name", "leadsource_bpo_id")
                              ->get();
        $data['states'] = getstate();
        $data['cities'] = array();
        $data['controller'] = "lead";
        $data['action'] = "leadadd";
        $data['leadsourcevalue'] = $leadsourcevalue;
        return view('lead/lead-create', $data);
    }

    public function citydetailskeyval(Request $request) {
        $stateid = $request['stateid'];
        return getcity($stateid);
    }

    public function leadCreate(Request $request) {
        $data = $this->addlead($request);
        if(Auth::user()->role=="Tele Caller"){
            return redirect()->route('lead-details', [$data->reference_id]);
        }
        return redirect()->route('unassigned-lead');
    }

    public function leadList(Request $request, $sep = null) {
        $arrsearch = array('option' => $request['option'], 'q' =>$request['q']);
        $role = Auth::user()->role;
        $user_id = Auth::user()->id;
        $dataunion = DB::table("leads")
                       ->join("users","id",'=','rel_manager_id')
                       ->leftjoin("interactions","interactions.interaction_id",'=','leads.last_interaction_id')
                       ->where("leads.status",'In progress')
                       ->where("leads.bpo_id", Auth::user()->bpo_id)
                       ->whereNotNull('rel_manager_id')
                       ->where(function($q) use ($role,$user_id){
                            if($role=='Team Lead'){
                                $q->where("reporting_manager",$user_id);
                            }elseif($role=='Tele Caller'){
                                $q->where("rel_manager_id",$user_id);
                            }
                        })
                        ->where(function ($query) use ($arrsearch) {
                            if (!empty($arrsearch['option'])) {
                                $search = $arrsearch['q'];
                                if ($arrsearch['option'] == 'ID')
                                    $query = $query->where('leads.reference_id', '=', "$search");
                                else if ($arrsearch['option'] == 'Name')
                                    $query = $query->where('leads.name', 'LIKE', "%$search%");
                                else if ($arrsearch['option'] == 'Mobile')
                                    $query = $query->where('leads.mobile', 'LIKE', "%$search%");
                                else if ($arrsearch['option'] == 'Email')
                                    $query = $query->where('leads.email', 'LIKE', "%$search%");
                                else if ($arrsearch['option'] == 'Status')
                                    $query = $query->where('leads.status', 'LIKE', "%$search%");
                            }
                        })
                        ->select("leads.reference_id", "leads.name", "leads.city_name", "leads.mobile", "leads.state_name", "leads.email", "leads.created", DB::raw("Null as applicant_name"), DB::raw("'Lead' as type"), "leads.rel_manager_id","leads.customer_id", "leads.lead_source", "leads.lead_source_info", "leads.customer_status", "leads.status", "leads.associative_no", "leads.form_no","users.name as username","interactions.call_substatus_id","interactions.call_status_id")
                        ->orderBy('reference_id','desc')
                        ->paginate(10);

        $data['result'] = $dataunion;
        $data['controller'] = 'lead';
        $data['action'] = 'assign-lead-list';
        return view('lead.lead-list', $data)->with($arrsearch);
    }
    
    
    
    public function tlverifyleadList(Request $request, $sep = null) {
        $arrsearch = array('option' => $request['option'], 'q' =>$request['q']);
        $role = Auth::user()->role;
        $user_id = Auth::user()->id;
        $dataunion = DB::table("leads")
                       ->join("users","id",'=','rel_manager_id')
                       ->leftjoin("interactions","interactions.interaction_id",'=','leads.last_interaction_id')
                       ->where("leads.status",'In progress')
                       ->where("leads.bpo_id", Auth::user()->bpo_id)
                       ->whereNotNull('rel_manager_id')
                       ->where(function($q) use ($role,$user_id){
                            if($role=='Team Lead'){
                                $q->where("reporting_manager",$user_id);
                            }elseif($role=='Tele Caller'){
                                $q->where("rel_manager_id",$user_id);
                            }
                        })
                        ->where(function ($query) use ($arrsearch) {
                            if (!empty($arrsearch['option'])) {
                                $search = $arrsearch['q'];
                                if ($arrsearch['option'] == 'ID')
                                    $query = $query->where('leads.reference_id', '=', "$search");
                                else if ($arrsearch['option'] == 'Name')
                                    $query = $query->where('leads.name', 'LIKE', "%$search%");
                                else if ($arrsearch['option'] == 'Mobile')
                                    $query = $query->where('leads.mobile', 'LIKE', "%$search%");
                                else if ($arrsearch['option'] == 'Email')
                                    $query = $query->where('leads.email', 'LIKE', "%$search%");
                                else if ($arrsearch['option'] == 'Status')
                                    $query = $query->where('leads.status', 'LIKE', "%$search%");
                            }
                        })
                        ->select("leads.reference_id", "leads.name", "leads.city_name", "leads.mobile", "leads.state_name", "leads.email", "leads.created", DB::raw("Null as applicant_name"), DB::raw("'Lead' as type"), "leads.rel_manager_id","leads.customer_id", "leads.lead_source", "leads.lead_source_info", "leads.customer_status", "leads.status", "leads.associative_no", "leads.form_no","users.name as username","interactions.call_substatus_id","interactions.call_status_id")
                        ->orderBy('reference_id','desc')
                        ->paginate(10);

        $data['result'] = $dataunion;
        $data['controller'] = 'tllead';
        $data['action'] = 'tlassign-lead-list';
        return view('lms.tllead', $data)->with($arrsearch);
    }

    public function leadDetails($id) {
        $result = DB::table("leads")
                ->where('bpo_id',Auth::user()->bpo_id)
                ->where("leads.reference_id", '=',$id)
                ->where('leads.status','In Progress')
                ->select("leads.appointment_date", "leads.appointment_time", "leads.verifier", "leads.reference_id", "leads.mobile", "leads.name", "leads.email", "leads.created", "leads.informed_to_reference", "leads.emi_size", "leads.status", "leads.reference_id", "leads.json_struct", "leads.converted_customer_id", "leads.customer_status", "leads.associative_no", "leads.form_no", "leads.stage_id", "leads.stage_remarks","rel_manager_id")
                  ->first();
        if(empty($result)){
            abort(501,"Id Not Found!!!");
        }
        $data['reference_data'] = json_decode(json_encode($result), 1);
        $data['callstatus'] = CallStatusSubStatus();
        $interaction = Interactions::where('reference_id', '=', $id)
                ->orderBy('created_at', 'desc')
                ->get();
        $data['interaction'] = json_decode(json_encode($interaction), 1);
        $user = User::where('bpo_id', Auth::user()->bpo_id)
                ->lists('name', 'id');
        $data['rms'] = json_decode(json_encode($user), 1);
        $emailtemplate = EmailTemplate::where('status','active')
                                      ->select('title','id')
                                      ->get();
        $data['emailtemplate'] = json_decode(json_encode($emailtemplate),1);
        $data['controller'] = 'lead';
        $data['action'] = 'assign-lead-list';
        return view('lead/pendinglead-details', $data);
    }

    public function postInteraction(Request $request) {
        $data = array();
        if ($request->isMethod('post')) {
            $callstatusname = callstatus();
            $callsubstatusname = callsubstatus();
            $rms = User::lists('name', 'id');
            $data = array();

            $dispcallback = $callbackdate = "";
            if ($request->nextdatetime != ""){
                $callbackdate = date("Y-m-d h:i:s", strtotime($request->nextdatetime));
                $dispcallback = date("M d, Y H:i",strtotime($request->nextdatetime));
            }
            $interaction = new Interactions;
            $interaction->user_type = $request->user_type;
            $interaction->reference_id = $request->reference_id;
            $interaction->bpo_id = Auth::user()->bpo_id;
            $interaction->rm_id = Auth::user()->id;
            $interaction->call_initiator = $request->callinitiator;
            $interaction->call_status_id = $request->callstatus;
            $interaction->call_substatus_id = $request->callsubstatus;
            $interaction->call_substatus_other = $request->callsubstatusother;
            $interaction->callback_datetime = $callbackdate;
            $interaction->remarks = $request->interaction;
            $interaction->save();

            $flagdelete = DB::table('callback_alerts')
                            ->where('customer_id', '=', $request->reference_id)
                            ->whereIn('user_type', ['reference', 'lead'])
                            ->delete();
            $sessiondata = $request->session()->get('notification') - $flagdelete;
            $request->session()->put('notification', $sessiondata);

            if ($request->nextdatetime != "" && $request->callsubstatus != "66") {
                $refdata = Lead::where([['reference_id', $request->reference_id], ['bpo_id', Auth::user()->bpo_id]])->get()->toArray();
                $type = (empty($refdata[0]['customer_id']) || $refdata[0]['customer_id'] == 0) ? 'lead' : 'reference';
                $objcallback = new CallbackAlerts();
                $objcallback->customer_id = $request->reference_id;
                $objcallback->customer_name = $refdata[0]['name'];
                ;
                $objcallback->customer_mobile = $refdata[0]['mobile'];
                ;
                $objcallback->callbacktime = date("Y-m-d h:i:s", strtotime($request->nextdatetime));
                $objcallback->relationship_manager = Auth::user()->id;
                $objcallback->call_status_id = $request->callstatus;
                $objcallback->call_sub_status_id = $request->callsubstatus;
                $objcallback->user_type = $type;
                $objcallback->bpo_id = Auth::user()->bpo_id;
                $objcallback->save();
                $dataconversion = date("Y-m-d", strtotime($request->nextdatetime));
                if (strtotime($dataconversion) == strtotime(date('Y-m-d'))) {
                    $sessiondata = $request->session()->get('notification') + 1;
                    $request->session()->put('notification', $sessiondata);
                }
            }
           
            $update_lead = Lead::where('reference_id', $request->reference_id)
                    ->update(['last_interaction_id'=>$interaction->interaction_id]);
            $data['interaction'] = array('datetime' => date("M d, Y H:i", strtotime($interaction->created_at)),
                'Logged_By' => $rms[$interaction->rm_id],
                'Remarks' => $interaction->remarks,
                'Call_Initiator' => $interaction->call_initiator,
                'Call_Status' => $callstatusname[$request->callstatus],
                'Call_Sub_Status' => $callsubstatusname[$request->callsubstatus],
                'Other' => $interaction->call_substatus_other,
                'Call_Back' => $dispcallback
            );
            $data['return'] = true;
        }
        return $data;
    }

    function fixAppointment(Request $request) {
        $appointment_date = $request['appointment_date'];
        if ($appointment_date != "") {
            $reference_id = $request['reference_id'];
            return Lead::where('reference_id',$reference_id)
                        ->update(['stage_id'=>1,'stage_remarks'=>'','appointment_date'=>date("Y-m-d H:i:s", strtotime($appointment_date))]);
            
        }
        return 0;
    }

    public function leaddata(Request $request) {
        $stateid = $request['stateid'];
        $array['name'][$request['name']] = 'no';
        $array['mobile'][$request['mobile']] = 'no';
        $array['alt_mobile'][$request['alt_mobile']] = 'no';
        $array['email'][$request['email']] = 'no';
        $array['gender'][$request['gender']] = 'no';
        $array['age'][$request['age']] = 'no';
        $array['marital_status'][$request['marital_status']] = 'no';
        $array['child'][$request['child']] = 'no';
        $array['address'][$request['address']] = 'no';
        $array['landmark'][$request['landmark']] = 'no';
        $array['pincode'][$request['pincode']] = 'no';
        $array['company_name'][$request['company_name']] = 'no';
        $array['department'][$request['department']] = 'no';
        $array['profession'][$request['profession']] = 'no';
        $array['income'][$request['income']] = 'no';
        $array['income_group'][$request['income_group']] = 'no';
        $array['batch_id'][$request['batch_id']] = 'no';
        $array['lead_code'][$request['lead_code']] = 'no';
        $array['campaign_code'][$request['campaign_code']] = 'no';
        $array['investment_question'][$request['investment_question']] = 'no';
        $array['purpose_of_call'][$request['purpose_of_call']] = 'no';
        $array['call_complaince'][$request['call_complaince']] = 'no';
        $array['recording_statement'][$request['recording_statement']] = 'no';
        $array['verification_interaction'][$request['verification_interaction']] = 'no';
        $array['call_status'][$request['call_status']] = 'no';
        $array['call_sub_status'][$request['call_sub_status']] = 'no';
        $array['meet'][$request['meet']] = 'no';
        $array['reason'][$request['reason']] = 'no';
        $array['state'][$request['state']] = 'no';
        $array['city'][$request['city']] = 'no';
        $array['tl_status']["pending"] = 'no';
        $data['verifier'] = json_encode($array);

        $arr['CustomerBGP']['reference_id'] = "";
        $arr['CustomerBGP']['applicant_name'] = $request['name'];
        $arr['CustomerBGP']['contact_email'] = $request['email'];
        $arr['CustomerBGP']['applicant_dob'] = $request['age'];
        $arr['CustomerBGP']['contact_mobile'] = $request['mobile'];
        $arr['CustomerBGP']['applicant_gender'] = ucfirst($request['gender']);
        $arr['CustomerBGP']['contact_phone_residence'] = $request['alt_mobile'];
        $arr['CustomerBGP']['mailing_address'] = $request['address'];
        $arr['CustomerBGP']['mailing_landmark'] = $request['landmark'];
        $arr['CustomerBGP']['mailing_country'] = $request['mailing_country'];
        $arr['CustomerBGP']['mailing_state'] = $request['statename'];
        $arr['CustomerBGP']['mailing_city'] = $request['cityname'];
        $arr['CustomerBGP']['mailing_city'] = $request['cityname'];
        $arr['CustomerBGP']['mailing_pincode'] = $request['pincode'];
        $arr['CustomerBGP']['mailing_district'] = "";
        $arr['CustomerBGP']['user_id'] = "";
        $arr['CustomerBGP']['contact_phone_office'] = '';
        $arr['CustomerBGP']['contact_pan_no'] = '';
        $arr['CustomerBGP']['nomination_name'] = '';
        $arr['CustomerBGP']['nomination_applicant_realtion'] = '';
        $arr['CustomerBGP']['nomination_address'] = '';
        $arr['CustomerBGP']['nomination_state'] = '';
        $arr['CustomerBGP']['nomination_dob'] = '';
        $arr['CustomerBGP']['partner_code'] = '';
        $arr['CustomerBGP']['lead_source'] = '';
        $arr['CustomerBGP']['lead_source_other'] = '';
        $arr['CustomerBGP']['fulfilment'] = '';
        $arr['CustomerBGP']['sendmailflag'] = '';
        $arr['CustomerBGP']['contact_email_previous'] = '';
        $arr['CustomerBGP']['contact_mobile_previous'] = '';
        $arr['CustomerBGP']['declaration_date'] = '';
        $arr['CustomerBGP']['declaration_place'] = '';
        $arr['CustomerPlan']['initial_amount'] = '';
        $arr['CustomerPlan']['initial_pay_by'] = '';
        $arr['CustomerPlan']['initial_cheque_dd_no'] = '';
        $arr['CustomerPlan']['initial_bank_name'] = '';
        $arr['CustomerPlan']['initial_cheque_dd_no_date'] = '';
        $arr['CustomerPlan']['contact_name'] = '';
        $arr['CustomerPlan']['contact_address'] = '';
        $arr['CustomerPlan']['pick_date'] = '';
        $arr['CustomerPlan']['pick_time'] = '';
        $arr['CustomerPlan']['courier_name'] = '';
        $arr['CustomerPlan']['courier_tracking'] = '';
        $arr['CustomerPlan']['transaction_id'] = '';
        $data['json_struct'] = json_encode($arr);
        return $data;
    }

    public function addlead($request) {
        $alldata = $this->leaddata($request);
        $state = $request['state'];
        $city = $request['city'];
        $informed = $request['informed'];
        $emi_size = $request['emi_size'];
        $leadcreate = new Lead();
        $leadcreate->name = $request['name'];
        $leadcreate->mobile = $request['mobile'];
        $leadcreate->email = $request['email'];
        $leadcreate->state_id = $state;
        $leadcreate->district_id = $city;
        $leadcreate->state_name = $request['statename'];
        $leadcreate->city_name = $request['cityname'];
        $leadcreate->informed_to_reference = 'No';
        $leadcreate->status = 'In Progress';
        $leadcreate->customer_status = 'Draft';
        $leadcreate->lead_source = $request['leadsourcevalue'];
        $leadcreate->source = 'RM';
        $auth = Auth::user();
        if (isset($auth)) {
            if(Auth::user()->role == "Tele Caller") {
               $leadcreate->rel_manager_id = !empty($request['user_id']) ? $request['user_id'] : Auth::user()->id;
            }     
        } else {
            $leadcreate->rel_manager_id = $request['user_id']; 
        }
        $leadcreate->emi_size = $emi_size;
        $leadcreate->verifier = $alldata['verifier'];
        $leadcreate->created = new DateTime();
        $leadcreate->modified = new DateTime();
        $leadcreate->bpo_id = !empty($request['bpo_id']) ? $request['bpo_id'] : Auth::user()->bpo_id;
        $leadcreate->json_struct = $alldata['json_struct'];
        ;
        $leadcreate->save();
        return $leadcreate;
    }

    public function leadedit(Request $request) {
        $alldata = $this->leaddata($request);
        $state = $request['state'];
        $city = $request['city'];
        $emi_size = $request['emi_size'];
        $leadcreate = Lead::findOrFail($request['reference_id']);
        $leadcreate->name = $request['name'];
        $leadcreate->mobile = $request['mobile'];
        $leadcreate->email = $request['email'];
        $leadcreate->state_id = $state;
        $leadcreate->district_id = $city;
        $leadcreate->state_name = $request['statename'];
        $leadcreate->city_name = $request['cityname'];
        $leadcreate->informed_to_reference = 'No';
        $leadcreate->status = 'In Progress';
        //$leadcreate->customer_status = 'Draft';
        $leadcreate->lead_source = $request['leadsourcevalue'];
        $leadcreate->source = 'RM';
        if (Auth::user()->role == "Tele Caller") {
            $leadcreate->rel_manager_id = !empty($request['user_id']) ? $request['user_id'] : Auth::user()->id;
        }
        $leadcreate->emi_size = $emi_size;
        $leadcreate->verifier = $alldata['verifier'];
        $leadcreate->created = new DateTime();
        $leadcreate->modified = new DateTime();
        $leadcreate->bpo_id = !empty($request['bpo_id']) ? $request['bpo_id'] : Auth::user()->bpo_id;
        $leadcreate->json_struct = $alldata['json_struct'];
        ;
        $leadcreate->save();
        return redirect()->route('lead-list');
    } 

    public function generatelink(Request $request){
        $id = $request['id'];
        $token = Session::get('api_key');
        $url = config('custom.selfurl').'register/?id='.base64_encode(http_build_query(['id'=>$id,'api_token'=>$token]));
        return $url;
    }

    public function checkemailmobile(Request $request){
        $email = $request['email'];
        $mobile = $request['mobile'];
        $leadid = $request['lead_id'];
        $data = Lead::where('mobile',$mobile)
                    ->where(function($q) use ($leadid){
                        if(!empty($leadid)){
                            $q->where('reference_id','!=',$leadid);
                        }
                    })
                    //->orWhere('email',$email)
                    ->first();
        if(empty($data)){
           return 0;
        }
        return 1;
    }

    public function editlist(){
        $result = DB::table("leads")
                    ->where('bpo_id',Auth::user()->bpo_id)
                    ->where("leads.reference_id", '=',$id)
                    ->where('leads.status','In Progress')
                    ->select("leads.appointment_date", "leads.appointment_time", "leads.verifier", "leads.reference_id", "leads.mobile", "leads.name", "leads.email", "leads.created", "leads.informed_to_reference", "leads.emi_size", "leads.status", "leads.reference_id", "leads.json_struct", "leads.email", "leads.converted_customer_id", "leads.customer_status", "leads.associative_no", "leads.form_no", "leads.stage_id", "leads.stage_remarks","rel_manager_id")
                    ->first();
        if(empty($result)){
            abort(501,"Id Not Found!!!");
        }
    }

    public function changerequestref($id){
        $result = Lead::where('bpo_id',Auth::user()->bpo_id)
                      ->where("leads.reference_id",'=',$id)
                      ->where('leads.status','In Progress')
                      ->first();
        if(empty($result)){
            abort(501,"Id Not Found!!!");
        }
        $data['reference_data'] = json_decode(json_encode($result),1);
        $state = getstate();
        $data['states'] = $state;
        $data['nomstates'] = $state;
        $data['controller'] = 'lead';
        $data['action'] = 'assign-lead-list';
        return view('lead.changerequest',$data);
    }

    public function getcitydistrictpincode(Request $request){
        $stateId = $request['stateid'];
        return citydistrictpincode($stateId);
    }

    public function getpincodelist(Request $request){
        $district = $request['district'];
        return pincode($district);
    }
    
    public function getdistrictlist(Request $request){
        $pincode = $request['pincode'];
        return district($pincode);
    }

    public function checkemail(Request $request){
        $email = $request['email'];
        return checkloginemail($email);
    }

    public function updaterequestref(Request $request){
        $arr['CustomerBGP']= $request->except(['_token','amount','login_email']);
        $arr['CustomerBGP']['reference_id'] = $request['reference_id'];
        $arr['CustomerPlan']['initial_amount']=$request['amount'];
        $arr['CustomerBGP']['user_id']="";
        $arr['CustomerBGP']['mailing_country']="India";
        $arr['CustomerBGP']['mailing_state'] = $request['mailing_state'];
        $arr['CustomerBGP']['mailing_city'] = $request['mailing_city'];
        $arr['CustomerBGP']['mailing_pincode'] = $request['pincode'];
        $arr['CustomerBGP']['mailing_district'] = $request['city_name'];
        $arr['CustomerBGP']['contact_email_previous']=$request['contact_email'];
        $arr['CustomerBGP']['contact_mobile_previous']=$request['contact_mobile'];
        $arr['CustomerBGP']['nominee_accept']="no";
        $arr['CustomerBGP']['declaration_date']=date('d-m-Y');
        $arr['CustomerBGP']['declaration_place']=$request['mailing_city'];
        $arr['CustomerPlan']['initial_pay_by']=$request['payment_mode'];
        $arr['CustomerPlan']['initial_cheque_dd_no']=$request['cheque_no'];
        $arr['CustomerPlan']['initial_bank_name']=$request['bank_name'];
        $arr['CustomerPlan']['initial_cheque_dd_no_date']=$request['cheque_date'];
        $arr['CustomerPlan']['contact_name']=$request['contact_person'];
        $arr['CustomerPlan']['contact_address']=$request['contact_address'];
        $arr['CustomerBGP']['applicant_name'] = $request['applicant_name'];
        $arr['CustomerBGP']['contact_email'] = $request['contact_email'];
        $arr['CustomerBGP']['applicant_dob'] = $request['applicant_dob'];
        $arr['CustomerBGP']['contact_mobile'] = $request['contact_mobile'];
        $arr['CustomerBGP']['applicant_gender'] = ucfirst($request['applicant_gender']);
        $arr['CustomerBGP']['contact_phone_residence'] = $request['contact_phone_residence'];
        $arr['CustomerBGP']['contact_phone_office'] = $request['contact_phone_office'];
        $arr['CustomerBGP']['mailing_address'] = $request['mailing_address'];
        $arr['CustomerBGP']['mailing_landmark'] = $request['mailing_landmark'];
        $arr['CustomerBGP']['contact_pan_no'] = $request['contact_pan_no'];
        $arr['CustomerBGP']['nomination_name'] = $request['nomination_name'];
        $arr['CustomerBGP']['nomination_applicant_realtion'] = $request['nomination_applicant_realtion'];
        $arr['CustomerBGP']['nomination_address'] = $request['nomination_address'];
        $arr['CustomerBGP']['nomination_state'] = $request['nomination_state'];
        $arr['CustomerBGP']['nomination_city'] = $request['nomination_city'];
        $arr['CustomerBGP']['nomination_pincode'] = $request['nomination_pincode'];
        $arr['CustomerBGP']['nomination_dob'] = $request['nomination_dob'];
        $arr['CustomerBGP']['partner_code'] = $request['partner_code'];
        $arr['CustomerBGP']['lead_source'] = $request['lead_source'];
        $arr['CustomerBGP']['lead_source_other'] = $request['lead_source_other'];
        $arr['CustomerBGP']['fulfilment'] = $request['fulfilment'];
        $arr['CustomerBGP']['sendmailflag'] = '';
        $arr['CustomerPlan']['pick_date']=$request['pickup_date'];
        $arr['CustomerPlan']['pick_time']=$request['pickup_time'];
        $arr['CustomerPlan']['courier_name']=$request['courier_name'];
        $arr['CustomerPlan']['courier_tracking']=$request['courier_tracking'];
        $arr['CustomerPlan']['transaction_id']=$request['transaction_id'];
        $jsondata=json_encode($arr);
        Lead::where(["reference_id" => $request['reference_id']])
                   ->update(['json_struct'=>$jsondata,
                           'user_id'=>"",
                           'name'=>$request['applicant_name'],
                           'mobile'=>$request['contact_mobile'],
                           'email'=>$request['contact_email'],
                           'emi_size'=>$request['amount'],
                           'source'=>$request['lead_source'],
                           'state_name'=>$request['mailing_state'],
                           'city_name'=>$request['mailing_district'],
                           'customer_id'=>$request['partner_code'],
                           'registration_data' => $request['login_email']
                        ]);
        return;
    }

    public function sendlinkpayment(Request $request){
        $id = $request['leadid'];
        $leaddata = Lead::where('bpo_id',Auth::user()->bpo_id)
                        ->where("leads.reference_id",'=',$id)
                        ->where('leads.status','In Progress')
                        ->first();
        if(!empty($leaddata)){
            $token = Session::get('api_key');
            echo $url = config('custom.selfurl').'register/payment?id='.base64_encode(http_build_query(['id'=>$id,'api_token'=>$token]));
            $email['messages'] = 'Thanks for showing Interest in your CGP plans. Please click following link for making a payment online <a href='.$url.'>pay</a>';
            $email['to_mail'] = $leaddata->email;
            $email['to_name'] = $leaddata->name;
            $email['from_email'] = "customerservice@cherishgold.com";
            $email['from_name'] = "Cherishgold";
            $email['subject'] = "Pay now link";
            $this->sendemail($email);
            return 1;
        }
        return 0;
    }

    public function sendregisterationlink(Request $request){
        $id = $request['id'];
        $leaddata = Lead::where('bpo_id',Auth::user()->bpo_id)
                        ->where("leads.reference_id",'=',$id)
                        ->where('leads.status','In Progress')
                        ->first();
        if(!empty($leaddata)){
            $url = $this->generatelink($request);
            $email['messages'] = 'Thanks for showing Interest in your CGP plans. Please click following link for making a payment online <a href='.$url.'>pay</a>';
            $email['to_mail'] = $leaddata->email;
            $email['to_name'] = $leaddata->name;
            $email['from_email'] = "customerservice@cherishgold.com";
            $email['from_name'] = "Cherishgold";
            $email['subject'] = "Pay now link";
            $this->sendemail($email);
            return 1;
        }
        return 0;
    }

    public function edit_lead_list(Request $request, $reference_id) {
        $bpo_id = Auth::user()->bpo_id;
        $leadsourcevalue = DB::table('leads')
                             ->where('reference_id', $reference_id)
                             ->where('status','In Progress')
                             ->where('bpo_id', $bpo_id)
                             ->select("*")
                             ->first();
        $leaddata = json_decode(json_encode($leadsourcevalue), 1);
        $data['states'] = getstate();
        $data['cities'] = array();
        $data['leadvalue'] = $leaddata;
        $data['leaddatalist'] = json_decode($data['leadvalue']['verifier']);
        $data['controller'] = 'lead';
        $data['action'] = 'edit_lead_list';
        $leadsourcevalue = DB::table('lead_source')
                             ->select("leadsource_id", "leadsource_name", "leadsource_bpo_id")
                             ->get();
        $data['leadsourcevalue'] = $leadsourcevalue;
        return view('lead/lead-editdata', $data);
    }

    
    //////////////////Here Code Start For Lms//////////////
        public function tl_edit_verifier(Request $request, $reference_id) {
        $bpo_id = Auth::user()->bpo_id;
        $leadsourcevalue = DB::table('leads')
                             ->where('reference_id', $reference_id)
                             ->where('status','In Progress')
                             ->where('bpo_id', $bpo_id)
                             ->select("*")
                             ->first();
        $leaddata = json_decode(json_encode($leadsourcevalue), 1);
        
       $result = DB::table("leads")
                ->where('bpo_id',Auth::user()->bpo_id)
                ->where("leads.reference_id", '=',$reference_id)
                ->where('leads.status','In Progress')
                ->select("leads.appointment_date", "leads.appointment_time", "leads.verifier", "leads.reference_id", "leads.mobile", "leads.name", "leads.email", "leads.created", "leads.informed_to_reference", "leads.emi_size", "leads.status", "leads.reference_id", "leads.json_struct", "leads.converted_customer_id", "leads.customer_status", "leads.associative_no", "leads.form_no", "leads.stage_id", "leads.stage_remarks","rel_manager_id")
                  ->first();
        if(empty($result)){
            abort(501,"Id Not Found!!!");
        }
        $data['reference_data'] = json_decode(json_encode($result), 1);
        $data['callstatus'] = CallStatusSubStatus();
        $interaction = Interactions::where('reference_id', '=', $reference_id)
                ->orderBy('created_at', 'desc')
                ->get();
        $data['interaction'] = json_decode(json_encode($interaction), 1);
        $user = User::where('bpo_id', Auth::user()->bpo_id)
                ->lists('name', 'id');
        $data['rms'] = json_decode(json_encode($user), 1);
        $emailtemplate = EmailTemplate::where('status','active')
                                      ->select('title','id')
                                      ->get();
        $data['emailtemplate'] = json_decode(json_encode($emailtemplate),1);
        
        
        
        
        $data['states'] = getstate();
        $data['cities'] = array();
        $data['leadvalue'] = $leaddata;
        $data['leaddatalist'] = json_decode($data['leadvalue']['verifier']);
        $data['controller'] = 'lead';
        $data['action'] = 'tl-edit-verifier';
        $leadsourcevalue = DB::table('lead_source')
                             ->select("leadsource_id", "leadsource_name", "leadsource_bpo_id")
                             ->get();
        $data['leadsourcevalue'] = $leadsourcevalue;
        return view('lms/tl-edit-verifier', $data);
    }




/////////////Here Code End //////////////
    public function leadinterestedlist(Request $request) {
        $arrsearch=['q'=>$request['q'],'option'=>$request['option']];
        $result = DB::table("leads")
                    ->join("users", "id", '=', 'rel_manager_id')
                    ->where("leads.status", 'In progress')
                    ->where("leads.bpo_id", Auth::user()->bpo_id)
                    ->select("leads.name as leadname", "leads.mobile as leadmobile", "leads.email as leademail","leads.reference_id as leadid","users.name as username")
                    ->where(function ($query) use ($arrsearch) {
                            if (!empty($arrsearch['option'])) {
                                $search = $arrsearch['q'];
                                if ($arrsearch['option'] == 'ID')
                                    $query = $query->where('leads.reference_id', '=', "$search");
                                else if ($arrsearch['option'] == 'Name')
                                    $query = $query->where('leads.name', 'LIKE', "%$search%");
                                else if ($arrsearch['option'] == 'Mobile')
                                    $query = $query->where('leads.mobile', 'LIKE', "%$search%");
                                else if ($arrsearch['option'] == 'Email')
                                    $query = $query->where('leads.email', 'LIKE', "%$search%");
                            }
                        })
                    ->paginate(20);
        $data['controller'] = "notinterested";
        $data['action'] = 'notinterestedlist';
        $data['leadsourcevalue'] = $result;
        return view('lead.lead-notinterested-list', $data)->with($arrsearch);
    }

    public function marknotinterested(Request $request){
        $id = $request['id'];
        return Lead::where('reference_id',$id)
                    ->update(['status'=>'Not Interested']);

    }

    public function leadchangerm(Request $request) {
        $rmid = $request['rm_id'];
        if(!$request->ajax()){
            //if($request->isMethod('post')){
            $this->setsessiondata($request,'ref_value','chk');
            $ref_id=$request->session()->get('ref_value');
            $lead_id = array_reduce($ref_id, 'array_merge', array());
            $this->changerm($lead_id,$rmid);
            return redirect()->back()->with(['messages'=>'RM Change Successfully!!']);
        }else{
            $lead_id = [$request['lead_id']];
            return $this->changerm($lead_id,$rmid);
        }
        

    }

    public function changerm($lead,$rm){
        return Lead::whereIn('reference_id',$lead)
                   ->update(['rel_manager_id'=>$rm]);
    }

    public function leadchangermlist(Request $request) {
        $arrsearch = ['q'=>$request['q'],'option'=>$request['option']];
        $role = Auth::user()->role;
        if($request->isMethod('post')){
            $this->setsessiondata($request,'ref_value','chk');
        }
        $tl = User::where('bpo_id',Auth::user()->bpo_id)
                  ->where('status','active');
        if($role=='Assistant Manager'){
            $tl = $tl->where('reporting_manager',Auth::user()->id);
        }elseif($role=='Team Lead'){
            $tl = $tl->where('id',Auth::user()->id);
        }
        $tl = $tl->where('role','Team Lead')
                 ->pluck('id');
        $user = DB::table('users as a1')
                  ->join('users as a2','a1.reporting_manager','=','a2.id')
                  ->where('a1.role','Tele Caller')
                  ->where('a1.bpo_id',Auth::user()->bpo_id)
                  ->whereIn('a1.reporting_manager',$tl)
                  ->where('a1.status','active')
                  ->select('a1.name as tellcallername','a1.id as tellcallerid','a2.name as tlname')
                  ->get();
        $data['reporting'] = json_decode(json_encode($user),1);

        $result = DB::table("leads")
                    ->join("users", "id",'=','rel_manager_id')
                    ->where("leads.status", 'In progress')
                    ->where("leads.bpo_id",Auth::user()->bpo_id)
                    ->where(function ($query) use ($arrsearch) {
                        if (!empty($arrsearch['option'])) {
                            $search = $arrsearch['q'];
                            if ($arrsearch['option'] == 'ID')
                                $query = $query->where('leads.reference_id', '=', "$search");
                            else if ($arrsearch['option'] == 'Name')
                                $query = $query->where('leads.name', 'LIKE', "%$search%");
                            else if ($arrsearch['option'] == 'Mobile')
                                $query = $query->where('leads.mobile', 'LIKE', "%$search%");
                            else if ($arrsearch['option'] == 'Email')
                                $query = $query->where('leads.email', 'LIKE', "%$search%");
                        }
                    })
                    ->select("leads.name as leadname", "leads.mobile as leadmobile", "leads.email as leademail", "leads.rel_manager_id as rel_manager_id","leads.reference_id as leadid", "users.name as username")
                    ->paginate(5);
        $data['controller'] = "reportc";
        $data['action'] = 'leadchangerm';
        $data['leadsourcevalue'] = $result;
        return view('lead/lead-changerm',$data)->with($arrsearch);
    }
    
    public function statecity(Request $request)
	{
		$data['cities'] = array();
		if ($request->isMethod('post')) {
			$data['cities'] = Cities::where("state_id","=",$request->input('stateid'))->lists('city','city_id');	
		}
		echo json_encode($data);
		exit;
	}

}
