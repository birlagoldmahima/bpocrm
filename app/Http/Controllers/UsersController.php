<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\User;
use Hash;
use App\Http\Controllers\Controller;
use Validator;
use DB;
use Auth;

class UsersController extends Controller {

    public function index(Request $request) {
        $arrsearch = array('search_option' => $request['search_option'], 'q' => $request['q']);
        $result = DB::table('users AS users ')
                ->Leftjoin('users AS T2 ', 'T2.id', '=', 'users.reporting_manager')
                ->where('users.role', '!=', 'Admin')
                ->where('users.bpo_id', Auth::user()->bpo_id)
                ->where(function ($query) use ($arrsearch) {
                    if (!empty($arrsearch['search_option'])) {
                        $search = $arrsearch['q'];
                        if ($arrsearch['search_option'] == 'refid')
                            $query = $query->where('users.id', '=', "$search");
                        else if ($arrsearch['search_option'] == 'ID')
                            $query = $query->where('users.id', '=', "$search");
                        else if ($arrsearch['search_option'] == 'Name')
                            $query = $query->where('users.name', 'LIKE', "%$search%");
                        else if ($arrsearch['search_option'] == 'Mobile')
                            $query = $query->where('users.mobile', 'LIKE', "%$search%");
                        else if ($arrsearch['search_option'] == 'Email')
                            $query = $query->where('users.email', 'LIKE', "%$search%");
                        else if ($arrsearch['search_option'] == 'Role')
                            $query = $query->where('users.role', "$search");
                    }
                })
                ->select(["users.id", "users.name", "users.email", "users.mobile", "users.role", "users.reporting_manager", "users.status", "T2.name as remanager"])
                ->paginate(30);

        $data['data'] = $result;
        $data['controller'] = "user";
        $data['action'] = "user-list";
        return view('user.user_list', $data)->with($arrsearch);
    }

    public function add_user_form() {
        $data['controller'] = "user";
        $data['action'] = "add-user";
        return view('user.add_user', $data);
    }

    public function getreporting(Request $request) {
        $role = $request['role'];
        $userrole = config('custom.userhierarchy')[$role];
        $state = User::where('role', $userrole)
                     ->where('status','active')
                     ->where('bpo_id', Auth::user()->bpo_id)
                     ->pluck('name', 'id');
        return json_decode(json_encode($state), 1);
    }

    public function usernamecheck(Request $request) {
        $name = $request['name'];
        $name = User::where('email', $name)
                ->pluck('name', 'id');
        $check = json_decode(json_encode($name), 1);
        if (count($check) > 0) {
            return 1;
        }
        return 0;
    }

    public function add_user(Request $request) {
        $role = config('custom');
        $iswith = true;
        $iswithdata = array('role' => $role);
        $rules['name'] = 'required';
        $rules['email'] = 'required';
        $rules['mobile'] = 'required';
        $rules['role'] = 'required';
        $rules['reporting_manager'] = 'required';
        $this->validate($request, $rules);
        $messages['required'] = '1';
        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            $error = json_decode(json_encode($validator->errors()), 1);
            if (isset($error['name']) || isset($error['email']) || isset($error['mobile']) || isset($error['role']) || isset($error['reporting_manager'])) {
                $error = "Required parameter missing!!!";
            }
            return ['status' => 0, 'msg' => $error];
        }
        $userObj = new User();
        $userObj->name = $request['name'];
        $userObj->email = $request['email'];
        $userObj->password = bcrypt($request['password']);
        $userObj->mobile = $request['mobile'];
        $userObj->remember_token = str_random(60);
        $userObj->role = $request['role'];
        $userObj->reporting_manager = $request['reporting_manager'];
        $userObj->bpo_id = Auth::user()->bpo_id;
        $userObj->reset = 0;
        $userObj->save();
        return redirect()->route('userlist')->with(['flash_message' => 'User successfully added!']);
    }

    public function user_profile() {
        $getuser = DB::table('users AS users ')
                ->where('users.id', Auth::user()->id)
                ->select(["users.id", "users.name", "users.email", "users.mobile", "users.role", "users.reporting_manager", "users.status", "T2.name as remanager"])
                ->Leftjoin('users AS T2 ', 'T2.id', '=', 'users.reporting_manager')
                ->first();
        $result['data'] = json_decode(json_encode($getuser), 1);
        $result['controller'] = "";
        $result['action'] = "";
        return view('user.userprofile', $result);
    }

    public function edit_user(Request $request, $id) {
        $users = User::findorFail($id);
        $usersdata = json_decode(json_encode($users), 1);
        $getreporting = User::where('bpo_id', Auth::user()->bpo_id)
                ->select(["id", "name", "role"])
                ->get();
        $reportingmanager = json_decode(json_encode($getreporting), 1);
        $role = config('custom');
        $iswith = true;
        $iswithdata = array('role' => $role, 'reportingm' => $reportingmanager);
        return $this->response('user.edit_user', ['status' => 1, 'msg' => 'form', 'data' => $usersdata], $iswith, $iswithdata);
    }

    public function update_user(Request $request, User $id) {
        $rules['name'] = 'required';
        $rules['email'] = 'required';
        $rules['mobile'] = 'required';
        $rules['role'] = 'required';
        $rules['rmanager'] = 'required';
        $rules['status'] = 'required';
        $messages['required'] = '1';
        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            $error = json_decode(json_encode($validator->errors()), 1);
            if (isset($error['name']) || isset($error['mobile']) || isset($error['email']) || isset($error['role']) || isset($error['reporting_manager']) || isset($error['status'])) {
                $error = "Required parameter missing!!!";
            }
            return ['status' => 0, 'msg' => $error];
        }
        $id = $request['id'];
        $userObj = User::findOrFail($id);
        $userObj->name = $request['name'];
        $userObj->email = $request['email'];
        $userObj->mobile = $request['mobile'];
        //$userObj->password = bcrypt($request['password']);
        $userObj->api_token = str_random(60);
        $userObj->mobile = $request['mobile'];
        $userObj->role = $request['role'];
        $userObj->reporting_manager = $request['rmanager'];
        $userObj->status = $request['status'];
        $userObj->save();
        if (json_decode(json_encode($userObj), 1) > 0) {
            $viewFileName = 'user.user_list';
            $result = ['status' => 1, 'msg' => 'User saved successfully !!!', 'data' => $userObj];
            return redirect()->route('user_list');
        } else {
            $viewFileName = 'user.user_list';
            $result = ['status' => 0, 'msg' => 'Something Went Wrong!!!'];
            return redirect()->route('user_list');
        }
    }

    
    public function updateuserstatus(Request $request) {
        $id = $request['id'];
        return User::where('id', $id)
                   ->update(['status' => 'inactive']);
        
    }

    public function resetpasswd(Request $request) {
        $id = $request['id'];
        return User::where('id', $id)
                   ->update(['password' => Hash::make('123456'), 'reset' => '0']);
    }

    public function changereporting(Request $request) {
        $id = $request['id'];
        $reporting = $request['reporting'];
        return User::where('id', $id)
                   ->update(['reporting_manager' =>$reporting]);
    }

    public function userlist_Download(Request $request) {
        $data = DB::table('users AS users ')
                ->where([['users.bpo_id', Auth::user()->bpo_id]])
                ->select(["users.id", "users.name", "users.email", "users.mobile", "users.role", "T2.name as remanager"])
                ->join('users AS T2 ', 'T2.id', '=', 'users.reporting_manager')
                ->get();
        $result = json_decode(json_encode($data), 1);
        $reportname = $result[0]['remanager'];
        if (count($result) > 0) {
            $header = ["Id", "Name", "Email", "Mobile", "Role", "Report Manager"];
            $this->createexcel("userlist", $result, $header);
        } else {
            $this->createexcel("userlist", array(), array('No Data Found'));
        }
    }

}
