<?php
namespace App\Http\Controllers;

use App\EmailTemplate;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Pagination\Paginator;
use App\Lead;


class EmailTemplateController extends Controller
{
	public function index(){
		
        $data['controller']="emailtemplate";
        $data['action'] = "emailtemplate";       
        return view('email/emailtemplate',$data);
	}

    public function addemailtemplate(Request $request)
    {   
        $this->validate($request, [
            'title' => 'required|max:120',
            'subject' => 'required',
            'content' => 'required'
        ]);
        $files = $request->file('attach');
        $arrfile=array();
        if(!empty($files[0])){
            $destinationPath = base_path().'/public/emailtemplate/';
            foreach($files as $file){
               if(!empty($file)){
                    $originamefilename=$file->getClientOriginalName();
                    $extension=$file->getClientOriginalExtension();
                    $filename=basename($originamefilename,'.'.$extension).strtotime(date('Y-m-d')).'.'.$extension;
                    $arrfile[]=$filename;
                    $file->move($destinationPath,$filename);
                }
            }    
        }
        $emailtemplates = new EmailTemplate();
        $emailtemplates->title = $request['title'];
        $emailtemplates->subject = $request['subject'];
        $emailtemplates->content = $request['content'];
        $emailtemplates->status = 'active';
        $emailtemplates->fileattach=implode('|',$arrfile);
        $emailtemplates->save();
		$request->session()->flash('flash_message', 'Email  successfully added!');
        return redirect()->back();
    }

    public function emailtemplatelists(Request $request){   
        $search=$request->input('q');
        $status=$request->input('status');
        
        $value = EmailTemplate::where(function ($query) use ($search){
                                    if(!empty($search)){
                                        $query=$query->orWhere('title','LIKE', "%$search%");
                                    } 
                              })
                              ->paginate(5);
        $data['emaillist'] =$value;
        $data['controller']="emailtemplate";
        $data['action'] = "emailtemplatelist";
        return view('email/emailtemplates-lists',$data)->with(['search'=>$search,'status'=>'active']);
    }
    
    public function emailtemplateEdit($id,Request $request){
        $currentPage=$request->page;
        if(!empty($currentPage)) {
            $data['currentPage'] = $currentPage;
        }
        else{
            $data['currentPage'] = 1;
        }
        $data['search'] = $request->q;
        $data['status'] = $request->status;
        $data['controller']="emailtemplate";
        $data['action'] = "emailtemplate";
        $emaillist = EmailTemplate::findorFail($id);
        $data['emaillist']=$emaillist;
        $objcustomer= new CustomerController();
        $removekeycustomer=$objcustomer->removekeyfromarray();
        $columnscustomer = \DB::connection('mysql2')->getSchemaBuilder()->getColumnListing("sha_customer_master");
        $arrdisplay = array_diff($columnscustomer, $removekeycustomer);
        $references = \DB::connection('mysql2')->getSchemaBuilder()->getColumnListing("sha_references");
        $plans = \DB::connection('mysql2')->getSchemaBuilder()->getColumnListing("sha_customer_plans");
        $count=count($arrdisplay);
        end($arrdisplay);
        $key=key($arrdisplay);
        $i=1;
        foreach($references as $value){
            $arrdisplay[$key+$i]=$value;
            $i++;
        }
        
        foreach ($plans as $value) {
            $arrdisplay[$key+$i]=$value;
            $i++;
        }
        $data['cloumns']=$arrdisplay;
        return view('email/emailtemplate-edit',$data);
    }

    public function emailtemplateUpdate(Request $request){  
        $this->validate($request, [
            'title' => 'required|max:120',
            'from_name' => 'required',
            'from_email' => 'required',
            'subject' => 'required',
            'content' => 'required'
        ]);
        $id=$request['id'];
        $perviousfile=EmailTemplate::where('email_template_id','=',$id)->get(['fileattach'])->toArray();
        $previousarrayfile=array();
        if(!empty($perviousfile[0]['fileattach'])){
            $previousarrayfile=explode("|",$perviousfile[0]['fileattach']);

        }
        $files = $request->file('attach');
        $destinationPath = base_path().'/public/emailtemplate/';

        $arrfile=array();
        $files = $request->file('attach');
        for($i=0;$i<count($files);$i++){
            $flag=false;
            if(!empty($files[$i])){
                $originamefilename=$files[$i]->getClientOriginalName();
                $extension=$files[$i]->getClientOriginalExtension();
                $filename=basename($originamefilename,'.'.$extension).strtotime(date('Y-m-d')).'.'.$extension;
                $arrfile[]=$filename;
                $files[$i]->move($destinationPath,$filename);
                $flag=true;
            }
            if(array_key_exists($i,$previousarrayfile)){
                if($flag==true){
                    //unlink(base_path().'/public/emailtemplate/temp1474502400.php');    
                   unlink($destinationPath.''.$previousarrayfile[$i]); 
                }else{
                    $arrfile[]=$previousarrayfile[$i];    
                }
            }
        }    
        $emailtemplates_title = $request['title'];
        $emailtemplates_from_name = $request['from_name'];
        $emailtemplates_from_email = $request['from_email'];
        $emailtemplates_to_email = $request['to_email'];
        $emailtemplates_subject = $request['subject'];
        $emailtemplates_content = $request['content'];
        $emailtemplates_status = $request['status'];
        $emailtemplates=EmailTemplate::find($id);
        $emailtemplates->title = $emailtemplates_title;
        $emailtemplates->from_name= $emailtemplates_from_name;
        $emailtemplates->from_email=$emailtemplates_from_email;
        $emailtemplates->to_email=$emailtemplates_to_email;
        $emailtemplates->subject=$emailtemplates_subject;
        $emailtemplates->content=$emailtemplates_content;
        $emailtemplates->status=$emailtemplates_status;
        $emailtemplates->fileattach=implode('|',$arrfile);
        $emailtemplates->save();
        $request->session()->flash('flash_message', 'EmailTemplate successfully updated!');
        return redirect()->route('emailtemplates-list',['q'=>$request->input('search'),'status'=>$request->input('status'),'page' => $request['page']]);
    }

    public function emailtemplatestatus(Request $request ,$id){
        $emailtemplates = EmailTemplate::find($id);
        if($emailtemplates->status=="active"){
            $emailtemplates->status='inactive';
        }elseif($emailtemplates->status=="inactive"){
            $emailtemplates->status='active';
        }
        $emailtemplates->save();
        return redirect()->route('emailtemplates-list');
    }

    public function emailcontent(Request $request){
        $id = $request['id'];
        if($id==9){
            $ref_id = $request['lead_id'];
            $data = Lead::where('reference_id',$ref_id)
                        ->first();
            $message = [];
            if(empty($data->email)){
                $message[]="Email ID Not Found";
            }
            if($data->emi_size==0 || empty($data->emi_size)){
                $message[]="EMI Size Not Found";   
            }
            if(count($message)>0){
                return ['error'=>$message];
            }
        }
        $emaildata = EmailTemplate::where('id',$id)
                                  ->select('subject','content','fileattach')
                                  ->first();
        $content = str_replace('{amount}',$data->emi_size,$emaildata->content);
        $email['messages'] = $content;
        $email['to_mail'] = $data->email;
        $email['to_name'] = $data->name;
        $email['from_email'] = "customerservice@cherishgold.com";
        $email['from_name'] = "Cherishgold";
        $email['subject'] = $data->subject;
        dd($email);
        if(!empty($data->fileattach)){
            $files=explode("|",$data->fileattach);
            foreach($files as $file){
                $data['attachfile'][]=base_path().'/public/emailtemplate/'.$file;
            }
        }
        $this->sendemail($email);
        return ['success'=>true];        
    }

}