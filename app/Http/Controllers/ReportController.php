<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Excel;

class ReportController extends Controller{

    public function leadlistreportdata(Request $request) {
		$arrchk = $request->all();
        $result = $this->leaddata($request)->paginate(10);
        $data['controller'] = "report";
        $data['action']='lead';
        $data['leadsourcevalue'] = $result;
        return view('report/lead-listreport',$data)->with($arrchk);
    }

    public function leaddata(Request $request){
    	$arrchk = $request->all();
        return DB::table("leads")
                	->leftjoin("users","id",'=','rel_manager_id')
                    ->leftjoin("interactions","interactions.interaction_id",'=','leads.last_interaction_id')
                   	->leftjoin("lead_source","lead_source.leadsource_id",'=','leads.lead_source')
                   	->where("leads.status",'In progress')
                    ->where("leads.bpo_id",Auth::user()->bpo_id)
                    ->where(function($qry) use ($arrchk){
                        if(!empty($arrchk['fromdate']) || !empty($arrchk['todate'])){
                            $obj=DB::raw("date(created)");
                            if((!empty($arrchk['fromdate'])) && (!empty($arrchk['todate']))){
                                $qry=$qry->whereBetween($obj, [date("Y-m-d", strtotime($arrchk['fromdate'])),date("Y-m-d", strtotime($arrchk['todate']))]);
                            } elseif((!empty($arrchk['fromdate'])) && (empty($arrchk['todate']))){
                                $qry=$qry->where($obj,">=" ,[date("Y-m-d", strtotime($arrchk['fromdate']))]);
                            } elseif((empty($arrchk['fromdate'])) && (!empty($arrchk['todate']))){
                                $qry=$qry->where($obj,"<=",[date("Y-m-d", strtotime($arrchk['todate']))]);
                            }
                        }
                    })
                    ->select("leads.name as leadname","leads.mobile as leadmobile","leads.email as leademail", "state_name","city_name","leads.reference_id as leadid","lead_source.leadsource_name","users.name as username","interactions.call_substatus_id","interactions.call_status_id","leads.created as leadcrated","interactions.created_at as interactioncreated",'interactions.remarks');
        
    }

    public function leadreportexport(Request $request){
    	$result = $this->leaddata($request)->get();
    	$data = json_decode(json_encode($result),1);
    	$filename = "Lead-data";
    	$call_status = callstatus();
        $sub_call_status= callsubstatus();
        Excel::create($filename.date("d-m-Y"), function($excel){
            $excel->sheet('Sheet 1', function($sheet) {});
        })->store('csv',storage_path('excel'));
        $header = ['Name','Mobile','Email','State','City','Lead ID','Lead Source','Tele Caller','Call Status','Call Sub Status','Lead Created','Call Date','Remarks'];
        Excel::load('storage/excel/'.$filename.date("d-m-Y").'.csv',function($doc) use ($data,$header,$filename,$call_status,$sub_call_status){
            $string='ABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $sheet = $doc->setActiveSheetIndex(0);
            for($i=0;$i<count($header);$i++){
                $sheet->setCellValue($string[$i].'1',$header[$i]);
            }
            $append=1;
            for($j=0;$j<count($data);$j++){
                $append=$append+1;
                $i=0;
                $sitedata = $data[$j];
                $callstatus = !empty($sitedata['call_status_id']) ? $call_status[$sitedata['call_status_id']] : "";
                $callsubstatus = !empty($sitedata['call_substatus_id']) ? $sub_call_status[$sitedata['call_substatus_id']] : "";
                $arr = array_merge($sitedata,['call_status_id'=>$callstatus,'call_substatus_id'=>$callsubstatus]);
                foreach($arr as $key=>$value){
                    $disp= $value;
                    $sheet->setCellValue($string[$i].$append,$disp);
                    $i++;
                }
            }
            unlink(storage_path('excel/'.$filename.date("d-m-Y").'.csv'));
        })->download('csv');
    }	

    public function convertedlead(Request $request){
        $data['result'] = $this->convertedleadreport($request)->paginate(50);
        $data['controller'] = "report";
        $data['action'] = "converted";
        return view('report.lead-converted',$data);
    }

    public function convertedleadreport(Request $request){
        $bpo_id = Auth::user()->bpo_id;
        $report_manager = Auth::user()->id;
        $role = Auth::user()->role;
        
        return  DB::table("leads")
                  ->leftJoin("users", "leads.rel_manager_id", '=', "users.id")
                  ->where('leads.bpo_id', $bpo_id)
                  ->where('leads.status', 'Converted')
                  ->where('leads.customer_status','Verified')
                  ->where(function($query) use ($role, $report_manager) {
                    if ($role == 'Team Lead') {
                        $query = $query->where('users.reporting_manager', '=', " $report_manager");
                    } else if ($role == 'Tele Caller') {
                        $query = $query->where('users.id', '=', " $report_manager");
                    } elseif ($role == 'Assistant Manager') {
                        $query->whereIn("rel_manager_id", $this->assitance_manager_to_tl());
                    }
                  })
                  ->select("leads.reference_id",DB::raw("DATE_FORMAT(leads.created,'%d-%m-%Y') as created_date"),DB::raw("DATE_FORMAT(leads.request_date,'%d-%m-%Y') as requestdate"),DB::raw("DATE_FORMAT(leads.converted_date,'%d-%m-%Y') as conversiondate"),DB::raw("DATE_FORMAT(leads.request_date,'%d-%m-%Y') as verifieddate"),"leads.name","leads.email","leads.mobile","users.name as tellcallername",DB::raw("DATE_FORMAT(leads.verified_date,'%d-%m-%Y') as verified_date"),"emiamount",DB::Raw("cast((goldallocate) as char) as gram"))
                  ->orderBy('reference_id','desc');
    }

    public function convertedexport(Request $request){
        $data = json_decode(json_encode($this->convertedleadreport($request)->get()),1);
        $header = ['Lead ID','Created','Request Date','Coversion Date','Verified Date','Name','Email','Mobile','Telle Caller','Verification Date','EMI Amount','Gram Allocated'];
        return $this->createexcel("Converted",$data,$header);
    }

    public function requestlead(Request $request){
        $data['result'] = $this->requestleadreport($request)->paginate(50);
        $data['controller'] = "report";
        $data['action'] = "request";
        return view('report.lead-request',$data);
    }

    public function requestleadreport(Request $request){
        $bpo_id = Auth::user()->bpo_id;
        $report_manager = Auth::user()->id;
        $role = Auth::user()->role;
        
        return  DB::table("leads")
                  ->leftJoin("users", "leads.rel_manager_id", '=', "users.id")
                  ->where('leads.bpo_id', $bpo_id)
                  ->where('leads.status', 'Received')
                  ->where('leads.customer_status','Request')
                  ->where(function($query) use ($role, $report_manager) {
                    if ($role == 'Team Lead') {
                        $query = $query->where('users.reporting_manager', '=', " $report_manager");
                    } else if ($role == 'Tele Caller') {
                        $query = $query->where('users.id', '=', " $report_manager");
                    } elseif ($role == 'Assistant Manager') {
                        $query->whereIn("rel_manager_id", $this->assitance_manager_to_tl());
                    }
                  })
                  ->select("leads.reference_id",DB::raw("DATE_FORMAT(leads.created,'%d-%m-%Y') as created_date"),DB::raw("DATE_FORMAT(leads.request_date,'%d-%m-%Y') as requestdate"),"leads.name","leads.email","leads.mobile","users.name as tellcallername","emiamount")
                  ->orderBy('reference_id','desc');
    }

    public function requestexport(Request $request){
        $data = json_decode(json_encode($this->requestleadreport($request)->get()),1);
        $header = ['Lead ID','Created','Request Date','Name','Email','Mobile','Telle Caller','EMI Amount'];
        return $this->createexcel("Request",$data,$header);
    }

    public function verificationlead(Request $request){
        $data['result'] = $this->verificationleadreport($request)->paginate(50);
        $data['controller'] = "report";
        $data['action'] = "verification";
        return view('report.lead-verification',$data);
    }

    public function verificationleadreport(Request $request){
        $bpo_id = Auth::user()->bpo_id;
        $report_manager = Auth::user()->id;
        $role = Auth::user()->role;
        
        return  DB::table("leads")
                  ->leftJoin("users", "leads.rel_manager_id", '=', "users.id")
                  ->where('leads.bpo_id', $bpo_id)
                  ->where('leads.status', 'Converted')
                  ->where('leads.customer_status','Active')
                  ->where(function($query) use ($role, $report_manager) {
                    if ($role == 'Team Lead') {
                        $query = $query->where('users.reporting_manager', '=', " $report_manager");
                    } else if ($role == 'Tele Caller') {
                        $query = $query->where('users.id', '=', " $report_manager");
                    } elseif ($role == 'Assistant Manager') {
                        $query->whereIn("rel_manager_id", $this->assitance_manager_to_tl());
                    }
                  })
                  ->select("leads.reference_id",DB::raw("DATE_FORMAT(leads.created,'%d-%m-%Y') as created_date"),"leads.name","leads.email","leads.mobile","users.name as tellcallername","emiamount")
                  ->orderBy('reference_id','desc');
    }

    public function verificationexport(Request $request){
        $data = json_decode(json_encode($this->verificationleadreport($request)->get()),1);
        $header = ['Lead ID','Created','Name','Email','Mobile','Telle Caller','EMI Amount'];
        return $this->createexcel("Verification",$data,$header);
    }

    public function notinterestedreport(Request $request){
        $data['result'] = $this->notinterested($request)->paginate(50);
        $data['controller'] = "report";
        $data['action'] = "notinterested";
        return view('report.lead-notinterested',$data);
    }

    public function notinterested(Request $request){
        $bpo_id = Auth::user()->bpo_id;
        $report_manager = Auth::user()->id;
        $role = Auth::user()->role;
        
        return  DB::table("leads")
                  ->leftJoin("users", "leads.rel_manager_id", '=', "users.id")
                  ->where('leads.bpo_id', $bpo_id)
                  ->where('leads.status', 'Not Interested')
                  ->where(function($query) use ($role, $report_manager) {
                    if ($role == 'Team Lead') {
                        $query = $query->where('users.reporting_manager', '=', " $report_manager");
                    } else if ($role == 'Tele Caller') {
                        $query = $query->where('users.id', '=', " $report_manager");
                    } elseif ($role == 'Assistant Manager') {
                        $query->whereIn("rel_manager_id", $this->assitance_manager_to_tl());
                    }
                  })
                  ->select("leads.reference_id",DB::raw("DATE_FORMAT(leads.created,'%d-%m-%Y') as created_date"),"leads.name","leads.email","leads.mobile","users.name as tellcallername","emiamount")
                  ->orderBy('reference_id','desc');
    }

    public function notinterestedexport(Request $request){
        $data = json_decode(json_encode($this->notinterested($request)->get()),1);
        $header = ['Lead ID','Created','Name','Email','Mobile','Telle Caller'];
        return $this->createexcel("NotInterested",$data,$header);
    }

    public function lead_interaction_report(Request $request) {
        $finaldata = array();
        $data = array();
        $searchemail = $request['searchemail'];
        if (isset($request['searchemail'])) {
            $searchemail = $request['searchemail'];
            $loginbpoid = Auth::user()->bpo_id;
            $leadstatushistory = Lead::where([['leads.email', 'like', '%' . $searchemail . '%'], ['leads.bpo_id', $loginbpoid]])
                    ->select(['leads.reference_id', 'leads.name', 'leads.email', 'leads.mobile', 'leads.state_name', 'leads.city_name', 'interactions.call_initiator', 'interactions.call_status_id', 'interactions.call_substatus_id', 'interactions.call_substatus_other', 'interactions.callback_datetime', 'interactions.remarks'])
                    ->leftjoin('interactions', 'leads.reference_id', '=', 'interactions.reference_id')
                    ->orderBy('leads.email')
                    ->get();
            $data = json_decode(json_encode($leadstatushistory), 1);

            foreach ($data as $key => $val) {
                $finaldata[$val['reference_id']][] = $val;
            }
        }
        $result = ['status' => 1, 'msg' => '', 'data' => $finaldata];
        $iswith = true;
        $iswithdata = array('searchemail' => $searchemail, 'controller' => 'lead', 'action' => 'lead_status_history');
        $viewFileName = "lead.lead_interaction_report";

        if ($request['flag'] == 1) {
            $header = ["Id", "Name", "Email", "Mobile", "State", "City", "Call Initiator", "Call Status", "Call Sub Status", "Call Sub Status(Other)", "Callback Time", "Remarks"];
            $this->createexcel("LeadInteraction", $data, $header);
            exit;
        }
        return view($viewFileName, $result)->with($iswithdata);
    }
}
