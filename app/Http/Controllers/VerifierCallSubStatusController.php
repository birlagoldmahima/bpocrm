<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\VerifierCallSubStatus;
use App\VerifierCallStatus;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;

use App\Http\Requests;

class VerifierCallSubStatusController extends Controller
{
	protected function validator(array $data)
	{
		return Validator::make($data, [
			'verifier_call_status' => 'required',
			'name' => 'required|max:255',
			'order' => 'required|max:50',
			'code' => 'required|max:10',
			'staus' => 'required'
		]);
	}

	public function listVerifierCallSubStatus(Request $request)
	{
    	$search = $request->input('q');
        $search_option = $request->input('search_option');
        $arraySearch = array('criteria'=>$search_option,'value'=>$search);
        
        $data['verifierCallStatus'] = VerifierCallStatus::lists('name','id');
        // dd($data['verifierCallStatus']);

        $data['results'] = VerifierCallSubStatus::where(function ($query) use($arraySearch) {
                                if(!empty($arraySearch['criteria'])) {
                                    $search = $arraySearch['value'];
                                    if($arraySearch['criteria'] == 'name'){
                                        $query = $query->where('name' , 'LIKE', "%$search%");
                                    }
                                    if($arraySearch['criteria'] == 'order'){
                                        $query = $query->where('order' , 'LIKE', "%$search%");
                                    }
                                    if($arraySearch['criteria'] == 'code'){
                                        $query = $query->where('code' , 'LIKE', "%$search%");
                                    }
                                    if($arraySearch['criteria'] == 'status'){
                                        $query = $query->where('status' , 'LIKE', "%$search%");
                                    }
                                }
                            })->orderBy('name','asc')
                            ->paginate(20)->appends(['q'=>$search, 'search_option' => $search_option]);

	    $data['controller'] = 'verifierCallSubStatus';
	    $data['action'] = 'verifierCallSubStatusList';

	    return view('verifierCallSubStatus/listVerifierCallSubStatus',$data)->with([
										    							'search'=>$search,
										                                'search_option'=>$search_option,
										                                'selectOption' => $arraySearch['criteria'],
    																]);


	}

	public function createVerifierCallSubStatus(Request $request)
	{
		$data['verifierCallStatus'] = VerifierCallStatus::all();
		$data['controller'] = 'verifierCallSubStatus';
    	$data['action'] = 'createVerifierCallSubStatus';

    	return view('verifierCallSubStatus/createVerifierCallSubStatus',$data);
	}

	public function storeVerifierCallSubStatus(Request $request)
	{		
		$this->validate($request, [
			'verifier_call_status_id' => 'required',
			'name' => 'required',
			'order' => 'required',
			'code' => 'required',
			'status' => 'required'
		]);

		$verifierCallSubStatus = new VerifierCallSubStatus;

		$verifierCallSubStatus->verifier_call_status_id = $request['verifier_call_status_id'];
		$verifierCallSubStatus->name = $request['name'];
		$verifierCallSubStatus->order = $request['order'];
		$verifierCallSubStatus->code = $request['code'];
		$verifierCallSubStatus->status = $request['status'];

		$verifierCallSubStatus->save();

		return redirect('listVerifierCallSubStatus');
	}

	public function editVerifierCallSubStatus(Request $request, $id)
	{

		$data['verifierCallStatus'] = VerifierCallStatus::all();
		// $data['verifierCallStatus'] = VerifierCallStatus::lists('name','id');
		$data['verifierCallSubStatus'] = VerifierCallSubStatus::findOrFail($id);
		$data['controller'] = 'verifierCallSubStatus';
		$data['action'] = 'editVerifierCallSubStatus';

		return view('verifierCallSubStatus/editVerifierCallSubStatus', $data);
	}

	public function updateVerifierCallSubStatus(Request $request)
	{
		$this->validate($request, [
			'verifier_call_status_id' => 'required',
			'name' => 'required',
			'order' => 'required',
			'code' => 'required',
			'status' => 'required'
		]);

		$id = $request->id;

		$post = VerifierCallSubStatus::findOrFail($id);

		$post->verifier_call_status_id = $request['verifier_call_status_id'];
		$post->name = $request['name'];
		$post->order = $request['order'];
		$post->code = $request['code'];
		$post->status = $request['status'];

		$post->save();

		return redirect('listVerifierCallSubStatus');
	}

	public function activateVerifierCallSubStatus(Request $request, $id)
	{
		$activate = VerifierCallSubStatus::find($id);
		$activate->status = 'active' ;

		$activate->save();

		return redirect()->back();
	}

	public function deactivateVerifierCallSubStatus(Request $request, $id)
	{
		$activate = VerifierCallSubStatus::find($id);
		$activate->status = 'inactive';

		$activate->save();

		return redirect()->back();
	}
    
}
