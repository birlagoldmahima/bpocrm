<?php

    namespace App\Http\Controllers;

    use Illuminate\Http\Request;
    use App\Http\Requests;
    use App\Interactions;
    use App\Lead;
    use App\CallbackAlerts;

    class DialerApiController extends Controller {

        public function CallStatusSubStatus() {
            return CallStatusSubStatus();
        }

        public function getstate() {
            return ['success' => true, 'data' => ['State' => getstate()]];
        }

        public function getcity($id) {
            return ['success' => true, 'data' => ['city' => getcity($id)]];
        }

        public function addlead(Request $req) {
            $data = (new LeadController())->addlead($req);
            $interaction = $req->all() + array('reference_id' => $data->reference_id);
            $this->interactionRecord($interaction);
            return ['success' => true, 'lead_id' => $data->reference_id];
        }

        public function followUp(Request $request) {
            $reference_id = $request['reference_id'];             
            $check = Lead::find($reference_id);

            if (isset($check['reference_id'])) {
                $data = $request->all() + array('reference_id' => $reference_id);
                $this->interactionRecord($data);
                return ['success' => true, 'message' => 'Record added successfully'];
            } else {
                return ['success' => false, 'message' => 'Reference Id is missing.'];
            }
        }

        public function interactionRecord($request) {

            $interaction = new Interactions;
            $interaction->user_type = 'lead';
            $interaction->reference_id = $request['reference_id'];
            $interaction->call_status_id = $request['call_status'];
            $interaction->call_substatus_id = $request['call_sub_status'];
            $interaction->rm_id = $request['user_id'];
            $interaction->rm_id = $request['user_id'];
            $interaction->bpo_id = $request['bpo_id'];
            $interaction->call_substatus_other = $request['call_substatus_other'];
            if ($request['nextdatetime'] != "")
                $interaction->callback_datetime = date("Y-m-d h:i:s", strtotime($request['nextdatetime']));
            else
                $interaction->callback_datetime = "";
            $interaction->remarks = $request['remarks'];

            $interaction->save();


            if ($request['nextdatetime'] != "" && $request['call_sub_status'] != "66") {
                $refdata = Lead::where('reference_id', $request['reference_id'])->first();
                $type = (empty($refdata[0]['customer_id']) || $refdata[0]['customer_id'] == 0) ? 'lead' : 'reference';
                $objcallback = new CallbackAlerts();
                $objcallback->customer_id = $request['reference_id'];
                $objcallback->customer_name = $refdata->name;
                $objcallback->customer_mobile = $refdata->mobile;
                $objcallback->callbacktime = date("Y-m-d h:i:s", strtotime($request['nextdatetime']));
                $objcallback->relationship_manager = $request['user_id'];
                $objcallback->call_status_id = $request['call_status'];
                $objcallback->call_sub_status_id = $request['call_sub_status'];
                $objcallback->user_type = $type;
                $objcallback->save();
            }
        }

    }
    