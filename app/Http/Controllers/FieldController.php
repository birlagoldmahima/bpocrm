<?php

    namespace App\Http\Controllers;

    use Mail;
    use App\Customer;
    use App\WEB_User;
    use App\CustomerPlan;
    use App\CustomerPlanPayment;
    use App\CustomerRequest;
    use App\References;
    use App\States;
    use App\Cities;
    use App\DistrictPincode;
    use App\CallInitiator;
    use App\CallStatus;
    use App\CallSubStatus;
    use App\Interactions;
    use App\RelationshipManager;
    use App\EmailTemplate;
    use App\SmsTemplate;
    use App\Language;
    use App\User;
    use DateTime;
    use Validator;
    use Excel;
    use App\SmsLogs;
    use App\EmailLogs;
    use Illuminate\Http\Request;
    use Illuminate\Http\Response;
    use Illuminate\Support\Facades\Auth;
    use Illuminate\Support\Facades\File;
    use Illuminate\Support\Facades\Storage;
    use Illuminate\Pagination\LengthAwarePaginator;
    use Illuminate\Database\Eloquent\Collection;
    use Illuminate\Pagination\Paginator;
    use App\InteractionCategory;
    use App\InteractionSubCategory;
    use App\InteractionTagging;
    use App\CallbackAlerts;
    use DB;
    use App\Lead;
    use Mbarwick83\Shorty\Facades\Shorty;
    use Carbon\Carbon;
    use App\VerifierCallStatus;
    use App\VerifierCallSubStatus;
    use App\VerifierInteraction;
    use App\FoRemarks;
    use App\LeadSms;
    use App\LmsForm;

    class FieldController extends Controller {

        public function __construct() {
            
        }

        public function jsondata($request) {
            //if(!empty($request['batch_id'])){
            $array['batch_id'][$request['batch_id']] = 'no';
            //}
            //if(!empty($request['campaign_code'])){
            $array['campaign_code'][$request['campaign_code']] = 'no';
            //}
            //if(!empty($request['lead_code'])){
            $array['lead_code'][$request['lead_code']] = 'no';
            //}
            $array['name'][$request['name']] = empty($request['name_status']) ? 'no' : $request['name_status'];
            $array['mobile'][$request['mobile']] = empty($request['mobile_status']) ? 'no' : $request['mobile_status'];
            $array['alt_mobile'][$request['alt_mobile']] = empty($request['alt_mobile_status']) ? 'no' : $request['alt_mobile_status'];
            $array['email'][$request['email']] = empty($request['email_status']) ? 'no' : $request['email_status'];
            $array['gender'][$request['gender']] = empty($request['gender_status']) ? 'no' : $request['gender_status'];
            $array['age'][$request['age']] = empty($request['age_status']) ? 'no' : $request['age_status'];
            $array['marital_status'][$request['marital_status']] = empty($request['marital_status_status']) ? 'no' : $request['marital_status_status'];
            $array['child'][$request['child']] = empty($request['child_status']) ? 'no' : $request['child_status'];
            $array['address'][$request['address']] = empty($request['address_status']) ? 'no' : $request['address_status'];
            $array['landmark'][$request['landmark']] = empty($request['landmark_status']) ? 'no' : $request['landmark_status'];
            $array['pincode'][$request['pincode']] = empty($request['pincode_status']) ? 'no' : $request['pincode_status'];
            $array['company_name'][$request['company_name']] = empty($request['company_name_status']) ? 'no' : $request['company_name_status'];
            $array['department'][$request['department']] = empty($request['department_status']) ? 'no' : $request['department_status'];
            $array['profession'][$request['profession']] = empty($request['profession_status']) ? 'no' : $request['profession_status'];
            $array['income_group'][$request['income_group']] = empty($request['income_group_status']) ? 'no' : $request['income_group_status'];
            $array['income'][$request['income']] = empty($request['income_status']) ? 'no' : $request['income_status'];
            $array['investment_question'][$request['investment_question']] = empty($request['investment_question_status']) ? 'no' : $request['investment_question_status'];
            $array['purpose_of_call'][$request['purpose_of_call']] = empty($request['purpose_of_call_status']) ? 'no' : $request['purpose_of_call_status'];
            $array['call_complaince'][$request['call_complaince']] = empty($request['call_complaince_status']) ? 'no' : $request['call_complaince_status'];
            $array['recording_statement'][$request['recording_statement']] = empty($request['recording_statement_status']) ? 'no' : $request['recording_statement_status'];
            $array['state'][$request['state']] = empty($request['state_status']) ? 'no' : $request['state_status'];
            $array['city'][$request['city']] = empty($request['city_status']) ? 'no' : $request['city_status'];
            return json_encode($array);
        }

        public function sendsms($mobile, $message) {

            $sms_site = env('SMS_SITE');
            $sms_method = env('SMS_METHOD');
            $sms_api_key = env('SMS_API_KEY');
            $sms_sender_id = env('SMS_SENDER_ID');
            $sendsms = new SendSmsController($sms_site, $sms_method, $sms_api_key, $sms_sender_id);

            $sendsms->send_sms($mobile, $message, '', 'xml');
        }

        public function interaction($array) {

            $interaction = new VerifierInteraction;
            $interaction->user_type = $array['type'];
            $interaction->reference_id = $array['id'];
            $interaction->rm_id = Auth::user()->id;
            $interaction->call_initiator = "";
            $interaction->verifier_call_status_id = $array['call_status'];
            $interaction->verifier_call_sub_status_id = $array['call_sub_status'];
            $interaction->verifier_call_sub_status_other = "";
            $interaction->callback_date = empty($array['appointment_date']) ? null : date('Y-m-d H:i:s', strtotime($array['appointment_date']));
            $interaction->remarks = $array['verification_interaction'];
            $interaction->roles = Auth::user()->role;

            if (isset($array['lateflag'])) {
                $interaction->call_initiator = $array['lateflag'];
            }
            $interaction->save();
            return $interaction;
        }

        public function interaction1($array) {
            $interaction = new VerifierInteraction;
            //$interaction->user_type = $array['type'];
            $interaction->reference_id = $array['id'];
            $interaction->rm_id = Auth::user()->id;
            $interaction->call_initiator = "";
            $interaction->verifier_call_status_id = $array['call_status'];
            $interaction->verifier_call_sub_status_id = $array['call_sub_status'];
            $interaction->verifier_call_sub_status_other = "";
            $interaction->callback_date = empty($array['appointment_date']) ? null : date('Y-m-d H:i:s', strtotime($array['appointment_date']));
            $interaction->remarks = $array['verification_interaction'];
            $interaction->roles = Auth::user()->role;
            if (isset($array['lateflag'])) {
                $interaction->call_initiator = $array['lateflag'];
            }
            $interaction->save();
            return $interaction;
        }

        public function smslog($array) {
            $obj = new LeadSms();
            $obj->lead_id = $array['id'];
            $obj->mobile = $array['mobile'];
            $obj->appointment_date = $array['appointment_date'];
            $obj->sendflag = 'no';
            $obj->aftermeeting = 'no';
            $obj->save();
        }

        public function assignedDetails(Request $request, $id) {
            $lead = References::find($id);
            $data['rm'] = User::lists('name', 'id');
            $data['lead'] = $lead;
            $data['states'] = States::lists('state', 'state_id');
            $data['cities1'] = Cities::where('state_id', $lead->state_id)->lists('city', 'city_id');
            $data['cities'] = Cities::lists('city', 'city_id');
            if ($request->isMethod('post')) {
                $state = $request['state'];
                $city = $request['city'];
                $informed = $request['informed'];
                $emi_size = $request['emi_size'];
                $lead->name = $request['name'];
                $lead->mobile = $request['mobile'];
                $lead->email = $request['email'];
                $lead->state_id = $state;
                $lead->district_id = $city;
                $lead->informed_to_reference = 'No';
                $lead->status = $request['status'];
                $lead->emi_size = $emi_size;
                $lead->verifier = $this->jsondata($request->all());
                $lead->created = new DateTime();
                $lead->modified = new DateTime();
                $lead->save();
                $request->session()->flash('flash_message', 'Lead  successfully updated!');
                return redirect()->back();
            }
            $data['controller'] = 'field';
            $data['action'] = 'assigned-field';
            return view('field.assigneddetails', $data);
        }

        public function verifierList(Request $request) {


            if (empty($request->input('sort'))) {
                $sortvalue = "reference_id";
                $sorttype = "desc";
            } else {
                $sortvalue = $request->input('sort');
                $sorttype = $request->input('sorttype');
            }

            $sort = array('sort' => $sortvalue, 'sorttype' => $sorttype);

            $search = $request->input('q');
            $search_option = $request->input('search_option');
            $arrsearch = array('criteria' => $search_option, 'value' => $search);
            $data['rm'] = User::whereIn('role', array('Verifier'))
                    ->where('status', 'active')
                    ->pluck('name', 'id');

            $dataunion = DB::table("leads")
                    ->whereNotNull("leads.appointment_date")
                    ->whereIn("leads.stage_id", [2, 8])
                    ->whereIn("leads.stage_remarks", ['accepted', 'vocrefix'])
                    ->where(function ($query) use ($arrsearch) {
                        if (!empty($arrsearch['criteria'])) {
                            $search = $arrsearch['value'];
                            if ($arrsearch['criteria'] == 'refid')
                                $query = $query->where('leads.reference_id', '=', "$search");
                            else if ($arrsearch['criteria'] == 'custid')
                                $query = $query->where('leads.customer_id', '=', "%$search%");
                            else if ($arrsearch['criteria'] == 'name')
                                $query = $query->where('leads.name', 'LIKE', "%$search%");
                            else if ($arrsearch['criteria'] == 'mobile')
                                $query = $query->where('leads.mobile', 'LIKE', "%$search%");
                            else if ($arrsearch['criteria'] == 'email')
                                $query = $query->where('leads.email', 'LIKE', "%$search%");
                            elseif ($arrsearch['criteria'] == 'rm_id')
                                $query = $query->where('leads.rel_manager_id', '=', "$search");
                        }
                    })
                    ->select("leads.reference_id", "leads.name", "leads.created", "leads.lead_source_info", "leads.appointment_date", "stage_id", "leads.customer_id")
                    ->orderBy($sort['sort'], $sort['sorttype'])
                    ->get();

            $datajson = json_decode(json_encode($dataunion), 1);
            $perPage = 20;
            $var = array_column($datajson, 'reference_id');
            $currentPage = LengthAwarePaginator::resolveCurrentPage();
            $col = new Collection($datajson);
            $currentPageSearchResults = $col->slice(($currentPage - 1) * $perPage, $perPage)->all();
            $data['results'] = new LengthAwarePaginator($currentPageSearchResults, count($col), $perPage);
            $data['results']->setPath($request->url());

            $data['controller'] = 'Field';
            $data['action'] = 'verifierList';

            return view('lms/verifier_list', $data)->with(['q' => $search, 'search_option' => $search_option, 'sel_option' => $arrsearch['criteria'], 'sort' => $sort['sort'], 'sorttype' => $sort['sorttype']]);
        }

        public function assignQuality(Request $request) {
            $cust_id = $request['ref_id'];
            $sales_id = $request['quality_id'];
            $obj = Lead::findorFail($cust_id);
            $obj->quality_id = $sales_id;
            if ($obj->stage_id == 8) {
                $obj->stage_id = 9;
                $obj->stage_remarks = 'AssignedVocQuality';
            } else {
                $obj->stage_id = 3;
                $obj->stage_remarks = 'AssignedQuality';
            }
            $obj->save();
            $request->session()->flash('flash_message', 'Lead Assiged To Quality Successfully!!');
            return 1;
        }

       public function qualityActionlist(Request $request) {
            $userid = Auth::user()->id;
           $role = Auth::user()->role;
            if (empty($request->input('sort'))) {
                $sortvalue = "reference_id";
                $sorttype = "desc";
            } else {
                $sortvalue = $request->input('sort');
                $sorttype = $request->input('sorttype');
            }

            $sort = array('sort' => $sortvalue, 'sorttype' => $sorttype);

            $search = $request->input('q');
            $search_option = $request->input('search_option');
            $arrsearch = array('criteria' => $search_option, 'value' => $search);

            $dataunion = DB::table("leads")
                    ->join("users", "leads.quality_id", '=', "users.id")
                    ->whereIn("leads.stage_id", [3, 9])
                    ->whereIn("leads.stage_remarks", ['AssignedVocQuality', 'AssignedQuality'])
                    ->where(function($q) use ($role, $userid) {
                        $q->where("leads.quality_id", Auth::user()->id)
                        ->Orwhere(function($qry2)use ($userid) {
                            if (in_array(Auth::user()->role, ['admin', 'Verifier Head'])) {
                                $qry2->whereNotNull("leads.quality_id");
                            }
                        });
                    })
                    ->select("leads.reference_id", "leads.name", "leads.mobile", "leads.email", "users.name as qualityname", "leads.appointment_date", "stage_id", "leads.customer_id")
                    ->orderBy($sort['sort'], $sort['sorttype']);

            $data['results'] = $this->pagination($dataunion, 20, $request);

            $data['controller'] = 'Field';
            $data['action'] = 'qualityverifyList';

            return view('lms/quality_verifier_list', $data);
        }

        public function qualityActionview($id) {
            $data['reference'] = Lead::findorFail($id);
            $stage_id = $data['reference']['stage_id'] == 9 ? 6 : 2;
            $data['call_status'] = VerifierCallStatus::where('id', $stage_id)->get(['id', 'name', 'status'])->toArray();
            $data['call_sub_status'] = VerifierCallSubStatus::where('verifier_call_status_id', $stage_id)->get(['id', 'name', 'verifier_call_status_id', 'status'])->toArray();
            $data['reference'] = Lead::findorFail($id);
            $data['states'] = States::get(['state', 'state_id', 'status'])->toArray();
            $data['cities'] = Cities::get(['city', 'city_id', 'state_id'])->toArray();
            if (!empty($data['reference']['rel_manager_id'])) {
                $manager = DB::table("users as a", "leads.rel_manager_id", "=", "a.id")
                        ->join("users as b", "a.reporting_manager", "=", "b.id")
                        ->where("a.id", "=", $data['reference']['rel_manager_id'])
                        ->select("a.name as emp", "b.name as head")
                        ->get();
                $data['manager'] = json_decode(json_encode($manager), true);
            }
            $data['intereaction'] = $this->rmRejectedreason($data['reference']->reference_id);
            $data['rms'] = User::lists('name', 'id');
            $data['controller'] = 'Field';
            $data['action'] = 'qualityverifyList';
            return view('lms.quality_verifier_view', $data);
        }

        public function updateQuality(Request $request) {

            $id = $request->id;

            $post = Lead::findorFail($id);
            $post->name = $request->name;
            $post->email = $request->email;
            $post->mobile = $request->mobile;
            $statename = $cityname = "";
            $post->state_id = $request->state;
            $post->district_id = $request->city;
            $post->state_name = $request['state_name'];
            $post->city_name = $request['city_name'];
            $post->informed_to_reference = $request->informed;
            $post->emi_size = $request->emi_size;
            $post->appointment_date = $request->appointment_date;

            $jsonData = $this->jsondata($request->all());
            $array['probability'] = $request['probability'];
            $array['rating'] = $request['rating'];
            $array['ticket'] = $request['ticket'];
            $jsonData = json_decode($jsonData, 1) + $array;
            $post->verifier = json_encode($jsonData);
            if (in_array($request['call_sub_status'], [3, 15])) {
                $post->stage_id = 4;
                $post->appointment_date = date("Y-m-d H:i:s", strtotime($request->appointment_date));
                $post->stage_remarks = 'accepted';
                $appointmentdate = date('d-m-Y', strtotime($post->appointment_date));
                $appointmenttime = date('h:i A', strtotime($post->appointment_date));
                $msg = "Dear Customer,in regards with cherish gold plan Our Manager will reach on time as per ur scheduled appointment dated on $appointmentdate at $appointmenttime.For queries call on 8451048705-Coordinator";

                $this->sendsms($request->mobile, $msg);
                $this->smslog(['appointment_date' => date("Y-m-d H:i:s", strtotime($request->appointment_date))] + $request->all());
            } elseif (in_array($request['call_sub_status'], [4])) {
                $post->stage_id = 4;
                $post->stage_remarks = 'rejected';
                $request['verification_interaction'] = $request['verification_interaction'] . ' Appointment:' . $post->appointment_date;
                $post->appointment_date = null;
            } elseif (in_array($request['call_sub_status'], [16])) {
                $post->stage_id = 10;
                $post->stage_remarks = 'rejected';
                $request['verification_interaction'] = $request['verification_interaction'] . ' Appointment:' . $post->appointment_date;
                $post->appointment_date = null;
            } elseif ($request['call_sub_status'] == 17) {
                $post->stage_id = 10;
                $post->stage_remarks = 'verifierreturnvvoc';
            }
            $post->save();
            $interaction = $request->all();
            $interaction['type'] = "lead";
            $interaction['id'] = $id;
            $this->interaction1($interaction);
            return redirect()->route('quality-list');
        }

        public function spocdata(Request $request) {
            $role = Auth::user()->role;
            $arrchk = $request->all();
            return DB::table("leads")
                            ->where(function($q) {
                                $q->whereIn("leads.stage_id", [5, 6, 7, 8, 9, 10, 11]);
                                $q->Orwhere(function($query) {
                                    $query->where("leads.stage_id", 4);
                                    $query->where("leads.stage_remarks", 'accepted');
                                });
                            })
                            ->where(function($qry) use ($arrchk, $role) {
                                if (!empty($arrchk['fromdate']) || !empty($arrchk['todate'])) {
                                    $obj = DB::raw("date(leads.appointment_date)");
                                    if ((!empty($arrchk['fromdate'])) && (!empty($arrchk['todate']))) {
                                        $qry = $qry->whereBetween($obj, [date("Y-m-d", strtotime($arrchk['fromdate'])), date("Y-m-d", strtotime($arrchk['todate']))]);
                                    } elseif ((!empty($arrchk['fromdate'])) && (empty($arrchk['todate']))) {
                                        $qry = $qry->where($obj, ">=", [date("Y-m-d", strtotime($arrchk['fromdate']))]);
                                    } elseif ((empty($arrchk['fromdate'])) && (!empty($arrchk['todate']))) {
                                        $qry = $qry->where($obj, "<=", [date("Y-m-d", strtotime($arrchk['todate']))]);
                                    }
                                }
                            })
                            ->select("leads.reference_id", "leads.name", "mobile", "email", "ftl_id", "fo_id", "appointment_date", "stage_id", "stage_remarks", "district_id", "state_id", "verifier", "customer_id")
                            ->orderBy("reference_id", "desc");
        }

        public function spoklist(Request $request) {
            $query = $this->spocdata($request);
            $data['results'] = $this->pagination($query, 50, $request);
            $result = User::whereIn('role', ['FTL', 'FO'])
                    ->pluck("name", "id");
            $data['user'] = json_decode(json_encode($result), 1);
            $data['controller'] = 'spokField';
            $data['action'] = 'spokList';
            return view('lms/spok_list', $data)->with($request->all());
        }

        public function spocreportexport(Request $request) {
            $results = $this->spocdata($request)->get();
            $data = json_decode(json_encode($results), 1);
            $header = array('ID', 'Name', 'Mobile', 'Email Id', 'FTL', 'FO', 'Appointment', 'City', 'State', 'Address', 'Stage', 'Type');
            $filename = 'Spocreport';
            $resultstate = States::where('status', 'Active')
                    ->pluck("state", "state_id");
            $state = json_decode(json_encode($resultstate), 1);
            $resultcity = Cities::where('status', 'Active')
                    ->pluck("city", "city_id");
            $city = json_decode(json_encode($resultcity), 1);
            $resultuser = User::whereIn('role', ['FTL', 'FO'])
                    ->pluck("name", "id");
            $user = json_decode(json_encode($resultuser), 1);
            Excel::create($filename . date("d-m-Y"), function($excel) {
                $excel->sheet('Sheet 1', function($sheet) {
                    
                });
            })->store('xlsx', storage_path('excel'));

            Excel::load('storage/excel/' . $filename . date("d-m-Y") . '.xlsx', function($doc) use ($data, $header, $filename, $state, $city, $user) {
                $string = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
                $sheet = $doc->setActiveSheetIndex(0);
                for ($i = 0; $i < count($header); $i++) {
                    if ($i >= 26) {
                        $rpt = ($i % 26);
                        $quo = (int) ($i / 26);
                        $str1 = $string[$quo - 1] . $string[$rpt];
                    } else {
                        $str1 = $string[$i];
                    }
                    $sheet->setCellValue($str1 . '1', $header[$i]);
                }
                $append = 1;
                for ($j = 0; $j < count($data); $j++) {

                    $append = $append + 1;
                    $i = 0;
                    $arrchange = array_diff_key($data[$j], ['stage_remarks' => 0, 'stage_id' => 0]);
                    $arrchange['stageremarks'] = $data[$j]['stage_id'];
                    $arrchange['verifier'] = $data[$j]['verifier'] . $data[$j]['stage_remarks'];
                    foreach ($arrchange as $key => $value) {

                        $json = [];
                        if ($key == 'verifier') {
                            $check = json_decode($value, 1);
                            if (!empty($check)) {
                                $removekey = array_flip(array_diff(array_keys($check), ["address"]));
                                $json = array_diff_key(json_decode($value, 1), $removekey);
                                $jsonincre = $i;
                                foreach ($json as $jsonkey => $jsonarray) {
                                    $jsonvalue = is_array($jsonarray) ? key($jsonarray) : $jsonarray;
                                    if ($jsonincre >= 26) {
                                        $rpt = ($jsonincre % 26);
                                        $quo = (int) ($jsonincre / 26);
                                        $str = $string[$quo - 1] . $string[$rpt];
                                    } else {
                                        $str = $string[$jsonincre];
                                    }
                                    $sheet->setCellValue($str . $append, $jsonvalue);
                                    $jsonincre++;
                                }
                            }
                        } elseif ($key == "state_id") {
                            $statevalue = isset($state[$value]) ? $state[$value] : "";
                            $sheet->setCellValue($string[$i] . $append, $statevalue);
                        } elseif ($key == "district_id") {
                            $cityvalue = isset($city[$value]) ? $city[$value] : "";
                            $sheet->setCellValue($string[$i] . $append, $cityvalue);
                        } elseif ($key == "appointment_date") {
                            $vtime = '-';
                            if (!empty($value) && $value != '0000-00-00 00:00:00') {
                                $vtime = date('d-m-Y H:i', strtotime($value));
                            }
                            $sheet->setCellValue($string[$i] . $append, $vtime);
                        } elseif ($key == "ftl_id") {
                            $ftl = "-";
                            if (!empty($value)) {
                                $ftl = isset($user[$value]) ? $user[$value] : "-";
                            }
                            $sheet->setCellValue($string[$i] . $append, $ftl);
                        } elseif ($key == "fo_id") {
                            $fo = "-";
                            if (!empty($value)) {
                                $fo = isset($user[$value]) ? $user[$value] : "-";
                            }
                            $sheet->setCellValue($string[$i] . $append, $fo);
                        } elseif ($key == "customer_id") {
                            $type = "Reference";
                            if (empty($value) || $value == 0) {
                                $type = "Lead";
                            }
                            $sheet->setCellValue($string[$i] . $append, $type);
                        } else {

                            $sheet->setCellValue($string[$i] . $append, $value);
                        }
                        $i++;
                    }
                }
                unlink(storage_path('excel/' . $filename . date("d-m-Y") . '.xlsx'));
            })->download('xlsx');
        }

        public function spokview($id) {


            $data['call_status'] = VerifierCallStatus::where('id', 3)->get(['id', 'name', 'status'])->toArray();
            $data['call_sub_status'] = VerifierCallSubStatus::where('verifier_call_status_id', 3)->get(['id', 'name', 'verifier_call_status_id', 'status'])->toArray();
            $data['reference'] = Lead::findorFail($id);
            $result = DB::table("states")
                    ->join("cities", "states.state_id", "=", "cities.state_id")
                    ->where('city_id', $data['reference']->district_id)
                    ->select("city", "state")
                    ->first();

            $data['area'] = json_decode(json_encode($result), 1);
            $results = User::whereIn('role', ['FO', 'FTL'])
                    //->where('city_id', $data['reference']->district_id)
                    ->get(['name', 'id', "role", "reporting_manager"]);

            $data['folist'] = json_decode(json_encode($results), 1);
            $data['controller'] = 'spokField';

            $data['action'] = 'spokList';

            return view('lms.spok_view', $data);
        }

        public function spokviewupdate(Request $request) {

            $flag = $request['call_sub_status'];
            //$arr['ftl_id'] = null;
            //$arr['fo_id'] = null;
            if ($flag == 5) {
                if ($request['fo_type'] == 'FTL') {
                    $remark = 'assign';
                    $arr['ftl_id'] = $request['sel_fo'];
                } elseif ($request['fo_type'] == 'FO') {
                    $remark = 'pending';
                    $arr['ftl_id'] = $request['ftl_id'];
                    $arr['fo_id'] = $request['sel_fo'];
                }
            } elseif ($flag == 6) {
                $remark = 'refix';
            } elseif ($flag == 7) {
                $remark = 'reschedule';
                $arr['appointment_date'] = date('Y-m-d H:i:s', strtotime($request['appointment_date']));
            } elseif ($flag == 8) {
                $remark = 'ni';
            }


            $arr['stage_id'] = 5;

            $arr['stage_remarks'] = $remark;

            Lead::where('reference_id', $request['id'])
                    ->update($arr);

            $interaction = $request->all();


            $interaction['type'] = "lead";

            $this->interaction($interaction);

            return redirect()->route('spok-list');
        }

        public function ftllist() {
            $results1 = DB::table("leads")
                    ->where("leads.stage_id", 5)
                    ->where("leads.stage_remarks", 'assign')
                    //->where("leads.ftl_id", Auth::user()->id)
                    ->where(function($q) {
                        $q->where("leads.ftl_id", Auth::user()->id)
                        ->Orwhere(function($qry2) {
                            if (in_array(Auth::user()->role, ['admin'])) {
                                $qry2->whereNotNull("leads.ftl_id");
                            }
                        });
                    })
                    ->where("leads.appointment_date", '>=', DB::raw('now()'))
                    ->select("leads.name", "leads.mobile", "leads.email", "leads.stage_remarks", "leads.appointment_date", "leads.reference_id")
                    ->get();
            $data['results'] = json_decode(json_encode($results1), 1);
            $results2 = User::where('role', 'FO')
                    ->where('reporting_manager', Auth::user()->id)
                    ->pluck('name', 'id');
            $data['user'] = json_decode(json_encode($results2), 1);
            $data['controller'] = "ftl";
            $data['action'] = "ftllist";
            //dd($data);
            return view('lms/ftl-list', $data);
        }

        public function ftlassignedlist() {
            $results1 = DB::table("leads")
                    //->join("$database2.sha_relationship_manager", "$database2.sha_references.rel_manager_id",'=', "$database2.sha_relationship_manager.manager_id")
                    ->whereIn("stage_id", [5, 6])
                    ->whereIn("stage_remarks", ['pending', 'rejected', 'accepted'])
                    //->where("ftl_id", Auth::user()->id)
                    ->where(function($q) {
                        $q->where("leads.ftl_id", Auth::user()->id)
                        ->Orwhere(function($qry2) {
                            if (in_array(Auth::user()->role, ['admin'])) {
                                $qry2->whereNotNull("leads.ftl_id");
                            }
                        });
                    })
                    ->select("name", "mobile", "email", "stage_remarks", "appointment_date", "reference_id", "ftl_id", "fo_id", "stage_id", "stage_remarks")
                    ->get();
            $data['results'] = json_decode(json_encode($results1), 1);
            $results2 = User::whereIn('role', ['FO', 'FTL'])->pluck('name', 'id');
            $data['user'] = json_decode(json_encode($results2), 1);
            $data['controller'] = "ftl";
            $data['action'] = "ftlassigned";
            return view('lms/ftl-assigned-list', $data);
        }

        public function assighfo(Request $request) {
            $cust_id = $request['ref_id'];
            $sales_id = $request['fo_id'];
            Lead::where(['reference_id' => $cust_id])
                    ->update(['fo_id' => $sales_id, 'stage_remarks' => 'pending']);
            $request->session()->flash('flash_message', 'Lead Assiged To FO Successfully!!');
            return;
        }

        public function ftassignlist() {
            $crm_database = $this->crm_database;
            $database2 = $this->database2;
            $results1 = DB::table("$database2.sha_references")
                    ->join("$database2.sha_relationship_manager", "$database2.sha_references.rel_manager_id", '=', "$database2.sha_relationship_manager.manager_id")
                    ->where("$database2.sha_references.stage_id", 5)
                    ->where("$database2.sha_references.stage_remarks", 'pending')
                    ->select("$database2.sha_references.name", "$database2.sha_references.mobile", "$database2.sha_references.email", "stage_remarks", "appointment_date", "reference_id", "manager_name")
                    ->get();
            $data['results'] = json_decode(json_encode($results1), 1);
            $results2 = User::where('role', 'FO')->pluck('name', 'id');
            $data['user'] = json_decode(json_encode($results2), 1);
            $data['controller'] = "ftl";
            $data['action'] = "ftllist";
            return view('lead/ftl-assign-list', $data);
        }

        public function leadsale($flag = null) {
            $user_id = Auth::user()->id;
            return DB::table("leads")
                            ->join("fo_remarks", "leads.reference_id", "=", "fo_remarks.reference_id")
                            ->join(DB::raw("(SELECT max(id) as mxid,reference_id FROM fo_remarks group by reference_id) remarks"), function($join) use ($user_id) {
                                $join->on("fo_remarks.id", '=', 'remarks.mxid');
                            })
                            ->where(function($q) use ($user_id, $flag) {
                                $q->where(function($query) use ($user_id) {
                                    $query->where("leads.stage_id", 7);
                                    $query->where("leads.stage_remarks", 'saledone');
                                });
                                if ($flag == "all") {
                                    $q->Orwhere(function($query) use ($user_id) {
                                        $query->where("leads.stage_id", 11);
                                        $query->where("leads.stage_remarks", 'activated');
                                    });
                                }
                            })
                            ->where(function($q) {
                                if (Auth::user()->role == "FO") {
                                    $q->where('fo_id', Auth::user()->id);
                                } elseif (Auth::user()->role == "FTL") {
                                    $q->where('ftl_id', Auth::user()->id);
                                }
                            })
                            ->select("leads.name", "leads.mobile", "leads.email", "leads.reference_id", "fo_remarks.plan_size", "fo_remarks.form_no", "fo_remarks.ecs_option")
                            ->get();
        }

        public function fofltsale() {

            $data['results'] = json_decode(json_encode($this->leadsale("all")), 1);
            $data['controller'] = "fc";
            $data['action'] = "fofltsale";
            return view('lms/fo-ftl-sale-list', $data);
        }

        public function accountactive() {
            $data['results'] = json_decode(json_encode($this->leadsale()), 1);
            $data['controller'] = "lmsstatus";
            $data['action'] = "accountactive";
            return view('lms/account-active', $data);
        }

        public function folist() {
            $results1 = DB::table("leads")
                    ->where("leads.stage_id", 5)
                    ->where("leads.fo_id", Auth::user()->id)
                    ->where("leads.stage_remarks", 'pending')
                    ->select("name", "mobile", "email", "stage_remarks", "appointment_date", "reference_id", 'customer_id')
                    ->get();
            $data['results'] = json_decode(json_encode($results1), 1);
            $data['controller'] = "fo";
            $data['action'] = "folist";
            return view('lms/fo-list', $data);
        }

        public function foupdate(Request $request) {
            if ($request['call_sub_status'] == '9') {
                $remarks = 'accepted';
            } elseif ($request['call_sub_status'] == '10') {
                $remarks = 'rejected';
            }
            $array = $request->all();
            $array['appointment_date'] = null;
            //dd($array);
            Lead::where(['reference_id' => $request['id']])
                    ->update(['stage_id' => 6, 'stage_remarks' => $remarks]);
            $this->interaction($array);
            return redirect()->route('fo-list')->with(['flash_message' => 'Data Saved successfully!!!']);
        }

        public function fomeetinglist(Request $request) {
            $results1 = DB::table("leads")
                    ->where("stage_id", 6)
                    ->where("stage_remarks", 'accepted')
                    ->where("fo_id", Auth::user()->id)
                    ->select("name", "mobile", "email", "stage_remarks", "appointment_date", "reference_id", "verifier", 'customer_id', 'emi_size')
                    ->get();
            $data['results'] = json_decode(json_encode($results1), 1);
            $data['controller'] = "fo";
            $data['action'] = "fomeeting";
            return view('lms/fo-meeting-list', $data);
        }

        public function fomeetingdetails($id) {
            $data['reference'] = Lead::findorFail($id);
            $data['paymentmode'] = config('custom.FoPayment');
            $data['action'] = "fomeeting";
            $data['controller'] = "fo";
            return view('lms/fo-meeting-view', $data);
        }

        public function fomeetingupdate(Request $request) {
           
            $remarks = $request['remarks'];
            $obj = new FoRemarks();
            $obj->reference_id = $request['id'];
            $obj->user_id = Auth::user()->id;
            $obj->meet = $request['meeting'];
            $obj->status = $request['remarks'];
            $obj->plan_size = $request['amount'];
            $obj->payment_mode = $request['paymentmode'];
            $obj->form_no = $request['form_no'];
            $obj->ecs_option = $request['ecs'];
            $obj->hth = $request['hth'];
            $obj->payment_date = !empty($request['paydate']) ? date('Y-m-d', strtotime($request['paydate'])) : null;
            $obj->ni_reason = $request['nireason'];
            $obj->ni_reason_other = $request['reasonother'];
            if ($request['paymentmode'] == 2) {
                $arrcheque['cheque_no'] = $request['chequeno'];
                $arrcheque['cheque_date'] = $request['chequedate'];
                $arrcheque['bank_name'] = $request['bankname'];
                if (!empty($request['subcategory'])) {
                    $arrcheque['subcategory'] = $request['subcategory'];
                }
                $obj->payment_data = json_encode($arrcheque);
            } elseif (in_array($request['paymentmode'], [10, 6])) {
                $obj->payment_data = json_encode(['transaction_id' => $request['transaction_id']]);
            }
            $obj->save();
            $arr = ['stage_id' => 7, 'stage_remarks' => $request['remarks']];
            $interaction = ['call_status' => 7, 'appointment_date' => null, 'verification_interaction' => ""];
            if ($request['remarks'] == "followup") {
                $arr['stage_remarks'] = $request['subcategory'];
            }
            if ($request['meeting'] == "notmet") {
                $arr['metcount'] = DB::raw('metcount+1');
            }
            Lead::where(['reference_id' => $request['id']])
                    ->update($arr);
            $result = VerifierCallSubStatus::where('code', $remarks)->first(['id']);
            $interaction['call_sub_status'] = json_decode(json_encode($result), 1)['id'];
            $interaction = array_merge($request->all(), $interaction);
            $this->interaction($interaction);
            $request->session()->flash('flash_message', 'Data Saved Successfully!');
            return 1;
        }

        public function notintestedvoc() {
            $results = DB::table("leads")
                    ->join("users as a", "leads.rel_manager_id", "=", "a.id")
                    ->join("users as b", "a.reporting_manager", "=", "b.id")
                    ->where('stage_id', 8)
                    ->where('stage_remarks', 'nointerest')
                    ->select("reference_id", "leads.name", "leads.mobile", "leads.email", "a.name as managername", "b.name as tlname")
                    ->get();
            $data['results'] = json_decode(json_encode($results), 1);
            $data['controller'] = 'voc';
            $data['action'] = 'vocnotinterested';
            return view('lms/voc-notinterested', $data);
        }

        public function vocdata(Request $request) {
            $arrchk = $request->all();
            $option = array_diff(array_merge_recursive(['refix', 'ni', 'callback', 'verifierreturnvvoc', 'voc', 'switchoff', 'ringing', 'rpnc', 'wrongno', 'shorthangup'], array_keys(config('custom.Notmet')), array_keys(config('custom.Met'))), [config('custom.Metkey')[0]]);

            return DB::table("leads")
                            ->leftJoin(DB::raw("(SELECT verifier_interaction.callback_date as callback,lastdate.reference_id as last_ref,verifier_interaction.verifier_call_sub_status_other as call_status_other,verifier_interaction.remarks as remarks,verifier_interaction.created_at as date from (select reference_id,max(id) as mxid FROM verifier_interaction  group by reference_id) as lastdate inner join verifier_interaction on lastdate.mxid=verifier_interaction.id) vocdata"), function($join) {
                                $join->on("leads.reference_id", '=', 'vocdata.last_ref');
                            })
                            ->leftJoin(DB::raw("(select tbl.lastmx as status_id,tbl.reference_id as status_ref,verifier_interaction.verifier_call_status_id as last_status_id,verifier_interaction.verifier_call_sub_status_id as last_status_sub_id from (SELECT max(id) as lastmx,reference_id FROM verifier_interaction  where roles!='Voc' group by reference_id) as tbl inner join verifier_interaction on verifier_interaction.id=tbl.lastmx) laststatus"), function($join) {
                                $join->on("leads.reference_id", '=', 'laststatus.status_ref');
                            })
                            ->leftJoin(DB::raw("(select count(tbl.mx) as countcall,tbl.reference_id countvocref from (SELECT max(id) as mx,reference_id FROM verifier_interaction where roles!='Voc' group by reference_id) as tbl inner join verifier_interaction on tbl.reference_id=verifier_interaction.reference_id and tbl.mx<verifier_interaction.id group by tbl.reference_id) countvoc"), function($join) {
                                $join->on("leads.reference_id", '=', 'countvoc.countvocref');
                            })
                            ->whereIn("leads.stage_id", [5, 7, 8, 0])
                            ->whereIn("leads.stage_remarks", $option)
                            ->where(function($q) use ($arrchk) {
                                if (!empty($arrchk['lead'])) {
                                    $q->where("leads.reference_id", $arrchk['lead']);
                                }
                            })
                            ->where(function($qry) use ($arrchk) {
                                if (!empty($arrchk['fromdate']) || !empty($arrchk['todate'])) {
                                    $obj = DB::raw("date(callback)");
                                    if ((!empty($arrchk['fromdate'])) && (!empty($arrchk['todate']))) {
                                        $qry = $qry->whereBetween($obj, [date("Y-m-d", strtotime($arrchk['fromdate'])), date("Y-m-d", strtotime($arrchk['todate']))]);
                                    } elseif ((!empty($arrchk['fromdate'])) && (empty($arrchk['todate']))) {
                                        $qry = $qry->where($obj, ">=", [date("Y-m-d", strtotime($arrchk['fromdate']))]);
                                    } elseif ((empty($arrchk['fromdate'])) && (!empty($arrchk['todate']))) {
                                        $qry = $qry->where($obj, "<=", [date("Y-m-d", strtotime($arrchk['todate']))]);
                                    }
                                }
                            })
                            ->select("leads.reference_id", "name", "mobile", "customer_id", DB::raw("DATE_FORMAT(callback,'%d-%c-%Y %H:%i') as callback"), "stage_remarks", "stage_id", "last_status_id", "last_status_sub_id", "countcall", "metcount", "call_status_other", "remarks", "date")
                            ->orderBy("callback", "asc");
        }

        public function voclist(Request $request) {
            $results = $this->vocdata($request);
            $data['results'] = $this->pagination($results, 20, $request);
            $data['call_status'] = VerifierCallStatus::where('id', 5)->get(['id', 'name', 'status'])->toArray();
            $data['call_sub_status'] = VerifierCallSubStatus::where('verifier_call_status_id', 5)->get(['id', 'name', 'verifier_call_status_id', 'status', 'code'])->toArray();
            $data['remark_status'] = VerifierCallStatus::pluck('name', 'id')->toArray();
            $data['remark_sub_status'] = VerifierCallSubStatus::pluck('name', 'id')->toArray();
            $data['action'] = "voclist";
            $data['controller'] = "voc";
            return view('lms/voc-list', $data)->with($request->all());
        }

        public function vocexcel(Request $request) {
            $data = json_decode(json_encode($this->vocdata($request)->get()), 1);
            $remark_status = VerifierCallStatus::pluck('name', 'id')->toArray();
            $remark_sub_status = VerifierCallSubStatus::pluck('name', 'id')->toArray();
            $filename = "Voc-data";
            Excel::create($filename . date("d-m-Y"), function($excel) {
                $excel->sheet('Sheet 1', function($sheet) {
                    
                });
            })->store('csv', storage_path('excel'));
            $header = ['ID', 'Name', 'Mobile', 'CallBack Date', 'Attempt', 'Count', 'Remarks', 'Last Attempt Date', 'Type', 'Source Stage', 'Current Stage', 'Call Status(Other)'];
            Excel::load('storage/excel/' . $filename . date("d-m-Y") . '.csv', function($doc) use ($data, $header, $filename, $remark_status, $remark_sub_status) {
                $string = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
                $sheet = $doc->setActiveSheetIndex(0);
                for ($i = 0; $i < count($header); $i++) {
                    $sheet->setCellValue($string[$i] . '1', $header[$i]);
                }
                $append = 1;
                $customarr = config('custom.VocStatus');
                for ($j = 0; $j < count($data); $j++) {
                    $append = $append + 1;
                    $i = 0;
                    $sitedata = $data[$j];
                    $arr = array_diff_key($sitedata, ['customer_id' => "", 'stage_remarks' => "", "stage_id" => "", "last_status_id" => "", 'last_status_sub_id' => "", 'call_status_other' => ""]);
                    $appointdate = $time = "";
                    $type = "Lead";
                    if (!empty($sitedata['customer_id'])) {
                        $type = "Reference";
                    }
                    $call_sub_status_other = isset($customarr[$sitedata['call_status_other']]) ? $customarr[$sitedata['call_status_other']] : '';
                    $sourcestage = (isset($remark_status[$sitedata['last_status_id']]) ? $remark_status[$sitedata['last_status_id']] : "" ) . ' - ' . (isset($remark_sub_status[$sitedata['last_status_sub_id']]) ? $remark_sub_status[$sitedata['last_status_sub_id']] : "");
                    $currenstage = status($sitedata['stage_id'], $sitedata['stage_remarks']);
                    if ($sitedata['callback'] == '00-0-0000 00:00' || $sitedata['callback'] == '01-1-1970 01:00')
                        $callbacks = '--';
                    else
                        $callbacks = $sitedata['callback'];

                    $arr = array_merge($arr, ['type' => $type, 'sourcestage' => $sourcestage, 'currenstage' => $currenstage, 'call_status' => $call_sub_status_other]);
                    foreach ($arr as $key => $value) {
                        if ($key == "lead_source") {
                            $disp = config('custom.ReferenceLeadSource.' . $value);
                        } else {
                            $disp = $value;
                        }
                        $sheet->setCellValue($string[$i] . $append, $disp);
                        $i++;
                    }
                }
                unlink(storage_path('excel/' . $filename . date("d-m-Y") . '.csv'));
            })->download('csv');
        }

        public function sendmailToVoc() {
            request()->merge(['fromdate' => "", 'todate' => date('d-m-Y')]);
            $data = json_decode(json_encode($this->vocdata(request())->get()), 1);
            if (count($data) > 0) {
                $table = '<table border="1" style="text-align:center;padding:10px;"><thead><tr><th>ID</th><th>Name</th><th>Mobile</th><th>Stage</th><tbody>';
                foreach ($data as $lead) {
                    $table .= '<tr><td>' . $lead['reference_id'] . '</td>';
                    $table .= '<td>' . $lead['name'] . '</td>';
                    $table .= '<td>' . $lead['mobile'] . '</td>';
                    $table .= '<td>' . status($lead['stage_id'], $lead['stage_remarks']) . '</td></tr>';
                }
                $table .= '</tbody></table>';
                $content = "<p>Dear Voc,</p><p>&nbsp; &nbsp; &nbsp; Below following Lead details is still <strong>pending.&nbsp;</strong></p><p><strong>&nbsp; &nbsp; &nbsp;&nbsp;</strong>Please update the status today end of day</p><br>" . $table;
                $email = array();
                //$email['to_mail']="surender@birlagold.com";
                $email['to_mail'] = "hashim.ansari@birlagold.com";
                $email['to_name'] = "Hashim Ansari";
                //	$email['cc']= ["deepak@birlagold.com","deepakgupta9920@gmail.com"];
                $email['cc'] = ["surender@birlagold.com", "chairman@kserasera.com", "harshupadhyay93@gmail.com", "pasanna@kgrsecurities.com", "sachit@birlagold.com"];
                $email['from_email'] = "customerservice.bgp@birlagold.com";
                $email['from_name'] = "Cherishgold";
                $email['subject'] = "Pending lead for Approval for Voc";
                $email['messages'] = $content;
                $this->sendemail($email);
                \Log::info(json_encode($data));
            } else {
                \Log::info("No Lead Found of Voc!!!");
            }
        }

        public function vocupdate(Request $request) {
            $this->validate($request, [
                'id' => 'required'
            ]);
            $interatciton = $this->interaction($request->all());
            $arr['stage_id'] = 8;
            $flag = $request['call_sub_status'];
            if ($flag == 11) {
                $arr['stage_remarks'] = 'nointerest';
                $arr['appointment_date'] = null;
                $arr['status'] = 'Not Interested';
            } elseif ($flag == 12) {
                $arr['stage_remarks'] = 'vocrefix';
                $appointment_date = $request['appointment_date'];
                $arr['appointment_date'] = date("Y-m-d H:i:s", strtotime($appointment_date));
            } elseif ($flag == 13) {
                $arr['stage_remarks'] = 'agent';
                $arr['appointment_date'] = null;
            } elseif (in_array($flag, [14, 24, 25, 26, 27, 28])) {
                $arr['stage_remarks'] = strtolower(str_replace(" ", null, $request['status_name']));
                $arr['appointment_date'] = null;
            }

            Lead::where(['reference_id' => $request['id']])
                    ->update($arr);
            if (!empty($request['voc_status'])) {
                VerifierInteraction::where('id', $interatciton->id)
                        ->update(['verifier_call_sub_status_other' => $request['voc_status']]);
            }
            return redirect()->back()->with(['flash_message' => 'Done successfully!!!']);
        }

        public function rmRejectedlist() {
            $userid = Auth::user()->id;
            $role = Auth::user()->role;
            $results = DB::table("leads")
                    ->join("relationship_manager as rm", "leads.rel_manager_id", '=', "rm.manager_id")
                    //   ->leftJoin("$database2.sha_customer_master", "$database2.sha_references.customer_id", '=', "$database2.sha_customer_master.customer_id")
                    ->whereIn("leads.stage_id", [2, 4, 8, 10])
                    ->whereIn("leads.stage_remarks", ['rejected', 'agent'])
                    ->where(function($q) use ($userid, $role) {
                        $q->WhereIn("leads.rel_manager_id", function($query)use($userid, $role) {
                            if (Auth::user()->role == 'Team Lead') {
                                $query->select('id')
                                ->from("users")
                                ->where('status', 'active')
                                ->where('reporting_manager', Auth::user()->id);
                            } elseif (in_array(\Auth::user()->role, ['Relationship Manager', 'Trainee'])) {
                                $query->select('id')
                                ->from("users")
                                ->where('status', 'active')
                                ->where('id', Auth::user()->id);
                            } else {
                                $query->select('id')
                                ->from("users")
                                ->where('status', 'active');
                            }
                        });
                    })
                    ->select("name", "mobile", "email", "stage_remarks", "appointment_date", "leads.reference_id", "stage_id", "leads.customer_id", "rel_manager_id")

                    // ->select("name", "mobile", "email", "stage_remarks", "appointment_date", "leads.reference_id", "stage_id", "leads.customer_id", "rel_manager_id", "relationship_manager")
                    ->get();

            $data['results'] = json_decode(json_encode($results), 1);
            $data['action'] = "rejectedlist";
            $data['controller'] = "fc";
            $user = User::whereIn('role', ['Tele Caller', 'Assistant Manager', 'Manager', 'Team Lead', 'Verifier Head', 'Verifier', 'Ftl', 'Fo', 'Voc'])->select('name', 'id', 'reporting_manager')
                    ->get();
            $userdata = json_decode(json_encode($user), 1);
            $data['user'] = array_column($userdata, null, 'id');
            return view('lms/rm-rejected-list', $data);
        }

        public function rmAcceptlist() {
            $userid = Auth::user()->id;
            $role = Auth::user()->role;
            $results = DB::table("leads")
                    ->join("relationship_manager as rm", "leads.rel_manager_id", '=', "rm.manager_id")
                    //   ->leftJoin("$database2.sha_customer_master", "$database2.sha_references.customer_id", '=', "$database2.sha_customer_master.customer_id")
                    ->whereIn("leads.stage_id", [2, 4, 8, 10])
                    ->whereIn("leads.stage_remarks", ['accepted', 'agent'])
                    ->where(function($q) use ($userid, $role) {
                        $q->WhereIn("leads.rel_manager_id", function($query)use($userid, $role) {
                            if (Auth::user()->role == 'Team Lead') {
                                $query->select('id')
                                ->from("users")
                                ->where('status', 'active')
                                ->where('reporting_manager', Auth::user()->id);
                            } elseif (in_array(\Auth::user()->role, ['Tele Caller', 'Assistant Manager', 'Manager', 'Team Lead', 'Verifier Head', 'Verifier', 'Ftl', 'Fo', 'Voc'])) {
                                $query->select('id')
                                ->from("users")
                                ->where('status', 'active')
                                ->where('id', Auth::user()->id);
                            } else {
                                $query->select('id')
                                ->from("users")
                                ->where('status', 'active');
                            }
                        });
                    })
                    ->select("name", "mobile", "email", "stage_remarks", "appointment_date", "leads.reference_id", "stage_id", "leads.customer_id", "rel_manager_id")

                    // ->select("name", "mobile", "email", "stage_remarks", "appointment_date", "leads.reference_id", "stage_id", "leads.customer_id", "rel_manager_id", "relationship_manager")
                    ->get();

            $data['results'] = json_decode(json_encode($results), 1);
            $data['action'] = "acceptedlist";
            $data['controller'] = "fc";
            $user = User::whereIn('role', ['Tele Caller', 'Assistant Manager', 'Manager', 'Team Lead', 'Verifier Head', 'Verifier', 'Ftl', 'Fo', 'Voc'])->select('name', 'id', 'reporting_manager')
                    ->get();
            $userdata = json_decode(json_encode($user), 1);
            $data['user'] = array_column($userdata, null, 'id');
            return view('lms/rm-accepted-list', $data);
        }

        public function rmRejectedreason($id) {

            $result = DB::table("verifier_interaction")
                    ->join("users", "verifier_interaction.rm_id", '=', "users.id")
                    ->join("verifier_call_status", "verifier_interaction.verifier_call_status_id", '=', "verifier_call_status.id")
                    ->join("verifier_call_sub_status", "verifier_interaction.verifier_call_sub_status_id", '=', "verifier_call_sub_status.id")
                    ->where("verifier_interaction.reference_id", $id)
                    ->select("users.name as rmname", "verifier_call_status.name as call_status", "verifier_call_sub_status.name as call_sub_status", "remarks", DB::raw("DATE_FORMAT(verifier_interaction.created_at,'%d %b,%y') as created_at"), DB::raw("DATE_FORMAT(verifier_interaction.callback_date,'%d-%m-%Y %H:%i') as callback_date"))
                    ->orderBy("verifier_interaction.created_at", "desc")
                    ->get();
            $data['verifier'] = json_decode(json_encode($result), 1);
            $result1 = DB::table("interactions")
                    ->join("users", "interactions.rm_id", '=', "users.id")
                    ->join("call_status", "interactions.call_status_id", '=', "call_status.status_id")
                    ->join("call_sub_status", "interactions.call_substatus_id", '=', "call_sub_status.sub_status_id")
                    ->where("interactions.reference_id", $id)
                    ->select("users.name as rmname", "call_status.name as call_status", "call_sub_status.name as call_sub_status", "callback_datetime", "remarks", DB::raw("DATE_FORMAT(interactions.created_at,'%d %b,%y') as created_at"))
                    ->orderBy("interactions.created_at", "desc")
                    ->get();

            $data['interaction'] = json_decode(json_encode($result1), 1);
            return $data;
        }

        public function qualityverifiedlist(Request $request) {
            $data['results'] = $this->pagination($this->qualityaccepted($request), 50, $request);
            $user = User::whereIn('role', ['Trainee', 'Relationship Manager', 'Customer Service', 'Team Lead'])->select('name', 'id', 'reporting_manager')
                    ->get();
            $userdata = json_decode(json_encode($user), 1);
            $data['user'] = array_column($userdata, null, 'id');
            $data['action'] = "qualityverified";
            $data['controller'] = "fc";
            return view('lead/verifier-accepted-list', $data)->with($request->all());
        }

        public function qualityaccepted(Request $request) {
            $crm_database = $this->crm_database;
            $database2 = $this->database2;
            $arrchk = $request->all();
            return DB::table("$database2.sha_references")
                            //  ->join("$crm_database.users as usera", "$database2.sha_references.rel_manager_id",'=', "usera.id")
                            //  ->join("$crm_database.users as userb", "usera.reporting_manager",'=',"userb.id")
                            ->leftJoin("$database2.sha_customer_master", "$database2.sha_references.customer_id", '=', "$database2.sha_customer_master.customer_id")
                            ->leftJoin(DB::raw("(SELECT reference_id,max(created_at) as mxdate FROM verifier_interaction where roles='Team Lead' group by reference_id) tldate"), function($join) use ($database2) {
                                $join->on("$database2.sha_references.reference_id", '=', 'tldate.reference_id');
                            })
                            ->whereIn("$database2.sha_references.stage_id", [4, 3, 9])
                            ->whereIn("$database2.sha_references.stage_remarks", ['accepted', 'AssignedQuality', 'rejected', 'AssignedVocQuality'])
                            ->where(function($qry) use ($arrchk, $database2) {
                                if (!empty($arrchk['fromdate']) || !empty($arrchk['todate'])) {
                                    if ($arrchk['datewise'] == 'app') {
                                        $obj = DB::raw("date($database2.sha_references.appointment_date)");
                                    } elseif ($arrchk['datewise'] == 'tlver') {
                                        $obj = DB::raw("date(tldate.mxdate)");
                                    }
                                    if ((!empty($arrchk['fromdate'])) && (!empty($arrchk['todate']))) {
                                        $qry = $qry->whereBetween($obj, [date("Y-m-d", strtotime($arrchk['fromdate'])), date("Y-m-d", strtotime($arrchk['todate']))]);
                                    } elseif ((!empty($arrchk['fromdate'])) && (empty($arrchk['todate']))) {
                                        $qry = $qry->where($obj, ">=", [date("Y-m-d", strtotime($arrchk['fromdate']))]);
                                    } elseif ((empty($arrchk['fromdate'])) && (!empty($arrchk['todate']))) {
                                        $qry = $qry->where($obj, "<=", [date("Y-m-d", strtotime($arrchk['todate']))]);
                                    }
                                }
                            })
                            ->select("$database2.sha_references.reference_id", "$database2.sha_references.name", DB::raw("DATE_FORMAT($database2.sha_references.created,'%d-%c-%Y %H:%i') as created"), DB::raw("DATE_FORMAT(tldate.mxdate,'%d-%c-%Y') as mxdate"), DB::raw("DATE_FORMAT(appointment_date,'%d-%c-%Y') as appointdate"), DB::raw("DATE_FORMAT(appointment_date,'%H:%i') as appointtime"), "stage_id", "stage_remarks", "$database2.sha_references.state_id", "$database2.sha_references.district_id", "$database2.sha_references.customer_id", "rel_manager_id", "relationship_manager", DB::raw("Null as type"), "verifier")
                            ->orderBy('appointment_date', 'desc');
        }

        public function qualityverifiedexport(Request $request) {
            $results = $this->qualityaccepted($request)->get();
            $data = json_decode(json_encode($results), 1);
            $header = array('ID', 'Name', 'Entry Date', 'TL Verifier Date', 'Appointment Date', 'Appointment Time', "Status", 'State', 'City', 'Relationship Manager', 'TL Name', "Type", "Mobile", "Alt mobile", "Email", "Gender", "Age", "Marital Status", "Child", "Address", "Landmark", "Pincode", "Company Name", "Department", "Profession", "Income_group", "Income", "Investment  Question");
            $filename = 'accptedlead';
            $resultstate = States::where('status', 'Active')
                    ->pluck("state", "state_id");
            $state = json_decode(json_encode($resultstate), 1);
            $resultcity = Cities::where('status', 'Active')
                    ->pluck("city", "city_id");
            $city = json_decode(json_encode($resultcity), 1);

            $resultuser = User::whereIn('role', ['Trainee', 'Relationship Manager', 'Customer Service', 'Team Lead'])->select('name', 'id', 'reporting_manager')
                    ->get();
            $userdata = json_decode(json_encode($resultuser), 1);
            $user = array_column($userdata, null, 'id');
            Excel::create($filename . date("d-m-Y"), function($excel) {
                $excel->sheet('Sheet 1', function($sheet) {
                    
                });
            })->store('xlsx', storage_path('excel'));
            Excel::load('storage/excel/' . $filename . date("d-m-Y") . '.xlsx', function($doc) use ($data, $header, $filename, $state, $city, $user) {
                $string = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
                $sheet = $doc->setActiveSheetIndex(0);
                for ($i = 0; $i < count($header); $i++) {
                    if ($i >= 26) {
                        $rpt = ($i % 26);
                        $quo = (int) ($i / 26);
                        $str1 = $string[$quo - 1] . $string[$rpt];
                    } else {
                        $str1 = $string[$i];
                    }
                    $sheet->setCellValue($str1 . '1', $header[$i]);
                }
                $append = 1;
                for ($j = 0; $j < count($data); $j++) {
                    $append = $append + 1;
                    $i = 0;
                    $remarksstage = "Pending";
                    if ($data[$j]['stage_id'] == 4 && in_array($data[$j]['stage_remarks'], ['accepted', 'rejected'])) {
                        $remarksstage = ucfirst($data[$j]['stage_remarks']);
                    }
                    $arrchange = array_diff_key($data[$j], ['stage_remarks' => 0, 'customer_id' => 0]);
                    $arrchange['stage_id'] = $remarksstage;
                    $customer_id = $data[$j]['customer_id'];
                    foreach ($arrchange as $key => $value) {
                        $json = [];
                        if ($key == 'verifier') {
                            $check = json_decode($value, 1);
                            $json = array_diff_key(json_decode($value, 1), ['batch_id' => 0, 'campaign_code' => 0, 'lead_code' => 0, 'name' => 0, 'state' => 0, 'city' => 0, 'purpose_of_call', 'call_complaince' => 0, 'recording_statement' => 0, '']);
                            $jsonincre = $i;
                            foreach ($json as $jsonkey => $jsonarray) {
                                $jsonvalue = is_array($jsonarray) ? key($jsonarray) : "";
                                if ($jsonincre >= 26) {
                                    $rpt = ($jsonincre % 26);
                                    $quo = (int) ($jsonincre / 26);
                                    $str = $string[$quo - 1] . $string[$rpt];
                                } else {
                                    $str = $string[$jsonincre];
                                }
                                $sheet->setCellValue($str . $append, $jsonvalue);
                                $jsonincre++;
                            }
                        } elseif ($key == "state_id") {
                            $statevalue = isset($state[$value]) ? $state[$value] : "";
                            $sheet->setCellValue($string[$i] . $append, $statevalue);
                        } elseif ($key == "district_id") {
                            $cityvalue = isset($city[$value]) ? $city[$value] : "";
                            $sheet->setCellValue($string[$i] . $append, $cityvalue);
                        } elseif ($key == "rel_manager_id" || $key == "relationship_manager") {
                            $rmname = '-';
                            $tlname = '-';
                            if (empty($customer_id) || $customer_id == 0) {
                                if (isset($user[$arrchange['rel_manager_id']])) {
                                    $rmname = $user[$arrchange['rel_manager_id']]['name'];
                                    if ($user[$arrchange['rel_manager_id']]['reporting_manager']) {
                                        $tlname = $user[$user[$arrchange['rel_manager_id']]['reporting_manager']]['name'];
                                    }
                                }
                            } else {
                                if (isset($user[$arrchange['relationship_manager']])) {
                                    $rmname = $user[$arrchange['relationship_manager']]['name'];
                                    if ($user[$arrchange['relationship_manager']]['reporting_manager']) {
                                        $tlname = $user[$user[$arrchange['relationship_manager']]['reporting_manager']]['name'];
                                    }
                                }
                            }
                            if ($key == "rel_manager_id") {
                                $sheet->setCellValue($string[$i] . $append, $rmname);
                            } elseif ($key == "relationship_manager") {
                                $sheet->setCellValue($string[$i] . $append, $tlname);
                            }
                        } elseif ($key == "type") {
                            if (empty($customer_id) || $customer_id == 0) {
                                $type = "Lead";
                            } else {
                                $type = "Reference";
                            }
                            $sheet->setCellValue($string[$i] . $append, $type);
                        } else {
                            $sheet->setCellValue($string[$i] . $append, $value);
                        }
                        $i++;
                    }
                }
                unlink(storage_path('excel/' . $filename . date("d-m-Y") . '.xlsx'));
            })->download('xlsx');
        }

        public function editVerifier(Request $request, $id) {
            $crm_database = $this->crm_database;
            $database2 = $this->database2;

            $data['call_status'] = VerifierCallStatus::get(['id', 'name', 'status'])->toArray();
            $data['call_sub_status'] = VerifierCallSubStatus::get(['id', 'name', 'verifier_call_status_id', 'status'])->toArray();
            $data['reference'] = References::findorFail($id);

            if ($data['reference']['customer_id'] > 0) {
                $customer = Customer::findorFail($data['reference']['customer_id']);
                if ($customer)
                    $data['reference']['rel_manager_id'] = $customer['relationship_manager'];
            }

            $data['callStatus'] = CallStatus::get(['name', 'status_id', 'status'])->toArray();
            $data['callSubStatus'] = CallSubStatus::get(['name', 'sub_status_id', 'status_id', 'status'])->toArray();

            $data['states'] = States::get(['state', 'state_id', 'status'])->toArray();
            $data['cities'] = Cities::get(['city', 'city_id', 'state_id'])->toArray();

            $data['rms'] = User::lists('name', 'id');

            $data['converted_ref'] = References::where('converted_customer_id', '=', $id)->get(['reference_id'])->toArray();
            $reference_tag = array_map(function ($arr) {
                return $arr['reference_id'];
            }, $data['converted_ref']);
            $data['interaction'] = Interactions::where('reference_id', $id)->orderBy('created_at', 'desc')->get();
            $data['verifierinteraction'] = VerifierInteraction::where('reference_id', $id)->orderBy('created_at', 'desc')->get();
            if (!empty($data['reference']['rel_manager_id'])) {
                $manager = DB::table("$crm_database.users as a", "$database2.sha_references.rel_manager_id", "=", "a.id")
                        ->join("$crm_database.users as b", "a.reporting_manager", "=", "b.id")
                        ->where("a.id", "=", $data['reference']['rel_manager_id'])
                        ->select("a.name as emp", "b.name as head")
                        ->get();

                $data['manager'] = json_decode(json_encode($manager), true);
            }
            $data['controller'] = 'lead';
            $data['action'] = 'verifierList';
            $data['role'] = \Auth::user()->role;
            return view('lead/edit-verifier', $data);
        }

        public function updateVerifier(Request $request, References $id) {
            $id = $request->id;
            $post = References::findorFail($id);
            $post->name = $request->name;
            $post->email = $request->email;
            $post->mobile = $request->mobile;
            $statename = $cityname = "";
            $post->state_id = $request->state;
            $post->district_id = $request->city;
            $post->state_name = $request->state;
            $post->city_name = $request->city;
            $post->informed_to_reference = $request->informed;
            $post->emi_size = $request->emi_size;
            $post->appointment_date = $request['appointment_date'];
            $post->appointment_time = $request['appointment_time'];
            $post->status = $request->status;
            $post->verifier = $this->jsondata($request->all());
            $post->save();
            $this->interaction($request->all() + ['type' => 'lead']);
            $data['controller'] = 'lead';
            $data['action'] = 'editVerifier';
            return redirect('verifier-list');
        }

        function fixAppointment(Request $request) {
            $crm_database = $this->crm_database;
            $reference_id = $request['reference_id'];
            $appointment_date = $request['appointment_date'];
            if ($appointment_date != "") {
                $reference = References::findorFail($reference_id);
                $reference->stage_id = 1;
                $reference->stage_remarks = '';
                $reference->appointment_date = date("Y-m-d H:i:s", strtotime($appointment_date));
                $reference->save();
                return 1;
            }
            return 0;
        }

        public function tlverifierList(Request $request) {


            return $this->leadverifier($request);
        }

        public function referenceverifier($request) {
            $crm_database = $this->crm_database;
            $database2 = $this->database2;
            $dataunion = DB::table("$database2.sha_references")
                    ->join("$database2.sha_customer_master", "$database2.sha_references.customer_id", '=', "$database2.sha_customer_master.customer_id")
                    ->join("$database2.sha_relationship_manager as a", "$database2.sha_customer_master.relationship_manager", '=', "a.manager_id")
                    ->leftJoin("$crm_database.users as b", "a.manager_id", "=", "b.id")
                    ->where("$database2.sha_references.status", '!=', 'Converted')
                    ->whereNotNull("$database2.sha_references.appointment_date")
                    ->where("$database2.sha_references.stage_id", 1)
                    ->where(function($whr)use ($database2) {



                        $whr->whereNotNull("$database2.sha_references.customer_id")
                        ->Orwhere("$database2.sha_references.customer_id", '!=', 0);
                    })
                    ->where(function($q) use ($crm_database, $database2) {

                        if (\Auth::user()->role == 'Team Lead') {
                            $q->WhereIn("$database2.sha_customer_master.relationship_manager", function($query)use($crm_database) {




                                $query->select('id')
                                ->from("$crm_database.users")
                                ->where('status', 'active')
                                ->where('reporting_manager', \Auth::user()->id);
                            });
                        }
                    })
                    ->select("$database2.sha_references.reference_id", "$database2.sha_references.name", "$database2.sha_references.created", "a.manager_name as manager_name", "$database2.sha_references.lead_source", "$database2.sha_references.lead_source_info", "$database2.sha_references.appointment_date", "b.name as tlname");

            $data['results'] = $this->pagination($dataunion, 20, $request);
            $data['controller'] = 'tl';
            $data['action'] = 'tlverifier-list-reference';
            return view('lead.tlreference', $data);
        }

        public function leadverifier($request) {


            $arrsearch = array('option' => $request['option'], 'q' => $request['q']);
            $arrchk = $request->all();
            $role = Auth::user()->role;
            $user_id = Auth::user()->id;
            $dataunion = DB::table("leads")
                    ->join("users as a", "leads.rel_manager_id", "=", "a.id")
                    ->leftJoin("users as b", "a.reporting_manager", "=", "b.id")
                    ->where("leads.status", '!=', 'Converted')
                    ->whereNotNull("leads.appointment_date")
                    ->where("leads.stage_id", 1)
                    ->where(function($q) use ($role) {

                        if (\Auth::user()->role == 'Team Lead') {
                            $q->WhereIn("leads.rel_manager_id", function($query)use($role) {
                                $query->select('id')
                                ->from("users")
                                ->where('status', 'active')
                                ->where('reporting_manager', \Auth::user()->id);
                            });
                        }
                    })

                    ->where(function($qry) use ($arrchk,$role){
                        if(!empty($arrchk['fromdate']) || !empty($arrchk['todate'])){
                            if($arrchk['datewise']=='app'){
                                $obj=DB::raw("date(leads.appointment_date)");
                            }elseif($arrchk['datewise']=='tlver'){
                                $obj=DB::raw("date(leads.sha_references.created)");
                            }
                            if((!empty($arrchk['fromdate'])) && (!empty($arrchk['todate']))){
                                $qry=$qry->whereBetween($obj, [date("Y-m-d", strtotime($arrchk['fromdate'])),date("Y-m-d", strtotime($arrchk['todate']))]);
                            } elseif((!empty($arrchk['fromdate'])) && (empty($arrchk['todate']))){
                                $qry=$qry->where($obj,">=" ,[date("Y-m-d", strtotime($arrchk['fromdate']))]);
                            } elseif((empty($arrchk['fromdate'])) && (!empty($arrchk['todate']))){
                                $qry=$qry->where($obj,"<=",[date("Y-m-d", strtotime($arrchk['todate']))]);
                            }
                        }
                      })
                    ->whereNull("leads.customer_id")
                    ->Orwhere("leads.customer_id", '=', 0)
                    ->select("leads.reference_id", "leads.name", "leads.created", "a.name as manager_name", "leads.lead_source", "leads.lead_source_info", "leads.appointment_date", "b.name as tlname")
                    ->paginate(10);

            $data['results'] = $dataunion;
            $data['controller'] = 'tl';
            $data['action'] = 'tlverifier-list-lead';
//            print_r($data['results']);
//            die;
            return view('lms.tllead', $data)->with($request->all());
        }

        public function tleditVerifier(Request $request, $id) {
            $data['call_status'] = VerifierCallStatus::where('id', 1)->get(['id', 'name', 'status'])->toArray();
            $data['call_sub_status'] = VerifierCallSubStatus::where('verifier_call_status_id', 1)->get(['id', 'name', 'verifier_call_status_id', 'status'])->toArray();
            $data['reference'] = Lead::findorFail($id);
            $data['states'] = States::get(['state', 'state_id', 'status'])->toArray();
            $data['cities'] = Cities::get(['city', 'city_id', 'state_id'])->toArray();


            if (!empty($data['reference']->rel_manager_id)) {

                $manager = DB::table("users as a", "leads.rel_manager_id", "=", "a.id")
                        ->join("users as b", "a.reporting_manager", "=", "b.id")
                        ->where("a.id", "=", $data['reference']['rel_manager_id'])
                        ->select("a.name as emp", "b.name as head")
                        ->get();

                $data['manager'] = json_decode(json_encode($manager), true);
            } else {
                $manager = DB::table("users as a")
                        // ->join("$database2.sha_customer_master", "$database2.sha_customer_master.relationship_manager", "=", "a.id")
                        ->join("users as b", "a.reporting_manager", "=", "b.id")
                        //  ->where("$database2.sha_customer_master.customer_id", "=", $data['reference']['customer_id'])
                        ->select("a.name as emp", "b.name as head")
                        ->get();
                $data['manager'] = json_decode(json_encode($manager), true);
            }
            $data['test'] = json_decode(json_encode($data['reference']), true);
            $data['intereaction'] = $this->rmRejectedreason($data['reference']->reference_id);

            $data['rms'] = User::lists('name', 'id');

            $data['controller'] = 'tl';
            $data['action'] = 'tlverifier-list';
            return view('lms/tl-edit-verifier', $data);
        }

        public function tlupdateVerifier(Request $request, Lead $id) {
            $id = $request->id;
            $post = Lead::findorFail($id);
            $post->name = $request->name;
            $post->email = $request->email;
            $post->mobile = $request->mobile;
            $post->state_id = $request->state;
            $post->district_id = $request->city;
            $post->state_name = $request['state_name'];
            $post->city_name = $request['city_name'];
            $post->informed_to_reference = $request->informed;
            $post->emi_size = $request->emi_size;
            $post->appointment_date = date("Y-m-d H:i:s", strtotime($request->appointment_date));
            $post->appointment_time = $request->appointment_time;
            $jsonData = $this->jsondata($request->all());
            $post->verifier = $jsonData;
            $post->stage_id = 2;

            if ($request['call_sub_status'] == 1) {

                $post->stage_remarks = 'accepted';

                $msg = "Thank you for giving your valuable time. We hope you will like our SIP product in Cherish Gold Plan.You will shortly receive a confirmation Via SMS.";

                $this->sendsms($request->mobile, $msg);
            } elseif ($request['call_sub_status'] == 2) {
                echo 'cccbb';
                die();
                $post->stage_remarks = 'rejected';
                $request['verification_interaction'] = $request['verification_interaction'] . ' Appointment:' . $post->appointment_date;
                $post->appointment_date = null;
            }

            $post->save();

            return redirect()->route('tllead-list');
        }

        public function pagination($items, $perPage = 20, Request $request) {
            $dataarray = clone($items);
            $count = $items->select([DB::raw("count(*) as cnt")])->first();
            $currentPage = LengthAwarePaginator::resolveCurrentPage();
            $leave = ($currentPage - 1) * $perPage;
            $currentPageSearchResults = $dataarray->skip($leave)->take($perPage)->get();
            $data = new LengthAwarePaginator($currentPageSearchResults, $count->cnt, $perPage);
            return $data->setPath($request->url());
        }

        public function generateexcel($filename, $data, $header) {
            Excel::create($filename . date("d-m-Y"), function($excel) {
                $excel->sheet('Sheet 1', function($sheet) {
                    
                });
            })->store('xlsx', storage_path('excel'));
            Excel::load('storage/excel/' . $filename . date("d-m-Y") . '.xlsx', function($doc) use ($data, $header, $filename) {
                $string = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
                $sheet = $doc->setActiveSheetIndex(0);
                for ($i = 0; $i < count($header); $i++) {
                    $sheet->setCellValue($string[$i] . '1', $header[$i]);
                }
                $append = 1;
                for ($j = 0; $j < count($data); $j++) {
                    $append = $append + 1;
                    $i = 0;
                    foreach ($data[$j] as $value) {
                        $sheet->setCellValue($string[$i] . $append, $value);
                        $i++;
                    }
                }
                unlink(storage_path('excel/' . $filename . date("d-m-Y") . '.xlsx'));
            })->download('xlsx');
        }

        public function lmsdata(Request $request) {
            $role = Auth::user()->role;
            $arrchk = $request->all();
            return DB::table("leads")
//                            ->leftJoin("$database2.sha_customer_master", "$database2.sha_references.customer_id", '=', "$database2.sha_customer_master.customer_id")->leftJoin(DB::raw("(SELECT reference_id,max(created_at) as tlmxdate FROM verifier_interaction where roles='Team Lead' group by reference_id) tldate"), function($join) use ($database2) {
//                                $join->on("$database2.sha_references.reference_id", '=', 'tldate.reference_id');
//                            })
                            ->leftJoin(DB::raw("(SELECT reference_id,max(id) as idverifier FROM verifier_interaction where roles='Verifier' group by reference_id) mxverifier"), function($join) use ($role) {
                                $join->on("leads.reference_id", '=', 'mxverifier.reference_id');
                            })
                            ->leftJoin("verifier_interaction as interactionverifier", "interactionverifier.id", '=', "mxverifier.idverifier")
                            ->leftJoin("users as verifieruser", "interactionverifier.rm_id", '=', "verifieruser.id")
                            ->leftJoin(DB::raw("(SELECT reference_id,max(id) as idspoc FROM verifier_interaction where roles='SPOC' group by reference_id) mxspoc"), function($join) use ($role) {
                                $join->on("leads.reference_id", '=', 'mxspoc.reference_id');
                            })
                            ->leftJoin("verifier_interaction as interactionspoc", "interactionspoc.id", '=', "mxspoc.idspoc")
                            ->where('stage_id', '>=', 1)
                            ->where(function($qry) use ($arrchk, $role) {
                                if (!empty($arrchk['fromdate']) || !empty($arrchk['todate'])) {
                                    if ($arrchk['datewise'] == 'app') {
                                        $obj = DB::raw("date(leads.appointment_date)");
                                    } elseif ($arrchk['datewise'] == 'tlver') {
                                        $obj = DB::raw("date(tldate.tlmxdate)");
                                    } elseif ($arrchk['datewise'] == 'ver') {
                                        $obj = DB::raw("date(interactionverifier.created_at)");
                                    }
                                    if ((!empty($arrchk['fromdate'])) && (!empty($arrchk['todate']))) {
                                        $qry = $qry->whereBetween($obj, [date("Y-m-d", strtotime($arrchk['fromdate'])), date("Y-m-d", strtotime($arrchk['todate']))]);
                                    } elseif ((!empty($arrchk['fromdate'])) && (empty($arrchk['todate']))) {
                                        $qry = $qry->where($obj, ">=", [date("Y-m-d", strtotime($arrchk['fromdate']))]);
                                    } elseif ((empty($arrchk['fromdate'])) && (!empty($arrchk['todate']))) {
                                        $qry = $qry->where($obj, "<=", [date("Y-m-d", strtotime($arrchk['todate']))]);
                                    }
                                }
                            })
                            ->select("leads.reference_id", "leads.name", DB::raw("DATE_FORMAT(leads.created,'%d-%c-%Y') as created"), DB::raw("DATE_FORMAT(appointment_date,'%d-%c-%Y') as appointdate"), DB::raw("DATE_FORMAT(appointment_date,'%H:%i') as appointtime"), DB::raw("DATE_FORMAT(interactionverifier.created_at,'%d-%c-%Y') as verifierdate"), DB::raw("DATE_FORMAT(interactionverifier.created_at,'%H:%i') as verifiertime"), "interactionverifier.remarks as verifier_remarks", "interactionspoc.remarks", "stage_id", "stage_remarks", "leads.state_id", "leads.district_id", "leads.customer_id", "rel_manager_id", DB::raw("Null as type"), "verifier")
                            ->orderBy("leads.reference_id", 'desc');
//                            ->select("leads.reference_id", "leads.name", DB::raw("DATE_FORMAT(leads.created,'%d-%c-%Y') as created"), DB::raw("DATE_FORMAT(appointment_date,'%d-%c-%Y') as appointdate"), DB::raw("DATE_FORMAT(appointment_date,'%H:%i') as appointtime"), DB::raw("DATE_FORMAT(tldate.tlmxdate,'%d-%c-%Y') as tldate"), DB::raw("DATE_FORMAT(tldate.tlmxdate,'%H:%i') as tltime"), "verifieruser.name as verifiername", DB::raw("DATE_FORMAT(interactionverifier.created_at,'%d-%c-%Y') as verifierdate"), DB::raw("DATE_FORMAT(interactionverifier.created_at,'%H:%i') as verifiertime"), "interactionverifier.remarks as verifier_remarks", "interactionspoc.remarks", "stage_id", "stage_remarks", "leads.state_id", "leads.district_id", "leads.customer_id", "rel_manager_id", "relationship_manager", DB::raw("Null as type"), "verifier")
//                            ->orderBy("leads.reference_id", 'desc');
        }

        public function lmsreport(Request $request) {
            $data['results'] = $this->pagination($this->lmsdata($request), 50, $request);
            $user = User::whereIn('role', ['Manager', 'Assistant Manager', 'Team Lead', 'Tele Caller', 'Verifier Head', 'Verifier', 'Ftl', 'Fo', 'Voc'])->select('name', 'id', 'reporting_manager')
                    ->get();
            $userdata = json_decode(json_encode($user), 1);
            $data['user'] = array_column($userdata, null, 'id');
            $data['controller'] = 'fc';
            $data['action'] = 'lmsreport';
            return view('lms/lms-report', $data)->with($request->all());
        }

        public function lmsreportexport(Request $request) {
            $results = $this->lmsdata($request)->get();
            $data = json_decode(json_encode($results), 1);
            $header = array('ID', 'Name', 'Entry Date', 'Appointment Date', 'Appointment Time', 'TL Verifier Date', "TL Verifier Time",
                "Spoc Remarks", 'Verifier Remarks', 'State', 'City', 'TL Name', "Type", "Status", "Mobile", "Alt mobile", "Email", "Gender", "Age", "Marital Status", "Child", "Address", "Landmark", "Pincode", "Company Name", "Department", "Profession", "Income_group", "Income", "Investment  Question", "Purpose Of Call", "Probability", "Rating", "Ticket Size");
            $filename = 'lmsreport';
            $resultstate = States::where('status', 'Active')
                    ->pluck("state", "state_id");
            $state = json_decode(json_encode($resultstate), 1);
            $resultcity = Cities::where('status', 'Active')
                    ->pluck("city", "city_id");
            $city = json_decode(json_encode($resultcity), 1);
            $resultuser = User::whereIn('role', ['Manager', 'Assistant Manager', 'Team Lead', 'Tele Caller', 'Verifier Head', 'Verifier', 'Ftl', 'Fo', 'Voc'])->select('name', 'id', 'reporting_manager')
                    ->get();
            $userdata = json_decode(json_encode($resultuser), 1);
            $user = array_column($userdata, null, 'id');
            Excel::create($filename . date("d-m-Y"), function($excel) {
                $excel->sheet('Sheet 1', function($sheet) {
                    
                });
            })->store('xlsx', storage_path('excel'));
            Excel::load('storage/excel/' . $filename . date("d-m-Y") . '.xlsx', function($doc) use ($data, $header, $filename, $state, $city, $user) {
                $string = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
                $sheet = $doc->setActiveSheetIndex(0);
                for ($i = 0; $i < count($header); $i++) {
                    if ($i >= 26) {
                        $rpt = ($i % 26);
                        $quo = (int) ($i / 26);
                        $str1 = $string[$quo - 1] . $string[$rpt];
                    } else {
                        $str1 = $string[$i];
                    }
                    $sheet->setCellValue($str1 . '1', $header[$i]);
                }
                $append = 1;

                for ($j = 0; $j < count($data); $j++) {

                    $append = $append + 1;
                    $i = 0;
                    $customer_id = $data[$j]['customer_id'];
                    $arrchange = array_diff_key($data[$j], ['stage_remarks' => 0, 'stage_id' => 0, 'verifier' => 0, 'customer_id' => 0]);
                    $arrchange['stageremarks'] = $data[$j]['stage_id'] . $data[$j]['stage_remarks'];
                    $arrchange['verifier'] = $data[$j]['verifier'];
                    foreach ($arrchange as $key => $value) {
                        $json = [];

                        if ($key == 'verifier') {
                            $check = json_decode($value, 1);
                            if (is_array($check)) {
                                $json = array_diff_key(json_decode($value, 1), ['batch_id' => 0, 'campaign_code' => 0, 'lead_code' => 0, 'name' => 0, 'state' => 0, 'city' => 0, 'call_complaince' => 0, 'recording_statement' => 0]);
                                $jsonincre = $i;
                                foreach ($json as $jsonkey => $jsonarray) {
                                    $jsonvalue = is_array($jsonarray) ? key($jsonarray) : $jsonarray;
                                    if ($jsonincre >= 26) {
                                        $rpt = ($jsonincre % 26);
                                        $quo = (int) ($jsonincre / 26);
                                        $str = $string[$quo - 1] . $string[$rpt];
                                    } else {
                                        $str = $string[$jsonincre];
                                    }
                                    $sheet->setCellValue($str . $append, $jsonvalue);
                                    $jsonincre++;
                                }
                            }
                        } elseif ($key == "state_id") {
                            $statevalue = isset($state[$value]) ? $state[$value] : "";
                            $sheet->setCellValue($string[$i] . $append, $statevalue);
                        } elseif ($key == "district_id") {
                            $cityvalue = isset($city[$value]) ? $city[$value] : "";
                            $sheet->setCellValue($string[$i] . $append, $cityvalue);
                        } elseif ($key == "verifiertime") {
                            $vtime = !empty($value) ? date('H:i', strtotime('+5 hours +30 minutes', strtotime($value))) : "";
                            $sheet->setCellValue($string[$i] . $append, $vtime);
                        } elseif ($key == "rel_manager_id" || $key == "reporting_manager") {

                            $rmname = '-';
                            $tlname = '-';
                            if (empty($customer_id) || $customer_id == 0) {
                                if (isset($user[$arrchange['rel_manager_id']])) {
                                    $rmname = $user[$arrchange['rel_manager_id']]['name'];
                                    if ($user[$arrchange['rel_manager_id']]['reporting_manager']) {
                                        $tlname = $user[$user[$arrchange['rel_manager_id']]['reporting_manager']]['name'];
                                    }
                                }
                            } else {
                                if (isset($user[$arrchange['manager']])) {
                                    $rmname = $user[$arrchange['manager']]['name'];
                                    if ($user[$arrchange['manager']]['manager']) {
                                        $tlname = $user[$user[$arrchange['manager']]['manager']]['name'];
                                    }
                                }
                            }
                            if ($key == "rel_manager_id") {
                                $sheet->setCellValue($string[$i] . $append, $rmname);
                            } elseif ($key == "manager") {
                                $sheet->setCellValue($string[$i] . $append, $tlname);
                            }
                        } elseif ($key == "type") {
                            if (empty($customer_id) || $customer_id == 0) {
                                $type = "Lead";
                            }
                            $sheet->setCellValue($string[$i] . $append, $type);
                        } else {
                            $sheet->setCellValue($string[$i] . $append, $value);
                        }
                        $i++;
                    }
                }
                unlink(storage_path('excel/' . $filename . date("d-m-Y") . '.xlsx'));
            })->download('xlsx');
        }

        public function followupoperation() {
            $crm_database = $this->crm_database;
            $database2 = $this->database2;
            $result = DB::table("$database2.sha_references")
                    ->leftJoin(DB::raw("(SELECT reference_id,max(id) as mxid FROM $crm_database.fo_remarks where status='followup' group by reference_id) mxforemarks"), function($join) use ($database2) {
                        $join->on("$database2.sha_references.reference_id", '=', 'mxforemarks.reference_id');
                    })
                    ->leftJoin("$crm_database.fo_remarks", "fo_remarks.id", '=', "mxforemarks.mxid")
                    ->where('stage_id', '=', 7)
                    ->where('stage_remarks', 'operation')
                    ->where(function($q) {
                        if (Auth::user()->role == "FO") {
                            $q->where('fo_id', Auth::user()->id);
                        } elseif (Auth::user()->role == "FTL") {
                            $q->where('ftl_id', Auth::user()->id);
                        }
                    })
                    ->select("$database2.sha_references.reference_id", "$database2.sha_references.name", "$database2.sha_references.mobile", "verifier", "payment_date")
                    ->orderBy("$crm_database.fo_remarks.payment_date", 'asc')
                    ->get();
            $data['results'] = json_decode(json_encode($result), true);
            $data['controller'] = 'lmsstatus';
            $data['action'] = 'followup';
            return view('lead.followupoperation', $data);
        }

        public function acccoutactiveop(Request $request) {
            $this->validate($request, [
                'lead_id' => 'required'
            ]);
            $id = $request['lead_id'];
            References::where('reference_id', $id)
                    ->update(['stage_id' => 11, 'stage_remarks' => 'activated']);
            $arr = ['id' => $request['lead_id'], 'call_status' => 8, 'call_sub_status' => 29, 'appointment_date' => null, 'verification_interaction' => 'Account Activated'] + $request->all();
            $this->interaction($arr);
            $request->session()->flash('flash_message', 'Lead Activated Successfully!');
            return;
        }

        public function lmsformupdate(Request $request) {
            $this->validate($request, [
                'lead_id' => 'required'
            ]);
            $obj = new LmsForm();
            $obj->lead_id = $request['lead_id'];
            $obj->user_id = Auth::user()->id;
            $obj->role = Auth::user()->role;
            $obj->save();
            References::where('reference_id', $request['lead_id'])
                    ->update(['form_stage_id' => array_search(Auth::user()->role, config('custom.LmsFormStage'))]);
        }

        public function formdataview(Request $request) {
            $crm_database = $this->crm_database;
            $database2 = $this->database2;
            $result = DB::table("$database2.sha_references")
                    ->join("$crm_database.fo_remarks", "$crm_database.fo_remarks.reference_id", "=", "$database2.sha_references.reference_id")
                    ->where(function($q) use($database2) {
                        $q->whereIn("$database2.sha_references.stage_id", [7, 11]);
                        $q->OrwhereIn("$database2.sha_references.stage_remarks", ['saledone', 'activated']);
                    })
                    ->where("$crm_database.fo_remarks.status", 'saledone')
                    //->where("$database2.sha_references.form_stage_id",'<',3)
                    ->where(function($q) {
                        if (Auth::user()->role == "FO") {
                            $q->where('fo_id', Auth::user()->id);
                        } elseif (Auth::user()->role == "FTL") {
                            $q->where('ftl_id', Auth::user()->id);
                        }
                    })
                    ->select("$database2.sha_references.name", "$database2.sha_references.mobile", "$database2.sha_references.email", "$database2.sha_references.reference_id", "$crm_database.fo_remarks.form_no", "$database2.sha_references.form_stage_id")
                    ->get();
            $data['results'] = json_decode(json_encode($result), 1);
            $data['controller'] = "lmsstatus";
            $data['action'] = "lmsformstatus";
            return view('lead.formdata', $data);
        }

        public function sendmailToSpoc() {
            $database2 = $this->database2;
            $result = DB::table("$database2.sha_references")
                    ->where(function($q) {
                        $q->where(function($q1) {
                            $q1->where("stage_id", 4);
                            $q1->where("stage_remarks", 'accepted');
                        });
                        $q->Orwhere(function($q2) {
                            $q2->where("stage_id", 5);
                            $q2->whereIn("stage_remarks", ['assign', 'pending']);
                        });
                        $q->Orwhere(function($q3) {
                            $q3->where("stage_id", 6);
                            $q3->where("stage_remarks", 'accepted');
                        });
                    })
                    ->where(DB::raw("date(appointment_date)"), '<', date('Y-m-d'))
                    ->where(DB::raw("date(appointment_date)"), ">=", '2017-10-26')
                    ->select("sha_references.reference_id", "$database2.sha_references.mobile", "$database2.sha_references.name", "appointment_date", "stage_id", "stage_remarks")
                    ->get();
            $table = '<table border="1" style="text-align:center;padding:10px;"><thead><tr><th>ID</th><th>Name</th><th>Mobile</th><th>Stage</th><tbody>';
            $data = json_decode(json_encode($result), 1);
            if (count($data) > 0) {
                foreach ($data as $lead) {
                    $table .= '<tr><td>' . $lead['reference_id'] . '</td>';
                    $table .= '<td>' . $lead['name'] . '</td>';
                    $table .= '<td>' . $lead['mobile'] . '</td>';
                    $table .= '<td>' . status($lead['stage_id'], $lead['stage_remarks']) . '</td></tr>';
                }
                $table .= '</tbody></table>';
                $content = "<p>Dear Spoc,</p><p>&nbsp; &nbsp; &nbsp; Below following Lead details is still <strong>pending.&nbsp;</strong></p><p><strong>&nbsp; &nbsp; &nbsp;&nbsp;</strong>Please update the status today end of day</p><br>" . $table;
                $email = array();
                //$email['to_mail']="deepakgupta9920@gmail.com";
                $email['to_mail'] = ["faizan.siddique@cherishgold.com", "anil.verma@cherishgold.com"];
                $email['to_name'] = "Faizan Siddique";
                $email['cc'] = ["surender@birlagold.com", "chairman@kserasera.com", "harshupadhyay93@gmail.com", "pasanna@kgrsecurities.com", "sachit@birlagold.com", "faizan.siddique@cherishgold.com"];
                // $email['cc']= ["deepak@birlagold.com"];
                $email['from_email'] = "customerservice@cherishgold.com";
                $email['from_name'] = "Cherishgold";
                $email['subject'] = "Pending lead for Approval";
                $email['messages'] = $content;
                $this->sendemail($email);
                \Log::info(json_encode($data));
            } else {
                \Log::info("No Lead Found of Lead!!!");
            }
        }

        public function sendemail($data) {
            Mail::send(['html' => 'email.layout'], ['content' => $data['messages']], function($message) use($data) {
                $message->to($data['to_mail'], $data['to_name']);
                $message->from($data['from_email'], $data['from_name']);
                $message->subject($data['subject']);
                if (isset($data['cc'])) {
                    $message->cc($data['cc']);
                }
                if (array_key_exists('attachfile', $data)) {
                    foreach ($data['attachfile'] as $file) {
                        $message->attach($file);
                    }
                }
            });
            if (count(Mail::failures()) > 0) {
                \Log::info("Mail Send");
            } else {
                \Log::info("Mail Not");
            }
        }

        public function customerrefdata(Request $request) {
            $id = $request['id'];
            $database2 = $this->database2;
            $data = DB::table("$database2.sha_customer_master")
                    ->join("$database2.sha_customer_plans", "$database2.sha_customer_master.last_plan_id", "=", "$database2.sha_customer_plans.plan_id")
                    ->join("$database2.sha_relationship_manager", "$database2.sha_customer_master.relationship_manager", '=', "$database2.sha_relationship_manager.manager_id")
                    ->where("$database2.sha_customer_master.customer_id", $id)
                    ->select(['applicant_name as name', 'mailing_address as address', 'initial_amount as amount', 'manager_name as rmname'])
                    ->first();
            return json_decode(json_encode($data), 1);
        }

        ////////////// Here Code Strat for assign quality mahima/////////
        public function leadassignqualitylist(Request $request) {
            
            $arrsearch = ['q' => $request['q'], 'option' => $request['option']];
            $role = Auth::user()->role;
            if ($request->isMethod('post')) {
                $this->setsessiondata($request, 'ref_value', 'chk');
            }
            $tl = User::where('bpo_id', Auth::user()->bpo_id)
                    ->where('status', 'active');
            if ($role == 'Quality Head') {
                $tl = $tl->where('reporting_manager', Auth::user()->id);
            } elseif ($role == 'Quality') {
                $tl = $tl->where('id', Auth::user()->id);
            }
//            $tl = $tl->where('role', 'Team Lead')
//                    ->pluck('id');
            $user = DB::table('users as a1')
                    ->join('users as a2', 'a1.reporting_manager', '=', 'a2.id')
                    ->where('a1.role', 'Quality')
                    ->orWhere('a1.role', '=', 'Quality Head')
                    ->where('a1.bpo_id', Auth::user()->bpo_id)
                    ->where('a1.status', 'active')
                    ->select('a1.name as tellcallername', 'a1.id as tellcallerid', 'a2.name as tlname')
                    ->get();
            $data['reporting'] = json_decode(json_encode($user), 1);
            $result = DB::table("leads")
                    ->join("users", "id", '=', 'rel_manager_id')
                    ->where("leads.status", 'In progress')
                    ->where("leads.bpo_id", Auth::user()->bpo_id)
                    ->where(function ($query) use ($arrsearch) {
                        if (!empty($arrsearch['option'])) {
                            $search = $arrsearch['q'];
                            if ($arrsearch['option'] == 'ID')
                                $query = $query->where('leads.reference_id', '=', "$search");
                            else if ($arrsearch['option'] == 'Name')
                                $query = $query->where('leads.name', 'LIKE', "%$search%");
                            else if ($arrsearch['option'] == 'Mobile')
                                $query = $query->where('leads.mobile', 'LIKE', "%$search%");
                            else if ($arrsearch['option'] == 'Email')
                                $query = $query->where('leads.email', 'LIKE', "%$search%");
                        }
                    })
                    ->select("leads.name as leadname", "leads.mobile as leadmobile", "leads.email as leademail", "leads.rel_manager_id as rel_manager_id", "leads.reference_id as leadid", "users.name as username", "leads.quality_id as qualityname")
                    ->paginate(5);

//                    echo "<pre>";
//                    print_r($result);
//                    echo "</pre>";
//                    die('mc,nmnbnmbc');
            $data['controller'] = "fc";
            $data['action'] = 'assignquality';
            $data['leadsourcevalue'] = $result;
            return view('assignquality/assign-quality', $data)->with($arrsearch);
        }

        public function Allassignquality(Request $request) {
            $lead_id = $request['lead_id'];
            $rm_id = $request['rm_id'];
            return Lead::where(['reference_id' => $lead_id])
                            ->update(['quality_id' => $rm_id]);
        }

        //////////End Code Here //////////
    }
    