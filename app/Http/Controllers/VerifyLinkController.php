<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\VerifyLink;
use Auth;

class VerifyLinkController extends Controller
{
	public function index(){

		$bpo_id=Auth::user()->bpo_id;
		$db_data = VerifyLink::where('bpo_id',$bpo_id)
						  ->orderBy('id','desc')
						  ->paginate(10);
		$data['result'] = $db_data;
		$data['controller'] = "verifylink";
		$data['action'] = "linkverification";
		return view('verifylink.verifylink',$data);
	}

	public function masklink(Request $request){
		$this->validate($request, [    
            'period' => 'required'
        ]);
		$period = $request['period'];
		$obj = new VerifyLink();
		$obj->bpo_id = Auth::user()->bpo_id;
		$obj->fromdate = date('Y-m-d');
		$obj->todate  = date("Y-m-d",strtotime("+".$period." days",strtotime(date('Y-m-d'))));
		$obj->save();
		return redirect()->back()->with(['message'=>'New Link Created Successfully!!!']);
	}
}
