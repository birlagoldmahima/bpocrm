<?php
namespace App\Http\Controllers;
use App\User;
use App\Lead;
use Illuminate\Http\Request;
use Auth;
use DB;

class UnassignedLeadController extends Controller{

	public function unassignedleadList(Request $request){
        
		$data['controller'] = "unassignedlead";
		$data['action'] = "unassignedlead-list";
		$arrsearch=array('search_option'=>$request['search_option'],'q'=>$request['q']);
        $this->setsessiondata($request,'ref_value','chkbox');
		$role = Auth::user()->role;
		$tl = User::where('bpo_id',Auth::user()->bpo_id)
                  ->where('status','active');
		if($role=='Assistant Manager'){
			$tl = $tl->where('reporting_manager',Auth::user()->id);
		}elseif($role=='Team Lead'){
			$tl = $tl->where('id',Auth::user()->id);
		}
		$tl = $tl->where('role','Team Lead')
				 ->pluck('id');
        
        
        $user = DB::table('users as a1')
                  ->join('users as a2','a1.reporting_manager','=','a2.id')
                  ->where('a1.role','Tele Caller')
                  ->where('a1.bpo_id',Auth::user()->bpo_id)
                  ->whereIn('a1.reporting_manager',$tl)
                  ->where('a1.status','active')
                  ->select('a1.name as tellcallername','a1.id as tellcallerid','a2.name as tlname')
                  ->get();
		$data['reporting'] = json_decode(json_encode($user),1);
        $result = Lead::where(function($q){
								$q->whereNull('rel_manager_id');
								$q->Orwhere('rel_manager_id',0);
							})
							->where('bpo_id',Auth::user()->bpo_id)
							->where('status','In Progress')
							->where(function ($query) use ($arrsearch){
            				    if($arrsearch['search_option']=='name')
                					$query=$query->where('name','LIKE', "%$search%");
                				else if($arrsearch['search_option']=='mobile')
                					$query=$query->where('mobile','LIKE', "%$search%");
                				else if($arrsearch['search_option']=='email')
                					$query=$query->where('email','LIKE', "%$search%");
                			    else if($arrsearch['search_option']=='lead_source')
                					$query=$query->where('lead_source','LIKE', "%$search%");
                			    else if($arrsearch['search_option']=='emi_size')
                					$query=$query->where('emi_size','LIKE', "%$search%");
                				else if($arrsearch['search_option']=='id')
                					$query=$query->where('reference_id','=', "$search");
            				})
              ->orderBy('reference_id','desc')
							->paginate(5);
		$data['result'] = $result;
		return view('unassignedlead/unassignedlead-list',$data)->with($arrsearch);
	}

	public function unassignedleadAssign(Request $request){
		$this->setsessiondata($request,'ref_value','chkbox');
		$ref_id=$request->session()->get('ref_value');
		$rm_id=$request->relationship_manager;
        if((count($ref_id)>0) && ($rm_id!="")){
        	$lead_id = array_reduce($ref_id, 'array_merge', array());
            Lead::whereIn('reference_id',$lead_id)
                ->update(['rel_manager_id'=>$rm_id]);
			$request->session()->flash('flash_message', 'Lead Assigned Successfully!!!');
         	$request->session()->flash('flash_alert','alert-success');
      	}else{
     		$request->session()->flash('flash_message', 'Something went wrong, Try again!!');
         	$request->session()->flash('flash_alert','alert-danger');
      	}
      	return redirect()->route('unassigned-lead');
  	}
}