<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\LeadSms;
use DB;
use Mbarwick83\Shorty\Facades\Shorty;

class LeadSmsController extends Controller
{
	public function sendsms($mobile,$message){
        $sms_site=env('SMS_SITE');
        $sms_method=env('SMS_METHOD');
        $sms_api_key=env('SMS_API_KEY');
        $sms_sender_id=env('SMS_SENDER_ID');
        $sendsms = new SendSmsController($sms_site, $sms_method,$sms_api_key,$sms_sender_id);
        $sendsms->send_sms($mobile,$message,'','xml');
    }

    public function msgBeforeTwoHour(){
    	return "Dear Customer,in regards with cherish gold plan our manager is on the way as per ur appointment at {time}. For queries call on 8451048705-Coordinator";
    }

    public function msgAfterMeeting(){
    	return "Dear Customer , you have requested for SIP in gold , has the Relationship Manager arrived ? Tell us here {link} if Not call on 7738199991";
    }

    public function sentSmsbeforeTwoHour(){

    	$result = DB::table('lead_sms')	
			  	     ->join(DB::raw("(SELECT max(id) as mxid FROM lead_sms group by lead_id) mxtable"),function($join){
                				$join->on("lead_sms.id",'=','mxtable.mxid');
              	    })
			  	    ->where(DB::raw("time_to_sec(timediff(DATE_FORMAT(appointment_date,'%Y-%m-%d %H:%i'),DATE_FORMAT((now() + INTERVAL 330 minute),'%Y-%m-%d %H:%i')))"),7200)
    				->where("sendflag",'no')
    				->select("id","mobile","appointment_date")
    				->get();
    	$data = json_decode(json_encode($result),1);
		
    	$arr = [];
	
    	$msg =$this->msgBeforeTwoHour();
    	foreach ($data as $key=>$value) {
    		$appointmenttime = date('h:i A',strtotime($value['appointment_date']));
            $message = str_replace('{time}',$appointmenttime,$msg);
    		$this->sendsms($value['mobile'],$message);
    		$arr[] = $value['id'] ;
    	}
    	 if(count($arr)>0){
    		 LeadSms::whereIn('id',$arr)
    			  ->update(['sendflag'=>'yes']);
		 }
    }

    public function afterMeetingFifteen(){
    	$result = DB::table('lead_sms')	
			  	     ->join(DB::raw("(SELECT max(id) as mxid FROM lead_sms group by lead_id) mxtable"),function($join){
                				$join->on("lead_sms.id",'=','mxtable.mxid');
              	    })
			  	    //->where(DB::raw("time_to_sec(timediff(DATE_FORMAT(now(),'%Y-%m-%d %H:%i'),DATE_FORMAT(appointment_date,'%Y-%m-%d %H:%i')))"),900)
    				->where("aftermeeting",'no')
    				->select("id","mobile","appointment_date")
    				->get();
    	$data = json_decode(json_encode($result),1);
    	$arr = [];
    	$msg =$this->msgAfterMeeting();
    	foreach ($data as $key=>$value) {
    		$url=config('custom.weburl').'crm/lead-response?id='.base64_encode($value['id']);
    		$smslink=$url;//Shorty::shorten($url);
    		$message = str_replace('{link}',$smslink,$msg);
    		var_dump($message);
    		dd();
    		$this->sendsms($value['mobile'],$message);
    		$arr[] = $value['id'] ;
    	}
    	if(count($arr)>0){
    		LeadSms::whereIn('id',$arr)
    			   ->update(['aftermeeting'=>'yes']);
		}
    }

    public function responseLead(Request $request){
    	$url_id = $request['id'];
    	if(!empty($url_id)){
    		$id = base64_decode($url_id);
    	}
    }



}
