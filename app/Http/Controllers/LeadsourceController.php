<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Validator;
use Auth;
use DB;
use App\Leadsource;

class LeadsourceController extends Controller {

    public function lead_source() {

        $leadsourcevalue = DB::table('lead_source')
                ->select("leadsource_id", "leadsource_name", "leadsource_bpo_id")
                ->get();
        $data['leadsourcelist'] = json_decode(json_encode($leadsourcevalue), 1);
        $data['controller'] = "leadsource";
        $data['action'] = "sourceadd";
        return view('lead/lead_source_create', $data);
    }

    public function edit_leadsource(Request $request, $leadsource_id) {
        $leadsourcevalue = DB::table('leads')
                ->where('lead_source', '=', $leadsource_id)
                ->select("reference_id", "lead_source", "bpo_id")
                ->get();

        if (!empty($leadsourcevalue)) {
            return redirect()->back()->with('status', 'Already Used!!');
        } else {

            $editvalue = DB::table('lead_source')
                    ->where('leadsource_id', '=', $leadsource_id)
                    ->select("leadsource_id", "leadsource_name", "leadsource_bpo_id")
                    ->get();
            $usersdata = json_decode(json_encode($editvalue), 1);
            $controller = "leadsource";
        $action = "sourceadd";
            return view('lead/lead_source_edit', compact('usersdata','controller','action'));
        }
    }

    public function edit_leadsourcedata(Request $request, Leadsource $leadsource_id)
    {
        $this->validate($request, [    
            'leadsourcename' => 'required'           
        ]);
     
        $leadsource_id = $request->leadsource_id;

        $post = Leadsource::findorFail($leadsource_id);
        $post->leadsource_name = $request->leadsourcename;
       
        $post->save();
        
        return redirect('lead_source')->withMessage('status', 'User updated successfuly!');
    }

    
    
    //////////////

    public function add_lead_source(Request $request) {
        $rules['leadsourcename'] = 'required';
        $messages['required'] = '1';
        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            $error = json_decode(json_encode($validator->errors()), 1);
            if (isset($error['leadsourcename'])) {
                $error = "Required parameter missing!!!";
            }
            return ['status' => 0, 'msg' => $error];
        }
        $leadsourceObj = new Leadsource();
        $leadsourceObj->leadsource_name = $request['leadsourcename'];
        $leadsourceObj->leadsource_bpo_id = Auth::user()->bpo_id;
        $leadsourceObj->save();
        $getdata = json_decode(json_encode($leadsourceObj), 1);
        $request->session()->flash('flash_message', 'Lead Source  successfully added!');
        return redirect()->route('lead_source');
    }

//
}
