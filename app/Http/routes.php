<?php

    Route::get('/', [
        'uses' => 'LoginController@login',
        'as' => 'login'
    ]);

    Route::get('/login', [
        'uses' => 'LoginController@login',
        'as' => 'login'
    ]);

    Route::post('/login', [
        'uses' => 'LoginController@userLogin',
        'as' => 'postlogin'
    ]);

    Route::get('logout', [
        'uses' => 'LoginController@logout',
        'as' => 'logout'
    ]);

    Route::get('/dashboard', [
        'uses' => 'DashboardController@dashboard',
        'as' => 'dashboard',
        'middleware' => 'auth'
    ]);

    Route::get('/call_backdetail/{id}', [
        'uses' => 'DashboardController@callback_detail',
        'as' => 'call_backdetail',
        'middleware' => 'auth'
    ]);


    Route::get('/check', [
        'uses' => 'UsersController@index',
        'as' => 'check'
    ]);

    Route::any('lead-list/{id?}', [
        'uses' => 'LeadController@leadList',
        'as' => 'lead-list',
        'middleware' => 'auth'
    ]);

    Route::get('add_user_form', [
        'uses' => 'UsersController@add_user_form',
        'as' => 'add_user_form',
        'middleware' => 'auth'
    ]);

    Route::post('add_user', [
        'uses' => 'UsersController@add_user',
        'as' => 'add_user',
        'middleware' => 'auth'
    ]);

    Route::get('edit_user/{id}', [
        'uses' => 'UsersController@edit_user',
        'as' => 'edit_user',
        'middleware' => 'auth'
    ]);

    Route::get('user-list', [
        'uses' => 'UsersController@index',
        'as' => 'userlist',
        'middleware' => 'auth'
    ]);

    Route::post('update_user', [
        'uses' => 'UsersController@update_user',
        'as' => 'update_user',
        'middleware' => 'auth'
    ]);

    Route::get('user_report', [
        'uses' => 'UsersController@userlist_Download',
        'as' => 'user_report',
        'middleware' => 'auth'
    ]);

    Route::post('updateuserstatus', [
        'uses' => 'UsersController@updateuserstatus',
        'as' => 'updateuserstatus',
        'middleware' => 'auth'
    ]);

    Route::post('reset-pass', [
        'uses' => 'UsersController@resetpasswd',
        'as' => 'resetpass',
        'middleware' => 'auth'
    ]);


    Route::post('leadrmchangesingle', [
        'uses' => 'LeadController@leadrmchangesingle',
        'as' => 'leadrmchangesingle',
        'middleware' => 'auth'
    ]);




    Route::post('/post-reference-interaction', [
        'uses' => 'LeadController@postInteraction',
        'as' => 'post-reference-interaction',
        'middleware' => 'auth'
    ]);

    Route::get('/lead', [
        'uses' => 'LeadController@index',
        'as' => 'lead',
        'middleware' => 'auth'
    ]);

    Route::get('/lead_source', [
        'uses' => 'LeadsourceController@lead_source',
        'as' => 'lead_source',
        'middleware' => 'auth'
    ]);


    Route::post('add_lead_source', [
        'uses' => 'LeadsourceController@add_lead_source',
        'as' => 'add_lead_source',
        'middleware' => 'auth'
    ]);

    Route::get('/edit_leadsource/{leadsource_id}', [
        'uses' => 'LeadsourceController@edit_leadsource',
        'as' => 'edit_leadsource',
        'middleware' => 'auth'
    ]);

    Route::post('/edit_leadsourcedata', [
        'uses' => 'LeadsourceController@edit_leadsourcedata',
        'as' => 'edit_leadsourcedata',
        'middleware' => 'auth'
    ]);



    Route::post('/lead-create', [
        'uses' => 'LeadController@leadCreate',
        'as' => 'lead-create',
        'middleware' => 'auth'
    ]);

    Route::get('/Searchlead-create', [
        'uses' => 'LeadController@SearchleadCreate',
        'as' => 'Searchlead-create',
        'middleware' => 'auth'
    ]);


    Route::get('lead-list', [
        'uses' => 'LeadController@leadList',
        'as' => 'lead_list',
        'middleware' => 'auth'
    ]);
///////////////// Here Code Start Lms Part/////////////

    Route::get('tllead-list', [
        'uses' => 'FieldController@tlverifierList',
        'as' => 'tllead-list',
        'middleware' => 'auth'
    ]);

    Route::any('tl-edit-verifier/{id?}', [
        'uses' => 'FieldController@tleditVerifier',
        'as' => 'tl-edit-verifier',
        'middleware' => 'auth'
    ]);

    Route::any('tl-update-verifier/{id?}', [
        'uses' => 'FieldController@tlupdateVerifier',
        'as' => 'tl-update-verifier',
        'middleware' => 'auth'
    ]);

    Route::get('verifier-list/{id?}', [
        'uses' => 'FieldController@verifierList',
        'as' => 'verifier-list',
        'middleware' => 'auth'
    ]);

    Route::get('assign-quality', [
        'uses' => 'FieldController@assignQuality',
        'as' => 'assign-quality',
        'middleware' => 'auth'
    ]);
    Route::get('quality-list', [
        'uses' => 'FieldController@qualityActionlist',
        'as' => 'quality-list',
        'middleware' => 'auth'
    ]);

    Route::get('quality-view/{id}', [
        'uses' => 'FieldController@qualityActionview',
        'as' => 'quality-view',
        'middleware' => 'auth'
    ]);

    Route::post('update-quality', [
        'uses' => 'FieldController@updateQuality',
        'as' => 'update-quality',
        'middleware' => 'auth'
    ]);

    Route::post('/set-city-state', [
        'uses' => 'LeadController@statecity',
        'as' => 'set-city-state',
        'middleware' => 'auth'
    ]);

    Route::get('spok-list', [
        'uses' => 'FieldController@spoklist',
        'as' => 'spok-list',
        'middleware' => 'auth'
    ]);

    Route::get('spok-view/{id}', [
        'uses' => 'FieldController@spokview',
        'as' => 'spok-view',
        'middleware' => 'auth'
    ]);


    Route::get('spok-report-export', [
        'uses' => 'FieldController@spocreportexport',
        'as' => 'spok-report-export',
        'middleware' => 'auth'
    ]);

    Route::post('spok-update', [
        'uses' => 'FieldController@spokviewupdate',
        'as' => 'spok-update',
        'middleware' => 'auth'
    ]);

    /* LMS FTL Routes */
    Route::get('lms/ftl-list', [
        'uses' => 'FieldController@ftllist',
        'as' => 'ftl-list',
        'middleware' => 'auth'
    ]);

    Route::get('lms/ftl-assigned-list', [
        'uses' => 'FieldController@ftlassignedlist',
        'as' => 'ftl-assigned-list',
        'middleware' => 'auth'
    ]);

    Route::get('ftl-assign-list', [
        'uses' => 'FieldController@ftassignlist',
        'as' => 'ftl-assign-list',
        'middleware' => 'auth'
    ]);

    Route::any('/assign-field', [
        'uses' => 'FieldController@assignField',
        'as' => 'assign-field',
        'middleware' => 'auth'
    ]);

    Route::post('assign-fo', [
        'uses' => 'FieldController@assighfo',
        'as' => 'assign-fo',
        'middleware' => 'auth'
    ]);

    Route::get('lms/fo-list', [
        'uses' => 'FieldController@folist',
        'as' => 'fo-list',
        'middleware' => 'auth'
    ]);

    Route::post('fo-update', [
        'uses' => 'FieldController@foupdate',
        'as' => 'fo-update',
        'middleware' => 'auth'
    ]);

    Route::get('lms/fo-meeting-list', [
        'uses' => 'FieldController@fomeetinglist',
        'as' => 'fo-meeting-list',
        'middleware' => 'auth'
    ]);

    Route::get('fo-meeting-details/{id}', [
        'uses' => 'FieldController@fomeetingdetails',
        'as' => 'fo-meeting-details',
        'middleware' => 'auth'
    ]);

    Route::post('fo-meeting-update', [
        'uses' => 'FieldController@fomeetingupdate',
        'as' => 'fo-meeting-update',
        'middleware' => 'auth'
    ]);

    Route::get('rm-rejected-reason/{id}', [
        'uses' => 'FieldController@rmRejectedreason',
        'as' => 'rm-rejected-reason',
        'middleware' => 'auth'
    ]);
    /* LMS FTL Routes end */

    /* LMS VOC Routes */
    Route::get('voc-excel', [
        'uses' => 'FieldController@vocexcel',
        'as' => 'voc-excel',
        'middleware' => 'auth'
    ]);

    Route::get('voc-list', [
        'uses' => 'FieldController@voclist',
        'as' => 'voc-list',
        'middleware' => 'auth'
    ]);

    Route::post('voc-update', [
        'uses' => 'FieldController@vocupdate',
        'as' => 'voc-update',
        'middleware' => 'auth'
    ]);

    Route::get('voc-notinterested', [
        'uses' => 'FieldController@notintestedvoc',
        'as' => 'voc-notinterested',
        'middleware' => 'auth'
    ]);
    /* LMS VOC Routes end */
    Route::get('rm-rejected-list', [
        'uses' => 'FieldController@rmRejectedlist',
        'as' => 'rm-rejected-list',
        'middleware' => 'auth'
    ]);

    Route::get('rm-accept-list', [
        'uses' => 'FieldController@rmAcceptlist',
        'as' => 'rm-accept-list',
        'middleware' => 'auth'
    ]);


    Route::get('fo-ftl-sale', [
        'uses' => 'FieldController@fofltsale',
        'as' => 'fo-ftl-sale',
        'middleware' => 'auth'
    ]);

    Route::any('lms-report', [
        'uses' => 'FieldController@lmsreport',
        'as' => 'lms-report',
        'middleware' => 'auth'
    ]);

    Route::get('spok-list', [
        'uses' => 'FieldController@spoklist',
        'as' => 'spok-list',
        'middleware' => 'auth'
    ]);

    Route::get('lms-report-excel', [
        'uses' => 'FieldController@lmsreportexport',
        'as' => 'lms-report-excel',
        'middleware' => 'auth'
    ]);


    Route::get('leadQualityList', [
        'uses' => 'FieldController@leadassignqualitylist',
        'as' => 'leadQualityList',
        'middleware' => 'auth'
    ]); 
        Route::post('changeassignquality', [
        'uses' => 'FieldController@Allassignquality',
        'as' => 'changeassignquality',
        'middleware' => 'auth'
    ]);
//////////////////  Here Code End //////////////////



    Route::post('lead-edit', [
        'uses' => 'LeadController@leadedit',
        'as' => 'leadedit',
        'middleware' => 'auth'
    ]);

    Route::get('edit-lead/{reference_id}', [
        'uses' => 'LeadController@edit_lead_list',
        'as' => 'edit-lead',
        'middleware' => 'auth'
    ]);

    Route::get('lead-details/{id}', [
        'uses' => 'LeadController@leadDetails',
        'as' => 'lead-details',
        'middleware' => 'auth'
    ]);


    Route::get('lead-list/{id?}', [
        'uses' => 'LeadController@leadList',
        'as' => 'lead-list',
        'middleware' => 'auth'
    ]);

    Route::get('/fix-appointment', [
        'uses' => 'LeadController@fixAppointment',
        'as' => 'fix-appointment',
        'middleware' => 'auth'
    ]);

    Route::post('/getcitylist', [
        'uses' => 'LeadController@citydetailskeyval',
        'as' => 'getcitylist',
        'middleware' => 'auth'
    ]);
    Route::get('/leadinteractionreport', [
        'uses' => 'LeadController@lead_interaction_report',
        'as' => 'leadinteractionreport',
        'middleware' => 'auth'
    ]);

    Route::get('/leadinteractionreport', [
        'uses' => 'LeadController@lead_interaction_report',
        'as' => 'leadinteractionreport',
        'middleware' => 'auth'
    ]);

    Route::get('/userprofile', [
        'uses' => 'UsersController@user_profile',
        'as' => 'userprofile',
        'middleware' => 'auth'
    ]);

    Route::get('/call_backdetail/{id}', [
        'uses' => 'DashboardController@callback_detail',
        'as' => 'call_backdetail',
        'middleware' => 'auth'
    ]);


    Route::get('/call_backdetail', [
        'uses' => 'DashboardController@callback_detail',
        'as' => 'call_backdetail',
        'middleware' => 'auth'
    ]);

    Route::get('/reset-password', [
        'uses' => 'LoginController@resetpassword',
        'as' => 'reset-password'
    ]);

    Route::post('/reset-password', [
        'uses' => 'LoginController@passwordreset',
        'as' => 'reset-password-post'
    ]);

    Route::post('/get-reporting', [
        'uses' => 'UsersController@getreporting',
        'as' => 'get-reporting',
        'middleware' => 'auth'
    ]);


    Route::post('/username-check', [
        'uses' => 'UsersController@usernamecheck',
        'as' => 'username-check',
        'middleware' => 'auth'
    ]);

    Route::any('/unassigned-lead', [
        'uses' => 'UnassignedLeadController@unassignedleadList',
        'as' => 'unassigned-lead',
        'middleware' => 'auth'
    ]);

    Route::post('/unassigned-lead-assign', [
        'uses' => 'UnassignedLeadController@unassignedleadAssign',
        'as' => 'unassigned-lead-assign',
        'middleware' => 'auth'
    ]);


    Route::get('/call_backdetail', [
        'uses' => 'DashboardController@callback_detail',
        'as' => 'call_backdetail',
        'middleware' => 'auth'
    ]);


    Route::get('/leadlist-report', [
        'uses' => 'ReportController@leadlistreportdata',
        'as' => 'leadlist_report',
        'middleware' => 'auth'
    ]);

    Route::get('/lead-report-export', [
        'uses' => 'ReportController@leadreportexport',
        'as' => 'lead-report-export',
        'middleware' => 'auth'
    ]);

    Route::get('/generate-link', [
        'uses' => 'LeadController@generatelink',
        'as' => 'generate-link',
        'middleware' => 'auth'
    ]);

    Route::get('/check-mobile', [
        'uses' => 'LeadController@checkemailmobile',
        'as' => 'check-mobile',
        'middleware' => 'auth'
    ]);

    Route::get('/lead-request/{id}', [
        'uses' => 'LeadController@changerequestref',
        'as' => 'leadrequest',
        'middleware' => 'auth'
    ]);

    Route::post('changerequest', [
        'uses' => 'LeadController@updaterequestref',
        'as' => 'updatechangerequest',
        'middleware' => 'auth'
    ]);


    Route::get('get-city-district-pincode', [
        'uses' => 'LeadController@getcitydistrictpincode',
        'as' => 'citydistrictpincode',
        'middleware' => 'auth'
    ]);


    Route::get('get-pincode-list', [
        'uses' => 'LeadController@getpincodelist',
        'as' => 'pincodelist',
        'middleware' => 'auth'
    ]);

    Route::get('get-district-list', [
        'uses' => 'LeadController@getdistrictlist',
        'as' => 'districtlist',
        'middleware' => 'auth'
    ]);

    Route::get('get-district-list', [
        'uses' => 'LeadController@getdistrictlist',
        'as' => 'districtlist',
        'middleware' => 'auth'
    ]);

    Route::get('check-login-email', [
        'uses' => 'LeadController@checkemail',
        'as' => 'checkloginemail',
        'middleware' => 'auth'
    ]);

    Route::get('send-payment-link', [
        'uses' => 'LeadController@sendlinkpayment',
        'as' => 'sendpaymentlink',
        'middleware' => 'auth'
    ]);

    Route::get('send-registeration-link', [
        'uses' => 'LeadController@sendregisterationlink',
        'as' => 'sendregisterationlink',
        'middleware' => 'auth'
    ]);

    Route::get('lead-requestreceive', [
        'uses' => 'LeadController@leadreceiveleadList',
        'as' => 'lead-requestreceive',
        'middleware' => 'auth'
    ]);

    Route::get('lead-convertedlist', [
        'uses' => 'LeadController@leadConvertedList',
        'as' => 'lead-convertedlist',
        'middleware' => 'auth'
    ]);

    Route::any('lead-change-rm', [
        'uses' => 'LeadController@leadchangermlist',
        'as' => 'leadchangerm',
        'middleware' => 'auth'
    ]);

    Route::post('change-rm', [
        'uses' => 'LeadController@leadchangerm',
        'as' => 'changerm',
        'middleware' => 'auth'
    ]);

    Route::get('converted-list', [
        'uses' => 'ReportController@convertedlead',
        'as' => 'convertedlist',
        'middleware' => 'auth'
    ]);

    Route::get('converted-list-export', [
        'uses' => 'ReportController@convertedexport',
        'as' => 'convertedlistexport',
        'middleware' => 'auth'
    ]);

    Route::get('request-list', [
        'uses' => 'ReportController@requestlead',
        'as' => 'requestlist',
        'middleware' => 'auth'
    ]);

    Route::get('request-list-export', [
        'uses' => 'ReportController@requestexport',
        'as' => 'requestlistexport',
        'middleware' => 'auth'
    ]);

    Route::get('verification-list', [
        'uses' => 'ReportController@verificationlead',
        'as' => 'verificationlist',
        'middleware' => 'auth'
    ]);

    Route::get('verification-list-export', [
        'uses' => 'ReportController@verificationexport',
        'as' => 'verificationlistexport',
        'middleware' => 'auth'
    ]);

    Route::get('verify-link', [
        'uses' => 'VerifyLinkController@index',
        'as' => 'verifylink',
        'middleware' => 'auth'
    ]);



    Route::post('leadrmchangesingle', [
        'uses' => 'LeadController@leadrmchangesingle',
        'as' => 'leadrmchangesingle',
        'middleware' => 'auth'
    ]);

    Route::get('not-interested-list/', [
        'uses' => 'LeadController@leadinterestedlist',
        'as' => 'not-interestedlist',
        'middleware' => 'auth'
    ]);



    Route::post('update-interested-list', [
        'uses' => 'LeadController@updateinterested',
        'as' => 'update-interested-list',
        'middleware' => 'auth'
    ]);

    Route::post('mask-link', [
        'uses' => 'VerifyLinkController@masklink',
        'as' => 'masklink',
        'middleware' => 'auth'
    ]);

    Route::post('mark-not-intersted', [
        'uses' => 'LeadController@marknotinterested',
        'as' => 'marknotintersted',
        'middleware' => 'auth'
    ]);

    Route::get('notinterested-list', [
        'uses' => 'ReportController@notinterestedreport',
        'as' => 'notinterestedlist',
        'middleware' => 'auth'
    ]);

    Route::get('notinterested-list-export', [
        'uses' => 'ReportController@notinterestedexport',
        'as' => 'notinterestedlistexport',
        'middleware' => 'auth'
    ]);


    Route::get('/email-template', [
        'uses' => 'EmailTemplateController@index',
        'as' => 'email-template',
        'middleware' => 'auth'
    ]);

    Route::post('/addemail-template', [
        'uses' => 'EmailTemplateController@addemailtemplate',
        'as' => 'addemail-template',
        'middleware' => 'auth'
    ]);

    Route::get('/emailtemplates-list', [
        'uses' => 'EmailTemplateController@emailtemplatelists',
        'as' => 'emailtemplates-list',
        'middleware' => 'auth'
    ]);

    Route::get('/email-template-edit/{id}', [
        'uses' => 'EmailTemplateController@emailtemplateEdit',
        'as' => 'email-template-edit',
        'middleware' => 'auth'
    ]);

    Route::post('/email-template-update', [
        'uses' => 'EmailTemplateController@emailtemplateUpdate',
        'as' => 'email-template-update',
        'middleware' => 'auth'
    ]);

    Route::get('/email-template-status/{id}', [
        'uses' => 'EmailTemplateController@emailtemplatestatus',
        'as' => 'email-template-status',
        'middleware' => 'auth'
    ]);

    Route::get('/email-content', [
        'uses' => 'EmailTemplateController@emailcontent',
        'as' => 'emailcontent',
        'middleware' => 'auth'
    ]);

    Route::get('/change-reporting', [
        'uses' => 'UsersController@changereporting',
        'as' => 'changereporting',
        'middleware' => 'auth'
    ]);

    Route::get('/call-back', [
        'uses' => 'DashboardController@callBackData',
        'as' => 'callback',
        'middleware' => 'auth'
    ]);






    /* Route::get('/create/{parameter}', [
      'uses' => 'LeadController@geturl',
      'as' => 'create-link'
      ]);

      Route::get('/register', [
      'uses' => 'RegisterController@register',
      'as' => 'register'
      ]);

      Route::get('/check-url', [
      'uses' => 'RegisterController@checkurl',
      'as' => 'check-url'
      ]);
     */


    Route::group(['prefix' => 'api'], function () {
    Route::post('/callStatusAndSubstatus','DialerApiController@CallStatusSubStatus')->middleware('authkey');
    Route::post('/state','DialerApiController@getstate')->middleware('authkey');
    Route::post('/city/{id}','DialerApiController@getcity')->middleware('authkey');
    Route::post('/addlead','DialerApiController@addlead')->middleware('authkey');
    Route::post('/followup','DialerApiController@followUp')->middleware('authkey');    
    Route::get('/check', function() {
        dd(Auth::user());
    });
    
});
    Route::get('/check', function() {
        dd(Auth::user());
    });

    





