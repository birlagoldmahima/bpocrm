<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Leadsource extends Authenticatable {

    public $table = "lead_source";
    protected $fillable = [
        'leadsource_id', 'leadsource_bpo_id',
    ];
    public $timestamps = false;
    public $primaryKey = 'leadsource_id';

}
