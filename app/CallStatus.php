<?php

namespace App;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class CallStatus extends Model{
     
     protected $connection = 'mysql';
     protected $primaryKey = 'status_id';
     public $table="call_status";
}
     
     
?>