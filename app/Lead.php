<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Lead extends Authenticatable {

    public $table = "leads";
    protected $fillable = [
        'reference_id', 'bpo_id',
    ];
    public $timestamps = false;
    public $primaryKey = "reference_id";
}
