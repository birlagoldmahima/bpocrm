<?php 
	function CallStatusSubStatus($key=""){
		$data = getdata()->CallStatusSubStatus();
		if(isset($data['data'])){
			if($key==""){
				return $data;
			}elseif(isset($data[$key])){
				return $data[$key];
			}
		}
		return [];
	}

	function Bpodata($id){
		$bpodata = getdata()->Bpodata($id);
		$bpodata = getdata()->State();
		if(isset($bpodata['data']['bpo'])){
            return $bpodata['data']['bpo'];
        }
        return [];

	}
	
	function getstate(){
		$statelist = getdata()->State();
		if(isset($statelist['data']['state'])){
            return $statelist['data']['state'];
        }
        return [];
	}
	
	function getcity($id){
		$citylist = getdata()->City($id);
		if(isset($citylist['data']['city'])){
            return $citylist['data']['city'];
        }
        return [];
	}

	function goldprice(){
		return getdata()->GoldPrice();
    }
	
	function customerdata(){
		//return getdata()->CallStatusSubStatus();
	}

	function createbpo(){
		return getdata()->createadminuser(request());
	}


	function getdata(){
		static $obj;
		if(!$obj){
			$obj = (new App\Repository\CurlRequestRepository());
		}
		return $obj;
	}

	function callstatus(){
		$callstatus = CallStatusSubStatus();
		$main_call = isset($callstatus['data']['callStatus'])? $callstatus['data']['callStatus'] : [] ;
        return array_column($main_call,'name','status_id');
	}

	function callsubstatus(){
		$callstatus = CallStatusSubStatus();
		$sub_call = isset($callstatus['data']['callSubStatus'])? $callstatus['data']['callSubStatus'] : [] ;
       	return array_column($sub_call,'name','sub_status_id');
	}

	function citydistrictpincode($id){
		return getdata()->citydistrictpincode($id);
	}

	function pincode($id){
		return getdata()->getpincode($id);
	}

	function district($id){
		return getdata()->getdistrict($id);
	}

	function checkloginemail($emil){
		return getdata()->checkloginemail($emil);
	}



?>