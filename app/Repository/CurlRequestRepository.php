<?php
namespace App\Repository;
use Cache;
use App\User;
use App\BpoDetail;
use Illuminate\Http\Request;
use Hash;
use Session;

class CurlRequestRepository {

	private $url;
	private $time;
	private $key;

	public function __construct(){
		$this->url = 'www.cherishgold.com/crm/api/';//config('custom.url');
		$this->time = config('custom.timecache');
		$this->key = config('custom.BpoCrmKey');
	}

	private function curlrequest($appendurl,$method="GET",$body=array()){
        //$url=$this->url.$appendurl;
        $url = "localhost/crmnew/api/".$appendurl;
        $ch = curl_init();
        $token = Session::get('api_key');
		curl_setopt($ch,CURLOPT_URL,$url);
        curl_setopt($ch,CURLOPT_CUSTOMREQUEST,$method);
        if(count($body)>0){
            curl_setopt($ch,CURLOPT_POSTFIELDS,$body);  
        }
        curl_setopt($ch,CURLOPT_HTTPHEADER,array("api_token: $token"));
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
        $response = curl_exec($ch);
        if(curl_error($ch)){
            abort(501,"Problem With API!!! Contact System Administratior");
        }
        curl_close($ch);
        return json_decode($response,1);
    }

	public function CallStatusSubStatus(){
		//$cachekey = $this->key['call'];
		//if(!Cache::has($cachekey)){
        	$data = $this->curlrequest('callStatusAndSubstatus',"POST");                
        	//Cache::put($cachekey,$data,$this->time);
        //}else{
            //$data = Cache::get($cachekey);
        //}              
        return $data;
	}

	public function Bpodata($id){
        //$cachekey = $this->key['bpo'];
		//if(!Cache::has($cachekey)){
        	$data = $this->curlrequest('bpo/'.$id,"POST");                
        	//Cache::put($cachekey,$data,$this->time);
        //}else{
           // $data = Cache::get($cachekey);
        //}
        return $data;
	}

	public function State(){
		//$cachekey = $this->key['state'];
		//if(!Cache::has($cachekey)){
        	$data = $this->curlrequest('state',"POST");                
        	//Cache::put($cachekey,$data,$this->time);
        //}else{
            //$data = Cache::get($cachekey);
       //}
             
        return $data;
	}

	public function City($id){
		$cachekey = $this->key['city'];
		//if(!Cache::has($cachekey)){
        	$data = $this->curlrequest('city/'.$id,"POST");                
        	//Cache::put($cachekey,$data,$this->time);
        //}else{
            //$data = Cache::get($cachekey);
        //}
        return $data;
	}

    public function GoldPrice(){
       // $cachekey = $this->key['city'];
        //if(!Cache::has($cachekey)){
   
		 $data = $this->curlrequest('goldprice',"POST");                
            //Cache::put($cachekey,$data,$this->time);
        //}else{
            //$data = Cache::get($cachekey);
        //}
        return $data;
    }

    public function createadminuser(Request $request){
        
    }

	public function customerdata(){
		/*$cachekey = $this->key['call'];
		if(!Cache::has($cachekey)){
        	$data = $this->curlrequest('callStatusAndSubstatus');                
        	Cache::put($cachekey,$data,$this->time);
        }else{
            $data = Cache::get($cachekey);
        }
        return $data;*/
	}

    public function citydistrictpincode($id){
       // $cachekey = $this->key['city'];
        //if(!Cache::has($cachekey)){
         $data = $this->curlrequest('statecitydistrictList',"POST",['stateid'=>$id]);                
            //Cache::put($cachekey,$data,$this->time);
        //}else{
            //$data = Cache::get($cachekey);
        //}
        return $data;
    }

    public function getpincode($id){
        // $cachekey = $this->key['city'];
        //if(!Cache::has($cachekey)){
            $data = $this->curlrequest('pincodeList',"POST",['district'=>$id]);
         //Cache::put($cachekey,$data,$this->time);
        //}else{
            //$data = Cache::get($cachekey);
        //}
        return $data;
    }

    public function getdistrict($id){
        // $cachekey = $this->key['city'];
        //if(!Cache::has($cachekey)){
            $data = $this->curlrequest('districtList',"POST",['pincode'=>$id]);
         //Cache::put($cachekey,$data,$this->time);
        //}else{
            //$data = Cache::get($cachekey);
        //}
        return $data;
    }

    public function checkloginemail($email){
        // $cachekey = $this->key['city'];
        //if(!Cache::has($cachekey)){
            $data = $this->curlrequest('checkloginemail',"POST",['email'=>$email]);
         //Cache::put($cachekey,$data,$this->time);
        //}else{
            //$data = Cache::get($cachekey);
        //}
        return $data;
    }
	

}
?>