<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class BpoDetail extends Authenticatable
{
  
	public $table = "bpo_details";
	protected $primaryKey = 'id';
    protected $fillable = [
        'id', 'bpo_id', 'api_key',
    ];
	public $timestamps = true;

}
