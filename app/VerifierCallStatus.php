<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VerifierCallStatus extends Model
{
   
    protected $primaryKey = 'id';
     public $timestamps = false;
    public $table="verifier_call_status";
}
