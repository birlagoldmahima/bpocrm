<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LeadSms extends Model
{
	protected $connection = 'mysql';
    protected $primaryKey = 'id';
    public $table="lead_sms";
}
