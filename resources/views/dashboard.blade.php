@extends('layout.master')
@section('page_title')
    Dashboard
@endsection
@section('page_level_style_top')
    <header class="page-header">
        <h2>Dashboard</h2>
        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li>
                    <a href="#">
                        <i class="fa fa-home"></i>
                    </a>
                </li>
                <li><span>Dashboard</span></li>
            </ol>
            <a class="sidebar-right-toggle" data-open="#"><i class="fa fa-chevron-left"></i></a>
        </div>
    </header>
@endsection
@section('content')
    <section role="main" class="content-body">
        <div class="row">
            <div class="col-md-6">
                <section class="panel panel-featured-left panel-featured-primary">
                    <div class="panel-body">
                        <div class="widget-summary">
                            <div class="widget-summary-col widget-summary-col-icon">
                                <div class="summary-icon bg-primary">
                                    <i class="fa fa-line-chart"></i>
                                </div>
                            </div>
                            <div class="widget-summary-col">
                                <div class="summary">
                                    <h4 class="title">Sale</h4>
                                    <div class="info">
                                        <strong class="amount">{{ $totalsales  }}</strong>
                                        <span class="text-primary"></span>
                                    </div>
                                </div>
                                <div class="summary-footer">
                                    @if(Auth::user()->role!="Tele Caller")
                                        <a  href="{{ URL::route('convertedlist')}}" class="text-muted text-uppercase">(View)</a>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <div class="col-md-6">
                <section class="panel panel-featured-left panel-featured-warning">
                    <div class="panel-body">
                        <div class="widget-summary">
                            <div class="widget-summary-col widget-summary-col-icon">
                                <div class="summary-icon bg-warning">
                                    <i class="fa fa-cubes" aria-hidden="true"></i>
                                </div>
                            </div>
                            <div class="widget-summary-col">
                                <div class="summary">
                                    <h4 class="title">Sale-Gram / Sale-Tola</h4>
                                    <div class="info">
                                        <strong class="amount">
                                            {{ $totalsalestola }} / {{ $totalsalestola/10 }}
                                        </strong>
                                        <span class="text-warning"></span>
                                    </div>
                                </div>
                                <div class="summary-footer">
                                    @if(Auth::user()->role!="Tele Caller")
                                        <a  href="{{ URL::route('convertedlist')}}" class="text-muted text-uppercase">(View)</a>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <section class="panel panel-featured-left panel-featured-success">
                    <div class="panel-body">
                        <div class="widget-summary">
                            <div class="widget-summary-col widget-summary-col-icon">
                                <div class="summary-icon bg-success">
                                    <i class="fa fa-check-circle-o"></i>
                                </div>
                            </div>
                            <div class="widget-summary-col">
                                 <div class="summary">
                                    <h4 class="title">Request Receive</h4>
                                    <div class="info">
                                        <strong class="amount">{{ $leadreceive }}</strong>
                                        <span class="text-primary"></span>
                                    </div>
                                </div>
                                <div class="summary-footer">
                                    @if(Auth::user()->role!="Tele Caller")
                                        <a  href="{{ URL::route('requestlist')}}" class="text-muted text-uppercase">(View)</a>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <div class="col-md-4">
                <section class="panel panel-featured-left panel-featured-success">
                    <div class="panel-body">
                        <div class="widget-summary">
                            <div class="widget-summary-col widget-summary-col-icon">
                                <div class="summary-icon bg-success">
                                    <i class="fa fa-check-circle-o"></i>
                                </div>
                            </div>
                            <div class="widget-summary-col">
                                 <div class="summary">
                                    <h4 class="title">Verification Pending</h4>
                                    <div class="info">
                                        <strong class="amount">{{ $verification }}</strong>
                                        <span class="text-primary"></span>
                                    </div>
                                </div>
                                <div class="summary-footer">
                                    @if(Auth::user()->role!="Tele Caller")
                                        <a  href="{{ URL::route('verificationlist')}}" class="text-muted text-uppercase">(View)</a>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <div class="col-md-4">
                <section class="panel panel-featured-left panel-featured-success">
                    <div class="panel-body">
                        <div class="widget-summary">
                            <div class="widget-summary-col widget-summary-col-icon">
                                <div class="summary-icon bg-tertiary">
                                    <i class="fa fa-check-circle-o"></i>
                                </div>
                            </div>
                            <div class="widget-summary-col">
                                 <div class="summary">
                                    <h4 class="title">Converted</h4>
                                    <div class="info">
                                        <strong class="amount">{{ $converted }}</strong>
                                        <span class="text-primary"></span>
                                    </div>
                                </div>
                                <div class="summary-footer">
                                    @if(Auth::user()->role!="Tele Caller")
                                        <a  href="{{ URL::route('convertedlist')}}" class="text-muted text-uppercase">(View)</a>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <section class="panel panel-featured-left panel-featured-success">
                    <div class="panel-body">
                        <div class="widget-summary">
                            <div class="widget-summary-col widget-summary-col-icon">
                                <div class="summary-icon bg-success">
                                    <i class="fa fa-check-circle-o"></i>
                                </div>
                            </div>
                            <div class="widget-summary-col">
                                 <div class="summary">
                                    <h4 class="title">Lead</h4>
                                    <div class="info">
                                        <strong class="amount">{{ $lead }}</strong>
                                        <span class="text-primary"></span>
                                    </div>
                                </div>
                                <div class="summary-footer">
                                    @if(Auth::user()->role!="Tele Caller")
                                        <a  href="{{ URL::route('leadlist_report')}}" class="text-muted text-uppercase">(View)</a>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div> 
            <div class="col-md-4">
                <section class="panel panel-featured-left panel-featured-success">
                    <div class="panel-body">
                        <div class="widget-summary">
                            <div class="widget-summary-col widget-summary-col-icon">
                                <div class="summary-icon bg-success">
                                    <i class="fa fa-check-circle-o"></i>
                                </div>
                            </div>
                            <div class="widget-summary-col">
                                 <div class="summary">
                                    <h4 class="title">Call Back</h4>
                                    <div class="info">
                                        <strong class="amount">{{ $callback }}</strong>
                                        <span class="text-primary"></span>
                                    </div>
                                </div>
                                <div class="summary-footer">
                                    <a  href="{{ URL::route('callback')}}" class="text-muted text-uppercase">(View)</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <div class="col-md-4">
                <section class="panel panel-featured-left panel-featured-success">
                    <div class="panel-body">
                        <div class="widget-summary">
                            <div class="widget-summary-col widget-summary-col-icon">
                                <div class="summary-icon bg-success">
                                    <i class="fa fa-check-circle-o"></i>
                                </div>
                            </div>
                            <div class="widget-summary-col">
                                 <div class="summary">
                                    <h4 class="title">Not intersted</h4>
                                    <div class="info">
                                        <strong class="amount">{{ $notintersted }}</strong>
                                        <span class="text-primary"></span>
                                    </div>
                                </div>
                                <div class="summary-footer">
                                    @if(Auth::user()->role!="Tele Caller")
                                        <a  href="{{ URL::route('notinterestedlist')}}" class="text-muted text-uppercase">(View)</a>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </section>
@endsection
@section('page_level_script_bottom')
    <script type="text/javascript">
        jQuery(document).ready(function() {
            $('#loading').hide();
        });
    </script>
@endsection