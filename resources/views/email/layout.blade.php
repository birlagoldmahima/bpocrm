<!doctype html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cherish Gold</title>
        <link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
    </head>
<body style="margin: 0 auto;width: 100%;color: #444f59; background-color: #ffffff; font-family: 'Roboto', sans-serif; font-size:13px;">
    <table cellpadding="0" cellspacing="0" border="0" style="width:100%; max-width: 600px; border: 1px solid #882c01; border-radius: 3px; margin: 0 auto; font-size:13px;">
            <tr>
            	<td style="font-size: 20px; background-color: #f7f7f7; padding: 0 2%; width: 96%; color: #fff; line-height: 40px; text-transform: uppercase; border-bottom: 2px solid #d6d4d4;">
                    <img src="{{config('custom.weburl')}}img/frontendTheme/catalog/cheshgold_logo_new.png" style="width: 170px; margin-top: 13px;">
                    
                    <a href="mailto:support@cherishgold.com" style="color: #fff; float: right; font-size: 13px; line-height: 30px; text-transform: capitalize;">
                        <img src="{{config('custom.weburl')}}img/mail_48.png" style="width:35px; margin-right: 10px; margin-top: 10px; ">
                    </a>  &nbsp;
                    <a href="#" style="color: #fff; float: right; font-size: 13px; line-height: 30px; text-transform: capitalize;">
                        <img src="{{config('custom.weburl')}}img/link_48.png" style="width:35px; margin-right: 10px; margin-top: 10px; ">
                    </a>&nbsp;
                    <a href="https://twitter.com/cherrishgold" style="color: #fff; float: right; font-size: 13px; line-height: 30px; text-transform: capitalize;">
                        <img src="{{config('custom.weburl')}}img/tw_48.png" style="width:35px; margin-right: 10px; margin-top: 10px; ">
                    </a>&nbsp; 
                    <a href="https://www.facebook.com/cherishgoldonline" style="color: #fff; float: right; font-size: 13px; line-height: 30px; text-transform: capitalize;">
                        <img src="{{config('custom.weburl')}}img/fb_48.png" style="width:35px; margin-right: 10px; margin-top: 10px; ">
                    </a>
                </td>
            </tr>

            <tr style="width:100%; background-size:100%;">

                <td style="width:96%; background:#fff; padding:2%;">
                    <p>
					{!! $content !!}
                    </p>
                </td>

            </tr>

            <tr>
                <td width="100%" style="text-align: center; background: #9e9e9e; color: #fff; padding: 4px 0;">
                    <!-- <a href="#" style="color: #fff;margin-right: 7px;font-size: 12px; margin-top: 10px;display: inline-block;text-decoration:none; float :left; padding-left: 15px;">Term Of Use</a>
                    <a href="#" style="color: #fff; margin-right: 7px;font-size: 12px;    margin-top: 10px;display: inline-block; text-decoration:none;  float :left;">Privacy Policy</a> -->
                    <img src="{{config('custom.weburl')}}img/footer_email.png" style="float:right;">
                </td>
            </tr>
    </table>
</body>
</html>