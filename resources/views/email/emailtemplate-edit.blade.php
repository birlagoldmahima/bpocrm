@extends('layout.master')

@section('page_title')
EmailTemplate Edit
@endsection

@section('breadcrumb')
<li>
<a href="#">EmailTemplate Edit</a>
</li>
@endsection

@section('page_level_style_top')
<!-- BEGIN PAGE LEVEL STYLES -->

<link rel="stylesheet" type="text/css" href="{{URL::to('assets/plugins/gritter/css/jquery.gritter.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{URL::to('assets/plugins/select2/select2_metro.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{URL::to('assets/plugins/clockface/css/clockface.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{URL::to('assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{URL::to('assets/plugins/bootstrap-datepicker/css/datepicker.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{URL::to('assets/plugins/bootstrap-timepicker/compiled/timepicker.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{URL::to('assets/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{URL::to('assets/plugins/bootstrap-datetimepicker/css/datetimepicker.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{URL::to('assets/plugins/jquery-multi-select/css/multi-select.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{URL::to('assets/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{URL::to('assets/plugins/jquery-tags-input/jquery.tagsinput.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{URL::to('assets/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css')}}">
<!-- END PAGE LEVEL SCRIPTS -->
@endsection
@section('content')
			<div class="row">
				<div class="col-md-12">
				    <div class ="caption pull-right" >
					 <a href="{{ URL::to('emailtemplates-list')}}" class="btn btn-success" style="padding: 6px 10px; margin-bottom:10px;" >EmailTemplate List &nbsp<i class="fa fa-plus"></i></a>
					</div>
					<!-- BEGIN VALIDATION STATES-->
					<div class="portlet box green">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-reorder"></i>Edit EmailTemplate
							</div>
							
							
						</div>
						<div class="portlet-body form">
							<!-- BEGIN FORM-->
						<form action="{{ route('email-template-update') }}" id="validateemailtemplate" class="form-horizontal" method="post" enctype="multipart/form-data">
								<div class="form-body">
									@if(Session::has('flash_message'))
	                                      <div class="alert alert-success text-cente ">
	                                      {{ Session::get('flash_message') }}
	                                     </div>
                                    @endif
                                    @if($errors->any())
									    <div class="alert alert-danger text-cente">
									        @foreach($errors->all() as $error)
									            <p>{{ $error }}</p>
									        @endforeach
									    </div>
                                    @endif
									
									<div class="form-group">
										<label class="control-label col-md-3">Title
										<span class="required">
											*
										</span>
										</label>
										<div class="col-md-4">
											<input type="text" name="title" class="form-control" readonly placeholder="Please Enter Title" value="{{$emaillist->title}}"> 
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-3">From Name
										<span class="required">
											*
										</span>
										</label>
										<div class="col-md-4">
											<input type="text" name="from_name" class="form-control" placeholder="Please Enter  From  Name" value="{{$emaillist->from_name}}">
											
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-3">From Email
										<span class="required">
											*
										</span>
										</label>
										<div class="col-md-4">
											<input type="email" name="from_email" class="form-control" placeholder="Please Enter  From  Email" value="{{$emaillist->from_email}}">
											
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-3">To Email
										</label>
										<div class="col-md-4">
											<input type="email" name="to_email" class="form-control" placeholder="Please Enter Personal To Name" value="{{$emaillist->to_email}}">
											
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-3">Subject
										<span class="required">
											*
										</span>
										</label>
										<div class="col-md-4">
											<input type="text" name="subject" class="form-control" placeholder="Please Enter Personal Subject" value="{{$emaillist->subject}}">
											
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-3">Column
										</label>
										<div class="col-md-4">
											<select name="placeholdermail" id="placeholdermail" class="form-control">
												<option value="">Select Template</option>
												@foreach($cloumns as $key=>$val)
													<option value="{{ $val }}">{{  strtoupper(str_replace('_',' ',$val)) }}</option>
												@endforeach
											</select>
										</div>
										<div class="col-md-3">
											<input type="button" class="btn" name="inserttxtmail" id="inserttxtmail" value="Insert">
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-3">Content
										<span class="required">
											*
										</span>
										</label>
										<div class="col-md-9">
											<textarea class="ckeditor form-control" name="content" id="editor1" rows="6" data-error-container="#editor2_error">{{$emaillist->content}}</textarea>
											<div id="editor2_error"></div>
										</div>
										
									</div>
									<div class="form-group">
										<label class="control-label col-md-3">Email Status
										</label>
										<div class="col-md-4">
											<select id="emailstatus" name="status" class="form-control">
									        <option value="active">Active</option>
									        <option value="inactive">Deactive Now</option>
									    	</select>
								    	</div>
									</div>
									<div class ="row from-group">
										<div class="control-label col-md-3">
										 	<input class="pull-right btn default" type="Button" value="Add More" id="addmore">
										</div>
										@if(!empty($emaillist->fileattach))
											<?php $filattach=explode("|",$emaillist->fileattach);
											 $path=base_path().'/public/emailtemplate';?>
											 <div class="row col-md-9" id="fileadd">
													@foreach($filattach as $file)
														<input type="file" name="attach[]">
														<span>{{ $file}}</span> 
														<br>
													@endforeach
											</div>
										@else
											<div class="col-md-9" id="fileadd">
												<input type="file" name="attach[]"> <br>
											</div>
										@endif
									</div>

								<div class="form-actions fluid">
									<div class="col-md-offset-3 col-md-9">
										<button type="submit" class="btn green">Submit</button>
										<input type="reset" class="btn default" value="Cancel">
										<input type="hidden" name="_token" value="{{ Session::token() }}">
										<input type="hidden" name="id" value="{{$emaillist->email_template_id}}">
										<input type="hidden" name="page" value="{{$currentPage}}" />
										<input type="hidden" name="search" value="{{$search}}" />
										<input type="hidden" name="status" value="{{$status}}" />
									</div>
								</div>
								
							</form>
							<!-- END FORM-->
						</div>
						<!-- END VALIDATION STATES-->
					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
@endsection

@section('page_level_script_bottom')
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="{{URL::to('assets/plugins/jquery-validation/dist/jquery.validate.min.js')}}"></script>
<script type="text/javascript" src="{{URL::to('assets/plugins/jquery-validation/dist/additional-methods.min.js')}}"></script>
<script type="text/javascript" src="{{URL::to('assets/plugins/select2/select2.min.js')}}"></script>
<script type="text/javascript" src="{{URL::to('assets/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js')}}"></script>
<script type="text/javascript" src="{{URL::to('assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js')}}"></script>
<script type="text/javascript" src="{{URL::to('assets/plugins/ckeditor/ckeditor.js')}}"></script>
<script type="text/javascript" src="{{URL::to('assets/plugins/bootstrap-markdown/js/bootstrap-markdown.js')}}"></script>
<script type="text/javascript" src="{{URL::to('assets/plugins/bootstrap-markdown/lib/markdown.js')}}"></script>
<script type="text/javascript" src="{{URL::to('assets/plugins/fuelux/js/spinner.min.js')}}"></script>
<script type="text/javascript" src="{{URL::to('assets/plugins/ckeditor/ckeditor.js')}}"></script>
<script type="text/javascript" src="{{URL::to('assets/plugins/bootstrap-fileupload/bootstrap-fileupload.js')}}"></script>

<script type="text/javascript" src="{{URL::to('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
<script type="text/javascript" src="{{URL::to('assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js')}}"></script>
<script type="text/javascript" src="{{URL::to('assets/plugins/clockface/js/clockface.js')}}"></script>
<script type="text/javascript" src="{{URL::to('assets/plugins/bootstrap-daterangepicker/moment.min.js')}}"></script>
<script type="text/javascript" src="{{URL::to('assets/plugins/bootstrap-daterangepicker/daterangepicker.js')}}"></script>

<script type="text/javascript" src="{{URL::to('assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.js')}}"></script>
<script type="text/javascript" src="{{URL::to('assets/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js')}}"></script>
<script type="text/javascript" src="{{URL::to('assets/plugins/jquery.input-ip-address-control-1.0.min.js')}}"></script>
<script type="text/javascript" src="{{URL::to('assets/plugins/jquery-multi-select/js/jquery.multi-select.js')}}"></script>
<script type="text/javascript" src="{{URL::to('assets/plugins/jquery-multi-select/js/jquery.quicksearch.js')}}"></script>

<script src="{{URL::to('assets/plugins/jquery.pwstrength.bootstrap/src/pwstrength.js')}}" type="text/javascript"></script>
<script src="{{URL::to('assets/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js')}}" type="text/javascript"></script>
<script src="{{URL::to('assets/plugins/jquery-tags-input/jquery.tagsinput.min.js')}}" type="text/javascript"></script>

<script src="{{URL::to('assets/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js')}}" type="text/javascript"></script>
<script src="{{URL::to('assets/plugins/bootstrap-touchspin/bootstrap.touchspin.js')}}" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL STYLES -->
<script src="{{URL::to('assets/scripts/app.js')}}"></script>
<script src="{{URL::to('assets/scripts/form-validation.js')}}"></script>
<script src="{{URL::to('assets/scripts/form-components.js')}}"></script>
<!-- END PAGE LEVEL STYLES -->
<script>
jQuery(document).ready(function() {
   // initiate layout and plugins
   App.init(); 
   FormComponents.init();
   FormValidation.init();
});  
$('#addmore').click(function(){
	$('#fileadd').append('<input type="file" name="attach[]"><br>');
}); 
$('#inserttxtmail').click(function(){
		var append=$('#placeholdermail option:selected').val();
		CKEDITOR.instances['editor1'].insertText('['+append+']');
	});
</script>

<!-- END JAVASCRIPTS -->
@endsection