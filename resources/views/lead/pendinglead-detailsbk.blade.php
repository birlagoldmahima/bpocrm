@extends('layout.master')
@section('page_title')
  Lead Details
@endsection
@section('breadcrumb')
  <li><a href="{{ URL::route('lead-list')}}">  Lead List </a></li>
  &nbsp;&nbsp;>&nbsp;&nbsp;
  <li><a href="#">Lead Details</a></li>
@endsection
@section('page_level_style_top')

@endsection
@section('content')

<section role="main" class="content-body">
    <header class="page-header">
        <h2>Add User</h2>

        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li>
                    <a href="index.html">
                        <i class="fa fa-home"></i>
                    </a>
                </li>
                <li><span>Users</span></li>
                <li><span>Add user</span></li>
            </ol>
			<a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
        </div>
    </header>

  <div class="row">
    <div class="loading" id="loading" style="display:none">
      <!--img class="loading-image" id="loading-image" src="{{URL::to('assets/img/ajax-loading.gif')}}" alt="Loading..." /-->
    </div>
    <div class="col-md-12">
      <div class="portlet box grey">
        <div class="portlet-title"></div>
          <div class="portlet-body">
            <div class="margin-top-10 margin-bottom-10 clearfix">
              <table class="table table-bordered table-striped table-font">
                <tr>
                  <td>Lead ID :</td>
                    <td>
                      <div id="pulsate-regular">
                        LEAD-{{$reference_data['reference_id']}}
                      </div>
                    </td>
  				          <td>Phone:</td>
                    <td>
                      <div id="pulsate-regular">
                        {{$reference_data['mobile']}}
                      </div>
                    </td>
					
                  </tr>
                <tr>
                    <td>Name :</td>
                    <td>
                      <div id="pulsate-once-target">
                        {{$reference_data['name'] }}
                      </div>
                    </td>
					<td>Email :</td>
                    <td>
                      <div id="pulsate-regular">
                        {{$reference_data['email']}}
                      </div>
                    </td>
					 
                   
                  </tr>
                <tr>
                    <td>
                      Relationship Manager :
                    </td>
                    <td>
                      <div id="pulsate-crazy-target">
            				{{$reference_data['reporting_manager'] }}		
                      </div>
                    </td>
					 <td>
  					Status :
  				  </td>
  				  <td>
  						{{$reference_data['status'] }}
  				  </td>
                  </tr>
  				<tr>
					<td>
					  Created at
					</td>
					<td>
					  {{ date('d-m-Y',strtotime($reference_data['created'])) }}
					</td>
					<td>
                      Possible EMI :
                    </td>
                    <td>
                      <div id="pulsate-crazy-target">
  						            {{$reference_data['emi_size']}}
                      </div>
                    </td>
					
				</tr>
    
  			
    		</table>
			
      </div>
    </div>
  </div>
       <div class="tabbable tabbable-custom">
            <ul class="nav nav-tabs">
              <li class="active"> <a href="#tab_1_0" id="interactiontoggle" data-toggle="tab">Interaction</a> </li>
             
             </ul>
            <div class="tab-content">
              <div class="tab-pane active" id="tab_1_0">
                <!-- BEGIN ACCORDION PORTLET-->
                <div style="border:none;" class="portlet box green">
                  <div class="portlet box purple ">
                    <div class="portlet-title">
                      <div class="caption"> <i class="fa fa-reorder"></i> Interaction </div>
                    </div>
                    <div class="portlet-body form">
				
						@if(Session::has('flash_message'))
                        <div class="alert alert-danger text-cente " id="interaction-error">
                          {{ Session::get('flash_message') }}
                        </div>
                       @endif
					   @if(Session::has('flash_message'))
                        <div class="alert alert-success text-cente " id="interaction-success">
                          {{ Session::get('flash_message') }}
                        </div>
                       @endif
            
                      <form class="form-horizontal" role="form" method="post" id="customer-interaction">
                        <div class="form-body">
                          <div class="container-fluid">
							   <div class="row">
							    <span class="col-md-6">
								  <div class="form-group">
									 <label class="col-md-3 control-label">Call Status</label>
									  <div class="col-md-9">
									
									   <select class="form-control" name="callstatus" id="callstatus" onchange="getSubStatus();">
											<option value="">Select Call Status</option>
													 @foreach($callStatus as $key=>$val)

													  @if($val['status']=='active')
											<option value="{{$val['status_id']}}">{{$val['name']}}</option>
													  @endif
													@endforeach
										 </select>
									  </div>
								   </div>
								   <div class="form-group">
								   <label class="col-md-3 control-label">Call Sub Status</label>
									<div class="col-md-9">
									   <select class="form-control" name="callsubstatus" id="callsubstatus" onchange="getOthers();">
											<option value=""></option>
										</select>
								    </div>
								   </div> 
									<div class="form-group substatus">
									   <label class="col-md-3 control-label">Call Sub Status Other </label>
										<div class="col-md-9">  
										<input type="text" class="form-control substatusother" disabled name="call_sub_status_other" id="call_sub_status_other">
									   </div>
								   </div>  
								   <div class="form-group">
									   <label class="col-md-3 control-label">Next Date/Time </label>
										<div class="col-md-9">  
										<input type="text" class="form-control form_datetime" readonly name="nextdatetime" id="nextdatetime">
									   </div>
								   </div> 
								   <div class="form-group">
										<label class="col-md-3 control-label">Remarks</label>
										<div class="col-md-9">
										  <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
										  <input type="hidden" name="reference_id" id="reference_id" value="{{ $reference_data['reference_id'] }}">
										  <textarea class="form-control" name="interaction" id="interaction" placeholder="Brief your call summary" rows="8" style="resize:none;"></textarea>
										</div>
								  </div> 
								</span>
						  	  </div>
						  </div>
                        </div>
                        <div class="form-actions right">
                          <input type="hidden" id="_tokencom" value="{{ csrf_token() }}">
						   <button type="submit" data-toggle="modal"  class="btn red appointment">Fix Appointment</button>
                          <button type="button" class="btn default" onclick="cancelIntersaction()">Cancel</button>
                          @if($reference_data['status'] != "Converted")
                            <button type="submit" class="btn green interactionsubmit" id="interactionsubmit">Submit</button>
                          @endif
                        </div>
                      </form>
                    </div>
                  </div>
                </div>

        <div class="portlet">
            <div class="portlet-title line">
              <div class="caption">
                <i class="fa fa-comments"></i>Previous Interaction (<b id="interactioncount">{{$interaction->count()}}</b>)
              </div>
               
            </div>
            <div class="row"> 
              <div class="col-md-12">
              <!-- BEGIN SAMPLE TABLE PORTLET-->
                <div class="portlet">
                  <div class="portlet-body">
                    <div class="table-responsive">
                      <table class="table table-striped table-bordered table-advance table-hover scroll">
                      <thead>
                        <tr>
                            <th style="width:16%;">
                          <i class="fa fa-briefcase"></i> Date
                        </th>
                        <th style="width:16%;">
                          <i class="fa fa-user"></i> Logged By
                        </th>
                        <th style="width:16%;">
                          <i class="fa fa-phone"></i> Call Status
                        </th>
                        <th style="width:16%;">
                          <i class="fa fa-random"></i> Call Sub Status
                        </th>
                        <th style="width:16%;">
                          <i class="fa fa-phone"></i> Call Back
                        </th>
                        <th style="width: 11%;">
                          <i class="fa fa-tags"></i> Tagging
                        </th>
                        <th style="width: 10%;">
                          <i class="fa fa-bookmark"></i> Remarks
                        </th>
                        </tr>
                      </thead>
                      <tbody id="tbodyajaxdata">
                      @if($interaction->count() > 0)
                      <?php $keysubstatus = array_map(function ($arr) {return $arr['sub_status_id'];},$callSubStatus);
                                    ?>
									
                                    <?php $iteration=0;?> 
                      @foreach($interaction as $key=>$value)
                      <tr>  
					  
                        <td style="width: 16%;"> 
                          {{date("M d, Y H:i",strtotime($value->created_at)+5*3960)}}
                        </td>
                        <td style="width: 16%;">
                          
						   
                        </td>
                         <td style="width: 16%;">
                          
						   
                        </td>
                        
                        <td style="width: 16%;">
						
                          @if($value->call_status_id > 0)
                           <?php 
                           // $callStatusvalue=array_search($value->call_status_id,array_column($callStatus,'status_id')); 
                            ?>
                            {{--$callStatus[$callStatusvalue]['name']--}}
                          @endif
                        </td> 
						
                        <td style="width: 16%;">
                          <?php 
                            
                            if(array_search($value->call_substatus_id,$keysubstatus)){
                              $keycallstatus =array_search($value->call_substatus_id,array_column($callSubStatus,'sub_status_id'));
                              echo $callSubStatus[$keycallstatus]['name'];
                              if($callSubStatus[$keycallstatus]['name'] == "Other"){ 
                                echo $value->call_substatus_other;
                              }
                          } ?>
                        </td>
                        <td style="width: 16%;">
						{{--@if($value->callback_datetime != "0000-00-00 00:00:00"){{date("M d, Y H:i",strtotime($value->callback_datetime))}}@endif--}}
                        </td>
						
                        <td style="width: 10%;">
                          <p type="text" data-toggle="modal" onclick="viewremarks({{ $iteration++ }})" data-target="#remarks_interaction" class="btn purple"><i class="fa fa-envelope"></i></p>
                        </td>
                      </tr>
                      @endforeach
                      @endif
					 
                    
                                      
                      </tbody>  
                      </table>
                    </div>
                  </div>
                </div>
              <!-- END SAMPLE TABLE PORTLET-->
              </div>
            </div>

          </div>
              </div>
                <?php
                    $profiledata=array();
                    $planamountdata=array();
                    $datajson_struct=$reference_data['json_struct'];
                    if(!empty($datajson_struct)){
                      $dataarray_struct=json_decode($datajson_struct,1);
                      if(array_key_exists('CustomerBGP',$dataarray_struct)){
                        $profiledata=$dataarray_struct['CustomerBGP'];
                      }
                      if(array_key_exists('CustomerPlan',$dataarray_struct)){
                        $planamountdata=$dataarray_struct['CustomerPlan'];
                      }
                    }
                    $name="";
                    if(array_key_exists('applicant_name',$profiledata))
                      $name=$profiledata['applicant_name'];
                    elseif(array_key_exists('name',$reference_data)){
                        $name=$reference_data['name'];
                    }
                    
                    $partnerdata="";
                    if(array_key_exists('partner_code',$profiledata)){
                      $partnerdata=$profiledata['partner_code'];
                    }elseif(array_key_exists('customer_id',$reference_data)){
                        $partnerdata=$reference_data['customer_id'];
                    }
                    $mobile_no="";
                    if(array_key_exists('contact_mobile',$profiledata)){
                      $mobile_no=trim($profiledata['contact_mobile']);
                    }elseif(array_key_exists('mobile',$reference_data)){
                        $mobile_no=$reference_data['mobile'];
                    }

                    $email_id="";
                    if(array_key_exists('contact_email',$profiledata)){
                      $email_id=trim($profiledata['contact_email']);
                    }elseif(array_key_exists('email',$reference_data)){
                        $email_id=$reference_data['email'];
                    }
                ?>
              
              
              </form>

              <!--Form End-->
              </div>

            
        </div>
      </div>
      
    </div>
  </div> 
 
<!-- END CONTENT -->
@endsection
<div class="modal fade" id="remarks_interaction" tabindex="-1" role="basic" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title text-purple"><b>Remarks</b></h4>
      </div>
      <div class="modal-body" >
         <p id="remarkinteraction"></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
  
</div>


<div class="modal fade" id="appointment" tabindex="-1" role="basic" aria-hidden="true"  data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Customer Appointment</h4>
      </div>
      <div class="modal-body">
            <span id="appointfed"></span>
            <div class="form-body">
				<div class="form-group">
					<div class="row">
					<label class="col-md-4 control-label">Appointment Datetime </label>
					<div class="col-md-6">	
						 <input type="text" name="appointment_date" id="appointmentdate" class="form-control form_datetime" data-required="1" readonly placeholder="Please Select Appointment Date" value="{{$reference_data['appointment_date']}}"/>
					</div>
					</div>
					
				</div>

            </div>
        </div>
        <div class="modal-footer">
          @if((in_array($reference_data['stage_id'],[2,4]) && $reference_data['stage_remarks']=="rejected") || empty($reference_data['appointment_date']))
            <button type="button" class="btn green" onclick="saveappointment();">Save</button>
          @endif
	   
          <button type="button" class="btn default" data-dismiss="modal">Close</button>
        </div>
     
    </div>
  </div>

</div>
@section('page_level_script_bottom')

  <script src="{{URL::asset('public/assets/javascripts/forms/form-validation.js')}}"></script>
	<script src="{{URL::asset('public/assets/javascripts/app.js')}}"></script>
  <script>
  function checklead(obj){
    $('#lead_source_other').val('');
    if(obj.value=='Other'){
      $('#lead_source_div').show();
    }else{
      $('#lead_source_div').hide();
    }
  }

  var datainteractionajax={!! json_encode(array_map(function ($arr) {return $arr['remarks'];},$interaction->toArray())) !!};
  jQuery(document).ready(function() {
    App.init(); 
    FormValidation.init(); 
    //TableEditable.init(); 
    $(".substatus").hide();
    var end = new Date();
    end.setFullYear(end.getFullYear() - 18);
    /* $('#applicant_dob').datepicker({
    format: "dd-mm-yyyy",
        endDate: format(end) 
    });
    
    $('#nomination_dob').datepicker({
        format: "dd-mm-yyyy",
        endDate: format(end) 
    });
	 */
    var d = new Date("{!! date('D M d Y H:i:s',strtotime('+5 hours +30 minutes', strtotime(date('Y-m-d H:i:s')))) !!}");
    /* $('#appointmentdate').datetimepicker({
        format: "dd-mm-yyyy hh:ii:ss",
        startDate: d,
    });
	 $('#appointmentdate').datepicker({
        format: "dd-mm-yyyy",
        endDate: format(maxapp),
		  startDate: '0d'
    });
    
    $(".form_datetime").datetimepicker({
      autoclose: true,
      isRTL: App.isRTL(),
      todayBtn: true,
      format: "dd-mm-yyyy hh:ii:ss",
      pickerPosition: (App.isRTL() ? "bottom-right" : "bottom-left")
    }); */
   
   

    $('.appointment').prop('disabled',true);
  });

  
  function getSubStatus(){
    statusid = $("#callstatus").val();
    $("#callsubstatus option").remove();
    var callsubstatus={!! json_encode($callSubStatus) !!};
	
    $("#callsubstatus").append($("<option></option>").text('').val(''));
    $.each(callsubstatus,function(index,item){
	   if(item.status_id==statusid  && item.status=='active'){
	   $("#callsubstatus").append(
			$("<option></option>").text(item.name).val(item.sub_status_id)
        );
      }
    });
	 return false;
  }


  function checkothers(value){
    var val=$('#subcategory'+value+' option:selected').html();
    if(val=='others'){
      //$("#remarksdiv"+value).show();
    }else{
      //$("#remarks"+value).val('');
      //$("#remarksdiv"+value).hide();
      
    }
  }



  function removeelement(value){
    $('#div'+value).remove();
  }

   $('#inserttxtmail').click(function(){
    var append=$('#placeholdermail option:selected').val();
    CKEDITOR.instances['editor1'].insertText(append);
  });

  
  $('#inserttxt').click(function(){
    var append=$('#placeholder option:selected').val();
    var cursorPos = $('#message').prop('selectionStart');
    var v = $('#message').val();
        var textBefore = v.substring(0,  cursorPos );
        var textAfter  = v.substring( cursorPos, v.length );
        $('#message').val( textBefore+ append +textAfter );
        keyupfunction();
  });

  
  
  function postInteraction(form){
    var interaction = $("#interaction").val();
    var callinitiator = "1";
    var callstatus = $("#callstatus").val();
    var callsubstatus = $("#callsubstatus").val();
    var nextdatetime = $("#nextdatetime").val();
    var callsubstatusother = $("#call_sub_status_other").val();
    var user_type = "lead";
    var _token = $("#_token").val();
    var reference_id = $("#reference_id").val();
    $('#loading').show();
    var m_category,m_subcategory,m_remarks;
    jsonObj ={'_token': _token,'reference_id': reference_id,'callsubstatusother': callsubstatusother, 'nextdatetime': nextdatetime,'callsubstatus':callsubstatus,'callstatus': callstatus,'callinitiator': callinitiator,'interaction': interaction,'user_type': user_type};
    $(".categoryclass").each(function(index) {
        if($(this).val()!=''){
          var category=$(this).attr('data-id');
          var subcategory=$('#subcategory'+category).val();
          var remarks=$('#remarks'+category).val();
          if(m_category==undefined){
            m_category = $(this).val();
            m_subcategory= subcategory;
            m_remarks=remarks
          }else{
            m_category =m_category+','+$(this).val();
            m_subcategory=m_subcategory+','+subcategory;
            m_remarks=m_remarks+','+remarks;
          }
        }
    });
    if (m_category!=""){
      jsonObj.category=m_category;
      jsonObj.subcategory=m_subcategory;
      jsonObj.cat_remarks=m_remarks;
    }

    $.ajax({
      url: "{{ route('post-reference-interaction')}}",
      data: jsonObj,
      type: 'post',
      cache: false,
      clearForm: false,
      success: function(response)     
      {
        $('#loading').hide();
        var dataresponse=JSON.parse(response);
        if(dataresponse['return'] == true){
          $("#interaction-error").hide();
          $("#interaction-success").show();
          $(".substatus").hide();
          $("#call_sub_status_other").attr("disabled",true);
          $('#interactioncount').html(parseInt($('#interactioncount').html())+1);
          $('#customer-interaction').clearForm();
          $( "div[id^='div']").remove();
          $('#subcategory1').removeAttr('required');
          datainteractionajax.push(dataresponse['interaction']['Remarks']);
          var columns= ["datetime","Logged_By","Call_Status","Call_Sub_Status","Call_Back","Tagging"];
          for(i=0;i<columns.length;i++){
            if(i==0){
              $("#tbodyajaxdata").prepend($('<tr></tr>'));    
            }
            $("#tbodyajaxdata tr:first").append(
                $("<td></td>").text(dataresponse['interaction'][columns[i]])
              );
            }
            $("#tbodyajaxdata tr:first").append(
              $("<td></td>").html('<p type="text" data-toggle="modal"  onclick="viewremarks('+(parseInt($('#interactioncount').html())-parseInt(1))+')"  data-target="#remarks_interaction" class="btn purple viewdetail"><i class="fa fa-envelope"></i></p>')
            );
        }
        else
        {
          $("#interaction-error").show();
          $("#interaction-success").hide();
        }
      }
    });
  }
  $.fn.clearForm = function() {
    return this.each(function() {
      var type = this.type, tag = this.tagName.toLowerCase();
      if (tag == 'form')
        return $(':input',this).clearForm();
      if (type == 'text' || type == 'password' || tag == 'textarea')
        this.value = '';
      else if (type == 'checkbox' || type == 'radio')
        this.checked = false;
      else if (tag == 'select')
        this.selectedIndex = -1;
    });
  };
  function format(date) {
    date = new Date(date);

    var day = ('0' + date.getDate()).slice(-2);
    var month = ('0' + (date.getMonth() + 1)).slice(-2);
    var year = date.getFullYear();

    return day + '-' + month + '-' + year;
  }

  function viewremarks(value){
  var remarrks = datainteractionajax[value];
  $('#remarkinteraction').html(remarrks);
}

function messageinteraction(body,messages){
    $('#callstatus').val(10);
    getSubStatus();
    $('#callsubstatus').val(49);
    $('#call_sub_status_other').val(body);
    $('#interaction').val(messages);
    postInteraction();  
  }

  $('#callsubstatus').change(function(){
    if($('#callsubstatus option:selected').val()==66){
      $('.appointment').prop('disabled',false);
      $('.interactionsubmit').prop('disabled',true);
    }else{
      $('.appointment').prop('disabled',true);
      $('.interactionsubmit').prop('disabled',false);
    }
  });

  $('.appointment').click(function(){
    if($('#customer-interaction').valid()==true){
      $('#appointment').modal('toggle');
    }  
  });
  

  var $remaining = $('#remaining'),
    $messages = $remaining.next();
    $('#message').keyup(function(){
      keyupfunction();
  });

  function keyupfunction()
  {
    var chars = $('#message').val().length,
        messages = Math.ceil(chars / 160),
        remaining = messages * 160 - (chars % (messages * 160) || messages * 160);
      // changeSms();
      $remaining.text(remaining + '/');
      $messages.text(messages + '');
  }
  
  function saveappointment()
  {
	  var appointmentdate = $("#appointmentdate").val();
    $('#nextdatetime').val(appointmentdate);
	  var reference_id = $("#reference_id").val();
	  if(appointmentdate==""){
      alert("Enter Appointment datetime");
      return false;
    }
    $.ajax({
		  url: "{{ route('fix-appointment')}}",
			data:{'appointment_time':'', 'reference_id': reference_id, 'appointment_date': appointmentdate},    
			type: 'get',
			cache: false,
			async: false,
			clearForm: false,
			success: function(response){
				if(response ==true){
          postInteraction();    
				  $("#appointfed").html("<div class='alert alert-success'>Success! Appointment Fixed</div>");
				  }	
        else{
					$("#appointfed").html("<div class='alert alert-danger'>Unable to Fix Appointment</div>");
				}
			}
		});
    setTimeout(function(){
      $('#appointment').modal('hide');
    },1000);
    $("#appointmentdate").val('');
    $("#appointfed").html("");
    window.location.reload();
	 }

   var checkdata = true;
 /*    jQuery.validator.addMethod("checkdatetime",function(value,element){
        return checkdata;
    },"Select Date & Time"); */

  function getOthers(){
    substatus = $('#callsubstatus option:selected').text();
    if(substatus == "Other"){
        $(".substatus").show();
        $("#call_sub_status_other").prop("disabled", false);
    }else{
        $(".substatus").hide();
        $("#call_sub_status_other").attr("disabled",true);
    }
    var substatusval = parseInt($('#callsubstatus option:selected').val());
    var applicable=[41,65,68,69,51,53,52,57];
    if($.inArray(substatusval,applicable) === -1){
        checkdata=true;
    }else{
        checkdata=false;
    }
 }

     $('#nextdatetime').change(function(){
          var value = $(this).val();
          if(value!="" && checkdata==false){
               checkdata=true;
          }
     });

  </script>
@endsection