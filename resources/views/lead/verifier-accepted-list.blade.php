@extends('layout.master')
@section('page_title')
	TL Verifier Lead List
@endsection
@section('breadcrumb')
<li>
	<a href="#">TL Accepted Lead List</a>
</li>
@endsection
@section('page_level_style_top')
	<link rel="stylesheet" type="text/css" href="{{URL::to('assets/plugins/select2/select2_metro.css')}}"/>
	<link rel="stylesheet" type="text/css" href="{{URL::to('assets/plugins/data-tables/DT_bootstrap.css')}}"/>
@endsection
@section('content')
	<div class="row">
		<div class="col-md-12">
			<div class="portlet box blue">
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-reorder"></i>TL Accepted Lead List
					</div>
				</div>
				<div class="portlet-body">
					<div class="tabbable tabbable-custom">
						<div class="tab-content">
							<form method="get" name="frm" id="frm">
								<div class="col-md-12">
									<div class="form-group">
										<div class="col-md-2">
											<select name="datewise" class="form-control">
												<option value="app" {{ (isset($datewise) && $datewise=="app") ? "selected" : "" }}>Appointment</option>
												<option value="tlver" {{ (isset($datewise) && $datewise=="tlver") ? "selected" : "" }}>TL Verified</option>
											</select>
										</div>
										<div class="col-md-2">
											<input type="text" name="fromdate" class="form-control" id="from_date" value="{{isset($fromdate)?$fromdate:""}}" placeholder="From Date" readonly>
										</div>
										<div class="col-md-2">
											<input type="text" name="todate" class="form-control" id="to_date" value="{{isset($todate)?$todate:""}}" placeholder="To Date" readonly>
										</div>
										<div class="col-md-4">
											<button type="submit" class="btn green">Submit</button>
											<a href="{{ route('verified-list') }}" class="btn red">Clear</a>
										</div>
										<div class="col-md-2">
											<a  class="btn btn-primary pull-right excel">Excel Export</a>
										</div>
									</div>
								</div><br><br>
							</form>
							<div class="table-scrollable">
								<table class="table table-striped table-hover table-bordered">
									<thead>
										<tr id="tbl">
											<th>
												Id
											</th>
											<th>
												Name
											</th>
											<th>
												Type
											</th>
											<th>
												Entry Date
											</th>
											<th>
												TL Verifier Date
											</th>
											<th>
												Relationship Manager
											</th>
											<th>
												TL Name
											</th>
											<th>
												Appointment Time
											</th>
											<th>
												Appointment Date
											</th>
											<th>
												Stage
											</th>
										</tr>
									</thead>
									<tbody id="tbl">
										@foreach($results as $data)
											<?php
												$rmname='-';
												$tlname='-';
												$type="";
												if(empty($data->customer_id) || $data->customer_id==0){
													$type="Lead";
													if(isset($user[$data->rel_manager_id])){
														$rmname = $user[$data->rel_manager_id]['name'];
														if($user[$data->rel_manager_id]['reporting_manager']){
															$tlname =  $user[$user[$data->rel_manager_id]['reporting_manager']]['name'];
														}
													}

												}else{
													$type="Reference";
													if(isset($user[$data->relationship_manager])){
														$rmname = $user[$data->relationship_manager]['name'];
														if($user[$data->relationship_manager]['reporting_manager']){
															$tlname =  $user[$user[$data->relationship_manager]['reporting_manager']]['name'];
														}
													}
												}
											?>
											<tr>
												<td>
													{{$data->reference_id}}
												</td>
												<td>
													{{$data->name}}
												</td>
												<td>
													{{ $type }}
												</td>
												
												<td>
													{{ $data->created }}
												</td>
												<td>
													{{ $data->mxdate }}
												</td>
											
												<td>
													{{ $rmname }}
												</td>
												<td>
													{{ $tlname }}
												</td>
												<td>
													{{ $data->appointtime }}
												</td>
												<td>
													{{ $data->appointdate }}
												</td>
												<td>
													@if($data->stage_id==4 && in_array($data->stage_remarks,['accepted','rejected']))
														{{  ucfirst($data->stage_remarks) }}
													@else
														Pending
													@endif
												</td>
											</tr>
										@endforeach
									</tbody>
								</table>
							</div>
						</div>
						<div class ="pull-right" style="margin-bottom:20px">
                  {!! $results->appends(request()->except('page'))->links() !!}
               </div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('page_level_script_bottom')
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="{{URL::to('assets/plugins/select2/select2.min.js')}}"></script>
<script type="text/javascript" src="{{URL::to('assets/plugins/data-tables/jquery.dataTables.js')}}"></script>
<script type="text/javascript" src="{{URL::to('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
<script type="text/javascript" src="{{URL::to('assets/plugins/data-tables/DT_bootstrap.js')}}"></script>
<script src="{{URL::to('assets/plugins/bootbox/bootbox.min.js')}}" type="text/javascript"></script>
<script src="{{URL::to('assets/scripts/app.js')}}"></script>
<script src="{{URL::to('assets/scripts/table-editable.js')}}"></script>
<script>
jQuery(document).ready(function() {  
   	App.init();      
   	TableEditable.init();
   	$('#from_date').datepicker({
    	format: "dd-mm-yyyy"
    });
    $('#to_date').datepicker({
    	format: "dd-mm-yyyy"
    });

    $('.excel').click(function(){
    	window.location.href="{{ route('verified-list-export') }}?"+$('#frm').serialize();
    })
});
</script>
@endsection