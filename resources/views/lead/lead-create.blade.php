@extends('layout.master')
@section('page_title')
    Add New Lead
@endsection
@section('page_level_style_top')
    <header class="page-header">
        <h2>Add Lead</h2>
        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li>
                    <a href="#">
                        <i class="fa fa-home"></i>
                    </a>
                </li>
                <li><span>Lead</span></li>
                <li><span>Add Lead</span></li>
            </ol>

            <a class="sidebar-right-toggle" data-open="#"><i class="fa fa-chevron-left"></i></a>
        </div>
    </header>
@endsection
@section('content')
    <section role="main" class="content-body">
        <section class="panel">
            <header class="panel-heading">
                <div class="row">
                    <h2 class="panel-title col-md-5">Add Lead</h2>
                    @if($errors->any())
                        <div class="alert alert-danger text-center col-md-3">
                            @foreach($errors->all() as $error)
                                <p>{{ $error }}</p>
                            @endforeach
                        </div>
                    @endif
                </div>
            </header>
            <div class="panel-body">
                <form id="form" action="{{ url('lead-create') }}" class="form-horizontal form-bordered" method="post">
                    <input type="hidden" name="batch_id" value="">
                    <input type="hidden" name="campaign_code" value="">
                    <input type="hidden" name="lead_code" value="">
                    <input type="hidden" name="investment_question" value="">
                    <input type="hidden" name="purpose_of_call" value="">
                    <input type="hidden" name="call_complaince" value="">
                    <input type="hidden" name="recording_statement" value="">
                    <input type="hidden" name="verification_interaction" value="">
                    <input type="hidden" name="call_status" value="">
                    <input type="hidden" name="call_sub_status" value="">
                    <input type="hidden" name="appointment_date" value="">
                    <input type="hidden" name="statename" value="" id="statename">
                     <input type="hidden" name="cityname" value="" id="cityname">
                     <input type="hidden" name="_token" id="_tokensc" value="{{ csrf_token() }}">
                   
                            <div class="form-group">
                                <label class="col-md-3 control-label"> Name <span class="required">*</span></label>
                                <div class="col-md-6">
                                    <input type="text" name="name" class="form-control" placeholder="Please Enter Name"/>
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="col-md-3 control-label">Mobile <span class="required">*</span></label>
                                <div class="col-md-6">
                                    <input type="text" name="mobile" class="form-control" placeholder="Please Enter Mobile" pattern="[789][0-9]{9}"/>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label">Alternate Mobile</label>
                                <div class="col-md-6">
                                    <input type="text" name="alt_mobile" class="form-control" placeholder="Please Enter Mobile" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Email</label>
                                <div class="col-md-6">                              

                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-envelope"></i>
                                        </span>
                                        <input type="email" name="email" class="form-control" placeholder="Please Enter Email" autocomplete="off" />
                                    </div>
                                </div>
                            </div>
							 <div class="form-group">
                                <label class="col-md-3 control-label">Alternate Email</label>
                                <div class="col-md-6">                              

                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-envelope"></i>
                                        </span>
                                        <input type="email" name="alternateemail" class="form-control" placeholder="Please Enter Email" autocomplete="off" />
                                    </div>
                                </div>
                            </div>
							
                            <div class="form-group">
                                <label class="col-md-3 control-label">Age</label>
                                <div class="col-md-6">
                                    <input type="text" name="age" class="form-control" placeholder="Please Enter Age" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label">Gender</label>
                                <div class="col-md-6">
                                    <select class="form-control" name="gender" id="gender">
                                        <option value=""> -- Select Gender --</option>
                                        <option value="male">Male</option>
                                        <option value="female">Female</option>
                                        <option value="not_to_say">Prefer Not to say</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Marital Status</label>
                                <div class="col-md-6">
                                    <select class="form-control" name="marital_status" id="marital_status" >
                                        <option value=""> -- Select Marital Status --</option>
                                        <option value="single">Single</option>
                                        <option value="married">Married</option>
                                        <option value="widow">Widow</option>
                                        <option value="divorced">Divorced</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label">Child</label>
                                <div class="col-md-6">
                                    <select class="form-control" name="child" id="child">
                                        <option value=""> -- Select Child --</option>
                                        <option value="0">0</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="5+">5+</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label">State <span class="required">*</span></label>
                                <div class="col-md-6">
                                    <select class="form-control" name="state" id="state">
                                        <option value="">Select State</option>
                                        @foreach($states as $key=>$val)
                                        <option value="{{$key}}">{{$val}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label">City <span class="required">*</span></label>
                                <div class="col-md-6">
                                    <select class="form-control" name="city" id="city">
                                        <option value="">Select City</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label">Address <span class="required">*</span></label>
                                <div class="col-md-6">
                                    <textarea style="width:100%;" class="form-control" rows="3" name="address" data-error-container="#editor1_error" placeholder="Please Enter Address" >{{Request::old('address')}}</textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Landmark <span class="required">*</span></label>
                                <div class="col-md-6">
                                    <input type="text" name="landmark" data-required="1" class="form-control" placeholder="Please Enter Landmark" value="{{Request::old('landmark')}}" />

                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Pincode </label>
                                <div class="col-md-6">
                                    <input type="text" name="pincode" data-required="1" class="form-control" placeholder="Please Enter Pincode" value="{{Request::old('pincode')}}" />

                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Company Name</label>
                                <div class="col-md-6">
                                    <input type="text" name="company_name" data-required="1" class="form-control" placeholder="Please Enter Company Name" value="{{Request::old('company_name')}}" />

                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Department</label>
                                <div class="col-md-6">
                                    <input type="text" name="department" data-required="1" class="form-control" placeholder="Please Enter Department" value="{{Request::old('department')}}" />

                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Profession</label>
                                <div class="col-md-6">
                                    <input type="text" name="profession" data-required="1" class="form-control" placeholder="Please Enter Profession" value="{{Request::old('profession')}}" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label">Income </label>
                                <div class="col-md-6">
                                    <input type="text" name="income" data-required="1" class="form-control" placeholder="Please Enter Income" value="{{Request::old('income')}}" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label">Plan Informed</label>
                                <div class="col-md-6">
                                    <select class="form-control" name="planinformed" id="informed" >
                                        <option value="no">No</option>
                                        <option value="yes">Yes</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Expected EMI Amount</label>
                                <div class="col-md-6">
                                    <input type="text" name="emi_size" data-required="1" class="form-control" placeholder="Please Enter Emi Amount" value="{{Request::old('emi_size')}}" id="emi_size" />

                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label">Lead Source</label>
                                <div class="col-md-6">
                                    <select class="form-control" name="leadsourcevalue" id="leadsourcevalue">
                                        <option value="">Select State</option>
                                        <?php
                                        foreach ($leadsourcevalue as $val) {
                                            ?>

                                            <option value="<?php echo $val->leadsource_id ?> "><?php echo $val->leadsource_name ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>


                        </div>
                        <footer class="panel-footer">
                            <div class="row">
                                <div class="col-sm-12" align="center">
                                    <button type="submit" class="btn green" >Submit</button>&nbsp;&nbsp;&nbsp;&nbsp;
                                    <a href="{{ URL::to('lead-list')}}" class="btn btn-default" >Cancel</a>
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                </div>
                            </div>
                        </footer>
                    </section>
                </form>
                </div>
            </div>
        </section>
    </section>
@endsection
@section('page_level_script_bottom')
    <script src="{{URL::asset('public/assets/vendor/jquery-validation/jquery.validate.js')}}"></script>
    <script src="{{URL::asset('public/assets/javascripts/forms/form-validation.js')}}"></script>
    <script src="{{URL::asset('public/assets/javascripts/app.js')}}"></script>
    <script>
    jQuery(document).ready(function () {
        App.init();
        
        FormValidation.init();
        $('#loading').hide();
    });

    $("#state").change(function () {
        var stateID = $("#state").val();
        var token = $("#_tokensc").val();
        $('#statename').val($("#state option:selected").html());
        $("#city option").not(':first').remove();
        if (stateID) {
            $.ajax({
                url: "{{route('getcitylist')}}",
                data: {'stateid': stateID, '_token': token},
                type: 'post',
                async: true,
                cache: false,
                clearForm: false,
                beforeSend:function(){	
    		      $('#loading').show();
                },
                success: function (response) {
                    $.each(response, function (idx, obj) {
                        $("#city").append(
                            $("<option></option>").text(obj).val(idx)
                        );
                    });
                },
                error:function(){
                    alert("Server is Busy!!");
                },
                complete:function (data) {
                    $('#loading').hide();
                }
            });
        } else {
            //$('#city').html('<option value="">No states</option>');
        }
    });
    $('#city').change(function(){
        $('#cityname').val($('#city option:selected').html());
    });

    function formsubmit(obj){
        var flag = false;
        var mobile = $('input[name="mobile"]').val();
        $.ajax({
            url: "{{route('check-mobile')}}",
            data: {'mobile': mobile},
            type: 'get',
            async: true,
            cache: false,
            clearForm: false,
            beforeSend:function(){  
              $('#loading').show();
            },
            success:function (response) {
                if(response== undefined){
                    var check = function(){
                        callback();
                    };
                }
                if(response==1){
                    alert("Mobile No Already Exists!!!");
                }else{
                    obj.submit();
                }
            },
            error:function(){
                alert("Server is Busy!!");
            },
            complete:function (data) {
                $('#loading').hide();
            }
        });
        return false;
    }

    function callback(){
        return check();
    }
</script>

@endsection
