@extends('layout.master')

@section('page_title')
	Verifier List
@endsection

@section('breadcrumb')
	<li>
		<a href="#">Verifier List</a>
	</li>
@endsection

@section('page_level_style_top')
	<link rel="stylesheet" type="text/css" href="{{URL::to('assets/plugins/select2/select2_metro.css')}}"/>
	<link rel="stylesheet" type="text/css" href="{{URL::to('assets/plugins/data-tables/DT_bootstrap.css')}}"/>
@endsection

@section('content')
	<div class="row">
		<div class="col-md-12">
			<div class="portlet box blue">
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-reorder"></i>Verifier List
					</div>
				</div>
				<div class="portlet-body">
					@if(Session::has('flash_message'))
                    	<div class="alert alert-success text-center ">
                        	{{ Session::get('flash_message') }}
                        </div>
                    @endif
					<div class="tabbable tabbable-custom">
						<div class="tab-content">
							<div class="table-scrollable">
								<table class="table table-striped table-hover table-bordered">
									<thead>
										<tr id="tbl">
											<th> Id	</th>
											<th> Name </th>
											<th> Entry Date </th>
											<th> Type </th>
											<th> Quality </th>
											<th> Appointment Date </th>
											<th> Assigned By </th>
											<th> Actions </th>
										</tr>
									</thead>
				
									<tbody id="tbl">
										<?php $count=1 ;
										?>
										@foreach($results as $keychk=>$data)
											<tr>
												<td>
													{{$data['reference_id']}}
												</td>
												<td>
													{{$data['name']}}
												</td>

												<td>
													{{date("d-m-Y",strtotime($data['created']))}}
												</td>											
												<td>
													@if(empty($data['customer_id']) || $data['customer_id']==0)
														Lead
													@else
														Reference
													@endif
												</td>
												
												<td>
												    <select name="quality" class="form-control" id="sel{{ $keychk }}">
		                                 				<option value="">-- Select --</option>	
                						 				@foreach($rm as $key=>$value)
                						   					<option value="{{ $key }}">{{ $value }}</option>
                						   				@endforeach
				                					</select>
				                				</td>
				                		
				                					<td>
														{{ date('d-m-Y H:i',strtotime($data['appointment_date']))}}
													</td>

													<td>
														@if($data['stage_id']==8)
															Assign by VOC
														@else
															Assign by TL
														@endif
													</td>
				                			
				                				<td>
				                					<a  class="btn assign" data-id="{{ $data['reference_id'] }}" data-key="{{ $keychk }}" data-ref="{{ $data['reference_id'] }}">Assign</a>
				                				</td>

											</tr>
										<?php $count++;?>

										@endforeach
									</tbody>
								</table>
							</div>
						</div>
						<div class ="pull-right" style="margin-bottom:20px">
							{!! $results->appends(['q'=>$q,'search_option'=>$search_option,'sort'=>$sort,'sorttype'=>$sorttype])->render() !!}
  						</div>
					</div>
				</div>
			</div>
		</div>
	</div>	
@endsection

@section('page_level_script_bottom')
	<!-- BEGIN PAGE LEVEL PLUGINS -->
	<script type="text/javascript" src="{{URL::to('assets/plugins/select2/select2.min.js')}}"></script>
	<script type="text/javascript" src="{{URL::to('assets/plugins/data-tables/jquery.dataTables.js')}}"></script>
	<script type="text/javascript" src="{{URL::to('assets/plugins/data-tables/DT_bootstrap.js')}}"></script>
	<script src="{{URL::to('assets/plugins/bootbox/bootbox.min.js')}}" type="text/javascript"></script>
	<script src="{{URL::to('assets/scripts/app.js')}}"></script>
	<script src="{{URL::to('assets/scripts/table-editable.js')}}"></script>
	<script>
		jQuery(document).ready(function() {  
		   	App.init();      
		   	TableEditable.init();
		});

		$('.assign').click(function(){
			var rm = $('#sel'+$(this).data('key')).val();
			if(rm==""){
				alert("Select Quality!!!");
				return false;
			}
			var ref = $(this).data('ref'); 
			$.ajax({
      			url: "{{ route('assign-quality')}}",
      			data:{'ref_id': ref,'quality_id':rm},    
      			type: 'get',
      			dataType: 'json',
      			async: "true",
      			success: function(response){
      				window.location.reload();		
      			},
    		});
		});
	</script>
@endsection