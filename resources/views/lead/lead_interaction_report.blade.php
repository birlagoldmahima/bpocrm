@extends('layout.master')
@section('page_title')
Lead Interaction Report
@endsection
@section('breadcrumb')
<li>
    <a href="#">Lead Interaction Report </a>
</li>
@endsection
@section('content')
<section role="main" class="content-body">
    <header class="page-header">
        <h2>Lead Interaction Report</h2>

        @if(Session::has('flash_message'))
        <div class="alert alert-success text-cente ">
            {{ Session::get('flash_message') }}
        </div>
        @endif
        @if($errors->any())
        <div class="alert alert-danger text-cente">
            @foreach($errors->all() as $error)
            <p>{{ $error }}</p>
            @endforeach
        </div>
        @endif

        <div class="right-wrapper pull-right">
            <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
        </div>
    </header>

    <!-- start: page -->
    <section class="panel">
        <header class="panel-heading">
            <h2 class="panel-title">Lead Interaction Report</h2>
        </header>
        <div class="panel-body">
            <form method="get" name="frm" id="frm" action="{{ url('leadinteractionreport') }}">
                <div class="form-group row">
                    <label class="col-md-1 control-label">Email:</label>
                    <div class="col-md-3">
                        <input type="text" name="searchemail" class="form-control" value="{{isset($searchemail)?$searchemail:""}}">
                    </div>
                    <div class="col-md-1">
                        <button type="submit" class="btn blue">Submit</button>
                    </div>
                    <div class="col-md-1">
                        <a class="btn red"  href="{{ url('leadinteractionreport')}}">Clear All</a>
                    </div>

                    <div class="col-md-1">
                        <a class="btn btn-success" href="{{ URL::route('leadinteractionreport',['searchemail'=>$searchemail,'flag'=>1])}}">Download Report</a>
                    </div>
                </div>

            </form>
            <table class="table table-bordered" id="datatable-details">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Category</th>
                        <th>Current Status</th>
                        <th>Current Sub Status</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $i = 1; ?>
                    @foreach ( $data as $key => $value)
                    <tr class="gradeX">
                        <td>{{$i}}</td>
                        <td>{{$value[0]['name']}}</td>
                        <td>{{$value[0]['email']}}</td>
                        <td class="center">{{$value[0]['mobile']}}</td>
                        <td class="center">{{$value[0]['state_name']}}</td>
                        <td class="center">{{$value[0]['city_name']}}</td>
                    </tr> 
                    <?php $i++; ?>
                    @endforeach

                </tbody>
            </table>
            <?php $i = 1; ?>
            @foreach ( $data as $key => $value )
            <?php $a = '<table class="" >'; ?>
            <?php $a .= '<thead>
											<tr>
											<th>Status</th>
											<th>Sub Status</th>
											<th>Status Date</th>
											<th>Sub Status Date</th>
											<th>Remark</th>
											<th>Status Type</th>
											</tr>
										   </thead>'; ?>
            <?php
            foreach ($value as $keytest => $valuetest) {
                $a .= '<tr class="b-top-none">
												   <td>' . $valuetest["call_initiator"] . '</td>
												   <td>' . $valuetest["call_status_id"] . '</td>
												   <td>' . $valuetest["call_substatus_id"] . '</td>
												   <td>' . $valuetest["call_substatus_other"] . '</td>
												   <td>' . $valuetest["callback_datetime"] . '</td>
												   <td>' . $valuetest["remarks"] . '</td>
												   </tr>
												  ';
            }
            ?>
            <?php $a .= '</table>'; ?>
            <span id="{{$i}}" style="display:none;"><?php echo $a; ?></span>
            <?php $i++; ?>
            @endforeach
        </div>
    </section>
    <!-- end: page -->
</section>
@endsection
@section('page_level_script_bottom')
<style>
    $(document).ready(function() {
        $('#datatable-details').DataTable( {
            "paging":   false,
                "ordering": false,
                "info":     false
        } );
    } );
</style>
@endsection			