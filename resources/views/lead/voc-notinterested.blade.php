@extends('layout.master')
@section('page_title')
	Not Interested Voc List 
@endsection
@section('breadcrumb')
<li>
	<a href="#">Not Interested Voc List </a>
</li>
@endsection
@section('page_level_style_top')
	<link rel="stylesheet" type="text/css" href="{{URL::to('assets/plugins/select2/select2_metro.css')}}"/>
  	<link rel="stylesheet" type="text/css" href="{{URL::to('assets/plugins/data-tables/DT_bootstrap.css')}}"/>
  	<link rel="stylesheet" type="text/css" href="{{URL::to('assets/plugins/bootstrap-datepicker/css/datepicker.css')}}"/>
  	<link rel="stylesheet" type="text/css" href="{{URL::to('assets/plugins/bootstrap-timepicker/compiled/timepicker.css')}}"/>
  	<link rel="stylesheet" type="text/css" href="{{URL::to('assets/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css')}}"/>
  	<link rel="stylesheet" type="text/css" href="{{URL::to('assets/plugins/bootstrap-datetimepicker/css/datetimepicker.css')}}"/>
@endsection
@section('content')
	<div class="row">
		<div class="loading" id="loading" style="display:none">
  			<img class="loading-image" id="loading-image" src="{{URL::to('assets/img/ajax-loading.gif')}}" alt="Loading..." />
		</div>
		<div class="col-md-12">
			<div class="portlet box litgreen">
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-reorder"></i>Not Interested Voc List 
					</div>
				</div>
				<div class="portlet-body">
					<form  method="post"  action="">
						<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
						<input type="hidden" name="currentpage" value="{{ !empty($page)?$page:"1" }}">
						<div class="table-scrollable">
							<table class="table table-striped table-hover table-bordered">
								<thead>
									<tr>
										<th>
											ID
										</th>
										<th>
											Lead Name
										</th>
										<th>
											Mobile
										</th>
										<th>
											Email
										</th>
										<th>
											RM
										</th>
										<th>
											Appointment Date
										</th>
										<th>
											Action
										</th>
									</tr>
								</thead>
								<tbody>
									<?php $count=1 ;
									$array = array_merge_recursive(config('custom.Met'),config('custom.Notmet'));?>
									@foreach($results as $key=>$data)
										<tr>
											<td>
												{{ $data['reference_id']}}
											</td>
											<td>
												{{$data['name']}}
											</td>
											<td>
												{{$data['mobile']}}
											</td>
											<td>
												{{ $data['email']}}
											</td>
											<td>
												{{ $data['managername']  }}
											</td>
											<td>
												{{ $data['tlname'] }}
											</td>
											<td>
												<a data-toggle="modal" data-lead="{{$data['reference_id']}}" data-target="#remarks" class="btn btn-primary responsedata">Response</a>
											</td>

											
										</tr>
										<?php $count++;?>
									@endforeach
								</tbody>
							</table>
						</div>
					</form>
					<div class ="pull-right" style="margin-bottom:20px">
  				 	</div>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="remarks" tabindex="-1" role="basic" aria-hidden="true" data-keyboard="false" data-backdrop="static">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title text-purple"><b>Interaction</b></h4>
				</div>
				<div class="modal-body" >
					<div class="row form-group">
						<div class="col-md-12" style='overflow:auto;height:500px;'>
							<h4>Interaction</h4>
							<div class="table-scrollable">
								<table class="table table-striped table-hover table-bordered">
									<thead>
										<tr>
											<th>#</th>
											<th>Created Date</th>
											<th>Logged By</th>
											<th>Call Status</th>
											<th>Call Sub Status</th>
											<th>Remarks</th>
										</tr>
									</thead>
									<tbody id="interaction">
									</tbody>
								</table>
							</div>
							<h4>Verifier Interaction</h4>
							<div class="table-scrollable">
								<table class="table table-striped table-hover table-bordered">
									<thead>
										<tr>
											<th>#</th>
											<th>Created Date</th>
											<th>Logged By</th>
											<th>Call Status</th>
											<th>Call Sub Status</th>
											<th>Remarks</th>
										</tr>
									</thead>
									<tbody id="verfierinteraction">
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn default" data-dismiss="modal" id="closebtn">Close</button>
				</div>
			</div>
		</div>
	</div>
@endsection
@section('page_level_script_bottom')
	<script type="text/javascript" src="{{URL::to('assets/plugins/jquery-validation/dist/jquery.validate.min.js')}}"></script>
	<script type="text/javascript" src="{{URL::to('assets/plugins/select2/select2.min.js')}}"></script>
	<script type="text/javascript" src="{{URL::to('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
	<script type="text/javascript" src="{{URL::to('assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js')}}"></script>
	<script type="text/javascript" src="{{URL::to('assets/plugins/clockface/js/clockface.js')}}"></script>
	<script type="text/javascript" src="{{URL::to('assets/plugins/bootstrap-daterangepicker/moment.min.js')}}"></script>
	<script type="text/javascript" src="{{URL::to('assets/plugins/bootstrap-daterangepicker/daterangepicker.js')}}"></script>

	<script type="text/javascript" src="{{URL::to('assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.js')}}"></script>
	<script type="text/javascript" src="{{URL::to('assets/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js')}}"></script>
	<script type="text/javascript" src="{{URL::to('assets/plugins/jquery.input-ip-address-control-1.0.min.js')}}"></script>
	<script type="text/javascript" src="{{URL::to('assets/plugins/jquery-multi-select/js/jquery.multi-select.js')}}"></script>
	<script type="text/javascript" src="{{URL::to('assets/plugins/jquery-multi-select/js/jquery.quicksearch.js')}}"></script>

	<script src="{{URL::to('assets/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js')}}" type="text/javascript"></script>
	<script src="{{URL::to('assets/plugins/jquery-tags-input/jquery.tagsinput.min.js')}}" type="text/javascript"></script>
	<script src="{{URL::to('assets/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js')}}" type="text/javascript"></script>
	<script src="{{URL::to('assets/plugins/bootstrap-touchspin/bootstrap.touchspin.js')}}" type="text/javascript"></script>
	<script src="{{URL::to('assets/scripts/app.js')}}"></script>
	<script src="{{URL::to('assets/scripts/form-validation.js')}}"></script>
	<script src="{{URL::to('assets/scripts/form-components.js')}}"></script>
	<script>
		jQuery(document).ready(function() { 
			App.init(); 
		});

		$('.responsedata').click(function(){
			var leadid=$(this).data('lead');
			$.ajax({
				url: "{{ URL::to('rm-rejected-reason') }}/"+$(this).data('lead'),
				//data: {'id':$(this).data('lead')}, 
				type: 'get',
				async:true,
				cache: false,
				beforeSend:function(){	
					$('#loading').show();
				},
				success:function(response){
					var append="";
					$('#interaction,#verfierinteraction').html('');
					$.each(response['interaction'],function(index,item){
	      				append+='<tr><td>'+(index+1)+'</td>';
	      				append+='<td>'+(item['created_at'])+'</td>';
	      				append+='<td>'+(item['rmname'])+'</td>';
	      				append+='<td>'+(item['call_status'])+'</td>';
	      				append+='<td>'+(item['call_sub_status'])+'</td>';
	      				append+='<td>'+(item['remarks'])+'</td></tr>';
					});
					$('#interaction').append('<tr>'+append+'</tr>');
					var append="";
					$.each(response['verifier'],function(index,item){
	      				append+='<tr><td>'+(index+1)+'</td>';
	      				append+='<td>'+(item['created_at'])+'</td>';
	      				append+='<td>'+(item['rmname'])+'</td>';
	      				append+='<td>'+(item['call_status'])+'</td>';
	      				append+='<td>'+(item['call_sub_status'])+'</td>';
	      				append+='<td>'+(item['remarks'])+'</td></tr>';
					});
					$('#verfierinteraction').append('<tr>'+append+'</tr>');
				},
				error:function(){
					alert("Server is Busy!!");
				},
				complete:function (data) {
	    			$('#loading').hide();
	    		}
			});
   		})


	</script>
@endsection
