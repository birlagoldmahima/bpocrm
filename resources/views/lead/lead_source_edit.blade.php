@extends('layout.master')
@section('page_title')

@endsection
@section('breadcrumb')
<li>
    <a href="#">Add New Lead Source</a>
</li>
@endsection
@section('page_level_style_top')
@endsection
@section('content')
<section role="main" class="content-body">
    @if($errors->any())
    <div class="alert alert-danger text-cente">
        @foreach($errors->all() as $error)
        <p>{{ $error }}</p>
        @endforeach
    </div>
    @endif
    <div class="row">

        <div class="col-md-12">
            <div class="col-md-12">
                <form id="form" action="{{ url('edit_leadsourcedata') }}" class="form-horizontal form-bordered" method="post">
                    <input type="hidden" name="_token" id="_tokensc" value="{{ csrf_token() }}">
                    <section class="panel">
                        <header class="panel-heading">
                            <h2 class="panel-title">Edit Lead Source</h2>
                        </header>
                        <div class="panel-body">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Edit Lead Source Name <span class="required">*</span></label>
                                <div class="col-md-6">
                                    <input type="hidden" name="leadsource_id" class="form-control" value="<?php echo $usersdata[0]['leadsource_id']; ?>" />
                                    <input type="text" name="leadsourcename" class="form-control" value="<?php echo $usersdata[0]['leadsource_name']; ?>" placeholder="Please Enter Value" required/>
                                </div>
                            </div>
                        </div>
                        <footer class="panel-footer">
                            <div class="row">
                                <div class="col-sm-12" align="center">
                                    <button type="submit" class="btn green" >Submit</button>&nbsp;&nbsp;&nbsp;&nbsp;
                                    <a href="{{ URL::to('lead_source')}}" class="btn btn-default" >Cancel</a>

                                </div>
                            </div>
                        </footer>
                    </section>
                </form>
            </div>
        </div>
    </div>
</section>


@endsection
@section('page_level_script_bottom')
<script src="{{URL::asset('public/assets/javascripts/app.js')}}"></script>
    <script>
    jQuery(document).ready(function() { 
       App.init();
       $('.pagination li a').addClass('clk');
       $('#loading').hide();
    });
</script>
@endsection


