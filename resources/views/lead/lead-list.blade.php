@extends('layout.master')
@section('page_title')
    Lead List 
@endsection
@section('page_level_style_top')
    <header class="page-header">
        <h2>Lead List</h2>
        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li>
                    <a href="#">
                        <i class="fa fa-home"></i>
                    </a>
                </li>
                <li><span>Lead</span></li>
                <li><span>Lead List</span></li>
            </ol>

            <a class="sidebar-right-toggle" data-open="#"><i class="fa fa-chevron-left"></i></a>
        </div>
    </header>
@endsection
@section('content')
    <section role="main" class="content-body">
        <section class="panel">
            <header class="panel-heading">
                <h2 class="panel-title">Lead List</h2>
            </header>
            <div class="panel-body">
                <div class="col-md-12">
                    <form class="navbar-form" role="search" action="{{ route('lead-list') }}" name="frm" id="frm">
                        <div class="row">
                            <label class="col-md-1 control-label">Search</label> 
                            <div class="col-md-3 form-group">
                                <input type="text" name='q' value="{{ $q }}" class="form-control" id="q">
                            </div>
                           <div class="form-group col-md-3">
                                <select class="form-control" name="option" id="option">
                                    <option value=""> -- Select Status -- </option>
                                    <option value="ID" @if($option=='ID') selected @endif>ID</option>
                                    <option value="Name" @if($option=='Name') selected @endif>Name</option>
                                    <option value="Email" @if($option=='Email')selected @endif>Email</option>                           
                                    <option value="Mobile" @if($option=='Mobile') selected @endif>Mobile</option>
                                </select>   
                            </div>
                            <div class="form-group col-md-3">
                                <input type="submit" class="btn" value="Search">
                                <a href="{{ route('lead-list') }}" class="btn btn-primary" >Clear</a>
                            </div>
                        </div>                                                  
                    </form>
                </div>
                <table class="table table-bordered table-striped mb-none">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Mobile</th>
                            <th>State</th>
                            <th>City</th>
                            <th>Tell Caller</th>
                            <th>Call Status</th>
                            <th>Call Sub Status</th>
                            <th> Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($result) > 0)
                            <?php $call_status = callstatus();
                            $sub_call_status= callsubstatus(); ?>
                            @foreach ($result as $key => $val)
                                <tr>
                                    <td>
                                        <a href="{{ URL::to('lead-details',$val->reference_id)}}" class="on-default edit-row">{{$val->reference_id}}</a>
                                    </td>
                                    <td>
                                        <?php echo empty($val->name) ? "-" : $val->name; ?>
                                    </td>
                                    <td>
                                        <?php echo $val->email; ?> 
                                    </td>
                                    <td class="center">
                                        <?php echo $val->mobile; ?> 
                                    </td>
                                    <td class="center">
                                        <?php echo $val->state_name; ?>  
                                    </td>
                                    <td class="center">
                                        <?php echo $val->city_name; ?>  
                                    </td>
                                    <td class="center">
                                        <?php echo $val->username; ?>  
                                    </td>
                                    <td class="center">
                                        {{ $call_status[$val->call_status_id] or "" }}
                                    </td>
                                     <td class="center">
                                        {{ $sub_call_status[$val->call_substatus_id]  or "" }}
                                    </td>
                                    <td class="center hidden-phone"> 
                                    <a href="{{ URL::route('edit-lead',$val->reference_id)}}" class="on-default edit-row"><i class="fa fa-pencil"></i></a> / 
                                    <a href="{{ URL::to('lead-details',$val->reference_id)}}" class="on-default edit-row"><i class="fa fa-sitemap"></i></a> /  
                                    <a href="{{ URL::route('leadrequest',$val->reference_id)}}" class="on-default edit-row"><i class="fa fa-info-circle"></i></a>
                                </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="8" align="center">
                                    Not Data Found
                                </td>
                            </tr>
                        @endif
                    </tbody>
                </table>
                <div class ="pull-right" style="margin-bottom:20px">
                    {!! $result->appends(['q'=>$q,'option'=>$option])->render() !!}
                </div>
            </div>
        </section>
    </section>
@endsection
@section('page_level_script_bottom')
    <script>
        jQuery(document).ready(function () {
            $('#loading').hide();
        });

        $('#frm').submit(function(){
            $('#loading').show();
            var search = $('#q').val()=="" ? $('#q').focus() : true;
            var option = $('#option option:selected').val()=="" ? $('#option').focus() : true;
            if(search==true && option==true){
                return true;
            }
            $('#loading').hide();
            return false;
        });
    </script>
@endsection			