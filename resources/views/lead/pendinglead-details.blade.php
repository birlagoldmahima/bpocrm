@extends('layout.master')
@section('page_title')
Lead Interaction
@endsection
@section('page_level_style_top')
<link rel="stylesheet" type="text/css" href="http://cherishgold.com/crm/assets/plugins/bootstrap-datetimepicker/css/datetimepicker.css">
<header class="page-header">
    <h2>Lead Interaction</h2>
    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="#">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span>Dashboard</span></li>
            <li><span>Add Interaction</span></li>
        </ol>
        <a class="sidebar-right-toggle" data-open="#"><i class="fa fa-chevron-left"></i></a>
    </div>
</header>
@endsection
@section('content')
<?php
    $call_status = callstatus();
    $sub_call_status = callsubstatus();
?>
<section role="main" class="content-body">
    <div class="row">
        <div class="col-md-12">
            <section class="panel panel-featured panel-featured-primary">
                <header class="panel-heading">
                    <h2 class="panel-title">Lead Details</h2>
                </header>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-condensed mb-none">
                            <tbody>
                                <tr>
                                    <td>Lead ID :</td>
                                    <td>
                                        <div id="pulsate-regular">
                                            LEAD-{{$reference_data['reference_id']}}
                                        </div>
                                    </td>
                                    <td>Phone:</td>
                                    <td>
                                        <div id="pulsate-regular">
                                            {{$reference_data['mobile']}}
                                        </div>
                                    </td>

                                </tr>
                                <tr>
                                    <td>Name :</td>
                                    <td>
                                        <div id="pulsate-once-target">
                                            {{$reference_data['name'] }}
                                        </div>
                                    </td>
                                    <td>Email :</td>
                                    <td>
                                        <div id="pulsate-regular">
                                            {{$reference_data['email']}}
                                        </div>
                                    </td>


                                </tr>
                                <tr>
                                    <td>
                                        Relationship Manager :
                                    </td> 
                                    <td>
                                        <div id="pulsate-crazy-target">
                                            {{ $rms[$reference_data['rel_manager_id']] or "" }}		
                                        </div>
                                    </td>
                                    <td>
                                        Status :
                                    </td>
                                    <td>
                                        {{$reference_data['status'] }}   {{ (isset($interaction[0]['call_substatus_id']) ? '/ '.$sub_call_status[$interaction[0]['call_substatus_id']] : "" )  }}
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        Created at
                                    </td>
                                    <td>
                                        {{ date('d-m-Y',strtotime($reference_data['created'])) }}
                                    </td>
                                    <td>
                                        Amount:
                                    </td>
                                    <td>
                                        <div id="pulsate-crazy-target">
                                            {{$reference_data['emi_size']}}
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <button class="btn btn-primary link">Generate Link</button>
                                    </td>
                                    <td colspan="3">   
                                        <div id="generatelink">
                                        </div>  
                                    </td>
                                </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
            </section>
        </div>
        <div class="col-md-12">
            <section class="panel panel-featured panel-featured-primary">
                <header class="panel-heading">
                    <h2 class="panel-title">Interaction</h2>
                </header>
                <form class="form-horizontal form-bordered"  role="form" method="post" id="customer-interaction">
                    <div class="panel-body">
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="inputSuccess">Call Status</label>
                            <div class="col-md-6">
                                <select class="form-control" name="callstatus" id="callstatus" onchange="getSubStatus();">
                                    <option value="">Select Call Status</option>
                                    @if(isset($callstatus['data']['callStatus']))
                                    @foreach($callstatus['data']['callStatus'] as $key=>$val)
                                    @if($val['status']=='active')
                                    <option value="{{$val['status_id']}}">{{$val['name']}}</option>
                                    @endif
                                    @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="inputSuccess">Call Sub Status</label>
                            <div class="col-md-6">
                                <select class="form-control" name="callsubstatus" id="callsubstatus" onchange="getOthers();">
                                    <option value="">Select Call Sub Status</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group substatus">
                            <label class="col-md-3 control-label" for="inputSuccess">Call Sub Status Other </label>
                            <div class="col-md-6">
                                <input type="text" class="form-control substatusother" name="call_sub_status_other" id="call_sub_status_other">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Next Call back Date/Time </label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                    <input type="text"  class="form-control form_datetime" readonly name="nextdatetime" id="nextdatetime">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="textareaDefault">Remarks</label>
                            <div class="col-md-6">
                                <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                                <input type="hidden" name="reference_id" id="reference_id" value="{{ $reference_data['reference_id'] }}">
                                <textarea class="form-control" name="interaction" id="interaction" placeholder="Brief your call summary" rows="3" style="resize:none;"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <div class="row">
                            <div class="col-sm-12" align="center">
                                <input type="hidden" id="_tokencom" value="{{ csrf_token() }}">
                                <button type="button" data-toggle="modal"  class="btn red appointment">Fix Appointment</button>
                                <button type="submit" class="btn green interactionsubmit" id="interactionsubmit">Submit</button>
                                <button type="button" class="btn btn-danger" onclick="cancelIntersaction()">Cancel</button>
                                <a class="btn btn-info" href="#emailsend" data-toggle="modal" onclick='document.getElementById("emailtitle").value = ""; document.getElementById("mailsendbtn").disabled = true;'>Email</a>
                            </div>
                        </div>
                    </div>
                </form>
            </section>
        </div>
        <div class="col-md-12">
            <section class="panel panel-featured panel-featured-primary">
                <div class="panel-body">
                    <header class="panel-heading">
                        <i class="fa fa-comments"></i>Previous Interaction (<b id="interactioncount">{{ count($interaction) }}</b>)
                    </header>
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-condensed mb-none">
                            <thead>
                                <tr>
                                    <th>Date</th>
                                    <th>Logged by</th>
                                    <th>Call Status</th>
                                    <th>Call Sub Status</th>
                                    <th>Call Back</th>
                                    <th>Remarks</th>
                                </tr>
                            </thead>
                            <tbody id="tbodyajaxdata">

                                @if(count($interaction) >0)
                                @foreach($interaction as $key=>$value)
                                <tr>  
                                    <td> 
                                        {{date("M d, Y H:i",strtotime($value['created_at'])+5*3960)}}
                                    </td>
                                    <td>
                                        {{$rms[$value['rm_id']] or ""}}
                                    </td>
                                    <td>
                                        {{ $call_status[$value['call_status_id']] }}
                                    </td> 
                                    <td>
                                        {{ $sub_call_status[$value['call_substatus_id']] }}
                                    </td>
                                    <td>
                                        @if($value['callback_datetime'] != "0000-00-00 00:00:00")
                                        {{date("M d, Y H:i",strtotime($value['callback_datetime']))}}
                                        @endif
                                    </td>
                                    <td>
                                        <p type="text" data-toggle="modal" onclick="viewremarks({{ $key++ }})" data-target="#remarks_interaction" class="btn purple"><a data-toggle="tooltip" title="{{ $value['remarks'] }}" data-placement="left"><i class="fa fa-envelope"></i></a></p>
                                    </td>
                                </tr>
                                @endforeach
                                @endif
                            </tbody> 
                             
                        </table>
                    </div>
                </div>
            </section>
        </div>
</section>
</div>
</div>
</section>
<div class="modal fade" id="remarks_interaction" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title text-purple"><b>Remarks</b></h4>
            </div>
            <div class="modal-body" >
                <p id="remarkinteraction"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="appointment" tabindex="-1" role="basic" aria-hidden="true"  data-keyboard="false" data-backdrop="static" style="z-index:10000000;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Customer Appointment</h4>
            </div>
            <div class="modal-body">
                <span id="appointfed"></span>
                <div class="form-body">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-4 control-label">Appointment Datetime </label>
                            <div class="col-md-6">  
                                <input type="text" name="appointment_date" id="appointmentdate" class="form-control form_datetime" data-required="1" readonly placeholder="Please Select Appointment Date" data-plugin-datepicker value="{{$reference_data['appointment_date']}}"/>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
            <div class="modal-footer">
                @if((in_array($reference_data['stage_id'],[2,4]) && $reference_data['stage_remarks']=="rejected") || empty($reference_data['appointment_date']))
                <button type="button" class="btn green" onclick="saveappointment();">Save</button>
                @endif
                <button type="button" class="btn default" data-dismiss="modal">Close</button>
            </div>

        </div>
    </div>
</div>

<div class="modal fade" id="sendlink" tabindex="-1" role="basic" aria-hidden="true"  data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Send SMS & Email</h4>
            </div>
            <div class="modal-body">
                <div class="form-body">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-2 control-label">Link:</label>
                            <div class="col-md-10">  
                                <textarea class="form-control"  rows="4" cols="50" readonly id="linkvalue"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-2 control-label">Email:</label>
                            <div class="col-md-10 emailid"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-2 control-label">Mobile:</label>
                            <div class="col-md-10 mobileno"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn green" onclick="sendsms();">Send</button>
                <button type="button" class="btn default" data-dismiss="modal">Close</button>
            </div>

        </div>
    </div>
</div>

<div class="modal fade" id="emailsend" tabindex="-1" role="basic" aria-hidden="true"  data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Send Email</h4>
            </div>
            <div class="modal-body">
                <div class="form-body">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-2 control-label">Title:</label>
                            <div class="col-md-10">
                                <select class="form-control" name="emailtitle" id="emailtitle">
                                    <option value="">Select Title</option>
                                    @foreach($emailtemplate as $key=>$val)
                                    <option value="{{$val['id']}}">{{$val['title']}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group message" style="display:none;">
                        <div class="row">
                            <div class="alert content col-md-8 col-md-offset-3"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn green" onclick="sendemail();" id="mailsendbtn" disabled>Send</button>
                <button type="button" class="btn default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


@endsection
@section('page_level_script_bottom')
<script src="{{URL::asset('public/assets/vendor/jquery-validation/jquery.validate.js')}}"></script>
<script type="text/javascript" src="http://cherishgold.com/crm/assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
<script src="{{URL::asset('public/assets/javascripts/forms/form-validation.js')}}"></script>
<script src="{{URL::asset('public/assets/javascripts/app.js')}}"></script>
<script src="{{URL::asset('public/assets/ckeditor/ckeditor.js')}}"></script>
<script>
                    var datainteractionajax = {!! json_encode(array_map(function ($arr) {return $arr['remarks']; }, $interaction)) !!};
                    jQuery(document).ready(function() {
                    $(".substatus").hide();
                    var end = new Date();
                    var d = new Date("{!! date('D M d Y H:i:s',strtotime('+5 hours +30 minutes', strtotime(date('Y-m-d H:i:s')))) !!}");
                    App.init();
                    FormValidation.init();
                    var end = new Date();
                    end.setFullYear(end.getFullYear() - 18);
                    $('#nextdatetime,#appointmentdate').datetimepicker({
                    format: "dd-mm-yyyy hh:ii:ss",
                            startDate: new Date()
                    });
                    $('.appointment').prop('disabled', true);
                    $('#loading').hide();
                    });
                    function getSubStatus(){
                    statusid = $("#callstatus").val();
                    $("#callsubstatus option").remove();
                    var callsubstatus = {!! json_encode($callstatus['data']['callSubStatus']) !!};
                    $("#callsubstatus").append($("<option></option>").text('Select Call Sub Status').val(''));
                    $.each(callsubstatus, function(index, item){
                    if (item.status_id == statusid && item.status == 'active'){
                    $("#callsubstatus").append(
                            $("<option></option>").text(item.name).val(item.sub_status_id)
                            );
                    }
                    });
                    return false;
                    }

                    function postInteraction(form){
                    var interaction = $("#interaction").val();
                    var callinitiator = "1";
                    var callstatus = $("#callstatus").val();
                    var callsubstatus = $("#callsubstatus").val();
                    var nextdatetime = $("#nextdatetime").val();
                    var callsubstatusother = $("#call_sub_status_other").val();
                    var user_type = "lead";
                    var _token = $("#_token").val();
                    var reference_id = $("#reference_id").val();
                    jsonObj = {'_token': _token, 'reference_id': reference_id, 'callsubstatusother': callsubstatusother, 'nextdatetime': nextdatetime, 'callsubstatus':callsubstatus, 'callstatus': callstatus, 'callinitiator': callinitiator, 'interaction': interaction, 'user_type': user_type};
                    $.ajax({
                    url: "{{ route('post-reference-interaction')}}",
                            data: jsonObj,
                            type: 'post',
                            cache: false,
                            clearForm: false,
                            beforeSend:function(){
                            $('#loading').show();
                            },
                            success: function(response){
                            var dataresponse = response;
                            if (dataresponse['return'] == true){
                            $("#interaction-error").hide();
                            $("#interaction-success").show();
                            $(".substatus").hide();
                            $("#call_sub_status_other").attr("disabled", true);
                            $('#interactioncount').html(parseInt($('#interactioncount').html()) + 1);
                            $('#customer-interaction :input').val('');
                            datainteractionajax.push(dataresponse['interaction']['Remarks']);
                            var columns = ["datetime", "Logged_By", "Call_Status", "Call_Sub_Status", "Call_Back"];
                            for (i = 0; i < columns.length; i++){
                            if (i == 0){
                            $("#tbodyajaxdata").prepend($('<tr></tr>'));
                            }
                            $("#tbodyajaxdata tr:first").append(
                                    $("<td></td>").text(dataresponse['interaction'][columns[i]])
                                    );
                            }
                            $("#tbodyajaxdata tr:first").append(
                                    $("<td></td>").html('<p type="text" data-toggle="modal"  onclick="viewremarks(' + (parseInt($('#interactioncount').html()) - parseInt(1)) + ')"  data-target="#remarks_interaction" class="btn purple viewdetail"><a data-toggle="tooltip" title="' + dataresponse['interaction']['Remarks'] + '"  data-placement="left"><i class="fa fa-envelope"></i></a></p>')
                                    );
                            } else{
                            $("#interaction-error").show();
                            $("#interaction-success").hide();
                            }
                            },
                            error:function(){
                            alert("Server is Busy!!");
                            },
                            complete:function (data) {
                            $('#loading').hide();
                            }
                    });
                    }

                    $.fn.clearForm = function() {
                    return this.each(function() {
                    var type = this.type, tag = this.tagName.toLowerCase();
                    if (tag == 'form')
                            return $(':input', this).clearForm();
                    if (type == 'text' || type == 'password' || tag == 'textarea')
                            this.value = '';
                    else if (type == 'checkbox' || type == 'radio')
                            this.checked = false;
                    else if (tag == 'select')
                            this.selectedIndex = - 1;
                    });
                    };
                    function viewremarks(value){
                    var remarrks = datainteractionajax[value];
                    $('#remarkinteraction').html(remarrks);
                    }

                    $('#callsubstatus').change(function(){
                    if ($('#callsubstatus option:selected').val() == 66){
                    $('.appointment').prop('disabled', false);
                    $('.interactionsubmit').prop('disabled', true);
                    } else{
                    $('.appointment').prop('disabled', true);
                    $('.interactionsubmit').prop('disabled', false);
                    }
                    });
                    $('.appointment').click(function(){
                    if ($('#customer-interaction').valid() == true){
                    $('#appointment').modal('toggle');
                    }
                    });
                    function saveappointment(){
                    var appointmentdate = $("#appointmentdate").val();
                    $('#nextdatetime').val(appointmentdate);
                    var reference_id = $("#reference_id").val();
                    if (appointmentdate == ""){
                    alert("Enter Appointment datetime");
                    return false;
                    }
                    $.ajax({
                    url: "{{ route('fix-appointment')}}",
                            data:{'appointment_time':'', 'reference_id': reference_id, 'appointment_date': appointmentdate},
                            type: 'get',
                            cache: false,
                            async: false,
                            clearForm: false,
                            success: function(response){
                            if (response == true){
                            postInteraction();
                            $("#appointfed").html("<div class='alert alert-success'>Success! Appointment Fixed</div>");
                            }
                           else{
                            $("#appointfed").html("<div class='alert alert-danger'>Unable to Fix Appointment</div>");
                            }
                            }
                    });
                    setTimeout(function(){
                    $('#appointment').modal('hide');
                    window.location.reload();
                    }, 1000);
                    }

                    var checkdata = true;
                    jQuery.validator.addMethod("checkdatetime", function(value, element){
                    return checkdata;
                    }, "Select Date & Time");
                    function getOthers(){
                    substatus = $('#callsubstatus option:selected').text();
                    if (substatus == "Other"){
                    $(".substatus").show();
                    $("#call_sub_status_other").prop("disabled", false);
                    } else{
                    $(".substatus").hide();
                    $("#call_sub_status_other").attr("disabled", true);
                    }
                    var substatusval = parseInt($('#callsubstatus option:selected').val());
                    var applicable = [41, 65, 68, 69, 51, 53, 52, 57];
                    if ($.inArray(substatusval, applicable) === - 1){
                    checkdata = true;
                    } else{
                    checkdata = false;
                    }
                    }

                    $('#nextdatetime').change(function(){
                    var value = $(this).val();
                    if (value != "" && checkdata == false){
                    checkdata = true;
                    }
                    });
                    $('.link').click(function(){
                    var email = "{{ $reference_data['email'] }}";
                    if (email == ""){
                    alert("Email Id Not Present !!! \nPlease Update Email !!!");
                    return false;
                    }
                    $.ajax({
                    url: "{{ route('generate-link') }}",
                            data:{'id':"{{ request()->segment(2) }}"},
                            type: 'get',
                            cache: false,
                            async: true,
                            clearForm: false,
                            beforeSend:function(){
                            $('#loading').show();
                            },
                            success: function(response){
                            $('#linkvalue').html(response);
                            $('.emailid').html("{{ $reference_data['email'] }}");
                            $('.mobileno').html("{{ $reference_data['mobile'] }}");
                            $('#sendlink').modal('toggle');
                            },
                            error:function(){
                            alert("Server is Busy!!");
                            },
                            complete:function (data) {
                            $('#loading').hide();
                            }
                    });
                    });
                    function sendsms(){
                    $.ajax({
                    url: "{{ route('sendregisterationlink') }}",
                            data:{'id':"{{ request()->segment(2) }}"},
                            type:'get',
                            cache: false,
                            async: true,
                            clearForm:false,
                            beforeSend:function(){
                            $('#loading').show();
                            },
                            success: function(response){
                            $('#sendsmslink').modal('toggle');
                            alert("Message Sent Successfully!!!");
                            $('#sendlink').modal('toggle');
                            //window.location.href="{{ route('lead-list') }}"
                            },
                            error:function(){
                            alert("Server is Busy!!");
                            },
                            complete:function (data) {
                            $('#loading').hide();
                            }
                    });
                    return false;
                    }

                    $('#emailtitle').change(function(){
                    var value = $(this).find('option:selected').val();
                    var flag = (value == "") ? true :false;
                    $('#mailsendbtn').prop('disabled', flag);
                    });
                    function sendemail(){
                    var id = $('#emailtitle').find('option:selected').val();
                    $.ajax({
                    url: "{{ route('emailcontent') }}",
                            data:{'lead_id':"{{ request()->segment(2) }}", 'id':id},
                            type:'get',
                            cache: false,
                            async: true,
                            clearForm:false,
                            beforeSend:function(){
                            $('#loading').show();
                            },
                            success: function(response){
                            if (response['error'] != undefined){
                            errorsHtml = '<ul>';
                            $('.message').show();
                            $.each(response['error'], function(key, value) {
                            errorsHtml += '<li>' + value + '</li>';
                            });
                            errorsHtml += '</ul>';
                            $('.content').addClass('alert-danger').html(errorsHtml);
                            } else if (response['success'] != undefined){
                            alert("Mail Send Successfully!!!");
                            $('#emailsend').modal('toggle');
                            }
                            },
                            error:function(){
                            alert("Server is Busy!!");
                            },
                            complete:function (data) {
                            $('#loading').hide();
                            }
                    });
                    return false;
                    }

                    function cancelIntersaction(){
                    $('#customer-interaction :input').val('');
                    }
</script>
@endsection