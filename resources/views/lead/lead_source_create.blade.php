@extends('layout.master')
@section('page_title')
    Lead Source List 
@endsection
@section('page_level_style_top')
    <header class="page-header">
        <h2>Lead Source List</h2>
        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li>
                    <a href="#">
                        <i class="fa fa-home"></i>
                    </a>
                </li>
                <li><span>Lead Source</span></li>
            </ol>
            <a class="sidebar-right-toggle" data-open="#"><i class="fa fa-chevron-left"></i></a>
        </div>
    </header>
@endsection
@section('content')
    <section role="main" class="content-body">
        <section class="panel">
            <header class="panel-heading">
                <div class="row">
                    <h2 class="panel-title col-md-4">Lead Source</h2>
                    @if($errors->any())
                        <div class="alert alert-danger text-cente">
                            @foreach($errors->all() as $error)
                                <p>{{ $error }}</p>
                            @endforeach
                        </div>
                    @endif
                    @if(Session::get('status'))
                        <div class="alert alert-success col-md-3 alert-danger" style="padding:7px;margin-bottom: -8px;">
                            {{ Session::get('status') }}
                        </div>
                    @endif
                </div>
            </header>
            <div class="panel-body">
                <form id="form" action="{{ url('add_lead_source') }}" class="form-horizontal form-bordered" method="post">
                    <input type="hidden" name="_token" id="_tokensc" value="{{ csrf_token() }}">
                    <div class="form-group">
                        <label class="col-md-3 control-label">Add Lead Source Name <span class="required">*</span></label>
                        <div class="col-md-6">
                            
                            <input type="text" name="leadsourcename" class="form-control" value="" placeholder="Please Enter Value" required/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12" align="center">
                            <button type="submit" class="btn green" >Submit</button>&nbsp;&nbsp;&nbsp;&nbsp;
                            <a href="{{ URL::to('lead_source')}}" class="btn btn-default" >Cancel</a>
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        </div>
                    </div>
                </form>
            </div>
        </section>
        <section class="panel">
            <div class="panel-body">
                <table class="table table-bordered table-striped mb-none" id="datatable-editable">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Lead Source Name</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                        if (count($leadsourcelist) > 0) {
                            foreach ($leadsourcelist as $val) {                
                                ?>
                                <tr>
                                    <td><?php echo $val['leadsource_id'] ; ?></td>
                                    <td><?php echo $val['leadsource_name'] ; ?></td>
                                    <td><a href="{{URL::to('edit_leadsource',$val['leadsource_id'])}}"><input type="submit" class="btn btn-primary" value="Edit"></a></td>
                                </tr>
                            <?php }
                        } else { ?>
                            <tr>
                                <td colspan="8" align="center">
                                    Not Data Found
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        
    </section>
@endsection
@section('page_level_script_bottom')
    <script src="{{URL::asset('public/assets/javascripts/app.js')}}"></script>
    <script>
        jQuery(document).ready(function() { 
            App.init();
            $('#loading').hide();
        });
    </script>
@endsection



