@extends('layout.master')
@section('page_title')
Request List 
@endsection
@section('page_level_style_top')
<header class="page-header">
    <h2>RM Changes List</h2>
    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="#">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span>RM Changes</span></li>
            <li><span>RM Changes List</span></li>
        </ol>
        <a class="sidebar-right-toggle" data-open="#"><i class="fa fa-chevron-left"></i></a>
    </div>
</header>
@endsection
@section('content')
    <section role="main" class="content-body">
        <section class="panel">
            <header class="panel-heading">
                <div class="row">
                    <h2 class="panel-title col-md-4">Lead RM Changes</h2>
                    <div class="alert alert-success col-md-5 message" style="padding:7px;margin-bottom: -8px;display:none;"></div>
                </div>
            </header> 
            <div class="panel-body">
                <div class="col-md-12" style="margin-bottom: 20px;">
                    <form class="form-horizontal" id="frm" name="frm" method="post">
                        <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                        <input type="hidden" name="currentpage" value="{{ !empty(request()->page)?request()->page:"1" }}">
                        <div class="row form-group">
                            <label class="control-label col-md-1">Search :</label>
                            <div class="col-md-3">
                                <input type="text" name='q' class="form-control" id="q" value="{{ $q }}">
                            </div>
                            <div class="col-md-3">
                                <select class="form-control" name="option" id="option">
                                    <option value=""> -- Select Status -- </option>
                                    <option value="ID" @if($option=='ID') selected @endif>ID</option>
                                    <option value="Name" @if($option=='Name') selected @endif>Name</option>
                                    <option value="Email" @if($option=='Email')selected @endif>Email</option>                           
                                    <option value="Mobile" @if($option=='Mobile') selected @endif>Mobile</option>
                                </select>   
                            </div>
                            <div class="col-md-3">
                                <button type="submit" class="btn litgreen" id="submitclick">Search</button>
                                <a class="btn btn-primary" href="{{ route('leadchangerm') }}">Clear All</a>
                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="control-label col-md-1">RM:</label>
                            <div class="col-md-3">
                                <select class="form-control" name="rm_id" id="managerall">                          
                                </select>
                            </div>
                            <div class="col-md-3">
                                <button type="submit" class="btn green" id="sbmt" >Submit</button>
                            </div>
                        </div><br>
                        <table class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>
                                        <input type="checkbox" name="chkall" id="chkall">
                                    </th>
                                    <th>
                                        Lead Id
                                    </th>
                                    <th>
                                        Lead Name
                                    </th>
                                    <th>
                                        Mobile
                                    </th>
                                    <th>
                                        Email
                                    </th>

                                    <th>
                                        Assigned Relationship Manager
                                    </th>
                                    <th>
                                        Relationship Manager
                                    </th>
                                    <th>
                                        Action
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $count = 1;$rm_array=[]; ?>
                                @foreach($leadsourcevalue as $key => $val)
                                    <tr>
                                        <td>
                                            <input type="checkbox" class="chkbox" name="chk[]" value="{{ $val->leadid }}"  @if(Session::has('ref_value') && array_key_exists(request()->page,Session::get('ref_value'))) @if(in_array($val->leadid, Session::get('ref_value')[request()->page])) checked @endif @endif>
                                        </td>

                                        <td>
                                            <?php echo $val->leadid ?> 
                                        </td>
                                        <td>
                                            <?php echo $val->leadname ?> 
                                        </td>
                                        <td>
                                            <?php echo $val->leadmobile ?> 
                                        </td>
                                        <td>
                                            <?php echo $val->leademail ?> 
                                        </td>
                                        <td id="{{ $count }}">
                                            {{ $val->username }}
                                        </td>
                                        <td>
                                            <select class="form-control manger" id="rm{{$count}}">                                    
                                            </select>
                                        </td>
                                        <td>
                                            <a   data-id="{{ $count }}" class="hrefcust btn green"  reference_id="{{ $val->leadid }}">Change</a>
                                        </td>
                                    </tr>
                                <?php $count++; 
                                $rm_array[] = $val->rel_manager_id;?>
                                @endforeach
                            </tbody>
                        </table>
                    </form>
                </div>
                <div class ="pull-right" style="margin-bottom:20px">
                    {!! $leadsourcevalue->appends(['q'=>$q,'option'=>$option])->render() !!}
                </div>
            </div>
        </section>
    </section>
@endsection
@section('page_level_script_bottom')
    <script src="{{URL::to('/public/assets/javascripts/app.js')}}"></script>
    <script src="{{URL::to('/public/assets/bootbox/bootbox.min.js')}}"></script>
    <script>
        jQuery(document).ready(function() {
            $('#loading').hide();
            App.init();
            $('.pagination li a').addClass('clk');
            var rmanager = {!! json_encode($reporting) !!} ;
            var assignedrm = {!! json_encode($rm_array) !!} ;
            $('#managerall').append($("<option></option>").text('--Select--').val(''));
            $.each(rmanager,function(index, item){
                $('#managerall').append($("<option></option>").text(item['tellcallername'] + ' (' + item['tlname'] + ')').val(item['tellcallerid']));
            });
            var obj = $(".manger");
            obj.append($("<option></option>").text('--Select--').val(''));
            $.each(obj,function(indexobj,itemobj){
                $.each(rmanager,function(index, item){
                    if(assignedrm[indexobj]!=item['tellcallerid']){
                        $(itemobj).append($("<option></option>").text(item['tellcallername'] + ' (' + item['tlname'] + ')').val(item['tellcallerid']));
                    }
                });
            });
            $(".clk").on("click", function(e) {
                e.preventDefault();
                var objurl = new URL($(this).attr('href'));
                var page = objurl.searchParams.get("page");
                document.frm.action = "{{ route('leadchangerm')}}?page=" + page;
                document.frm.submit();
                return false;
            });
            @if(Session::has('messages'))
                $('.message').show().html("{{ Session::get('messages') }}");
                window.scrollTo(0,0);
                setTimeout(function(){$('.message').hide().html('');},800);
            @endif
        });
        
        $('.hrefcust').click(function(){
            var lead_id = $(this).attr('reference_id');   
            var hrefid = $(this).attr('data-id');
            var rmid = $('#rm' + hrefid + ' option:selected').val();
            var token = $('#token').val();
            if (rmid == ""){
                alert("Select Relationship Manager")
                return false;
            }
            bootbox.confirm("Are you sure, You want to change Relationship Manager of Lead Id " + lead_id + "?", function(result) {
                if (result == true){
                    $.ajax({
                        url: "{{ route('changerm')}}",
                        data:{'rm_id':rmid, '_token': token, 'lead_id':lead_id},
                        type: 'post',
                        cache: false,
                        clearForm: false,
                        beforeSend:function(){  
                            $('#loading').show();
                        },
                        success: function(response){
                            if(response==1){
                                $('.message').show().html("Lead Id "+lead_id+" Change Successfully!!!");
                                window.scrollTo(0,0);
                                setTimeout(function(){
                                    window.location.href="{{ route('leadchangerm') }}";
                                },1000);
                            }else{
                                alert("Something Went Wrong!!!! \n Please try after sometimes!!!");
                            }
                        },
                        error:function(){
                            alert("Server is Busy!!");
                        },
                        complete:function (data) {
                            $('#loading').hide();
                        }
                    });
                }
            });
        });



        $("#chkall").change(function () {
            if($(this).is(":checked")){
                $("div.checker span").addClass('checked');
                $(".chkbox").each(function(){
                    $(this).prop('checked',true);
                }) 
            }else{
                $("div.checker span").removeClass('checked');
                $(".chkbox").each(function(){
                    $(this).prop('checked',false);
                }) 
            }   
        });

     
        
        $('#sbmt').click(function(){
            var check = {!! json_encode(Session::get('ref_value')) !!};
            var len = parseInt($('.chkbox:checked').length);
            if (check === null && len == 0){
                alert("Please Select Reference");
                return false;
            }
            if ($('#managerall option:selected').val() == ""){
                alert("Relationship Manager");
                return false;
            }
            $('#loading').show();
            document.frm.action = "{{ URL::route('changerm') }}"
            document.frm.submit();
            return true;
        });

        $('#submitclick').click(function(){
            $('#loading').show();
            var search = $('#q').val()=="" ? $('#q').focus() : true;
            var option = $('#option option:selected').val()=="" ? $('#option').focus() : true;
            if(search==true && option==true){
                return true;
            }
            $('#loading').hide();
            return false;
        });
    </script>
@endsection         