@extends('layout.master')
@section('page_title')
  Lead Details
@endsection
@section('page_level_style_top')
	<header class="page-header">
	    <h2>Lead Details </h2>
	    <div class="right-wrapper pull-right">
	        <ol class="breadcrumbs">
	            <li>
	                <a href="#">
	                    <i class="fa fa-home"></i>
	                </a>
	            </li>
	            <li><span>Lead</span></li>
	            <li><span>Add Lead</span></li>
	        </ol>

	        <a class="sidebar-right-toggle" data-open="#"><i class="fa fa-chevron-left"></i></a>
	    </div>
	</header>
@endsection
	<?php
	$profiledata=array();
    $planamountdata=array();
    $dataarray_struct=array();
    $datajson_struct=$reference_data['json_struct'];
    if(!empty($datajson_struct)){
		$dataarray_struct=json_decode($datajson_struct,1);
    	$profiledata=$dataarray_struct['CustomerBGP'];
    	$planamountdata=$dataarray_struct['CustomerPlan'];
    }
    
    $name="";
    if(array_key_exists('applicant_name',$profiledata))
    	$name=$profiledata['applicant_name'];
    elseif(array_key_exists('name',$reference_data)){
        $name=$reference_data['name'];
    }
   
    $partnerdata="";
    if(array_key_exists('partner_code',$profiledata)){
    	$partnerdata=$profiledata['partner_code'];
    }elseif(array_key_exists('customer_id',$reference_data)){
        $partnerdata=$reference_data['customer_id'];
    }
    
    $mobile_no="";
    if(array_key_exists('contact_mobile',$profiledata)){
      	$mobile_no=trim($profiledata['contact_mobile']);
    }elseif(array_key_exists('mobile',$reference_data)){
        $mobile_no=$reference_data['mobile'];
    }

	$email_id="";
    if(array_key_exists('contact_email',$profiledata)){
    	$email_id=trim($profiledata['contact_email']);
    }elseif(array_key_exists('email',$reference_data)){
       	$email_id=$reference_data['email'];
    }?>
@section('content')
    <section role="main" class="content-body">
	 	<div class="row">
	    	<div class="col-md-12">
	    		<div class="panel panel-default">
	        		<div class="panel-heading"><h2 class="panel-title">Lead Request</h2></div>
	            		<div class="panel-body">
							<form  method="post" name="leaddetails" id="changerequest">
								<input type="hidden" name="_token" id="_token1" value="{{ csrf_token() }}">
					    		<input type="hidden" name="reference_id" id="reference_id" value="{{ $reference_data['reference_id']}}">
					    		<input type="hidden" name="sendmailflag" id="sendmailflag">
					    		<input type="hidden" name="lead_source" value="BPO">
								<h5><b>&nbsp;Login Details</b></h5>
						       	<table class="table table-striped table-bordered table-advance table-hover table-font">
						        	<tbody>
						            	<tr> 
						                  	<td style="font-weight: 600;">Login Id</td>
						                    <td class="form-group">
						                    	<input type="email" name="login_email" id="checkemail"  data-required="1" class="form-control" placeholder="Login ID" value="{{$email_id}}" onblur="getemail();" /><span class="displayerroremail"  style="color:red;" @if(!empty($reference_data['registration_data'])) readonly @endif ></span>
						                    </td>
						                </tr>
						            </tbody>
						        </table> 
								<h5><b>&nbsp;Personal Details</b></h5>
						        <table class="table table-striped table-bordered table-advance table-hover table-font">
							        <tbody>
							        	<tr> 
							                <td style="font-weight: 600;"> Customer Name </td>
							                <td class="form-group">
							                	<input type="text" name="applicant_name" data-required="1" class="form-control" placeholder="Please Enter Name" value="{{$name}}" />  
							                </td> 
							                <td style="font-weight: 600;"> Email Id </td>
							                <td class="form-group">
							                    <input type="email" name="contact_email" class="form-control" placeholder="Please Enter Email Address" value="{{$email_id}}">
							                 </td>
							            </tr> 
							            <tr> 
							            	<td style="font-weight: 600;"> Date of Birth </td>
							                <td class="form-group">
							                    <div class="input-group input-medium" data-date="12-02-2012" data-date-format="dd-mm-yyyy" data-date-viewmode="years">
							                    	<input type="text" name="applicant_dob" id="applicant_dob" class="form-control" data-required="1" readonly placeholder="Please Select Data of Birth" value="@if(array_key_exists('applicant_dob',$profiledata)){{ $profiledata['applicant_dob'] }}@endif">
							                    	<span class="input-group-btn">
							                    		<button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
							                    	</span>
							                    </div>  
							                </td> 
							                <td style="font-weight: 600;"> Mobile No</td>
							                <td class="form-group">
							                	<input type="text" name="contact_mobile" data-required="1" class="form-control" placeholder="Please Enter Mobile No" value="{{$mobile_no}}" />
							                </td>
							            </tr>
							            <tr> 
							                <td style="font-weight: 600;">Gender</td>
							              	<td class="form-group">
							                	<div class="radio-list" data-error-container="#form_2_membership_error">
							                    	<label class="radio-inline">
							                        <input type="radio" name="applicant_gender" value="Male" @if(array_key_exists('applicant_gender',$profiledata) && $profiledata['applicant_gender']=="Male")checked @endif />Male</label>
							                        <label class="radio-inline">
							                        <input type="radio" name="applicant_gender" value="Female" @if(array_key_exists('applicant_gender',$profiledata) && $profiledata['applicant_gender']=="Female"){{ "checked"}} @endif />Female</label>
							                    </div>
							                    <div id="form_2_membership_error"></div> 
							                </td>
							                <td style="font-weight: 600;">PAN Number</td>
							                <td> 
							                	<input type="text" name="contact_pan_no" data-required="1" class="form-control"  placeholder="Please Enter PAN No" value="@if(array_key_exists('contact_pan_no',$profiledata)){{ $profiledata['contact_pan_no']}}@endif" />
							                </td>
							            </tr>
							        </tbody>
						    	</table>
						        <h5><b>&nbsp;Contact Details</b></h5>
						        <table class="table table-striped table-bordered table-advance table-hover table-font">
						        	<tbody>
						        		<tr>
						                	<td style="font-weight: 600;">Residency Phone Number</td>
						                    <td>
						                    	<input type="text" name="contact_phone_residence" data-required="1" class="form-control" placeholder="Please Enter Residence No" value="@if(array_key_exists('contact_phone_residence',$profiledata)){{ $profiledata['contact_phone_residence']}}@endif" />
						                   	</td>
						                   	<td style="font-weight: 600;">Office Phone Number</td>
						                    <td>
						                    	<input type="text" name="contact_phone_office" data-required="1" class="form-control" placeholder="Please Enter Office No" value="@if(Request::old('contact_phone_office')){{Request::old('contact_phone_office')}}@endif" />
						                    </td>
						                </tr>
						            </tbody>
						        </table>
								<h5><b>&nbsp;Nominee Details</b></h5>
						        <table class="table table-striped table-bordered table-advance table-hover table-font">
						        	<tbody>
						            	<tr>
						            		<td style="font-weight: 600;">Nominee Name </td>
						                    <td>
						                    	<input type="text" name="nomination_name" data-required="1" class="form-control" placeholder="Please Enter nominee Name" value="@if(array_key_exists('nomination_name',$profiledata)){{$profiledata['nomination_name']}}@endif" /> 
						 					</td>
						 					<td style="font-weight: 600;">Relationship</td>
						                    <td>
						                    	<input type="text" name="nomination_applicant_realtion" data-required="1" class="form-control" placeholder="Relation with nominee" value="@if(array_key_exists('nomination_applicant_realtion',$profiledata)){{ $profiledata['nomination_applicant_realtion']}}@endif" /> 
						                    </td>
						                </tr>
						                <tr>
						               		<td style="font-weight: 600;">Nominee Address</td>
						                    <td colspan="3">
						                    	<textarea style="width:42%;" class="form-control" rows="3" name="nomination_address" data-error-container="#editor1_error" placeholder="Please Enter Nominee Address">@if(array_key_exists('nomination_address',$profiledata)){{ $profiledata['nomination_address']}}@endif</textarea>
											</td>
						                </tr> 
						                <tr>
						                	<td style="font-weight: 600;">State</td>
						                    <td>
						                    	<select name="nomination_state" id="nomination_state" class="form-control select2me" data-placeholder="Select State" onchange="getCitynominy();">
						                        	<option value="">Select State</option>
						                        	@foreach($nomstates as $key=>$val)
						                            	<option value="{{$val}}" data-status="{{$key}}" @if(array_key_exists('nomination_state',$profiledata) && $profiledata['nomination_state']==$val) selected @endif >{{$val}}</option>
						                            @endforeach
						                        </select>
						                    </td>
						                	<td style="font-weight: 600;">City</td>
						                    <td>
						                    	<select name="nomination_city" class="form-control select2me" data-placeholder="Select City" id="nomination_city"></select>
						                    </td>
						                </tr>
						               	<tr>
						             		<td style="font-weight: 600;">Pincode</td>
						                    <td>
						                    	<select name="nomination_pincode" class="form-control select2me" data-placeholder="Select Pincode" id="nomination_pincode">
						                    	</select>
											</td>
						                    <td style="font-weight: 600;">Date of Birth</td>
						                    <td>
						                    	<div class="input-group input-medium date date-picker" data-date-format="dd-mm-yyyy" data-date-viewmode="years">
						                    		<input type="text" name="nomination_dob" id="nomination_dob" class="form-control" data-required="1" readonly placeholder="Please Select Data of Birth" value="@if(array_key_exists('nomination_dob',$profiledata)){{$profiledata['nomination_dob']}}@endif" />
						                        	<span class="input-group-btn">
						                        		<button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
						                        	</span>
						                       	</div>
						                    </td>
						                </tr>
						             </tbody>
						        </table>
						        <h5><b>&nbsp;Mailing Details</b></h5>
						        <table class="table table-striped table-bordered table-advance table-hover table-font">
						            <tbody>
						                <tr> 
						                    <td style="font-weight: 600;">Address</td>
						                    <td colspan="3" class="form-group">
						                    	<textarea style="width:42%;" class="form-control" rows="3" name="mailing_address" placeholder="Please Enter Address">@if(array_key_exists('mailing_address',$profiledata)){{ $profiledata['mailing_address']}}@endif</textarea>
						                    </td>
						                </tr>
						                <tr>
						                    <td style="font-weight: 600;">Landmark</td>
						                    <td class="form-group">
						                    	<input type="text" name="mailing_landmark" data-required="1" class="form-control" placeholder="Please Enter Mailing Landmark" value="@if(array_key_exists('mailing_landmark',$profiledata)){{ $profiledata['mailing_landmark']}}@endif" />
						                    </td>
						                    <td style="font-weight: 600;">State</td>
						                    <td class="form-group">
						                    	<select name="mailing_state" class="form-control select2me" data-placeholder="Select State" onchange="getCity();" id="stateID">
						                        	<option value="">Select State</option>
						                            @foreach($states as $key=>$val)
						                            	<option value="{{$val}}" data-status="{{$key}}" @if(array_key_exists('mailing_state',$profiledata) && $profiledata['mailing_state']==$val) selected @endif >{{$val}}</option>
						                            @endforeach
						                        </select>
						                    </td>
						                </tr>
						                <tr>
						              		<td style="font-weight: 600;">City</td>
						              		<td class="form-group">
						                		<select name="mailing_city" class="form-control select2me" data-placeholder="Select State" id="citylist"></select>
						               		</td>
						               		<td style="font-weight: 600;">District</td>
						              		<td class="form-group">
						                    	<select name="mailing_district" class="form-control select2me" data-placeholder="Select District" id="districtlist" onchange="getPincode();" ></select>
						                    </td>
						                </tr> 
						               	<tr>
						                    <td style="font-weight: 600;">Pin Code</td>
						                    <td class="form-group">
						                    	<select name="mailing_pincode" class="form-control select2me" data-placeholder="Select State" id="pincodelist" onchange="getDistrict();">
						                    	</select>
						                    </td> 
						                    <td style="font-weight: 600;">Amount</td>
						                    <td class="form-group">
						                    	<input type="text" name="amount" id="amount" data-required="1" class="form-control" placeholder="Please Enter Amount" value="@if(array_key_exists('initial_amount',$planamountdata)){{ $planamountdata['initial_amount']}}@endif" /> 
						                   	</td>
						                </tr>
									</tbody>
								</table> 
								<h5><b>&nbsp;Fulfillment</b></h5>
						        <table class="table table-striped table-bordered table-advance table-hover table-font">
						        	<tbody>
						            	<tr>
						                	<td class="form-group">
						                    	<label class="radio-inline">
						                        	<input type="radio" name="fulfilment"  value="Coin" @if(array_key_exists('fulfilment',$profiledata) && $profiledata['fulfilment']=="Coin") {{ "checked"}} @endif /> Gold Jewellery
						                        </label>
						                        <label class="radio-inline">
						                            <input type="radio" name="fulfilment"  value="Jewellery" @if(array_key_exists('fulfilment',$profiledata) && $profiledata['fulfilment']=="Jewellery") {{ "checked"}} @endif/> Diamond Jewellery
						                        </label>
						                        <label class="radio-inline">
						                            <input type="radio" name="fulfilment" value="Pendant" @if(array_key_exists('fulfilment',$profiledata) && $profiledata['fulfilment']=="Pendant") {{ "checked"}} @endif/>Gold Pendant
						                        </label>
						                    </td>
											<div id="form_2_membership_error"></div>
						            	</tr>
						            </tbody>
						        </table>  
								
								<div>
	                        		<button type="button" class="btn default">Cancel</button>
										<button type="submit" class="btn litgreen">Submit</button>
	                        	</div>
		                    </div>
	                		</form>
	            		</div>
	            	</div>
	            </div>
	        </div>
	    </div>
	</section>
	<div class="modal fade" id="sendsmslink" tabindex="-1" role="basic" aria-hidden="true"  data-keyboard="false" data-backdrop="static">
	    <div class="modal-dialog">
	        <div class="modal-content">
	            <div class="modal-header">
	                <h4 class="modal-title">Send Email & SMS</h4>
	            </div>
	            <div class="modal-body">
	                <span id="appointfed"></span>
	                <div class="form-body">
	                    <div class="form-group">
	                        <div class="row">
	                            <label class="col-md-3 control-label">Name:</label>
	                            <label class="col-md-9 control-label customername"></label>
	                            <label class="col-md-3 control-label">Email:</label>
	                            <label class="col-md-9 control-label customeremail"></label>
	                           	<label class="col-md-3 control-label">Mobile:</label>
	                            <label class="col-md-9 control-label customermobile"></label>
	                        </div>
						</div>
					</div>
	            </div>
	            <div class="modal-footer">
	                <button type="button" class="btn green" onclick="sendsmslink();">Send</button>
	                <button type="button" class="btn default" data-dismiss="modal">Close</button>
	            </div>
			</div>
	    </div>
	</div>
@endsection
@section('page_level_script_bottom')
	<script src="{{URL::asset('public/assets/vendor/select2/select2.js')}}"></script>
	<script src="{{URL::asset('public/assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
	<script src="{{URL::asset('public/assets/vendor/jquery-validation/jquery.validate.js')}}"></script>
  	<script src="{{URL::asset('public/assets/javascripts/forms/form-validation.js')}}"></script>
    <script src="{{URL::asset('public/assets/javascripts/app.js')}}"></script>
  	<script>
  	var checkcall=false;
	jQuery(document).ready(function() {
		App.init(); 
	    FormValidation.init(); 
	    var end = new Date();
	    end.setFullYear(end.getFullYear() - 18);
	    $('#applicant_dob').datepicker({
	    	format: "dd-mm-yyyy",
	        endDate: format(end) 
	    });
	    
	    $('#nomination_dob').datepicker({
	        format: "dd-mm-yyyy",
	        endDate: format(end) 
	    });
	    getCity();
	    @if(array_key_exists('mailing_city',$profiledata))
      		$('#citylist').val("{{$profiledata['mailing_city']}}").trigger("change");
    	@endif

    	@if(array_key_exists('mailing_pincode',$profiledata))
    		$('#pincodelist').val("{{$profiledata['mailing_pincode']}}").trigger("change");
    	@endif
    	@if(array_key_exists('mailing_district',$profiledata))
    		$('#districtlist').val("{{$profiledata['mailing_district']}}").trigger("change");
    	@endif
    	getemail();
    	getCitynominy();
    	@if(array_key_exists('nomination_city',$profiledata))
    		$('#nomination_city').val("{{$profiledata['nomination_city']}}").trigger('change');
    	@endif
    	
    	@if(array_key_exists('nomination_pincode',$profiledata))
    		$('#nomination_pincode').val("{{$profiledata['nomination_pincode']}}").trigger('change');
    	@endif
	    $('#loading').hide();
	});

	function getCity(){
    	var stateID=$('#stateID option:selected').attr('data-status');
    	$('#citylist').html('').select2([{option: '', text: ''}]);
    	$('#districtlist').html('').select2([{option: '', text: ''}]);
    	$('#pincodelist').html('').select2([{option: '', text: ''}]);
    	if(stateID!="" && stateID!=undefined){
    		$('#loading').show();
		    $.ajax({
                url: "{{route('citydistrictpincode')}}",
                data: {'stateid': stateID},
                type: 'get',
                async: false,
                cache: false,
                clearForm: false,
                beforeSend:function(){	
    		    	$('#loading').show();
                },
                success: function (response) {
                    $.each(response.city, function (idx, obj) {
                        $("#citylist").append(
                            $("<option></option>").text(obj).val(obj)
                        );
                    });

                     $.each(response.district, function (idx, obj) {
                        $("#districtlist").append(
                            $("<option></option>").text(obj).val(obj)
                        );
                    });

                    $.each(response.pincode, function (idx, obj) {
                        $("#pincodelist").append(
                            $("<option></option>").text(obj).val(obj)
                        );
                    });
				},
                error:function(){
                    alert("Server is Busy!!");
                },
                complete:function (data) {
                    $('#loading').hide();
                }
            });
			$('#loading').hide();
    	}
  	} 
  
  	function getPincode(){
    	districtname = $("#districtlist").val();
    	$('#pincodelist').html('').select2([{option: '', text: ''}]);
    	if(districtname){
    		$('#pincodelist').prop('selectedIndex',0);
	    	$.ajax({
	      		url: "{{ route('pincodelist') }}",
	      		data:{'district': districtname},    
	      		type: 'get',
	      		dataType: 'json',
	      		async: false,	
	      		cache: false,
	      		clearForm: false,
	      		beforeSend:function(){	
    		      $('#loading').show();
                },
	      		success: function(response){
	        		$.each(response.pincode, function (idx, obj) {
                        $("#pincodelist").append(
                            $("<option></option>").text(obj).val(obj)
                        );
                    });
	      		},
                error:function(){
                    alert("Server is Busy!!");
                },
                complete:function (data) {
                    $('#loading').hide();
                }
	    	});
	    }
  	}  
  
  	function getDistrict(){
    	pincode = $("#pincodelist").val();
    	$('#districtlist').html('').select2([{option: '', text: ''}]);
    	$.ajax({
      		url: "{{ route('districtlist') }}",
      		data:{'pincode': pincode},    
      		type: 'get',
      		dataType: 'json',
      		async: false,
      		cache: false,
      		clearForm: false,
	      	beforeSend:function(){	
    		    $('#loading').show();
            },
      		success: function(response){
        		$.each(response.district, function (idx, obj) {
                    $("#districtlist").append(
                        $("<option></option>").text(obj).val(obj)
                    );
                }); 
      		},
            error:function(){
                alert("Server is Busy!!");
            },
            complete:function (data) {
                $('#loading').hide();
            }
    	});
  	}

  	$.fn.clearForm = function() {
    	return this.each(function() {
	      	var type = this.type, tag = this.tagName.toLowerCase();
	      	if (tag == 'form')
		        return $(':input',this).clearForm();
	      	if (type == 'text' || type == 'password' || tag == 'textarea')
		        this.value = '';
	      	else if (type == 'checkbox' || type == 'radio')
	        	this.checked = false;
	      	else if (tag == 'select')
	        	this.selectedIndex = -1;
    	});
  	};
  	
  	function format(date) {
    	date = new Date(date);
		var day = ('0' + date.getDate()).slice(-2);
    	var month = ('0' + (date.getMonth() + 1)).slice(-2);
    	var year = date.getFullYear();
		return day + '-' + month + '-' + year;
  	}

  	function getemail(){
    	var emailuser = $("#checkemail").val();
    	if(emailuser == ''){
      		return false;
    	}
    	$.ajax({
      		url: "{{ URL::route('checkloginemail') }}",
      		data:{'email':emailuser},
      		type: 'get',
      		async: false,
      		cache: false,
      		clearForm: false,
      		beforeSend:function(){	
    		    $('#loading').show();
            },
      		success: function(response){
	        	if(response['data']==1){
	         		$('.displayerroremail').text("Email Already Exists");
	        	}else{
	          		$('.displayerroremail').text("");
	        	}
      		},
            error:function(){
                alert("Server is Busy!!");
            },
            complete:function() {
                $('#loading').hide();
            }
    	});
    	$('input[name="contact_email"]').val(emailuser);
    }

  	function getCitynominy(){
    	var stateID=$('#nomination_state option:selected').attr('data-status');
    	$('#nomination_city').html('').select2([{option: '', text: ''}]);
    	$('#nomination_pincode').html('').select2([{option: '', text: ''}]);
    	if(stateID){
    		$('#loading').show();
      		$.ajax({
		        url: "{{ route('citydistrictpincode')}}",
		        data:{'stateid': stateID},    
		        type: 'get',
		        dataType: 'json',
		        cache: false,
		        async: false,
		        clearForm: false,
		        beforeSend:function(){	
    		    	$('#loading').show();
            	},
		        success: function(response){
		        	$.each(response.city, function (idx, obj) {
                        $("#nomination_city").append(
                            $("<option></option>").text(obj).val(idx)
                        );
                    });
					
					$.each(response.pincode, function (idx, obj) {
                        $("#nomination_pincode").append(
                            $("<option></option>").text(obj).val(idx)
                        );
                    });
		        },
                error:function(){
                    alert("Server is Busy!!");
                },
                complete:function (data) {
                    $('#loading').hide();
                }
      		});
      		$('#loading').hide();
		}
  	}

  	function validatedata(){
  		$.ajax({
	        url: "{{ route('updatechangerequest') }}", 
	        data:$('#changerequest').serialize(),    
	        type:'post',
	        cache: false,
	        async: true,
	        clearForm:false,
			beforeSend:function(){	
    		    $('#loading').show();
            },
	        success: function(response){
	        	$('.customeremail').html($('input[name="contact_email"]').val());
	        	$('.customermobile').html($('input[name="contact_mobile"]').val())
	        	$('.customername').html($('input[name="applicant_name"]').val())
	        	$('#sendsmslink').modal('toggle');
	        },
            error:function(){
                alert("Server is Busy!!");
            },
            complete:function (data) {
                $('#loading').hide();
            }
  		});
  		return false;
  	}

  	function sendsmslink(){
  		$.ajax({
	        url: "{{ route('sendpaymentlink') }}", 
	        data:{'leadid':"{{ request()->segment(2) }}"},    
	        type:'get',
	        cache: false,
	        async: true,
	        clearForm:false,
			beforeSend:function(){	
    		    $('#loading').show();
            },
	        success: function(response){
	        	$('#sendsmslink').modal('toggle');
	        	alert("Message Sent Successfully!!!");
	        	//window.location.href="{{ route('lead-list') }}"
	        },
            error:function(){
                alert("Server is Busy!!");
            },
            complete:function (data) {
                $('#loading').hide();
            }
  		});
  		return false;
  	}
  </script>
@endsection