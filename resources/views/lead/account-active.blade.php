@extends('layout.master')
@section('page_title')
	Account Activate
@endsection
@section('breadcrumb')
<li>
	<a href="#">Account Activate </a>
</li>
@endsection
@section('page_level_style_top')
	<link rel="stylesheet" type="text/css" href="{{URL::to('assets/plugins/select2/select2_metro.css')}}"/>
	<link rel="stylesheet" type="text/css" href="{{URL::to('assets/plugins/data-tables/DT_bootstrap.css')}}"/>
@endsection
@section('content')
	<div class="row">
		<div class="col-md-12">
			<div class="portlet box litgreen">
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-reorder"></i> Account Activate 
					</div>
				</div>
				<div class="portlet-body">
					@if(Session::has('flash_message'))
                      	<div class="alert alert-success text-center ">
                      		{{ Session::get('flash_message') }}
                     	</div>
                    @endif
					<form  method="post"  action="">
						<div class="table-scrollable">
							<table class="table table-striped table-hover table-bordered">
								<thead>
									<tr>
										<th>
											#
										</th>
										<th>
											ID
										</th>
										<th>
											Lead Name
										</th>
										<th>
											Plan Size
										</th>
										<th>
											Form No
										</th>
										<th>
											Action
										</th>
									</tr>
								</thead>
								<tbody>
									@foreach($results as $key=>$data)
										<tr>
											<td>
												{{ $key+1 }}
											</td>
											<td>
												{{ $data['reference_id']}}
											</td>

											<td>
												{{$data['name']}}
											</td>
											<td>
												{{ $data['plan_size']  }}
											</td>
											<td>
												{{ $data['form_no'] }}
											</td>
											<td>
												<a  data-ref="{{ $data['reference_id'] }}" class="assign btn btn-primary green" data-status="9">Activate</a>
											</td>
										</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</form>
					<div class ="pull-right" style="margin-bottom:20px">
  				 	</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container">
	  	<div class="modal fade" id="myModal" role="dialog">
	 		<div class="modal-dialog modal-sm">
	      		<div class="modal-content">
	       			<div class="modal-body" id="alt">
	          			<p id="para"></p>
	        		</div>
	      		</div>
	    	</div>
	  	</div>
	</div>
@endsection
@section('page_level_script_bottom')
	<script type="text/javascript" src="{{URL::to('assets/plugins/select2/select2.min.js')}}"></script>
	<script type="text/javascript" src="{{URL::to('assets/plugins/data-tables/jquery.dataTables.js')}}"></script>
	<script type="text/javascript" src="{{URL::to('assets/plugins/data-tables/DT_bootstrap.js')}}"></script>
	<script src="{{URL::to('assets/scripts/app.js')}}"></script>
	<script src="{{URL::to('assets/scripts/table-editable.js')}}"></script>
	<script src="{{URL::to('assets/plugins/bootbox/bootbox.min.js')}}" type="text/javascript"></script>
	<script>
		jQuery(document).ready(function() { 
			App.init(); 
	   		TableEditable.init();
	   	});
		
		$('.assign').click(function(){
	   		var ref = $(this).data('ref');
	   		console.log(ref);
			bootbox.confirm("Are you sure, You want to Activate Lead Id "+ref, function(result) {
				if(result == true){
					$.ajax({
						url: "{{ route('account-active-account') }}",
						data: {'lead_id':ref,'type':'lead'},
						type: 'get',
						async:false,
						cache:false,
						dataType:'json',
						beforeSend:function(){	
							$('#loading').show();
						},
						success:function(response){
							window.location.reload();
						},
						error:function(data){
							if(data.status==500){
								alert("Server is Busy!!!");
								return false;
							}
							var errorsHtml='';
							$.each(JSON.parse(data.responseText),function(key,value) {
                    			for(valuedata in value){
                    				errorsHtml += value[valuedata]+ '\n';
                    			}
							});
                 			alert(errorsHtml);
						},
						complete:function (data) {
			    			$('#loading').hide();
			    		}
					});
				}
			});
		});
	</script>
@endsection
