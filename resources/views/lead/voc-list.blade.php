@extends('layout.master')
@section('page_title')
	Voc List 
@endsection
@section('breadcrumb')
<li>
	<a href="#">Voc List </a>
</li>
@endsection
@section('page_level_style_top')
	<link rel="stylesheet" type="text/css" href="{{URL::to('assets/plugins/select2/select2_metro.css')}}"/>
  	<link rel="stylesheet" type="text/css" href="{{URL::to('assets/plugins/data-tables/DT_bootstrap.css')}}"/>
  	<link rel="stylesheet" type="text/css" href="{{URL::to('assets/plugins/bootstrap-datepicker/css/datepicker.css')}}"/>
  	<link rel="stylesheet" type="text/css" href="{{URL::to('assets/plugins/bootstrap-timepicker/compiled/timepicker.css')}}"/>
  	<link rel="stylesheet" type="text/css" href="{{URL::to('assets/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css')}}"/>
  	<link rel="stylesheet" type="text/css" href="{{URL::to('assets/plugins/bootstrap-datetimepicker/css/datetimepicker.css')}}"/>
@endsection
@section('content')
	<div class="row">
		<div class="loading" id="loading" style="display:none">
  			<img class="loading-image" id="loading-image" src="{{URL::to('assets/img/ajax-loading.gif')}}" alt="Loading..." />
		</div>
		<div class="col-md-12">
			<div class="portlet box litgreen">
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-reorder"></i> Voc List 
					</div>
				</div>
				<div class="portlet-body">
					@if(Session::has('flash_message'))
                      	<div class="alert alert-success text-center ">
                      		{{ Session::get('flash_message') }}
                     	</div>
                    @endif
					<form method="get" name="frm" id="frm">
						<div class="col-md-12" style="margin-bottom:10px;">
							<div class="form-group">
								<div class="col-md-2" style="margin-top:6px;">CallBack Date:</div>
								<div class="col-md-2">
									<input type="text" name="fromdate" class="form-control" id="from_date" value="{{isset($fromdate)?$fromdate:""}}" placeholder="From Date" readonly>
								</div>
								<div class="col-md-2">
									<input type="text" name="todate" class="form-control" id="to_date" value="{{isset($todate)?$todate:""}}" placeholder="To Date" readonly>
								</div>
								<div class="col-md-2">
									<input type="text" name="lead" class="form-control"  value="{{isset($lead)?$lead:""}}" placeholder="ID">
								</div>
								<div class="col-md-4">
									<button type="submit" class="btn green">Submit</button>
									<a href="{{ route('voc-list') }}" class="btn red">Clear</a>
									<a  class="btn green excel">Excel</a>
								</div>
							</div>	
						</div>
					</form>
						<div class="table-scrollable">
							<table class="table table-striped table-hover table-bordered">
								<thead>
									<tr>
										<th>
											#
										</th>
										<th>
											ID
										</th>
										<th>
											Lead Name
										</th>
										<th>
											Mobile
										</th>
										<th>
											Type
										</th>
										<th>
											Callback Date
										</th>
										<th>
											Source Stage
										</th>
										<th>
											Current Stage
										</th>
										<th>
											Attepmt
										</th>
										<th>
											Count
										</th>
										<th>
											Action
										</th>
									</tr>
								</thead>
								<tbody>
									<?php $count=1 ;
									$array = array_merge_recursive(config('custom.Met'),config('custom.Notmet'),['voc'=>'Followup (Met)']);?>
									@foreach($results as $key=>$data)
										<tr>
											<td>
												{{ $key+1  }}
											</td>
											<td>
												{{ $data->reference_id}}
											</td>
											<td>
												{{$data->name}}
											</td>
											<td>
												{{$data->mobile}}
											</td>
											<td>
												@if(empty($data->customer_id) || $data->customer_id==0)
													Lead
												@else
													Reference
												@endif
											</td>

											<td>
												{{ $data->callback }}
											</td>
											<td>

												{{ (isset($remark_status[$data->last_status_id])? $remark_status[$data->last_status_id] : "" ).' - '.(isset($remark_sub_status[$data->last_status_sub_id]) ? $remark_sub_status[$data->last_status_sub_id] : "") }}
											</td>
											<td>
												{{ status($data->stage_id,$data->stage_remarks) }}
											</td>
											<td>
												{{ $data->countcall }}
											</td>
											<td>
												{{ $data->metcount }}
											</td>
											<td>
												@if($data->metcount<3)
													<a href="#" data-fo="{{ $key }}" data-ref="{{ $data->reference_id }}" class="assign">Action</a> /
												@endif  
												<a  href="#" data-toggle="modal" data-lead="{{$data->reference_id }}" data-target="#remarks" class="responsedata">Response</a>

											</td>

										</tr>
										<?php $count++;?>
									@endforeach
								</tbody>
							</table>
						</div>
					</form>
				</div>
				<div class ="pull-right" style="margin-bottom:20px">
					{!! $results->appends(request()->except('page'))->links() !!}
  				 </div>
			</div>
		</div>
	<div class="modal fade" id="appointment" tabindex="-1" role="basic" aria-hidden="true"  data-keyboard="false" data-backdrop="static">
	  	<div class="modal-dialog">
	  		<form action="{{ route('voc-update') }}" method="post" id="vocupdate" name="vocupdate" role="search">
	  			<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
	  			<input type="hidden" name="id" id="id" value="">
	  			<input type="hidden" name="type" value="lead">
	  			<input type="hidden" name="status_name" id="status_name" value="">
			    <div class="modal-content">
			    	<div class="modal-header">
			        	<h4 class="modal-title">Voc Remarks for Lead ID <span id="showlead"></span></h4>
			      	</div>
			      	<div class="modal-body">
			            <span id="appointfed"></span>
			            <div class="form-body">
							<div class="form-group">
								<div class="row">
									<label class="col-md-4 control-label">Call Status:</label>
									<div class="col-md-6">	
										<select class="form-control" name="call_status" id="call_status">
											@foreach($call_status as $key=>$val)
												<option value="{{$val['id']}}">{{$val['name']}}</option>
	                						@endforeach
										</select>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<label class="control-label col-md-4">Contact:</label>
									<div class="col-md-8">
										<div class="radio-list" data-error-container="#form_2_membership_error">
											<label class="radio-inline">
												<input type="radio" name="contact" value="contact" class="meet"/>Contactable
											</label>
											<label class="radio-inline">
												<input type="radio" name="contact" value="noncontact" class="meet"/>Not Contactable
											</label>
										</div>
										<div id="form_2_membership_error"></div>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<label class="col-md-4 control-label">Call Sub Status:</label>
									<div class="col-md-6">	
										<select class="form-control" name="call_sub_status" id="call_sub_status" >
											<option value=""> --- Select Call Sub Status ---</option>
										</select>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<label class="col-md-4 control-label">Remarks:</label>
									<div class="col-md-6">	
										<textarea type="textarea" rows="4" name="verification_interaction" id="verification_interaction" class="form-control" value="" placeholder="Please Enter Verification Interaction " style="resize:vertical;" ></textarea>	
									</div>
								</div>
							</div>
							<div class="form-group" style="display:none;"  id="datetime">
								<div class="row">
									<label class="col-md-4 control-label">Callback Datetime:</label>
									<div class="col-md-6">	
										<input type="text" name="appointment_date" id="appointmentdate" class="form-control" value="" readonly>	
									</div>
								</div>
							</div>
						</div>
			        </div>
			        <div class="modal-footer">
						<button type="submit" class="btn green">Save</button>
				   		<button type="button" class="btn default" data-dismiss="modal">Close</button>
			        </div>
				</div>
			</form>
		</div>	
	</div>
	<div class="modal fade" id="remarks" tabindex="-1" role="basic" aria-hidden="true" data-keyboard="false" data-backdrop="static">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title text-purple"><b>Interaction</b></h4>
				</div>
				<div class="modal-body" >
					<div class="row form-group">
						<div class="col-md-12" style='overflow:auto;height:400px;'>
							<!-- <h4>Interaction</h4> -->
							<!-- <div class="table-scrollable">
								<table class="table table-striped table-hover table-bordered">
									<thead>
										<tr>
											<th>#</th>
											<th>Created Date</th>
											<th>Logged By</th>
											<th>Call Status</th>
											<th>Call Sub Status</th>
											<th>Remarks</th>
										</tr>
									</thead>
									<tbody id="interaction">
									</tbody>
								</table>
							</div> -->
							<h4>Verifier Interaction</h4>
							<div class="table-scrollable">
								<table class="table table-striped table-hover table-bordered">
									<thead>
										<tr>
											<th>#</th>
											<th>Created Date</th>
											<th>Logged By</th>
											<th>Call Status</th>
											<th>Call Sub Status</th>
											<th>Call Back</th>
											<th>Remarks</th>
										</tr>
									</thead>
									<tbody id="verfierinteraction">
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn default" data-dismiss="modal" id="closebtn">Close</button>
				</div>
			</div>
		</div>
	</div>
@endsection
@section('page_level_script_bottom')
	<script type="text/javascript" src="{{URL::to('assets/plugins/jquery-validation/dist/jquery.validate.min.js')}}"></script>
	<script type="text/javascript" src="{{URL::to('assets/plugins/select2/select2.min.js')}}"></script>
	<script type="text/javascript" src="{{URL::to('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
	<script type="text/javascript" src="{{URL::to('assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js')}}"></script>
	<script type="text/javascript" src="{{URL::to('assets/plugins/clockface/js/clockface.js')}}"></script>
	<script type="text/javascript" src="{{URL::to('assets/plugins/bootstrap-daterangepicker/moment.min.js')}}"></script>
	<script type="text/javascript" src="{{URL::to('assets/plugins/bootstrap-daterangepicker/daterangepicker.js')}}"></script>

	<script type="text/javascript" src="{{URL::to('assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.js')}}"></script>
	<script type="text/javascript" src="{{URL::to('assets/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js')}}"></script>
	<script type="text/javascript" src="{{URL::to('assets/plugins/jquery.input-ip-address-control-1.0.min.js')}}"></script>
	<script type="text/javascript" src="{{URL::to('assets/plugins/jquery-multi-select/js/jquery.multi-select.js')}}"></script>
	<script type="text/javascript" src="{{URL::to('assets/plugins/jquery-multi-select/js/jquery.quicksearch.js')}}"></script>

	<script src="{{URL::to('assets/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js')}}" type="text/javascript"></script>
	<script src="{{URL::to('assets/plugins/jquery-tags-input/jquery.tagsinput.min.js')}}" type="text/javascript"></script>
	<script src="{{URL::to('assets/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js')}}" type="text/javascript"></script>
	<script src="{{URL::to('assets/plugins/bootstrap-touchspin/bootstrap.touchspin.js')}}" type="text/javascript"></script>
	<script src="{{URL::to('assets/scripts/app.js')}}"></script>
	<script src="{{URL::to('assets/scripts/form-validation.js')}}"></script>
	<script src="{{URL::to('assets/scripts/form-components.js')}}"></script>
	<script>
		jQuery(document).ready(function() { 
			App.init(); 
			FormComponents.init();
	   		FormValidation.init();
	   		$('.pagination li a').addClass('clk');
	   		var start = new Date("{!! date('D M d Y H:i:s',strtotime('+5 hours +30 minutes', strtotime(date('Y-m-d H:i:s')))) !!}");
			var end = new Date("{!! date('D M d Y H:i:s',strtotime('+6 day', strtotime(date('Y-m-d H:i:s')))) !!}");
			end.setHours(23);
	   		$('#appointmentdate').datetimepicker({
        		format: "dd-mm-yyyy hh:ii:ss",
        		startDate: start,
        		endDate: end,
        		autoclose: true,
			});
			$('#from_date,#to_date').datepicker({
    			format: "dd-mm-yyyy",
    			autoclose: true,
    		});
    	});

	   	$('.assign').click(function(){
			var ref = $(this).data('ref');
			$('#id').val(ref);
			$('#showlead').html(ref);
			$('#vocupdate')[0].reset();
			$('#call_sub_status').trigger('change');
			$('#appointment').modal('toggle');
		});

		$('#call_sub_status').change(function(){
			var value = $('#call_sub_status option:selected').val();
			$('#status_name').val($('#call_sub_status option:selected').html())
			$('#datetime').hide();
			$('#datetime :input').val('');
			if(value==12 || value==14 || value==24 || value==25 || value==26 || value==28){
				$('#datetime').show();
			}
		});

		$('.responsedata').click(function(){
			var leadid=$(this).data('lead');
			$.ajax({
				url: "{{ URL::to('rm-rejected-reason') }}/"+$(this).data('lead'),
				//data: {'id':$(this).data('lead')}, 
				type: 'get',
				async:true,
				cache: false,
				beforeSend:function(){	
					$('#loading').show();
				},
				success:function(response){
					/*var append="";
					$.each(response['interaction'],function(index,item){
	      				append+='<tr><td>'+(index+1)+'</td>';
	      				append+='<td>'+(item['created_at'])+'</td>';
	      				append+='<td>'+(item['rmname'])+'</td>';
	      				append+='<td>'+(item['call_status'])+'</td>';
	      				append+='<td>'+(item['call_sub_status'])+'</td>';
	      				append+='<td>'+(item['remarks'])+'</td></tr>';
					});
					$('#interaction').append('<tr>'+append+'</tr>');*/
					$('#verfierinteraction').html('');
					var append="";
					$.each(response['verifier'],function(index,item){
						var calldate = item['callback_date'];
						if(item['callback_date']==null){
							calldate="-";
						}
	      				append+='<tr><td>'+(index+1)+'</td>';
	      				append+='<td>'+(item['created_at'])+'</td>';
	      				append+='<td>'+(item['rmname'])+'</td>';
	      				append+='<td>'+(item['call_status'])+'</td>';
	      				append+='<td>'+(item['call_sub_status'])+'</td>';
	      				append+='<td>'+(calldate)+'</td>';
	      				append+='<td>'+(item['remarks'])+'</td></tr>';
					});
					$('#verfierinteraction').append('<tr>'+append+'</tr>');
				},
				error:function(){
					alert("Server is Busy!!");
				},
				complete:function (data) {
	    			$('#loading').hide();
	    		}
			});
   		});

		var call_sub_status = {!! json_encode($call_sub_status) !!};
		$('.meet').change(function(){
			$('#call_sub_status option').not(":first").remove();
			var meet = $(this).val();
			$.each(call_sub_status,function(index,item){ 
				if(item.code==meet && item.id!=27){
		   			$("#call_sub_status").append(
		        		$("<option></option>").text(item.name).val(item.id)
		      		);
		   		}
	    	});
	    });

	    $('.excel').click(function(){
			window.location.href="{{ route('voc-excel') }}?"+$('#frm').serialize();
		})
	</script>
@endsection
