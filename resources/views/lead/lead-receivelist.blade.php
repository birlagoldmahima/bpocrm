@extends('layout.master')
@section('page_title')
User List 
@endsection
@section('breadcrumb')
<li>
    <a href="#">Lead Requested </a>
</li>
@endsection
@section('content')
<header class="page-header">
    <h2>Lead Requested</h2>
    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="#">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span>Lead</span></li>
            <li><span>Lead Requested List</span></li>
        </ol>

        <a class="sidebar-right-toggle" data-open="#"><i class="fa fa-chevron-left"></i></a>
    </div>
</header>
<section role="main" class="content-body">
    <section class="panel">
        <div class="panel-body">
            <div class="col-md-12>"

        </div>
        <table class="table table-bordered table-striped mb-none" id="datatable-editable">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th class="hidden-phone">Mobile</th>                      
                    <th class="hidden-phone">Tell Caller</th>
                    <th class="hidden-phone">Emi Amount</th>


                </tr>
            </thead>
            <tbody>
                @if(count($result) > 0)
                <?php
                $call_status = callstatus();
                $sub_call_status = callsubstatus();
                ?>
                @foreach ($result as $key => $val)
                <tr class="gradeX">
                    <td>
                        <a href="{{ URL::to('lead-details',$val->reference_id)}}" class="on-default edit-row">{{$val->reference_id}}</a>
                    </td>
                    <td>
                        <?php echo empty($val->name) ? "-" : $val->name; ?>
                    </td>
                    <td>
                        <?php echo $val->email; ?> 
                    </td>
                    <td class="center hidden-phone">
                        <?php echo $val->mobile; ?> 
                    </td>

                    </td>
                    <td class="center hidden-phone">
                        <?php echo $val->username; ?>  
                    </td>
                    <td class="center hidden-phone">
                        <?php echo $val->emiamount; ?>  
                    </td>


                </tr>
                @endforeach
                @else
                <tr>
                    <td colspan="8" align="center">
                        Not Data Found
                    </td>
                </tr>
                @endif
            </tbody>
        </table>
        <div class ="pull-right" style="margin-bottom:20px">
            {!! $result->appends(['q'=>$q,'search_option'=>$search_option])->render() !!}

        </div>
    </div>
</section>
</section>
@endsection
@section('page_level_script_bottom')
@endsection			