@extends('layout.master')
    @section('page_title')
        Unassigned Lead
    @endsection
@section('page_level_style_top')
	<header class="page-header">
        <h2>Unassigned Lead</h2>
		<div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li>
                    <a href="#">
                        <i class="fa fa-home"></i>
                    </a>
                </li>
                <li><span>User</span></li>
                <li><span>Unassigned Lead</span></li>
            </ol>
			<a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
        </div>
    </header>
@endsection
@section('content')
	<section role="main" class="content-body">
    	<section class="panel">
	        <header class="panel-heading">
	        	<div class="row">
		            <h2 class="panel-title col-md-4">Unassigned Lead</h2>
		            @if(Session::get('flash_message'))
		            	<div class="alert alert-success col-md-3 {{ Session::get('flash_alert') }}" style="padding:7px;margin-bottom: -8px;">
		              		{{ Session::get('flash_message') }}
		            	</div>
		            @endif
		        </div>
	        </header>
        	<div class="panel-body">
        		<form class="navbar-form" name='frm' method='post' role="search" action="{{ route('unassigned-lead') }}">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<input type="hidden" name="currentpage" value="{{ !empty(request()->page)?request()->page:"1" }}">
					<table class="table">
						<tbody>
							<tr>
								<td>
	                      			<select class="form-control" name="relationship_manager" id="relationship_manager">
						        		<option value="">Select Telle Caller</option>
						        		@foreach($reporting as $value)
						        			<option value="{{ $value['tellcallerid'] }}">{{ $value['tellcallername'].' - '.$value['tlname'] }}</option>
						        		@endforeach
								 	</select>
								</td>
								<td>
									<button type="submit" id="assignRM" class="btn blue">Submit</button>
								</td>
								<td>&nbsp;</td>
							</tr>							
						</tbody>
					</table>
					<div class="table-scrollable">
						<table class="table table-striped table-hover table-bordered">
							<thead>
								<tr>
									<th>
										<input type="checkbox" class="group-checkable"  id="chkall"/>
									</th>
									<th>
										Id
									</th>
									<th>
										Lead Name
									</th>
									<th>
										Date Of Entry
									</th>
									<th>
										Mobile
									</th>
									<th>
										Email
									</th>
									
								</tr>
							</thead>
							<tbody>
								<?php $count=1 ;?>
								@foreach($result as $data)
								<tr>
									<td>
									    <div class="form-group">
										
											<div class="checkbox-list">
												<label>
												<?php $page = empty(request()->page) ? 1 : request()->page; ?>
												<input type="checkbox" name='chkbox[]' class="get_customet_id checkboxes" value="{{ $data->reference_id }}" id="{{$data->reference_id}}" @if(Session::has('ref_value') && array_key_exists($page,Session::get('ref_value'))) @if(in_array($data->reference_id, Session::get('ref_value')[$page])) checked @endif @endif ></label>
												
											</div>
										</div>
										
										
									</td>
									<td>
										{{$data->reference_id}}
									</td>
									<td>
										{{$data->name}}
									</td>
									<td>
										{{date("d-m-Y",strtotime('+330 minutes', strtotime($data->created)))}}
									</td>
									<td>
									   {{$data->mobile}}
									</td>
									<td>
									   {{$data->email}}
									</td>
									
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
					<div class ="pull-right" style="margin-bottom:20px">
		            	{{ $result->links() }}		
		            </div>
		        </form>
	        </div>
	    </section>
	</section>
</section>
@endsection
@section('page_level_script_bottom')
	<script src="{{URL::asset('public/assets/javascripts/app.js')}}"></script>
    <script>
	jQuery(document).ready(function() { 
	   App.init();
	   $('.pagination li a').addClass('clk');
	   $('#loading').hide();
	   $(".clk").on("click", function(e) {
			var parameter=($(this).attr('href').split('?')[1]);
			page = parameter.split('=')[1];
	    	document.frm.action="{{ route('unassigned-lead')}}?page="+page;
			document.frm.submit();
			return false;
		});
	});
	$('.submitbutton').on("click", function(e) {
		$('input[name=currentPage]').val('');
		return true;
	});

	$("#chkall").change(function () {
		if($(this).is(":checked")){
			$("div.checker span").addClass('checked');
		    $(".checkboxes").each(function(){
	        	$(this).prop('checked',true);
	      	}) 
		}else{
			$("div.checker span").removeClass('checked');
			$(".checkboxes").each(function(){
	        	$(this).prop('checked',false);
	      	}) 
		}	
	});

	$("#assignRM").on("click", function(e) {
		var check={!! json_encode(Session::get('ref_value')) !!};
   		var len=parseInt($('.checkboxes:checked').length);
   		if(check===null && len==0){
			alert("Please Select Lead!!!");
	   		return false;
	   	}

		var relationship_manager_id=$('#relationship_manager').val();
	 	if(relationship_manager_id==""){
	  		alert('Please Select Telle Caller!!');
	  		return false;
	  	}
		document.frm.action="{{ route('unassigned-lead-assign')}} ";
	  	document.frm.submit();
	  	return true;
	});
</script>
@endsection			