<aside id="sidebar-left" class="sidebar-left">

    <div class="sidebar-header">
        <div class="sidebar-title">
            Cherish Gold
        </div>
        <div class="sidebar-toggle hidden-xs" data-toggle-class="sidebar-left-collapsed" data-target="html" data-fire-event="sidebar-left-toggle">
            <i class="fa fa-bars" aria-label="Toggle sidebar"></i>
        </div>
    </div>

    <div class="nano">
        <div class="nano-content">
            <nav id="menu" class="nav-main" role="navigation">
                <ul class="nav nav-main">
                    <li @if($controller=="dashboard") class="nav-parent nav-expanded nav-active" @else class="nav-parent"  @endif>
                         <a href="{{URL::to('dashboard')}}">
                            <i class="fa fa-home" aria-hidden="true"></i>
                            <span>Dashboard </span>
                        </a>
                    </li>
                    @if(Auth::user()->role!=="Tele Caller")
                    <li @if($controller=="user") class="nav-parent nav-expanded nav-active" @else class="nav-parent"  @endif>
                         <a>
                            <i class="fa fa-users" aria-hidden="true"></i>
                            <span>Users</span>
                        </a>
                        <ul class="nav nav-children">
                            <li @if($action=="user-list") class="nav-active" @endif>
                                 <a href="{{ URL::route('userlist')}}">
                                    User List
                                </a>
                            </li>
                            <li @if($action=="add-user") class="nav-active" @endif>
                                 <a href="{{ URL::to('add_user_form')}}">
                                    Add User
                                </a>
                            </li>

                        </ul>
                    </li>
                    @endif

                    <li @if($controller=="lead") class="nav-parent nav-expanded nav-active" @else class="nav-parent"  @endif>
                         <a>
                            <i class="fa fa-user" aria-hidden="true"></i>
                            <span>Lead</span>
                        </a>
                        <ul class="nav nav-children">
                            <li @if($action=="assign-lead-list") class="nav-active" @endif>
                                 <a href="{{ URL::route('lead-list')}}">Lead List</a>
                            </li> 
                            <li @if($action=="leadadd") class="nav-active" @endif>
                                 <a href="{{ URL::to('lead')}}">Add Lead</a>
                            </li>
                        </ul>
                    </li>

                    @if(Auth::user()->role =="admin")
                    <li @if($controller=="fc") class="nav-parent nav-expanded nav-active" @else class="nav-parent"  @endif>
                         <a>
                            <i class="fa fa-align-left" aria-hidden="true"></i>
                            <span> Lms </span>
                        </a>
                        <ul class="nav nav-children">                            
                            <li class="nav-parent">
                                <a>Lead History</a>
                                <ul class="nav nav-children">

                                    <li @if($action=="rejectedlist") class="nav-active" @endif>
                                         <a href="{{ URL::route('rm-rejected-list')}}">
                                            Rejected List
                                        </a>
                                    </li>
                                    <li @if($action=="acceptedlist") class="nav-active" @endif>
                                         <a href="{{ URL::route('rm-accept-list')}}">
                                            Accepted List
                                        </a>
                                    </li>

                                    <li @if($action=="fofltsale") class="nav-active" @endif>
                                         <a href="{{ URL::route('fo-ftl-sale')}}">
                                            Sale History
                                        </a>
                                    </li>  

                                    <li @if($action=="lmsreport") class="nav-active" @endif>
                                         <a href="{{ URL::route('lms-report')}}">
                                            Lms Report
                                        </a>
                                    </li>   
                                </ul>
                            </li>

                            <li class="nav-parent">
                                <a>SPOC Management</a>
                                <ul class="nav nav-children">                                  
                                    <li @if($action=="spokField") class="nav-active" @endif>
                                         <a href="{{ URL::route('spok-list')}}">
                                            Assign Field
                                        </a>
                                    </li>                                    
                                </ul>
                            </li>   


                            <li @if($action=="qualityverifyList") class="nav-active" @endif>
                                 <a href="{{ URL::route('quality-list')}}">
                                    Verify Lead
                                </a>
                            </li>
                            <li @if($action=="voclist") class="nav-active" @endif>
                                 <a href="{{ URL::route('voc-list')}}">
                                    VOC List 
                                </a>
                            </li>




                            <li class="nav-parent">
                                <a>Ftl Details</a>
                                <ul class="nav nav-children">                                  
                                    <li @if($action=="ftllist") class="nav-active" @endif>
                                         <a href="{{ URL::route('ftl-list')}}">
                                            Assign FO 
                                        </a>
                                    </li>
                                    <li @if($action=="ftlassigned") class="nav-active" @endif>
                                         <a href="{{ URL::route('ftl-assigned-list')}}">
                                            Assigned FO 
                                        </a>
                                    </li>                                   
                                </ul>
                            </li> 

                            <li class="nav-parent">
                                <a>Fo Details</a>
                                <ul class="nav nav-children">                                  
                                    <li @if($action=="folist") class="nav-active" @endif>
                                         <a href="{{ URL::route('fo-list')}}">
                                            Lead Acceptance 
                                        </a>
                                    </li>
                                    <li @if($action=="fomeeting") class="nav-active" @endif>
                                         <a href="{{ URL::route('fo-meeting-list')}}">
                                            Lead Meeting 
                                        </a>
                                    </li>                                  
                                </ul>
                            </li> 

                            <li class="nav-parent">
                                <a>Tl Details</a>
                                <ul class="nav nav-children">                                  
                                    <li @if($action=="tlassign-lead-list") class="nav-active" @endif>
                                         <a href="{{ URL::route('tllead-list')}}">
                                            TL Verify 
                                        </a>
                                    </li>                                  
                                </ul>
                            </li> 

                        </ul>
                    </li>

                    @endif 




                    @if(Auth::user()->role =="admin")
                    <li @if($controller=="fc") class="nav-parent nav-expanded nav-active" @else class="nav-parent"  @endif>
                         <a>
                            <i class="fa fa-user" aria-hidden="true"></i>
                            <span>Lead Quality Assign t</span>
                        </a>
                        <ul class="nav nav-children">
                            <li @if($action=="assignquality") class="nav-active" @endif>
                                 <a href="{{ URL::route('leadQualityList')}}">
                                    Assign Quality Lead
                                </a>
                            </li>  

                        </ul>
                    </li>
                    @endif 


                    @if(Auth::user()->role!=="Tele Caller")
                    <li @if($controller=="leadsource") class="nav-parent nav-expanded nav-active" @else class="nav-parent"  @endif>
                         <a>
                            <i class="fa fa-user" aria-hidden="true"></i>
                            <span>Lead Source</span>
                        </a>
                        <ul class="nav nav-children">
                            <li @if($action=="sourceadd") class="nav-active" @endif>
                                 <a href="{{ URL::route('lead_source')}}">
                                    Add Lead Source 
                                </a>
                            </li>

                        </ul>
                    </li>
                    @endif

                    @if(Auth::user()->role!=="Tele Caller")
                    <li @if($controller=="unassignedlead") class="nav-parent nav-expanded nav-active" @else class="nav-parent"  @endif>
                         <a>
                            <i class="fa fa-user" aria-hidden="true"></i>
                            <span>Unassigned Lead</span>
                        </a>
                        <ul class="nav nav-children">
                            <li @if($action=="unassignedlead-list") class="nav-active" @endif>
                                 <a href="{{ URL::route('unassigned-lead')}}">
                                    Unassigned Lead 
                                </a>
                            </li>

                        </ul>
                    </li>
                    @endif


                    @if(Auth::user()->role=="Team Lead")
                    <li @if($controller=="unassignedlead") class="nav-parent nav-expanded nav-active" @else class="nav-parent"  @endif>
                         <a>
                            <i class="fa fa-user" aria-hidden="true"></i>
                            <span>LMS</span>
                        </a>
                        <ul class="nav nav-children">
                            <li @if($action=="tlassign-lead-list") class="nav-active" @endif>
                                 <a href="{{ URL::route('tllead-list')}}">
                                    TL Verify 
                                </a>
                            </li>

                        </ul>
                    </li>
                    @endif    




                    @if(Auth::user()->role=="Verifier Head")
                    <li @if($controller=="verifierList") class="nav-parent nav-expanded nav-active" @else class="nav-parent"  @endif>
                         <a>
                            <i class="fa fa-user" aria-hidden="true"></i>
                            <span>LMS</span>
                        </a>
                        <ul class="nav nav-children">
                            <li @if($action=="qualityverifyList") class="nav-active" @endif>
                                 <a href="{{ URL::route('quality-list')}}">
                                    Verify Lead
                                </a>

                            </li>
                            <li @if($action=="verifierList") class="nav-active" @endif>
                                 <a href="{{ URL::route('verifier-list')}}">
                                    Assign Lead
                                </a>

                            </li>
                        </ul>
                    </li>
                    @endif 




                    @if(Auth::user()->role=="Verifier")
                    <li @if($controller=="qualityverifyList") class="nav-parent nav-expanded nav-active" @else class="nav-parent"  @endif>
                         <a>
                            <i class="fa fa-user" aria-hidden="true"></i>
                            <span>LMS</span>
                        </a>

                        <ul class="nav nav-children">
                            <li @if($action=="qualityverifyList") class="nav-active" @endif>
                                 <a href="{{ URL::route('quality-list')}}">
                                    Verify Lead
                                </a>

                            </li>                            
                        </ul>
                    </li>
                    @endif 

                    @if(Auth::user()->role=="SPOC")
                    <li @if($controller=="spokField") class="nav-parent nav-expanded nav-active" @else class="nav-parent"  @endif>
                         <a>
                            <i class="fa fa-user" aria-hidden="true"></i>
                            <span>LMS</span>
                        </a>

                        <ul class="nav nav-children">
                            <li @if($action=="spokField") class="nav-active" @endif>
                                 <a href="{{ URL::route('spok-list')}}">
                                    Assign Lead
                                </a>

                            </li>                            
                        </ul>
                    </li>
                    @endif 



                    @if(Auth::user()->role=="FTL")
                    <li @if($controller=="ftl") class="nav-parent nav-expanded nav-active" @else class="nav-parent"  @endif>
                         <a>
                            <i class="fa fa-user" aria-hidden="true"></i>
                            <span>LMS</span>
                        </a>
                        <ul class="nav nav-children">
                            <li @if($action=="ftllist") class="nav-active" @endif>
                                 <a href="{{ URL::route('ftl-list')}}">
                                    LMS FTL Assign FO 
                                </a>
                            </li>
                            <li @if($action=="ftlassigned") class="nav-active" @endif>
                                 <a href="{{ URL::route('ftl-assigned-list')}}">
                                    LMS FTL Assigned FO 
                                </a>
                            </li>

                        </ul>
                    </li>
                    @endif

                    @if(Auth::user()->role ==  "VOC")
                    <li @if($controller=="ftl") class="nav-parent nav-expanded nav-active" @else class="nav-parent"  @endif>
                         <a>
                            <i class="fa fa-user" aria-hidden="true"></i>
                            <span>LMS</span>
                        </a>
                        <ul class="nav nav-children">
                            <li @if($action=="voclist") class="nav-active" @endif>
                                 <a href="{{ URL::route('voc-list')}}">
                                    VOC List 
                                </a>
                            </li>
                            <li @if($action=="vocnotinterested") class="nav-active" @endif>
                                 <a href="{{ URL::route('voc-notinterested')}}">
                                    VOC Not Interested 
                                </a>
                            </li>

                        </ul>
                    </li>
                    @endif

                    @if(in_array(Auth::user()->role,array('FO','Admin')))
                    <li @if($controller=="FO") class="nav-parent nav-expanded nav-active" @else class="nav-parent"  @endif>
                         <a>
                            <i class="fa fa-user" aria-hidden="true"></i>
                            <span>LMS</span>
                        </a>
                        <ul class="nav nav-children">
                            <li @if($action=="folist") class="nav-active" @endif>
                                 <a href="{{ URL::route('fo-list')}}">
                                    LMS FO Lead Acceptance 
                                </a>
                            </li>
                            <li @if($action=="fomeeting") class="nav-active" @endif>
                                 <a href="{{ URL::route('fo-meeting-list')}}">
                                    LMS FO Lead Meeting 
                                </a>
                            </li>

                        </ul>
                    </li>
                    @endif


                    @if(Auth::user()->role!=="Tele Caller")
                    <li @if($controller=="verifylink") class="nav-parent nav-expanded nav-active" @else class="nav-parent"  @endif>
                         <a>
                            <i class="fa fa-user" aria-hidden="true"></i>
                            <span>Mass link</span>
                        </a>
                        <ul class="nav nav-children">
                            <li @if($action=="linkverification") class="nav-active" @endif>
                                 <a href="{{ URL::route('verifylink')}}">
                                    Mass link
                                </a>
                            </li>

                        </ul>
                    </li>
                    @endif

                    @if(Auth::user()->role!=="Tele Caller")
                    <li @if($controller=="notinterested") class="nav-parent nav-expanded nav-active" @else class="nav-parent"  @endif>
                         <a>
                            <i class="fa fa-user" aria-hidden="true"></i>
                            <span>Not Interested</span>
                        </a>
                        <ul class="nav nav-children">
                            <li @if($action=="notinterestedlist") class="nav-active" @endif>
                                 <a href="{{ URL::route('not-interestedlist')}}">
                                    Not Interested List
                                </a>
                            </li>

                        </ul>
                    </li>
                    @endif

                    @if(Auth::user()->role!=="Tele Caller")
                    <li @if($controller=="reportc") class="nav-parent nav-expanded nav-active" @else class="nav-parent"  @endif>
                         <a>
                            <i class="fa fa-user" aria-hidden="true"></i>
                            <span> Change Rm</span>
                        </a>
                        <ul class="nav nav-children">
                            <li @if($action=="leadchangerm") class="nav-active" @endif>
                                 <a href="{{ URL::route('leadchangerm')}}">
                                    Change Rm
                                </a>
                            </li>

                        </ul>
                    </li>
                    @endif

                    @if(Auth::user()->role!=="Tele Caller")
                    <li @if($controller=="report") class="nav-parent nav-expanded nav-active" @else class="nav-parent"  @endif>
                         <a>
                            <i class="fa fa-user" aria-hidden="true"></i>
                            <span>Report</span>
                        </a>
                        <ul class="nav nav-children">
                            <li @if($action=="lead") class="nav-active" @endif>
                                 <a href="{{ URL::to('leadlist-report')}}">
                                    Lead List
                                </a>
                            </li>
                            <li @if($action=="converted") class="nav-active" @endif>
                                 <a href="{{ URL::route('convertedlist')}}">
                                    Converted List
                                </a>
                            </li>
                            <li @if($action=="request") class="nav-active" @endif>
                                 <a href="{{ URL::route('requestlist')}}">
                                    Request List
                                </a>
                            </li>
                            <li @if($action=="verification") class="nav-active" @endif>
                                 <a href="{{ URL::route('verificationlist')}}">
                                    Verification List
                                </a>
                            </li>
                            <li @if($action=="notinterested") class="nav-active" @endif>
                                 <a href="{{ URL::route('notinterestedlist')}}">
                                    Not Interested List
                                </a>
                            </li>
                        </ul>
                    </li>
                    @endif

                    @if(Auth::user()->role=="Team Lead")
                    <li @if($controller=="reportc") class="nav-parent nav-expanded nav-active" @else class="nav-parent"  @endif>
                         <a>
                            <i class="fa fa-user" aria-hidden="true"></i>
                            <span> Lms </span>
                        </a>
                        <ul class="nav nav-children">
                            <li @if($action=="assign-tllead-list") class="nav-active" @endif>
                                 <a href="{{ URL::route('tllead-list')}}">Lead List</a>
                            </li> 

                        </ul>
                    </li>
                    @endif

                    <!-- <li @if($controller=="report") class="nav-parent nav-expanded nav-active" @else class="nav-parent"  @endif>
                         <a>
                            <i class="fa fa-users" aria-hidden="true"></i>
                            <span>Change RM</span>
                        </a>
                        <ul class="nav nav-children">
                            <li @if($action=="leadchangerm") class="nav-active" @endif>
                                 <a href="{{ URL::route('leadchangerm')}}">Change RM List</a>
                            </li> 

                        </ul>
                    </li> -->

                </ul>
            </nav>


        </div>

    </div>

</aside>
