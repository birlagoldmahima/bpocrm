<header class="header">
    <div class="logo-container">
        <a href="{{ route('dashboard') }}" class="logo">
            <img src="{{URL::asset('public/assets/images/logo1.png')}}" height="35" alt="Cherish Gold BPO-CRM" />
		</a>
        <div class="visible-xs toggle-sidebar-left" data-toggle-class="sidebar-left-opened" data-target="html" data-fire-event="sidebar-left-opened">
            <i class="fa fa-bars" aria-label="Toggle sidebar"></i>
        </div>
    </div>
    <div class="header-right">
        <span class="separator"></span>
        <ul class="notifications">
            <li>
                <div>
                    <strong>Gold Price : {{ Session::get('goldprice') }} (22 K)</strong>    
                </div>
            </li>
            
        </ul>
        <span class="separator"></span>
        <div id="userbox" class="userbox">
            <a href="#" data-toggle="dropdown">
                <figure class="profile-picture">
                    <?php if(empty(Session::get('company_image'))){
                        $img = "No_image.jpg";
                    }else{
                        $img = Session::get('company_image');
                        if(!file_exists(config('custom.urlimage').'BPO/'.$img)){
                            $img = "No_image.jpg";
                        }
                    }?>
                    <img src="{{ config('custom.urlimage').'BPO/'.$img }}" alt="No Image" class="img-circle" />
                </figure>
                <div class="profile-info" data-lock-name="John Doe" data-lock-email="johndoe@okler.com">
                    <span class="name">{{ Session::get('company_name') }}</span>
                    <span class="role">{{ Auth::user()->name }}</span>
                </div>
                <i class="fa custom-caret"></i>
            </a>

            <div class="dropdown-menu">
                <ul class="list-unstyled">
                    <li class="divider"></li>
                    <li>
                        <a role="menuitem" tabindex="-1" href="{{URL::to('userprofile')}}"><i class="fa fa-user"></i> My Profile</a>
                    </li>
                    <li>
                        <a role="menuitem" tabindex="-1" href="{{URL::to('logout')}}"><i class="fa fa-power-off"></i> Logout</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</header>