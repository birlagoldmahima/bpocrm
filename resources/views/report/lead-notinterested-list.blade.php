@extends('layout.master')
@section('page_title')
User List 
@endsection
@section('breadcrumb')
<li>
    <a href="#">User List </a>
</li>
@endsection
@section('content')
<section role="main" class="content-body">
    <header class="page-header">
        <h2>Lead Marked In </h2>
        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li>
                    <a href="#">
                        <i class="fa fa-home"></i>
                    </a>
                </li>
                <li><span>Lead</span></li>
               
            </ol>

            <a class="sidebar-right-toggle" data-open="#"><i class="fa fa-chevron-left"></i></a>
        </div>
    </header>
    <section class="panel">
        <div class="panel-body">

            <div class="col-md-12" style="margin-bottom: 20px;">
<!--                <form class="navbar-form" role="search" action="#" name="frm">
                    <table class="table table-striped table-hover table-bordered ">
                        <tbody>
                            <tr>
                                <td>
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Search :</label>
                                        <div class="col-md-8">
                                            <input type="text" name='q' value="" class="form-control">
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">

                                        <div class="col-md-4">
                                            <select name="search_option" class="form-control">
                                                <option value="">-- Select --</option>
                                            </select>
                                        </div>

                                    </div>
                                </td>
                                <td class="text-center">
                                    <div class="form-group">
                                        <button type="submit" class="btn blue">Submit</button>
                                        <a class="btn red" href="">Clear All</a>
                                    </div>
                                </td>
                            </tr>													
                        </tbody>
                    </table>
                    <input type="hidden" id="sort" name="sort" value="">
                    <input type="hidden" id="sorttype" name="sorttype" value="">	
                </form>-->
            </div>
            <table class="table table-bordered table-striped mb-none" id="datatable-editable">
                <thead>
                    <tr>

                        <th>ID</th>
                        <th>Name</th>

                        <th>Report Manager</th>                   
                        <th>Call Status</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>                  
                    @if(count($leadsourcevalue) > 0)
                    <?php
                    $count = 1;
                    $call_status = callstatus();
                    $sub_call_status = callsubstatus();
                    ?>
                    @foreach ($leadsourcevalue as $key => $val)
                    <tr class="gradeX tr"  data-id="{{ $count }}">

                        <td> 
                            <?php echo $val->leadid ?>  
                        </td>
                        <td>
                            <?php echo $val->leadname ?> 
                        </td>

                        <td class="center hidden-phone">
                            <?php echo $val->reporting_manager ?> 
                            <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">

                        </td>


                        <td>
                            {{ !empty($val->call_status_id) ? $call_status[$val->call_status_id] : "" }}
                        </td> 
                        <td>
                            <?php echo $val->status ?> 
                        </td>
                        <td class="center hidden-phone">
                            <a id="{{ $val->leadid }}" data-value="{{ $count }}" class="btn green clk  btn-primary">Mark Not Interested</a>

                        </td>
                    </tr>
                    <?php $count++; ?>
                    @endforeach
                    @else
                    <tr>
                        <td colspan="8" align="center">
                            Not Data Found
                        </td>
                    </tr>
                    @endif
                </tbody>
            </table>
        </div>
        <div class ="pull-right" style="margin-bottom:20px">
            {!! $leadsourcevalue->appends(['fromdate'=>(isset($fromdate)?$fromdate:""),'todate'=>(isset($todate)?$todate:"")])->render() !!}


        </div>
    </section>
</section>

<div class="container">
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-body" id="alt">
                    <p id="para"></p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('page_level_script_bottom')
<script src="{{URL::to('/public/assets/javascripts/app.js')}}"></script>
<script src="{{URL::to('/public/assets/bootbox/bootbox.min.js')}}"></script>
<script type="text/javascript">

$('.excel').click(function () {
    window.location.href = "{{ route('lead-report-export') }}?" + $('#frm').serialize();
})
</script>

<script>

    jQuery(document).ready(function () {

        $('.clk').click(function () {

            var id = $(this).attr('id');
            var trvalue = $(this).attr('data-value');
            var token = $('#_token').val();
            //alert(token);
            bootbox.confirm("Are you sure, You want to mark this Reference Not interested", function (result) {
                if (result == true) {
                    $.ajax({
                        url: "{{ route('update-interested-list')}}",
                        data: {'id': id, '_token': token},
                        type: 'post',
                        cache: false,
                        clearForm: false,
                        success: function (response) {
                            $('.tr[data-id="' + trvalue + '"]').remove();
                            $('#myModal').modal('show');
                            $('#alt').addClass('alert-success');
                            $('#alert').addClass('alert-success');
                            $('#para').html('Lead  Marked as Not Interested!!');
                        }
                    });
                }
            });
        });

    });

</script>

@endsection			