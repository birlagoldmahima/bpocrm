@extends('layout.master')
@section('page_title')
    Lead List 
@endsection
@section('page_level_style_top')
    <header class="page-header">
        <h2>Lead List</h2>
        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li>
                    <a href="#">
                        <i class="fa fa-home"></i>
                    </a>
                </li>
                <li><span>Lead</span></li>
                <li><span>Lead List</span></li>
            </ol>
            <a class="sidebar-right-toggle" data-open="#"><i class="fa fa-chevron-left"></i></a>
        </div>
    </header>
@endsection
@section('content')
    <section role="main" class="content-body">
        <section class="panel">
            <header class="panel-heading">
                <h2 class="panel-title">Lead List</h2>
            </header>
        </section>
        <div class="panel-body">
            <form class="form-horizontal" id="frm">
                <table class="table"> 
                    <tbody>
                        <tr>
                            <td>From Date</td>
                            <td>
                                <input type="text" class="form-control datepick" name="fromdate" value="{{isset($fromdate)?$fromdate:""}}" readonly>
                            </td>
                            <td>To Date</td>
                            <td>
                                <input type="text" class="form-control datepick" name="todate" value="{{isset($todate)?$todate:""}}" readonly>
                            </td>
                            <td>
                                <button type="submit" class="btn btn-primary" >Submit</button>
                                <a href="{{ route('leadlist_report') }}" class="btn btn-primary" >Clear</a>
                            </td>
                            <td>
                                <button class="btn btn-primary excel" type="button">Excel</button>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </form>
            <div class="table-scrollable">
                <table class="table table-bordered table-striped mb-none" id="datatable-editable" style="display: block; overflow: scroll; table-layout: fixed;">
                    <thead>
                        <tr>
                            <th>Created</th>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Mobile</th>
                            <th>State</th>
                            <th>City</th>
                            <th>Lead Source</th>
                            <th>Tele Caller</th>
                            <th>Call Status</th>
                            <th>Call Sub Status</th>
                            <th>Remarks</th>
                        </tr>
                    </thead>
                    <tbody>                  
                        @if(count($leadsourcevalue) > 0)
                            <?php $call_status = callstatus();
                            $sub_call_status= callsubstatus();?>
                            @foreach ($leadsourcevalue as $key => $val)
                                <tr class="gradeX">
                                    <td> 
                                        <?php echo date('d-m-Y',strtotime($val->leadcrated))?>  
                                    </td>
                                    <td> 
                                        <?php echo $val->leadid ?>  
                                    </td>
                                    <td>
                                        <?php echo $val->leadname ?> 
                                    </td>
                                    <td>
                                        <?php echo $val->leademail ?> 
                                    </td>
                                    <td class="center hidden-phone">
                                        <?php echo $val->leadmobile ?> 
                                    </td>
                                    <td class="center hidden-phone">
                                        <?php echo $val->state_name ?>  
                                    </td>
                                    <td class="center hidden-phone">
                                        <?php echo $val->city_name ?>  
                                    </td>
                                    <td class="center hidden-phone">
                                        <?php echo $val->leadsource_name ?>  
                                    </td>
                                    <td class="center hidden-phone">
                                        <?php echo $val->username ?>  
                                    </td>
                                    <td>
                                        {{ !empty($val->call_status_id) ? $call_status[$val->call_status_id] : "" }}
                                    </td> 
                                    <td>
                                        {{ !empty($val->call_substatus_id) ? $sub_call_status[$val->call_substatus_id] : "" }}
                                    </td>
                                    <td class="center hidden-phone">
                                        <?php echo $val->remarks ?>  
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="12" align="center">
                                    Not Data Found
                                </td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            </div>
            <div class ="pull-right" style="margin-bottom:20px">
                {!! $leadsourcevalue->appends(['fromdate'=>(isset($fromdate)?$fromdate:""),'todate'=>(isset($todate)?$todate:"")])->render() !!}
            </div>
        </div>
    </section>
@endsection
@section('page_level_script_bottom')
    <script src="{{URL::asset('public/assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
    <script type="text/javascript"> 
        jQuery(document).ready(function() { 
            $('.datepick').datepicker({
                format: "dd-mm-yyyy" 
            });
            $('#loading').hide();
        });

        $('.excel').click(function(){
            window.location.href="{{ route('lead-report-export') }}?"+$('#frm').serialize();
        })
    </script>

@endsection			