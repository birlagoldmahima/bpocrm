@extends('layout.master')
@section('page_title')
    Not Interested List 
@endsection
@section('page_level_style_top')
    <header class="page-header">
        <h2>Not Interested List</h2>
        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li>
                    <a href="#">
                        <i class="fa fa-home"></i>
                    </a>
                </li>
                <li><span>Dashboard</span></li>
                <li><span>Not Interested List</span></li>
            </ol>
            <a class="sidebar-right-toggle" data-open="#"><i class="fa fa-chevron-left"></i></a>
        </div>
    </header>
@endsection
@section('content')
    <section role="main" class="content-body">
        <section class="panel">
            <header class="panel-heading">
                <h2 class="panel-title">Not Interested List</h2>
            </header>
        </section>
        <div class="panel-body">
            <table class="table table-striped table-hover table-bordered"> 
                <tbody>
                    <tr>
                        <td>
                            <button type="button" id="clk" class="btn btn-primary">Excel</button>
                        </td>
                    </tr>
                </tbody>
            </table><br>
            <!-- <div class="col-md-12">
                <div class="row">
                    <form class="form-horizontal">
                        <div class="col-md-1">From Date</div>
                        <div class="col-md-2">  
                            <input type="text" name="q" class="form-control" id="exampleInputUsername2" placeholder="Search" value="{{isset($q)?$q:""}}">
                        </div>
                        <div class="col-md-1">From Da</div>
                        <div class="col-md-2">  
                            <input type="text" name="q" class="form-control" id="exampleInputUsername2" placeholder="Search" value="{{isset($q)?$q:""}}">
                        </div>
                        <div class="col-md-3">
                            <button type="submit" class="btn btn-primary">Submit</button>
                           
                        </div>
                    </form>
                </div>
            </div> -->
            <table class="table table-bordered table-striped mb-none" id="datatable-editable">
                <thead>
                    <tr>
                        <th>Lead ID</th>
                        <th>Created Date</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Mobile</th>                      
                        <th>Tell Caller</th>
                    </tr>
                </thead>
                <tbody>
                    @if(count($result) > 0)
                        @foreach ($result as $key => $val)
                            <tr>
                                <td>
                                    {{ $val->reference_id }}
                                </td>
                                <td>
                                    {{ $val->created_date }}
                                </td>
                                <td>
                                    {{ $val->name }}
                                </td>   
                                <td>
                                    {{ $val->email }}
                                </td>
                                <td>
                                    {{ $val->mobile }}
                                </td>
                                <td>
                                    {{ $val->tellcallername }}
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="8" align="center">
                                Not Data Found
                            </td>
                        </tr>
                    @endif
                </tbody>
            </table>
            <div class ="pull-right" style="margin-bottom:20px">
                {!! $result->render() !!}
            </div>
        </div>
    </section>
@endsection
@section('page_level_script_bottom')
    <script type="text/javascript">
        $(document).ready(function(){
            $('#loading').hide();
        });

        $('#clk').click(function(){
            window.location.href="{{ route('notinterestedlistexport') }}";
        })
    </script>
@endsection			