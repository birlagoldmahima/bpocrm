@extends('layout.master')
@section('page_title')
    Call Back List 
@endsection
@section('page_level_style_top')
    <header class="page-header">
        <h2>Call Back List</h2>
        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li>
                    <a href="#">
                        <i class="fa fa-home"></i>
                    </a>
                </li>
                <li><span>Call Back</span></li>
                <li><span>Call Back List</span></li>
            </ol>
            <a class="sidebar-right-toggle" data-open="#"><i class="fa fa-chevron-left"></i></a>
        </div>
    </header>
@endsection
@section('content')
    <section role="main" class="content-body">
        <section class="panel">
            <header class="panel-heading">
                <h2 class="panel-title">Call Back List</h2>
            </header>
            <div class="panel-body">
                <div class="col-md-12>"
                    
                </div>
                <table class="table table-bordered table-striped mb-none">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Mobile</th>
                            <th>Tele Caller</th>
                            <th>CallBack</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($result)>0)
                            @foreach ($result as $key => $val)
                                <tr>
                                    <td>{{ $val->reference_id }}</td>
                                    <td>{{ $val->leadname }}</td>
                                    <td>{{ $val->leademail }}</td>
                                    <td>{{ $val->leadmobile }}</td>
                                    <td>{{ $val->username }}</td>
                                    <td>{{ $val->callbacktime }}</td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="8" align="center">
                                    Not Data Found
                                </td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            </div>
            <div class ="pull-right" style="margin-bottom:20px">
                {!! $result->render() !!}
            </div>
        </section>
    </section>
@endsection
@section('page_level_script_bottom')
    <script>
        jQuery(document).ready(function () {
            $('#loading').hide();
        });
    </script>
@endsection         