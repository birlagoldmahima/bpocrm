@extends('layout.master')
@section('page_title')
    Mass Link
@endsection
@section('page_level_style_top')
    <header class="page-header">
        <h2>Add Lead</h2>
        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li>
                    <a href="#">
                        <i class="fa fa-home"></i>
                    </a>
                </li>
                <li><span>Mass Link</span></li>
                <li><span>Mass Link</span></li>
            </ol>

            <a class="sidebar-right-toggle" data-open="#"><i class="fa fa-chevron-left"></i></a>
        </div>
    </header>
@endsection
@section('content')
    <section role="main" class="content-body">
        <section class="panel">
            <header class="panel-heading">
                <div class="row">
                    <div class="col-md-10">
                        <h2 class="panel-title col-md-4">Mass Link</h2>
                        @if(Session::get('message'))
                            <div class="alert alert-success col-md-5 alert-success text-center" style="padding:7px;margin-bottom: -8px;">
                                {{ Session::get('message') }}
                            </div>
                        @endif
                         @if($errors->any())
                            <div class="alert-danger text-center alert col-md-5" style="padding:7px;margin-bottom: -8px;">
                                @foreach($errors->all() as $error)
                                    <p>{{ $error }}</p>
                                @endforeach
                            </div>
                        @endif
                    </div>
                    <div class="col-md-2">
                        <a class="btn btn-primary" href="#sendsmslink" id="addnewlink" data-toggle="modal">Add New Link</a>
                    </div>
                </div>
            </header>
            <div class="panel-body">
                <table class="table table-bordered table-striped mb-none">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>From Date</th>
                            <th>To Date</th>
                            <th>Link</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $count = 0;?>
                        @if(count($result) > 0)
                            @foreach ($result as $key => $val)
                                <tr>
                                    <td>
                                        {{ $key+1 }}
                                    </td>
                                    <td>
                                        {{ date('d-m-Y',strtotime($val->fromdate)) }}
                                    </td>
                                    <td>
                                        {{ date('d-m-Y',strtotime($val->todate)) }}
                                    </td>
                                    <td>
                                        <textarea  class="form-control" rows="4" readonly>{{ config('custom.selfurl').'register/bpo?id='.base64_encode(json_encode(['id'=>$val->id,'token'=>Session::get('api_key')])) }}</textarea>
                                    </td>
                                    <td>
                                        @if(strtotime($val->todate)< strtotime(date('Y-m-d')))
                                            In active
                                        @else
                                            Active
                                            <?php $count +=1; ?>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="8" align="center">
                                    Not Data Found
                                </td>
                            </tr>
                        @endif
                    </tbody>
                </table>
                <div class ="pull-right" style="margin-bottom:20px">
                    {!! $result->render() !!}
                </div>
            </div>
        </section>
    </section>
    @if($count<2)
        <div class="modal fade" id="sendsmslink" tabindex="-1" role="basic" aria-hidden="true"  data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form method="post" action="{{ route('masklink') }}">
                        <input type="hidden" value="{{ csrf_token() }}" name="_token">
                        <div class="modal-header">
                            <h4 class="modal-title">Add New Mask Link</h4>
                        </div>
                        <div class="modal-body">
                            <span id="appointfed"></span>
                            <div class="form-body">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-2 control-label">Period:</label>
                                        <div class="col-md-6">
                                            <select name="period" id="period" class="form-control">
                                                <option value="">Select Period</option>
                                                <option value="3">3 Days</option>
                                                <option value="5">5 Days</option>
                                                <option value="7">7 Days</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn green" onclick="return check()">Create</button>
                            <button type="button" class="btn default" data-dismiss="modal">Close</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    @endif
@endsection
@section('page_level_script_bottom')
    <script>
        jQuery(document).ready(function () {
            $('#loading').hide();
            @if($count>=2)
                $('#addnewlink').remove();
            @endif
        });

        function open(){
            $('#sendsmslink').modal('toggle');
        }

        function check(){
            var value = $('#period option:selected').val();
            if(value==""){
                alert("Please Select Period!!");
                return false;
            }
            return true;
        }
    </script>
</script>

@endsection
