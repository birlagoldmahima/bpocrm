@extends('layout.master')
@section('page_title')
    Add User
@endsection
@section('breadcrum')
    <li><a href="#">Add User</a></li>
@endsection
@section('content')
<section role="main" class="content-body">
    <header class="page-header">
        <h2>Add User</h2>
        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li>
                    <a href="#">
                        <i class="fa fa-home"></i>
                    </a>
                </li>
                <li><span>Users</span></li>
                <li><span>Add user</span></li>
            </ol>
            <a class="sidebar-right-toggle" data-open="#"><i class="fa fa-chevron-left"></i></a>
        </div>
    </header>
    <div class="row">
         <div class="loading" id="loading"> 
            <img class="loading-image" id="loading-image" src="{{URL::asset('public/assets/images/ajax-loading.gif')}}" alt="Loading..." />
        </div>
        <div class="col-lg-12">
            <form id="form" action="{{ url('add_user') }}" class="form-horizontal form-bordered" method="post" name="form">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <section class="panel">
                    <header class="panel-heading">
                        <h2 class="panel-title">Add User</h2>
                    </header>
                    <div class="panel-body">
                        <div class="form-group">
                            <label class="col-sm-3 control-label"> Name <span class="required">*</span></label>
                            <div class="col-sm-6">
                                <input type="text" name="name" class="form-control" placeholder="Please Enter Name" id="name" required/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Mobile</label>
                            <div class="col-sm-6">
                                <span id="mobile"/></span>
                            </div>
                        </div>  
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Email <span class="required">*</span></label>
                            <div class="col-sm-6">                              
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-envelope"></i>
                                    </span>
                                    <input type="email" name="email" class="form-control" placeholder="Please Enter Email" autocomplete="off" required id="email"/>
                                </div>
                            </div>
                        </div>
                       
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Password <span class="required">*</span></label>
                            <div class="col-sm-6">
                                <input type="password" name="password" class="form-control" placeholder="Please Enter Password" required />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Re-enter Password<span class="required">*</span></label>
                            <div class="col-sm-6">
                                <input type="password" name="password" class="form-control" placeholder="Please Enter Password" required />
                            </div>
                        </div>
                    </div>
                    <footer class="panel-footer">
                        <div class="row">
                            <div class="col-sm-12" align="center">
                                <button class="btn btn-primary" id="submit" type="submit">Submit</button>
                                <a href="{{ url('user_list') }}" class="btn btn-default">Cancel </a>
                            </div>
                        </div>
                    </footer>
                </section>
            </form>
        </div>
    </div>
</section>
@endsection
@section('page_level_script_bottom')
    <script src="{{URL::asset('public/assets/javascripts/forms/form-validation.js')}}"></script>
    <script src="{{URL::asset('public/assets/javascripts/app.js')}}"></script>
    <script>
    jQuery(document).ready(function () {
        App.init();
        //FormComponents.init();
        FormValidation.init();
        $.ajax({
            url: "{{route('check-url')}}",
            type: 'get',
            async: true,
            cache: false,
            clearForm: false,
            beforeSend:function(){  
              $('#loading').show();
            },
            success: function (response) {
                if ('error' in response){
                    
                }else if('success' in response){
                    var data = response['success'];
                    $('#name').val(data['name']);
                    $('#email').val(data['email']);
                    $('#mobile').html(data['mobile']);
                }
            },
            error:function(){
                alert("Server is Busy!!");
            },
            complete:function (data) {
                $('#loading').hide();
            }
        });
    });

    $('#submit').click(function(e){
        if($('#form').valid()==false){
            return false;
        }
        var name=$("#email").val();
        var flag = false;
        $('#loading').show();
        $.ajax({
            url: "{{route('username-check')}}",
            data: {'name': name, '_token': "{{ csrf_token() }}"},
            type: 'post',
            async: false,
            cache: false,
            clearForm: false,
            success: function (response) {
                if(response==1){
                    alert("User Name Already Exist!!!!");
                    $('#loading').hide();
                }else{
                    flag = true;
                }
            },
            error:function(){
                alert("Server is Busy!!");
            },
            complete:function (data) {
                
            }
        });
        return flag;
    });
</script>

@endsection
