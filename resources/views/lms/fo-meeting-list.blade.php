@extends('layout.master')
@section('page_title')
LMS FO Meeting Details
@endsection
@section('page_level_style_top')

<header class="page-header">
    <h2>FO Meeting Details</h2>
    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="#">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span>FO Meeting Details</span></li>
            <li><span>FO Meeting</span></li>
        </ol>

        <a class="sidebar-right-toggle" data-open="#"><i class="fa fa-chevron-left"></i></a>
    </div>
</header>
@endsection
@section('content')

<section role="main" class="content-body">
    <section class="panel">
        <header class="panel-heading">
            <h2 class="panel-title">FO Meeting</h2>
        </header>
        <div class="panel-body">
            <table class="table table-bordered table-striped mb-none">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>ID</th>
                        <th>Lead Name</th>
                        <th>Type</th>
                        <th>Appointment Date</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $count = 1; ?>
                    @foreach($results as $key=>$data)
                    <tr>
                        <td>
                            {{ $key+1 }}
                        </td>
                        <td>
                            {{ $data['reference_id']}}
                        </td>
                        <td>
                            {{$data['name']}}
                        </td>
                        <td>
                            @if(empty($data['customer_id']) || $data['customer_id']==0)
                            Lead
                            @else
                            Reference
                            @endif
                        </td>
                        <td>
                            {{ date('d M ,Y H:i',strtotime($data['appointment_date'])) }}
                        </td>
                        <td>
                            <a href="{{ route('fo-meeting-details',[$data['reference_id']]) }}" class="btn btn-primary">Remarks</a> /  <p type="text" data-toggle="modal" onclick="viewremarks({{ $key++ }})" data-target="#remarks_interaction" class="btn purple"><i class="fa fa-envelope"></i></p>
                            @if(!(empty($data['customer_id']) || $data['customer_id']==0))
                            / <a class="btn btn-primary customerdetails" data-id="{{ $data['customer_id'] }}" >Customer</a>
                            @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <div class ="pull-right" style="margin-bottom:20px">

            </div>
        </div>
        <div class="modal fade" id="remarks_interaction" tabindex="-1" role="basic" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <h4 class="modal-title text-purple"><b>Address</b></h4>
                    </div>
                    <div class="modal-body">
                        <p id="remarkinteraction"></p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="customer_data" tabindex="-1" role="basic" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <h4 class="modal-title text-purple"><b>Customer Details</b></h4>
                    </div>
                    <div class="modal-body">
                        <table class="table table-striped table-hover table-bordered" id="tabledata">
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </section>
</section>
@endsection

@section('page_level_script_bottom')

<script src="{{URL::asset('public/assets/vendor/jquery-validation/jquery.validate.js')}}"></script>
<script src="{{URL::asset('public/assets/javascripts/app.js')}}"></script>
<script>
    jQuery(document).ready(function() {
    $('#loading').hide();
    App.init();

    });


</script>
@endsection
