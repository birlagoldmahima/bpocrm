@extends('layout.master')
@section('page_title')
RM Rejected List 
@endsection
@section('breadcrumb')
<li>
    <a href="#">RM Rejected List </a>
</li>
@endsection
@section('page_level_style_top')
@endsection
@section('content')
<header class="page-header">
    <h2>Dashboard </h2>
    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="#">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span>Lead</span></li>
            <li><span>RM Rejected List</span></li>
        </ol>

        <a class="sidebar-right-toggle" data-open="#"><i class="fa fa-chevron-left"></i></a>
    </div>
</header>
<section role="main" class="content-body">
    <section class="panel">
        <header class="panel-heading">
            <div class="row">
                <h3>RM Rejected List</h3>
                @if(Session::has('flash_message'))
                <div class="alert {{ Session::get('flash_alert') }}">
                    {!! Session::get('flash_message') !!}
                </div>
                @endif
                @if($errors->any())
                <div class="alert alert-danger text-center">
                    @foreach($errors->all() as $error)
                    <p>{{ $error }}</p>
                    @endforeach
                </div>
                @endif
            </div>
        </header>
        <div class="panel-body">

            <div class="loading" id="loading" style="display:none">
                <img class="loading-image" id="loading-image" src="{{URL::to('assets/img/ajax-loading.gif')}}" alt="Loading..." />
            </div>
            <div class="col-md-12">
                <div class="portlet box litgreen">

                    <div class="portlet-body">
                        @if(Session::has('flash_message'))
                        <div class="alert alert-success text-center ">
                            {{ Session::get('flash_message') }}
                        </div>
                        @endif
                        <form  method="post"  action="">
                            <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                            <div class="table-scrollable">
                                <table class="table table-striped table-hover table-bordered">
                                    <thead>
                                        <tr>
                                            <th>
                                                ID
                                            </th>
                                            <th>
                                                Lead Name
                                            </th>
                                            <th>
                                                Mobile
                                            </th>
                                            <th>
                                                Email
                                            </th>
                                            <th>
                                                Type
                                            </th>
                                            <th>
                                                RM
                                            </th>
                                            <th>
                                                Reason
                                            </th>
                                            <th>
                                                Action
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $count = 1; ?>
                                        @foreach($results as $key=>$data)
                                        <?php
                                            $rmname = '-';
                                            $type = "";
                                            if (empty($data['customer_id']) || $data['customer_id'] == 0) {
                                                $type = "Lead";
                                                if (isset($user[$data['rel_manager_id']])) {
                                                    $rmname = $user[$data['rel_manager_id']]['name'];
                                                }
                                            } else {
                                                $type = "Reference";
                                                if (isset($user[$data['relationship_manager']])) {
                                                    $rmname = $user[$data['relationship_manager']]['name'];
                                                }
                                            }
                                        ?>
                                        <tr>
                                            <td>
                                                {{ $data['reference_id']}}
                                            </td>
                                            <td>
                                                {{$data['name']}}
                                            </td>
                                            <td>
                                                {{$data['mobile']}}
                                            </td>
                                            <td>
                                                {{ $data['email']}}
                                            </td>
                                            <td>
                                                {{ $type }}
                                            </td>
                                            <td>
                                                {{ $rmname }}
                                            </td>
                                            <td>
                                                @if($data['stage_id']==2)
                                                Rejected By TL
                                                @elseif($data['stage_id']==4)
                                                Rejected By Verifier
                                                @elseif($data['stage_id']==8)
                                                Return By Voc
                                                @elseif($data['stage_id']==10)
                                                Rejected by Verifier (VOC)
                                                @endif
                                            </td>
                                            <td>
                                                <a data-toggle="modal" data-lead="{{$data['reference_id']}}" data-target="#remarks" class="btn responsedata">Response</a> / <a href="{{ route('lead-details',[$data['reference_id']]) }}">Verify</a>
                                            </td>
                                        </tr>
                                        <?php $count++; ?>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </form>
                        <div class ="pull-right" style="margin-bottom:20px">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="remarks" tabindex="-1" role="basic" aria-hidden="true" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title text-purple"><b>Interaction</b></h4>
                    </div>
                    <div class="modal-body" >
                        <div class="row form-group">
                            <div class="col-md-12" style='overflow:auto;height:500px;'>
                                <h4>Interaction</h4>
                                <div class="table-scrollable">
                                    <table class="table table-striped table-hover table-bordered">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Created Date</th>
                                                <th>Logged By</th>
                                                <th>Call Status</th>
                                                <th>Call Sub Status</th>
                                                <th>Remarks</th>
                                            </tr>
                                        </thead>
                                        <tbody id="interaction">
                                        </tbody>
                                    </table>
                                </div>
                                <h4>Verifier Interaction</h4>
                                <div class="table-scrollable">
                                    <table class="table table-striped table-hover table-bordered">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Created Date</th>
                                                <th>Logged By</th>
                                                <th>Call Status</th>
                                                <th>Call Sub Status</th>
                                                <th>Remarks</th>
                                            </tr>
                                        </thead>
                                        <tbody id="verfierinteraction">
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn default" data-dismiss="modal" id="closebtn">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </section>
</section>
@endsection
@section('page_level_script_bottom')
<script src="{{URL::asset('public/assets/vendor/jquery-validation/jquery.validate.js')}}"></script>
<script src="{{URL::asset('public/assets/javascripts/forms/form-validation.js')}}"></script>
<script src="{{URL::asset('public/assets/javascripts/app.js')}}"></script>
<script>
jQuery(document).ready(function () {
    $('#loading').hide();
    App.init();
    TableEditable.init();
    $('.pagination li a').addClass('clk');
});

$('.responsedata').click(function () {
    var leadid = $(this).data('lead');
    $.ajax({
        url: "{{ URL::to('rm-rejected-reason') }}/" + $(this).data('lead'),

        type: 'get',
        async: true,
        cache: false,
        beforeSend: function () {
            $('#loading').show();
        },
        success: function (response) {
            var append = "";
            $('#interaction,#verfierinteraction').html('');
            $.each(response['interaction'], function (index, item) {
                append += '<tr><td>' + (index + 1) + '</td>';
                append += '<td>' + (item['created_at']) + '</td>';
                append += '<td>' + (item['rmname']) + '</td>';
                append += '<td>' + (item['call_status']) + '</td>';
                append += '<td>' + (item['call_sub_status']) + '</td>';
                append += '<td>' + (item['remarks']) + '</td></tr>';
            });
            $('#interaction').append('<tr>' + append + '</tr>');
            var append = "";
            $.each(response['verifier'], function (index, item) {
                append += '<tr><td>' + (index + 1) + '</td>';
                append += '<td>' + (item['created_at']) + '</td>';
                append += '<td>' + (item['rmname']) + '</td>';
                append += '<td>' + (item['call_status']) + '</td>';
                append += '<td>' + (item['call_sub_status']) + '</td>';
                append += '<td>' + (item['remarks']) + '</td></tr>';
            });
            $('#verfierinteraction').append('<tr>' + append + '</tr>');
        },
        error: function () {
            alert("Server is Busy!!");
        },
        complete: function (data) {
            $('#loading').hide();
        }
    });
})

</script>
@endsection
