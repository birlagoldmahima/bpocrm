@extends('layout.master')
@section('page_title')
@endsection
@section('breadcrumb')
<li><a href="#">SPOC List</a></li>
@endsection
@section('page_level_style_top')
<link rel="stylesheet" type="text/css" href="http://cherishgold.com/crm/assets/plugins/bootstrap-datetimepicker/css/datetimepicker.css">
@endsection
@section('content')

<header class="page-header">
    <h2>SPOC Management </h2>
    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="#">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span>Dashboard</span></li>
            <li><span>Spoc List</span></li>
        </ol>

        <a class="sidebar-right-toggle" data-open="#"><i class="fa fa-chevron-left"></i></a>
    </div>
</header>

<section role="main" class="content-body">
    <section class="panel">
        <header class="panel-heading">
            <div class="row">
                <h2 class="panel-title col-md-5">Spoc List</h2>
                @if($errors->any())
                <div class="alert alert-danger text-center col-md-3">
                    @foreach($errors->all() as $error)
                    <p>{{ $error }}</p>
                    @endforeach
                </div>
                @endif
            </div>
        </header>
        <div class="panel-body">

            <form method="get" name="frm" id="frm">
                <div class="col-md-7" style="margin-bottom:10px;">
                    <div class="form-group">
                        <div class="col-md-1" style="margin-top:6px;">Appoint</div>
                        <div class="col-md-3">
                            <input type="text" name="fromdate" class="form-control" id="appointmentdate" value="{{isset($fromdate)?$fromdate:""}}" placeholder="From Date" readonly>
                        </div>
                        <div class="col-md-3">
                            <input type="text" name="todate" class="form-control" id="todate" value="{{isset($todate)?$todate:""}}" placeholder="To Date" readonly>
                        </div>
                        <div class="col-md-5">
                            <button type="submit" class="btn green">Submit</button>
                            <a href="{{ route('spok-list') }}" class="btn red">Clear</a>
                            <a  class="btn green excel">Excel</a>
                        </div>
                    </div>	
                </div>

                <div class="caption col-md-5" style="font-weight:bold;">
                    <i class="fa fa-square-o" style="background-color:#ef1b1b;"></i>1:30 hour Before Appointment &nbsp;&nbsp;&nbsp;
                    <i class="fa fa-square-o" style="background-color: #756b66;"></i>After Appointment
                </div>
            </form>
            <div class="table-scrollable">
                <table class="table table-striped table-hover table-bordered">
                    <thead>
                        <tr>
                            <td>
                                #
                            </td>
                            <th>
                                ID
                            </th>
                            <th>
                                Lead Name
                            </th>
                            <th>
                                Type
                            </th>
                            <th>
                                FTL
                            </th>
                            <th>
                                FO
                            </th>
                            <th>
                                Appointment Date
                            </th>
                            <th>
                                Stage
                            </th>
                            <th>
                                Action
                            </th>
                        </tr>
                    </thead>

                    <tbody>
                        <?php
                            //$array = array_merge_recursive(config('custom.Met'), config('custom.Notmet'));
                        ?>
                        @foreach($results as $key=>$data)
                        <?php
                            $class = "#fffff";
                            $flag = 0;
                            if (empty($data->fo_id) && !empty($data->ftl_id) && !empty($data->appointment_date) && $data->stage_id == 5) {
                                $appdate = strtotime($data->appointment_date);
                                $currdate = strtotime('+5 hours +30 minutes', strtotime(date('Y-m-d H:i:s')));
                                if ($appdate > $currdate) {
                                    $date1 = date_create(date('Y-m-d H:i', $appdate));
                                    $date2 = date_create(date('Y-m-d H:i', $currdate));
                                    $diff = date_diff($date1, $date2);
                                    $days = $diff->d;
                                    $months = $diff->m;
                                    $years = $diff->y;
                                    $hour = $diff->h;
                                    $min = $diff->i;
                                    $format = $diff->format("%r%a");
                                    if ($days == 0 && $months == 0 && $years == 0) {
                                        $valuediff = $hour * 60 + $min;
                                        if ($valuediff <= 90) {
                                            $class = "#ef1b1b";
                                        }
                                    }
                                } else {
                                    $class = "#756b66";
                                    $flag = 1;
                                }
                            }
                        ?>

                        <tr>
                            <td style="background-color:{{ $class }};">
                                {{ ($key+1) + (((empty(request()->page)) ? 0 : (request()->page-1) * $results->perPage()))  }}
                            </td>
                            <td>
                                {{ $data->reference_id}}
                            </td>
                            <td>
                                {{$data->name}}
                            </td>
                            <td>
                                @if(empty($data->customer_id) || $data->customer_id==0)
                                Lead
                                @else
                                Reference
                                @endif
                            </td>
                            <td>    
                                @if(!empty($data->ftl_id))
                                {{ $data->ftl_id }}
                                @else
                                -
                                @endif
                            </td>
                            <td> 

                                @if(!empty($data->fo_id))
                                {{ $data->fo_id  }}
                                @else
                                -
                                @endif

                            </td>
                            <td>

                                @if(!empty($data->appointment_date) && $data->appointment_date!='0000-00-00 00:00:00')
                                {{ date('d-m-Y H:i',strtotime($data->appointment_date)) }}
                                @else
                                -
                                @endif
                                
                               

                            </td>
                            <td>
                               
                                {{ ($data->stage_id) }}  {{ ($data->stage_remarks) }}
                                 
                            </td>
                            <td>
                                @if(!(in_array($data->stage_id,[7,8,9,10,11])) &&  (!($data->stage_id==5 && in_array($data->stage_remarks,['refix','ni']))))
                                <a href="{{ route('spok-view',[$data->reference_id,'flag'=>$flag]) }}">Assign</a>
                                @endif

                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

        </div>
        <div class ="pull-right" style="margin-bottom:20px">
            {!! $results->appends(request()->except('page'))->links() !!}
        </div>
    </section>
</section>
@endsection
@section('page_level_script_bottom')
<script src="{{URL::asset('public/assets/vendor/jquery-validation/jquery.validate.js')}}"></script>
<script type="text/javascript" src="http://cherishgold.com/crm/assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
<script src="{{URL::asset('public/assets/javascripts/forms/form-validation.js')}}"></script>
<script src="{{URL::asset('public/assets/javascripts/app.js')}}"></script>
<script src="{{URL::asset('public/assets/ckeditor/ckeditor.js')}}"></script>

<script>

jQuery(document).ready(function () {
    App.init();

    FormValidation.init();
    $('#loading').hide();
    var d = new Date("{!! date('D M d Y H:i:s',strtotime('+5 hours +30 minutes', strtotime(date('Y-m-d H:i:s')))) !!}");
    $('#appointmentdate').datetimepicker({
        format: "dd-mm-yyyy hh:ii:ss",
        //startDate: d,
    });
    $('#todate').datetimepicker({
        format: "dd-mm-yyyy hh:ii:ss",
        //startDate: d,
    });
});
$('.excel').click(function () {
window.location.href = "{{ route('spok-report-export') }}?" + $('#frm').serialize();
})
</script>
@endsection