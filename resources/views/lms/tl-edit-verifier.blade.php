@extends('layout.master')
@section('page_title')
Add New Lead
@endsection
@section('page_level_style_top')
<header class="page-header">
    <h2>TL Verify</h2>
    <link rel="stylesheet" type="text/css" href="http://cherishgold.com/crm/assets/plugins/bootstrap-datetimepicker/css/datetimepicker.css">
    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="#">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span>Lead</span></li>
            <li><span>Lead Details </span></li>
        </ol>

        <a class="sidebar-right-toggle" data-open="#"><i class="fa fa-chevron-left"></i></a>
    </div>
</header>
@endsection
@section('content')
<?php
    if (!empty($reference->verifier)) {
        $decode = json_decode($reference->verifier, 1);

        // print_r($decode['age']);
        // die;
    }
?>
<section role="main" class="content-body">
    <section class="panel">
        <header class="panel-heading">
            <div class="row">
                <h2 class="panel-title col-md-5">TL Verify</h2>
                @if($errors->any())
                <div class="alert alert-danger text-center col-md-3">
                    @foreach($errors->all() as $error)
                    <p>{{ $error }}</p>
                    @endforeach
                </div>
                @endif
            </div>
        </header>

        <div class = "panel-body">
            <form action = "{{ route('tl-update-verifier') }}" id = "editVerifier" class = "form-horizontal" method = "post">
                <input type = "hidden" name = "_token" id = "_tokensc" value = "{{ csrf_token() }}">
                <input type = "hidden" name = "id" value = "{{$reference->reference_id}}">
                <input type = "hidden" name = "type" value = "">

                <input type = "hidden" name = "state_name" id = "state_name" value = "{{ $reference->state_name }}">
                <input type = "hidden" name = "city_name" id = "city_name" value = "{{ $reference->city_name }}">

                <div class = "form-body">
                    @if(Session::has('flash_message'))
                    <div class = "alert alert-success text-center ">
                        {{ Session::get('flash_message') }}
                    </div>
                    @endif
                    @if($errors->any())
                    <div class = "alert alert-danger text-center">
                        @foreach($errors->all() as $error)
                        <p>{{ $error }}</p>
                        @endforeach
                    </div>
                    @endif

                    <input type = "hidden" name = "batch_id" data-required = "1" class = "form-control" placeholder = "Please Enter Batch Id" value = "@if(isset($decode['batch_id'])) {{key($decode['batch_id'])}} @endif" />
                    <input type = "hidden" name = "campaign_code" data-required = "1" class = "form-control" placeholder = "Please Enter Campaign Code" value = "@if(isset($decode['campaign_code'])) {{key($decode['campaign_code'])}} @endif" />
                    <input type = "hidden" name = "lead_code" data-required = "1" class = "form-control" placeholder = "Please Enter Lead Code" value = "@if(isset($decode['lead_code'])) {{key($decode['lead_code'])}} @endif" />

                    <div class = "form-group">
                        <label class = "control-label col-md-3"> Name
                            <span class = "required"> * </span>
                        </label>
                        <div class = "col-md-5">
                            <input type = "text" name = "name" data-required = "1" class = "form-control" placeholder = "Please Enter Name" value = "{{$reference->name}}" />
                        </div>


                        <div class = "col-md-2">
                            <select class = "form-control quality" name = "name_status" >
                                <option value = "no" @if(isset($decode['name']))@if($decode['name'][key($decode['name'])] == 'no' ) selected @endif @endif >No</option>
                                <option value = "yes" @if(isset($decode['name']))@if($decode['name'][key($decode['name'])] == 'yes' ) selected @endif @endif >Yes</option>
                            </select>
                        </div>
                        <div class = "col-md-2"></div>
                    </div>

                    <div class = "form-group">
                        <label class = "control-label col-md-3"> Email Id </label>
                        <div class = "col-md-5">
                            <input type = "email" name = "email" data-required = "1" class = "form-control" placeholder = "Please Enter Email Id" value = "{{$reference->email}}" />
                        </div>

                        <div class = "col-md-2">
                            <select class = "form-control quality" name = "email_status" >
                                <option value = "no" @if(isset($decode['email'])) @if($decode['email'][key($decode['email'])] == 'no' ) selected @endif @endif >No</option>
                                <option value = "yes" @if(isset($decode['email'])) @if($decode['email'][key($decode['email'])] == 'yes' ) selected @endif @endif >Yes</option>
                            </select>
                        </div>
                        <div class = "col-md-2"></div>
                    </div>

                    <div class = "form-group">
                        <label class = "control-label col-md-3"> Mobile
                            <span class = "required"> * </span>
                        </label>
                        <div class = "col-md-5">
                            <input type = "text" name = "mobile" data-required = "1" class = "form-control" placeholder = "Please Enter Mobile Number" value = "{{$reference->mobile}}" />
                        </div>

                        <div class = "col-md-2">
                            <select class = "form-control quality" name = "mobile_status" >
                                <option value = "no" @if(isset($decode['mobile'])) @if($decode['mobile'][key($decode['mobile'])] == 'no' ) selected @endif @endif >No</option>
                                <option value = "yes" @if(isset($decode['mobile'])) @if($decode['mobile'][key($decode['mobile'])] == 'yes' ) selected @endif @endif >Yes</option>
                            </select>
                        </div>

                    </div>

                    <div class = "form-group">
                        <label class = "control-label col-md-3">Alternate Phone Number
                        </label>
                        <div class = "col-md-5">
                            <input type = "text" name = "alt_mobile" class = "form-control" placeholder = "Please Enter Alternate Phone Number" value = "@if(isset($decode['alt_mobile'])) {{trim(key($decode['alt_mobile']))}} @endif" />
                        </div>

                        <div class = "col-md-2">
                            <select class = "form-control quality" name = "alt_mobile_status" >
                                <option value = "no" @if(isset($decode['alt_mobile'])) @if($decode['alt_mobile'][key($decode['alt_mobile'])] == 'no' ) selected @endif @endif >No</option>
                                <option value = "yes" @if(isset($decode['alt_mobile'])) @if($decode['alt_mobile'][key($decode['alt_mobile'])] == 'yes' ) selected @endif @endif >Yes</option>
                            </select>
                        </div>

                    </div>

                    <div class = "form-group">
                        <label class = "control-label col-md-3">Age

                        </label>
                        <div class = "col-md-5">
                            <input type = "text" name = "age" data-required = "1" class = "form-control" placeholder = "Please Enter Age" value = "@if(isset($decode['age'])){{trim(key($decode['age']))}}@endif" />

                        </div>
                        <div class = "col-md-2">
                            <select class = "form-control quality" name = "state_status" >
                                <option value = "no" @if(isset($decode['age'])) @if($decode['age'][key($decode['age'])] == 'no' ) selected @endif @endif >No</option>
                                <option value = "yes" @if(isset($decode['age'])) @if($decode['age'][key($decode['age'])] == 'yes' ) selected @endif @endif >Yes</option>
                            </select>
                        </div>
                    </div>

                    <div class = "form-group">
                        <label class = "control-label col-md-3">Gender </label>
                        <div class = "col-md-5">
                            <select class = "form-control" name = "gender" >
                                <option value = ""> --- Select Gender ---</option>
                                <option value = "male" @if(isset($decode['age'])) @if(key($decode['gender']) == 'male' ) selected @endif @endif >Male</option>
                                <option value = "female" @if(isset($decode['age'])) @if(key($decode['gender']) == 'female' ) selected @endif @endif >Female</option>
                                <option value = "not_to_say" @if(isset($decode['age'])) @if(key($decode['gender']) == 'not_to_say' ) selected @endif @endif>Prefer Not to say</option>
                            </select>
                        </div>

                        <div class = "col-md-2">
                            <select class = "form-control quality" name = "gender_status" >
                                <option value = "no" @if(isset($decode['gender'])) @if($decode['gender'][key($decode['gender'])] == 'no' ) selected @endif @endif >No</option>
                                <option value = "yes" @if(isset($decode['gender'])) @if($decode['gender'][key($decode['gender'])] == 'yes' ) selected @endif @endif >Yes</option>
                            </select>
                        </div>
                        <div class = "col-md-2"></div>
                    </div>

                    <div class = "form-group">
                        <label class = "control-label col-md-3">Marital Status </label>
                        <div class = "col-md-5">
                            <select class = "form-control" name = "marital_status" >
                                <option value = ""> --- Select Marital Status ---</option>
                                <option value = "single" @if(isset($decode['marital_status'])) @if(key($decode['marital_status']) == 'single' ) selected @endif @endif >Single</option>
                                <option value = "married" @if(isset($decode['marital_status'])) @if(key($decode['marital_status']) == 'married' ) selected @endif @endif >Married</option>
                                <option value = "widow" @if(isset($decode['marital_status'])) @if(key($decode['marital_status']) == 'widow' ) selected @endif @endif >widow</option>
                                <option value = "divorced" @if(isset($decode['marital_status'])) @if(key($decode['marital_status']) == 'divorced' ) selected @endif @endif >divorced</option>
                            </select>
                        </div>
                        <div class = "col-md-2">
                            <select class = "form-control quality" name = "marital_status_status" >
                                <option value = "no" @if(isset($decode['marital_status'])) @if($decode['marital_status'][key($decode['marital_status'])] == 'no' ) selected @endif @endif >No</option>
                                <option value = "yes" @if(isset($decode['marital_status'])) @if($decode['marital_status'][key($decode['marital_status'])] == 'yes' ) selected @endif @endif >Yes</option>
                            </select>
                        </div>
                        <div class = "col-md-2"></div>
                    </div>

                    <div class = "form-group">
                        <label class = "control-label col-md-3">Number Of Child </label>
                        <div class = "col-md-5">
                            <select class = "form-control" name = "child" >
                                <option value = ""> --- Select Number Of Child ---</option>
                                <option value = "0" @if(isset($decode['child'])) @if(key($decode['child']) == '0' ) selected @endif @endif >0</option>
                                <option value = "1" @if(isset($decode['child'])) @if(key($decode['child']) == '1' ) selected @endif @endif >1</option>
                                <option value = "2" @if(isset($decode['child'])) @if(key($decode['child']) == '2' ) selected @endif @endif >2</option>
                                <option value = "3" @if(isset($decode['child'])) @if(key($decode['child']) == '3' ) selected @endif @endif >3</option>
                                <option value = "4" @if(isset($decode['child'])) @if(key($decode['child']) == '4' ) selected @endif @endif >4</option>
                                <option value = "5" @if(isset($decode['child'])) @if(key($decode['child']) == '5' ) selected @endif @endif >5</option>
                                <option value = "5+" @if(isset($decode['child'])) @if(key($decode['child']) == '5+' ) selected @endif @endif >5+</option>
                            </select>
                        </div>

                        <div class = "col-md-2">
                            <select class = "form-control quality" name = "child_status" >
                                <option value = "no" @if(isset($decode['child'])) @if($decode['child'][key($decode['child'])] == 'no' ) selected @endif @endif >No</option>
                                <option value = "yes" @if(isset($decode['child'])) @if($decode['child'][key($decode['child'])] == 'yes' ) selected @endif @endif >Yes</option>
                            </select>
                        </div>
                        <div class = "col-md-2"></div>
                    </div>

                    <div class = "form-group">
                        <label class = "control-label col-md-3">State <span class = "required"> * </span></label>
                        <div class = "col-md-5">

                            <select class = "form-control" name = "state" id = "state" >
                                <option value = ""> --- Select State ---</option>
                                @foreach($states as $key => $val)
                                @if($val['status'] == 'Active')
                                <option value = "{{$val['state_id']}}" @if(isset($decode['state'])) @if(key($decode['state']) == $val['state_id']) selected @endif @endif >{{$val['state']}}</option>
                                @endif
                                @endforeach
                            </select>
                        </div>

                        <div class = "col-md-2">
                            <select class = "form-control quality" name = "state_status" >
                                <option value = "no" @if(isset($decode['state'])) @if($decode['state'][key($decode['state'])] == 'no' ) selected @endif @endif >No</option>
                                <option value = "yes" @if(isset($decode['state'])) @if($decode['state'][key($decode['state'])] == 'yes' ) selected @endif @endif >Yes</option>
                            </select>
                        </div>
                        <div class = "col-md-2"></div>
                    </div>

                    <div class = "form-group">
                        <label class = "control-label col-md-3">City <span class = "required"> * </span></label>
                        <div class = "col-md-5">
                            <select class = "form-control" name = "city" id = "city">
                                <option value = ""> --- Select City ---</option>
                                @foreach($cities as $key => $val)
                                <option value = "{{$val['city_id']}}" @if($reference['city_name'] == $val['city_id'] ) selected @endif >{{$val['city']}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class = "col-md-2">
                            <select class = "form-control quality" name = "city_status" >
                                <option value = "no" @if(isset($decode['city'])) @if($decode['city'][key($decode['city'])] == 'no' ) selected @endif @endif >No</option>
                                <option value = "yes" @if(isset($decode['city'])) @if($decode['city'][key($decode['city'])] == 'yes' ) selected @endif @endif >Yes</option>
                            </select>
                        </div>
                        <div class = "col-md-2"></div>
                    </div>

                    <div class = "form-group">
                        <label class = "control-label col-md-3">Address
                            <span class = "required"> * </span>
                        </label>
                        <div class = "col-md-5">
                            <textarea type = "textarea" rows = "6" name = "address" class = "form-control" value = "" placeholder = "Please Enter Address " style = "resize:vertical;" >@if(isset($decode['address'])) {{trim(key($decode['address']))}} @endif
                            </textarea>
                        </div>

                        <div class = "col-md-2">
                            <select class = "form-control quality" name = "address_status" >
                                <option value = "no" @if(isset($decode['address'])) @if($decode['address'][key($decode['address'])] == 'no' ) selected @endif @endif >No</option>
                                <option value = "yes" @if(isset($decode['address'])) @if($decode['address'][key($decode['address'])] == 'yes' ) selected @endif @endif >Yes</option>
                            </select>
                        </div>
                        <div class = "col-md-2"></div>
                    </div>

                    <div class = "form-group">
                        <label class = "control-label col-md-3">Landmark
                            <span class = "required"> * </span>
                        </label>
                        <div class = "col-md-5">
                            <input type = "text" name = "landmark" class = "form-control" placeholder = "Please Enter Landmark" value = "@if(isset($decode['landmark'])) {{trim(key($decode['landmark']))}} @endif" />
                        </div>

                        <div class = "col-md-2">
                            <select class = "form-control quality" name = "landmark_status" >
                                <option value = "no" @if(isset($decode['landmark'])) @if($decode['landmark'][key($decode['landmark'])] == 'no' ) selected @endif @endif >No</option>
                                <option value = "yes" @if(isset($decode['landmark'])) @if($decode['landmark'][key($decode['landmark'])] == 'yes' ) selected @endif @endif >Yes</option>
                            </select>
                        </div>
                        <div class = "col-md-2"></div>
                    </div>

                    <div class = "form-group">
                        <label class = "control-label col-md-3">Pincode</label>
                        <div class = "col-md-5">
                            <input type = "text" name = "pincode" class = "form-control" placeholder = "Please Enter Pincode" value = "@if(isset($decode['pincode'])){{trim(key($decode['pincode']))}}@endif" />
                        </div>

                        <div class = "col-md-2">
                            <select class = "form-control quality" name = "pincode_status" >
                                <option value = "no" @if(isset($decode['pincode'])) @if($decode['pincode'][key($decode['pincode'])] == 'no' ) selected @endif @endif >No</option>
                                <option value = "yes" @if(isset($decode['pincode'])) @if($decode['pincode'][key($decode['pincode'])] == 'yes' ) selected @endif @endif >Yes</option>
                            </select>
                        </div>
                        <div class = "col-md-2"></div>
                    </div>

                    <div class = "form-group">
                        <label class = "control-label col-md-3">Company Name </label>
                        <div class = "col-md-5">
                            <input type = "text" name = "company_name" class = "form-control" placeholder = "Please Enter Company Name" value = "@if(isset($decode['company_name'])) {{trim(key($decode['company_name']))}} @endif" />
                        </div>

                        <div class = "col-md-2">
                            <select class = "form-control quality" name = "company_name_status" >
                                <option value = "no" @if(isset($decode['company_name'])) @if($decode['company_name'][key($decode['company_name'])] == 'no' ) selected @endif @endif >No</option>
                                <option value = "yes" @if(isset($decode['company_name'])) @if($decode['company_name'][key($decode['company_name'])] == 'yes' ) selected @endif @endif >Yes</option>
                            </select>
                        </div>
                        <div class = "col-md-2"></div>
                    </div>

                    <div class = "form-group">
                        <label class = "control-label col-md-3">Profession </label>
                        <div class = "col-md-5">
                            <input type = "text" name = "profession" class = "form-control" placeholder = "Please Enter Profession" value = "@if(isset($decode['profession'])) {{trim(key($decode['profession']))}} @endif" />
                        </div>

                        <div class = "col-md-2">
                            <select class = "form-control quality" name = "profession_status" >
                                <option value = "no" @if(isset($decode['profession'])) @if($decode['profession'][key($decode['profession'])] == 'no' ) selected @endif @endif >No</option>
                                <option value = "yes" @if(isset($decode['profession'])) @if($decode['profession'][key($decode['profession'])] == 'yes' ) selected @endif @endif >Yes</option>
                            </select>
                        </div>
                        <div class = "col-md-2"></div>
                    </div>

                    <div class = "form-group">
                        <label class = "control-label col-md-3">Department </label>
                        <div class = "col-md-5">
                            <input type = "text" name = "department" class = "form-control" placeholder = "Please Enter Department" value = "@if(isset($decode['department'])) {{trim(key($decode['department']))}} @endif" />
                        </div>

                        <div class = "col-md-2">
                            <select class = "form-control quality" name = "department_status" >
                                <option value = "no" @if(isset($decode['department'])) @if($decode['department'][key($decode['department'])] == 'no' ) selected @endif @endif >No</option>
                                <option value = "yes" @if(isset($decode['department'])) @if($decode['department'][key($decode['department'])] == 'yes' ) selected @endif @endif >Yes</option>
                            </select>
                        </div>
                        <div class = "col-md-2"></div>
                    </div>

                    <div class = "form-group">
                        <label class = "control-label col-md-3">Income Group </label>
                        <div class = "col-md-5">
                            <select class = "form-control" name = "income_group" >
                                <option value = ""> --- Select Income Group ---</option>
                                <option value = "1 Lac to 2 Lac" @if(isset($decode['income_group'])) @if(key($decode['income_group']) == '1 Lac to 2 Lac' ) selected @endif @endif >1 Lac to 2 Lac</option>
                                <option value = "2 Lac to 5 Lac" @if(isset($decode['income_group'])) @if(key($decode['income_group']) == '2 Lac to 5 Lac' ) selected @endif @endif >2 Lac to 5 Lac</option>
                                <option value = "5 Lac to 9 Lac" @if(isset($decode['income_group'])) @if(key($decode['income_group']) == '5 Lac to 9 Lac' ) selected @endif @endif >5 Lac to 9 Lac</option>
                                <option value = "9 Lac to 13 Lac" @if(isset($decode['income_group'])) @if(key($decode['income_group']) == '9 Lac to 13 Lac' ) selected @endif @endif >9 Lac to 13 Lac</option>
                                <option value = "13 Lac +" @if(isset($decode['income_group'])) @if(key($decode['income_group']) == '13 Lac +' ) selected @endif @endif >13 Lac
                                        +</option>
                            </select>
                        </div>

                        <div class = "col-md-2">
                            <select class = "form-control quality" name = "income_group_status" >
                                <option value = "no" @if(isset($decode['income_group'])) @if($decode['income_group'][key($decode['income_group'])] == 'no' ) selected @endif @endif >No</option>
                                <option value = "yes" @if(isset($decode['income_group'])) @if($decode['income_group'][key($decode['income_group'])] == 'yes' ) selected @endif @endif >Yes</option>
                            </select>
                        </div>
                        <div class = "col-md-2"></div>
                    </div>

                    <div class = "form-group">
                        <label class = "control-label col-md-3">Income </label>
                        <div class = "col-md-5">
                            <input type = "text" name = "income" class = "form-control" placeholder = "Please Enter your Income" value = "@if(isset($decode['income'])) {{trim(key($decode['income']))}} @endif" />
                        </div>

                        <div class = "col-md-2">
                            <select class = "form-control quality" name = "income_status" >
                                <option value = "no" @if(isset($decode['income'])) @if($decode['income'][key($decode['income'])] == 'no' ) selected @endif @endif >No</option>
                                <option value = "yes" @if(isset($decode['income'])) @if($decode['income'][key($decode['income'])] == 'yes' ) selected @endif @endif >Yes</option>
                            </select>
                        </div>
                        <div class = "col-md-2"></div>
                    </div>

                    <div class = "form-group">
                        <label class = "control-label col-md-3">Agent Name </label>
                        <div class = "col-md-5">
                            <input type = "text" name = "agent_name" class = "form-control" placeholder = "Please Enter Agent Name" value = "<?php echo $manager[0]['emp']; ?>" disabled />
                        </div>
                        <div class = "col-md-2"></div>
                    </div>

                    <div class = "form-group">
                        <label class = "control-label col-md-3">TL Name </label>
                        <div class = "col-md-5">
                            <input type = "text" name = "tl_name" class = "form-control" placeholder = "Please Enter TL Name" value = "<?php echo $manager[0]['head']; ?>" disabled />
                        </div>

                        <div class = "col-md-2"></div>
                    </div>

                    <div class = "form-group">
                        <label class = "control-label col-md-3">Investment Question </label>
                        <div class = "col-md-5">
                            <input type = "text" name = "investment_question" class = "form-control" placeholder = "Please Enter Investment Question" value = "@if(isset($decode['investment_question'])) {{trim(key($decode['investment_question']))}} @endif" />
                        </div>

                        <div class = "col-md-2">
                            <select class = "form-control quality" name = "investment_question_status" >
                                <option value = "no" @if(isset($decode['investment_question']))@if($decode['investment_question'][key($decode['investment_question'])] == 'no' ) selected @endif @endif >No</option>
                                <option value = "yes" @if(isset($decode['investment_question']))@if($decode['investment_question'][key($decode['investment_question'])] == 'yes' ) selected @endif @endif >Yes</option>
                            </select>
                        </div>
                        <div class = "col-md-2"></div>
                    </div>

                    <div class = "form-group">
                        <label class = "control-label col-md-3">Purpose Of Call </label>
                        <div class = "col-md-5">
                            <input type = "text" name = "purpose_of_call" class = "form-control" placeholder = "Please Enter Purpose Of Call" value = "@if(isset($decode['purpose_of_call'])) {{trim(key($decode['purpose_of_call']))}} @endif" />
                        </div>

                        <div class = "col-md-2">
                            <select class = "form-control quality" name = "purpose_of_call_status" >
                                <option value = "no" @if(isset($decode['purpose_of_call'])) @if($decode['purpose_of_call'][key($decode['purpose_of_call'])] == 'no' ) selected @endif @endif >No</option>
                                <option value = "yes" @if(isset($decode['purpose_of_call'])) @if($decode['purpose_of_call'][key($decode['purpose_of_call'])] == 'yes' ) selected @endif @endif >Yes</option>
                            </select>
                        </div>
                        <div class = "col-md-2"></div>
                    </div>

                    <div class = "form-group">
                        <label class = "control-label col-md-3">Call Complaince </label>
                        <div class = "col-md-5">
                            <input type = "text" name = "call_complaince" class = "form-control" placeholder = "Please Enter Call Complaince " value = "@if(isset($decode['call_complaince'])) {{trim(key($decode['call_complaince']))}} @endif" />
                        </div>

                        <div class = "col-md-2">
                            <select class = "form-control quality" name = "call_complaince_status" >
                                <option value = "no" @if(isset($decode['call_complaince'])) @if($decode['call_complaince'][key($decode['call_complaince'])] == 'no' ) selected @endif @endif >No</option>
                                <option value = "yes" @if(isset($decode['call_complaince'])) @if($decode['call_complaince'][key($decode['call_complaince'])] == 'yes' ) selected @endif @endif >Yes</option>
                            </select>
                        </div>
                        <div class = "col-md-2"></div>
                    </div>

                    <div class = "form-group">
                        <label class = "control-label col-md-3">Recording Statement </label>
                        <div class = "col-md-5">
                            <input type = "text" name = "recording_statement" class = "form-control" placeholder = "Please Enter Recording Statement " value = "@if(isset($decode['recording_statement'])) {{trim(key($decode['recording_statement']))}} @endif" />
                        </div>

                        <div class = "col-md-2">
                            <select class = "form-control quality" name = "recording_statement_status" >
                                <option value = "no" @if(isset($decode['recording_statement'])) @if($decode['recording_statement'][key($decode['recording_statement'])] == 'no' ) selected @endif @endif >No</option>
                                <option value = "yes" @if(isset($decode['recording_statement'])) @if($decode['recording_statement'][key($decode['recording_statement'])] == 'yes' ) selected @endif @endif >Yes</option>
                            </select>
                        </div>
                        <div class = "col-md-2"></div>
                    </div>

                    <div class = "form-group">
                        <label class = "control-label col-md-3">Plan Informed
                        </label>
                        <div class = "col-md-4">
                            <select class = "form-control" name = "informed" id = "informed" >
                                <option value = "no" @if($reference->informed == "no") selected @endif>No</option>
                                <option value = "yes" @if($reference->informed == "yes") selected @endif>Yes</option>
                            </select>

                        </div>
                    </div>

                    <div class = "form-group">
                        <label class = "control-label col-md-3">Expected Sale Value
                        </label>
                        <div class = "col-md-4">
                            <input type = "text" name = "emi_size" data-required = "1" class = "form-control" placeholder = "Please Enter Emi Size" value = "{{$reference->emi_size}}" />
                        </div>
                    </div>

                    <div class = "portlet">
                        <div class = "portlet-title line">
                            <div class = "caption">
                                <i class = "fa fa-comments"></i>Lead Interactions
                            </div>
                        </div>
                        <div class = "row">
                            <div class = "col-md-12">
                                <!--BEGIN SAMPLE TABLE PORTLET-->
                                <div class = "portlet">
                                    <div class = "portlet-body">
                                        <div class = "table-responsive">
                                            <table class = "table table-striped table-bordered table-advance table-hover scroll">
                                                <thead>
                                                    <tr>
                                                        <th style = "width:18%;">
                                                            <i class = "fa fa-briefcase"></i> Date
                                                        </th>
                                                        <th style = "width:18%;">
                                                            <i class = "fa fa-user"></i> Logged By
                                                        </th>
                                                        <th style = "width:18%;">
                                                            <i class = "fa fa-phone"></i> Call Status
                                                        </th>
                                                        <th style = "width:18%;">
                                                            <i class = "fa fa-random"></i> Call Sub Status
                                                        </th>
                                                        <th style = "width:18%;">
                                                            <i class = "fa fa-phone"></i> Call Back
                                                        </th>
                                                        <th style = "width: 10%;">
                                                            <i class = "fa fa-bookmark"></i> Remarks
                                                        </th>
                                                    </tr>
                                                </thead>

                                                <tbody>
                                                    @foreach($intereaction['interaction'] as $key => $value)
                                                    <tr>
                                                        <td style = "width: 18%;">
                                                            {{ $value['created_at'] }}
                                                        </td>
                                                        <td style = "width: 18%;">
                                                            {{ $value['rmname'] }}
                                                        </td>
                                                        <td style = "width: 18%;">
                                                            {{ $value['call_status'] }}
                                                        </td >
                                                        <td style = "width: 18%;">
                                                            {{ $value['call_sub_status'] }}
                                                        </td>
                                                        <td style = "width: 18%;">
                                                            {{ $value['callback_datetime'] }}
                                                        </td>
                                                        <td style = "width: 10%;">
                                                            <p type = "text" data-toggle = "modal" onclick = "viewremarks({{ $key++ }})" data-target = "#remarks_interaction" class = "btn purple"><i class = "fa fa-envelope"></i></p>
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div style = "border:none;" class = "portlet box green">
                        <div class = "portlet box greens ">
                            <div class = "portlet-title">
                                <div class = "caption"> <i class = "fa fa-phone-square" style = "font-size: 20px;"></i> Verifier Interaction </div>
                            </div>

                            <div class = "portlet-body form">
                                <input type = "hidden" name = "id" value = "{{$reference->reference_id}}">
                                <div class = "form-body">
                                    <div class = "container-fluid">
                                        <div class = "row">
                                            <div class = "col-md-6">
                                                <div class = "form-group">
                                                    <label class = "col-md-6 control-label">Call Status</label>
                                                    <div class = "col-md-6">
                                                        <select class = "form-control" name = "call_status" id = "call_status" onchange = "getSubStatus();">
                                                            <option value = ""> --- Select Call Status ---</option>
                                                            @foreach($call_status as $key => $val)
                                                            <option value = "{{$val['id']}}">{{$val['name']}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class = "form-group">
                                                    <label class = "col-md-6 control-label">Call Sub Status</label>
                                                    <div class = "col-md-6">
                                                        <select class = "form-control" name = "call_sub_status" id = "call_sub_status" >
                                                            <option value = ""> --- Select Call Sub Status ---</option>
                                                            @foreach($call_sub_status as $key => $val)
                                                            <option value = "{{$val['id']}}">{{$val['name']}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class = "form-group">
                                                    <label class = "col-md-6 control-label">Appointment DateTime </label>
                                                    <div class = "col-md-6">
                                                        <input type = "text" name = "appointment_date" id = "appointmentdate" class = "form-control" data-required = "1" readonly placeholder = "Please Select Appointment Date" value = "{{ date('d-m-Y H:i:s',strtotime($reference->appointment_date)) }}" />


                                                    </div>
                                                </div>
                                            </div>
                                            <div class = "col-md-6">
                                                <div class = "form-group">
                                                    <label class = "col-md-4 control-label"> Interaction Summary </label>
                                                    <div class = "col-md-8">
                                                        <textarea type = "textarea" rows = "6" name = "verification_interaction" class = "form-control" value = "" placeholder = "Please Enter Verification Interaction " style = "resize:vertical;" ></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class = "portlet">
                        <div class = "portlet-title line">
                            <div class = "caption">
                                <i class = "fa fa-comments"></i>Verifier Interactions
                            </div>
                        </div>

                        <div class = "row">
                            <div class = "col-md-12">
                                <div class = "portlet">
                                    <div class = "portlet-body">
                                        <div class = "table-responsive">
                                            <table class = "table table-striped table-bordered table-advance table-hover scroll">
                                                <thead>
                                                    <tr>
                                                        <th style = "width: 18%;">
                                                            <i class = "fa fa-calendar"></i> Created Date
                                                        </th>
                                                        <th style = "width: 18%;">
                                                            <i class = "fa fa-user"></i> Logged By
                                                        </th>
                                                        <th style = "width: 18%;">
                                                            <i class = "fa fa-phone"></i> Call Status
                                                        </th>
                                                        <th style = "width: 18%;">
                                                            <i class = "fa fa-random"></i> Call Sub Status
                                                        </th>
                                                        <th style = "width: 18%;">
                                                            <i class = "fa fa-phone"></i> Appointment DateTime
                                                        </th>
                                                        <th style = "width: 10%;">
                                                            <i class = "fa fa-phone"></i> Remarks
                                                        </th>
                                                    </tr>
                                                </thead>

                                                <tbody>
                                                    @foreach($intereaction['verifier'] as $key => $value)
                                                    <tr>
                                                        <td style = "width: 18%;">
                                                            {{ $value['created_at'] }}
                                                        </td>
                                                        <td style = "width: 18%;">
                                                            {{ $value['rmname'] }}
                                                        </td>
                                                        <td style = "width: 18%;">
                                                            {{ $value['call_status'] }}
                                                        </td >
                                                        <td style = "width: 18%;">
                                                            {{ $value['call_sub_status'] }}
                                                        </td>
                                                        <td style = "width: 18%;">
                                                            {{ $value['callback_date'] }}
                                                        </td>
                                                        <td style = "width: 10%;">
                                                            <p type = "text" data-toggle = "modal" onclick = "verifierremarks({{ $key++ }})" data-target = "#remarks_interaction" class = "btn purple"><i class = "fa fa-envelope"></i></p>
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class = "form-actions fluid">
                    <div class = "col-md-offset-4 col-md-6">
                        <button type = "submit" class = "btn green">Submit</button>&nbsp;
                        &nbsp;
                        &nbsp;
                        &nbsp;
                        <a href = "" class = "btn btn-default" >Cancel</a>
                        <input type = "hidden" name = "_token" value = "{{ Session::token() }}">
                    </div>
                </div>
            </form>
        </div>
    </section>
</section>

<div class = "col-md-12">
    <section class = "panel panel-featured panel-featured-primary">

    </section>
</div>

@endsection
@section('page_level_script_bottom')
<script type="text/javascript" src="http://cherishgold.com/crm/assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
<script src="{{URL::asset('public/assets/vendor/jquery-validation/jquery.validate.js')}}"></script>
<!--<script src="{{URL::asset('public/assets/javascripts/forms/form-validation.js')}}"></script>-->
<script src="{{URL::asset('public/assets/javascripts/form-validation.js')}}"></script>
<script src="{{URL::asset('public/assets/javascripts/app.js')}}"></script>


<script>
                                                                jQuery(document).ready(function () {
                                                                $('#loading').hide();
                                                                App.init();
                                                                FormValidation.init();
                                                                $(".quality").attr("style", "display:none");
                                                                $('#state').trigger('change');
                                                                $('#city').val("{{ isset($decode['city'])? key($decode['city']) :"" }}");
                                                                var d = new Date("{!! date('D M d Y H:i:s',strtotime('+5 hours +30 minutes', strtotime(date('Y-m-d H:i:s')))) !!}");
                                                                $('#appointmentdate').datetimepicker({
                                                                format: "dd-mm-yyyy hh:ii:ss",
                                                                        //startDate: d,
                                                                });
                                                                });
                                                                function format(date) {
                                                                date = new Date();
                                                                var day = ('0' + date.getDate()).slice( - 2);
                                                                var month = ('0' + (date.getMonth() + 1)).slice( - 2);
                                                                var year = date.getFullYear();
                                                                return day + '-' + month + '-' + year + ' ' + date.getHours() + ':' + date.getMinutes() + ':';
                                                                }
                                                                var datainteractionajax = {!! json_encode(array_column($intereaction['interaction'], 'remarks')) !!};
                                                                var datainteractionajax1 = {!! json_encode(array_column($intereaction['verifier'], 'remarks')) !!};
                                                                function viewremarks(value){
                                                                var remarrks = datainteractionajax[value];
                                                                $('#remarkinteraction').html(remarrks);
                                                                }

                                                                function verifierremarks(value){
                                                                var remarrks = datainteractionajax1[value];
                                                                $('#remarkinteraction').html(remarrks);
                                                                }
                                                                function getSubStatus(){
                                                                statusId = $("#call_status").val();
                                                                $("#call_sub_status option").remove();
                                                                var callSubStatus = {!! json_encode($call_sub_status) !!};
                                                                $("#call_sub_status").append($("<option> 2222 </option>").text('').val(''));
                                                                $.each(callSubStatus, function(index, item){
                                                                if (item.verifier_call_status_id == statusId && item.status == 'active'){
                                                                $("#call_sub_status").append(
                                                                        $("<option></option>").text(item.name).val(item.id)
                                                                        );
                                                                }
                                                                });
                                                                }

                                                                $("#state").change(function () {
                                                                var stateID = $("#state").val();
                                                                var token = $("#_tokensc").val();
                                                                $('#statename').val($("#state option:selected").html());
                                                                $("#city option").not(':first').remove();
                                                                if (stateID) {
                                                                $.ajax({
                                                                url: "{{route('getcitylist')}}",
                                                                        data: {'stateid': stateID, '_token': token},
                                                                        type: 'post',
                                                                        async: true,
                                                                        cache: false,
                                                                        clearForm: false,
                                                                        beforeSend: function () {
                                                                        $('#loading').show();
                                                                        },
                                                                        success: function (response) {
                                                                        $.each(response, function (idx, obj) {
                                                                        $("#city").append(
                                                                                $("<option></option>").text(obj).val(idx)
                                                                                );
                                                                        });
                                                                        },
                                                                        error: function () {
                                                                        alert("Server is Busy!!");
                                                                        },
                                                                        complete: function (data) {
                                                                        $('#loading').hide();
                                                                        }
                                                                });
                                                                } else {
                                                                $('#city').html('<option value="">No states</option>');
                                                                }
                                                                });
                                                                $('#city').change(function(){
                                                                $('#city_name').val($('#city option:selected').html());
                                                                })


                                                                        function cancelInteraction()
                                                                        {
                                                                        $('#customer-interaction').clearForm();
                                                                        }

</script>

@endsection
