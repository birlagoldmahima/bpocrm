@extends('layout.master')
@section('page_title')
    Add New Lead
@endsection
@section('page_level_style_top')
    <header class="page-header">
        <h2>FO Meeting Details</h2>
        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li>
                    <a href="#">
                        <i class="fa fa-home"></i>
                    </a>
                </li>
                <li><span>FO Meeting Details</span></li>
                <li><span>FO Meeting Details</span></li>
            </ol>

            <a class="sidebar-right-toggle" data-open="#"><i class="fa fa-chevron-left"></i></a>
        </div>
    </header>
@endsection
@section('content')
    <section role="main" class="content-body">
		<div class="loading" id="loading" style="display:none">
  			<img class="loading-image" id="loading-image" src="{{URL::to('assets/img/ajax-loading.gif')}}" alt="Loading..." />
		</div>
        <section class="panel">
            <header class="panel-heading">
                <div class="row">
                    <h2 class="panel-title col-md-5">&nbsp;FO Meeting Details</h2>
                    @if($errors->any())
                        <div class="alert alert-danger text-center col-md-3">
                            @foreach($errors->all() as $error)
                                <p>{{ $error }}</p>
                            @endforeach
                        </div>
                    @endif
                </div>
            </header>
            <div class="panel-body">
                <form id="fomeeting" class="form-horizontal" method="post">
					{{ csrf_field() }}
                    <input type="hidden" name="id" id="id" value="{{ $reference->reference_id }}">
				 	<input type="hidden" name="type" value="lead">
				 	
                    <div class="panel-body">
						<?php //dd($reference); ?>
                        <div class="form-group">
							<label class="control-label col-md-2">Name:</label>
							<div class="col-md-3">
								<input type="text" name="name" data-required="1" class="form-control" placeholder="Please Enter Name" value="{{$reference->name}}" readonly/>
							</div>
							<label class="control-label col-md-1">Id:</label>
							<div class="col-md-2">
								<input type="text" name="name" data-required="1" class="form-control" placeholder="Please Enter Name" value="{{$reference->reference_id}}" readonly/>
							</div>
						</div>
						<h4><i class="fa fa-comments"></i>FO Remarks</h4>
						<div class="table-responsive">
							<div class="form-group">
								<label class="control-label col-md-3">Meeting
									<span class="required"> * </span>
								</label>
								<div class="col-md-6">
									<div class="radio-list" data-error-container="#form_2_membership_error">
										<label class="radio-inline">
											<input type="radio" name="meeting" value="met" class="meet"/>Met
										</label>
										@if(!(!empty(request()->flag) && request()->flag==1))
											<label class="radio-inline">
												<input type="radio" name="meeting" value="notmet" class="meet"/>Not Met
											</label>
										@endif
									</div>
									<div id="form_2_membership_error"></div>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3">Status
									<span class="required"> * </span>
								</label>
								<div class="col-md-3">
									<select class="form-control" name="remarks" id="remarks">
										<option value="">Select Status</option>
									</select>
								</div>
							</div>
							<div class="form-group followup disphide">
								<label class="control-label col-md-3">Category
									<span class="required"> * </span>
								</label>
								<div class="col-md-3">
									<select class="form-control" name="subcategory" id="subcategory">
										<option value="">Select Subcategory</option>
										<option value="voc">Family Discussion</option>
										<option value="operation">Cheque Pickup</option>
									</select>
								</div>
							</div>
							<div class="form-group saledone followup disphide">
								<label class="control-label col-md-3">Plan Size
									<span class="required"> * </span>
								</label>
								<div class="col-md-3">
									<input type="text" class="form-control" name="amount">
								</div>
							</div>
							<div class="form-group saledone disphide">
								<label class="control-label col-md-3">Payment Mode
									<span class="required"> * </span>
								</label> 
								<div class="col-md-3">	
									<select class="form-control" name="paymentmode" id="paymentmode">
									<option value="">Select Payment Mode</option>
									@foreach($paymentmode as $key=>$value)
										<option value="{{ $key }}">{{ $value }}</option>
									@endforeach
									</select>
								</div>
							</div>
							<div class="form-group saledone followup disphide">
								<label class="control-label col-md-3">Form No
									<span class="required"> * </span>
								</label>
								<div class="col-md-3">
									<input type="text" class="form-control" name="form_no">
								</div>
							</div>
							<div class="form-group saledone followup disphide">
								<label class="control-label col-md-3">
									ECS <span class="required">*</span>
								</label>
								<div class="col-md-3">
									<div class="radio-list" data-error-container="#form_2_membership_error">
										<label class="radio-inline">
											<input type="radio" class="ecs" name="ecs" value="yes"/>Yes
										</label>
										<label class="radio-inline">
											<input type="radio" class="ecs" name="ecs" value="no"/>NO
										</label>
									</div>
									<div id="form_2_membership_error"></div>
								</div>
							</div>
							<div class="form-group saledone disphide">
								<label class="control-label col-md-3">
									Home Try Home <span class="required">*</span>
								</label>
								<div class="col-md-3">
									<div class="radio-list" data-error-container="#form_2_membership_error">
										<label class="radio-inline">
											<input type="radio" name="hth" class="hth" value="yes"/>Yes
										</label>
										<label class="radio-inline">
											<input type="radio" name="hth" class="hth" value="no"/>NO
										</label>
									</div>
									<div id="form_2_membership_error"></div>
								</div>
							</div>
							<div class="form-group transaction disphide">
								<label class="control-label col-md-3">Transaction ID
									<span class="required"> * </span>
								</label>
								<div class="col-md-3">
									<input type="text" class="form-control" name="transaction_id" id="transaction_id"> 
								</div>
							</div>
							<div class="form-group cheque disphide">
								<label class="control-label col-md-3">Cheque No
									<span class="required"> * </span>
								</label>
								<div class="col-md-3">
									<input type="text" class="form-control" name="chequeno" id="chequeno"> 
								</div>
							</div>
							<div class="form-group cheque disphide">
								<label class="control-label col-md-3">Cheque Date
									<span class="required"> * </span>
								</label>
								<div class="col-md-3">
									<input type="text" class="form-control" name="chequedate" id="chequedate" readonly>
								</div>
							</div>
							<div class="form-group cheque disphide">
								<label class="control-label col-md-3">Bank Name
									<span class="required"> * </span>
								</label>
								<div class="col-md-3">
									<input type="text" class="form-control" name="bankname" id="bankname">
								</div>
							</div>
							<div class="form-group followup callback disphide">
								<label class="control-label col-md-3">Payment/Callback Date
									<span class="required"> * </span>
								</label>
								<div class="col-md-3">
									<input type="text" class="form-control" name="paydate" id="paydate" readonly>
								</div>
							</div>
							<div class="form-group notint disphide">
								<label class="control-label col-md-3"> Reason
									<span class="required"> * </span>
								</label>
								<div class="col-md-3">
									<select class="form-control" name="nireason" id="nireason">
									<option value="">Select NI Reason</option>
									@foreach(config('custom.niremarks') as $key=>$value)
										<option value="{{ $key }}">{{ $value }}</option>
									@endforeach
									</select>
								</div>
							</div>
							<div class="form-group noother disphide">
								<label class="control-label col-md-3">Other Reason <span class="required"> * </span>
								</label>
								<div class="col-md-3">
									<input type="text" class="form-control" name="reasonother" id="reasonother">
								</div>
							</div>
						</div>
                    </div>
                    <footer class="panel-footer">
                        <div class="row">
                            <div class="col-sm-12" align="center">
                                <button type="submit" class="btn btn-success green" >Submit</button>&nbsp;&nbsp;&nbsp;&nbsp;
                                <a href="{{ URL::to('lead-list')}}" class="btn btn-default" >Cancel</a>
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            </div>
                        </div>
                    </footer>
                </form>
            </div>
        </section>
    </section>
@endsection
@section('page_level_script_bottom')
    <script src="{{URL::asset('public/assets/vendor/jquery-validation/jquery.validate.js')}}"></script>
  
    <script src="{{URL::asset('public/assets/javascripts/form-validation.js')}}"></script>
    <script src="{{URL::asset('public/assets/javascripts/app.js')}}"></script>
    
    <script>
		var reference=[];
		jQuery(document).ready(function() {
			$('#loading').hide();
			App.init(); 
			FormValidation.init();
			$('.disphide').hide();
			
			@if(!empty(request()->flag) && request()->flag==1)
				$('.meet').filter('[value="met"]').prop('checked', true);
				$('.meet').parent().addClass('checked');
				$('.meet').trigger('change');
				$('#remarks option').not(':first,:last').not(':first').remove();
			@endif
		}); 


		$('.meet').change(function(){
			$('#remarks option').not(":first").remove();
			var met={!! json_encode(config('custom.Met')) !!};
			var nomet={!! json_encode(config('custom.Notmet')) !!};
			var meet = $(this).val();
			$('.disphide').hide();
			$('.disphide :input').not('.ecs,.hth').val('');
			if(meet=='met'){
				var remarks = met;
			}else if(meet=='notmet'){
				var remarks = nomet;
			}
			$.each(remarks,function(index,item){ 
				$("#remarks").append(
					$("<option></option>").text(item).val(index)
				);
			});
		});

		$('#remarks').change(function(){
			$('.disphide').hide();
			$('.disphide :input').not('.ecs,.hth').val('');
			$('.ecs,.hth :input').prop('checked',false);
			$('.ecs,.hth').parent().removeClass('checked');
			var value = $(this).val();
			if(value!=""){
				$('.'+value).show();
				if(value=='saledone'){
					$('#reference').show();	
				}
			}
		});

		$('#nireason').change(function(){
			var value = $('#nireason option:selected').val();
			$('.noother :input').val('');
			$('.noother').hide();	
			if(value=="other"){
				$('.noother').show();	
			}
		})

		function callpost(){                      
			$.ajax({
                          
				url: "{{ route('fo-meeting-update') }}",
				data: $('#fomeeting').serialize(),//+'&reference='+JSON.stringify(reference),  
				type: 'post',
				async:true,
				cache:false,
				dataType:'json',
				beforeSend:function(){	
					$('#loading').show();
				},
				success:function(){ 
                                   // console.log()
					window.location = "{{ route('dashboard') }}";
				},
				error:function(){
                                   
					alert("Server is Busyfcg1f1hgf1hg1!!");
				},
				complete:function (data) {
					$('#loading').hide();
				}
			});
		}

		$('#paymentmode').change(function(){
			$('.cheque,.transaction :input').val('');
			var value = $('#paymentmode option:selected').val();
			$('.cheque,.transaction').hide();
			if(value==2){	
				$('.cheque').show();
			}else if(value==10 || value==6){
				$('.transaction').show();
			}
		});

		
		$('#saveref').click(function(){
			//var data = JSON.stringify($('#reference :input').serializeArray());
			var jsonObj = {};
			$.map($('#reference :input').serializeArray(), function(n,i) {
				jsonObj[n.name] = n.value;
			});
			reference.push(jsonObj);
			var append="";
			$('#append').html('');
			$.each(reference,function(index,item){
				append+='<tr><td>'+(index+1)+'</td>';
				append+='<td>'+(item['ref_name'])+'</td>';
				append+='<td>'+(item['ref_mobile'])+'</td>';
				append+='<td>'+(item['ref_email'])+'</td>';
				append+='<td>'+(item['ref_state'])+'</td>';
				append+='<td>'+(item['ref_city'])+'</td>';
				append+='<td><a data-id='+index+' class="delete">Delete</a></td></tr>';
			});
			$('#append').append('<tr>'+append+'</tr>');
			$('#reference :input').not('#saveref').val('');
		});
		
		
	</script>

@endsection
