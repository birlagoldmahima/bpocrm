@extends('layout.master')
@section('page_title')
    LMS FTL Assigned FO
@endsection
@section('page_level_style_top')
	<header class="page-header">
        <h2>Assign FO</h2>
        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li>
                    <a href="#">
                        <i class="fa fa-home"></i>
                    </a>
                </li>
                <li><span>FTL Assigned FO</span></li>
                <li><span>Assigned FO</span></li>
            </ol>

            <a class="sidebar-right-toggle" data-open="#"><i class="fa fa-chevron-left"></i></a>
        </div>
    </header>
@endsection

@section('page_level_style_top')
	<link rel="stylesheet" type="text/css" href="{{URL::to('assets/plugins/select2/select2_metro.css')}}"/>
	<link rel="stylesheet" type="text/css" href="{{URL::to('assets/plugins/data-tables/DT_bootstrap.css')}}"/>
@endsection


@section('content')

<section role="main" class="content-body">
	<section class="panel">
		<header class="panel-heading">
			<h2 class="panel-title">Lead List</h2>
		</header>
		<div class="panel-body">
			<table class="table table-bordered table-striped mb-none">
				<thead>
					<tr>
						<th>#</th>
						<th>ID</th>
						<th>Lead Name</th>
						<th>Appointment Date</th>
						<th>FTL</th>
						<th>FO</th>
						<th>Status</th>
					</tr>
				</thead>
				<tbody>
					<?php $count=1 ;?>
					@foreach($results as $key=>$data)
						<tr>
							<td>
								{{ $key+1 }}
							</td>
							<td>
								{{ $data['reference_id']}}
							</td>
							<td>
								{{$data['name']}}
							</td>
							<td>
								{{ date('d M ,Y H:i',strtotime($data['appointment_date'])) }}
							</td>
							<td>
								@if(!empty($data['ftl_id']))
									{{ $user[$data['ftl_id']] }}
								@endif
							</td>
							<td>
								@if(!empty($data['fo_id']))
									{{ $user[$data['fo_id']] }}
								@endif
							</td>
							<td>
								{{ ucfirst($data['stage_remarks']) }} By FO
							</td>
						</tr>
						<?php $count++;?>
					@endforeach
				</tbody>
			</table>
			<div class ="pull-right" style="margin-bottom:20px">
				
			</div>
		</div>
	</section>
</section>

	

@endsection

@section('page_level_script_bottom')
	<script type="text/javascript" src="{{URL::to('assets/plugins/select2/select2.min.js')}}"></script>
	<script type="text/javascript" src="{{URL::to('assets/plugins/data-tables/jquery.dataTables.js')}}"></script>
	<script type="text/javascript" src="{{URL::to('assets/plugins/data-tables/DT_bootstrap.js')}}"></script>
	<script src="{{URL::to('assets/scripts/app.js')}}"></script>
	<script src="{{URL::to('assets/scripts/table-editable.js')}}"></script>
	<script src="{{URL::to('assets/plugins/bootbox/bootbox.min.js')}}" type="text/javascript"></script>
	<script>
		var ftldata= {!! json_encode($user) !!};
		jQuery(document).ready(function() { 
			$('#loading').hide();
			App.init(); 
	   		TableEditable.init();
	   		$('.pagination li a').addClass('clk');
	   	});
	</script>
@endsection
