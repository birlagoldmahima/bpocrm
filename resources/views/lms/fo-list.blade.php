@extends('layout.master')
@section('page_title')
    LMS FO Lead Acceptance
@endsection
@section('page_level_style_top')

	<header class="page-header">
        <h2>Lead Acceptance</h2>
        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li>
                    <a href="#">
                        <i class="fa fa-home"></i>
                    </a>
                </li>
                <li><span>FTL FO Acceptance</span></li>
                <li><span>FO Acceptance</span></li>
            </ol>

            <a class="sidebar-right-toggle" data-open="#"><i class="fa fa-chevron-left"></i></a>
        </div>
    </header>
@endsection
@section('content')

<section role="main" class="content-body">
	<section class="panel">
		<header class="panel-heading">
			<h2 class="panel-title">Lead List</h2>
		</header>
		<div class="panel-body">
			<table class="table table-bordered table-striped mb-none">
				<thead>
					<tr>
						<th>#</th>
						<th>ID</th>
						<th>Lead Name</th>
						<th>Type</th>
						<th>Appointment Date</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					@foreach($results as $key=>$data)
						<tr>
							<td>
								{{ $key+1 }}
							</td>
							<td>
								{{ $data['reference_id']}}
							</td>
							<td>
								{{$data['name']}}
							</td>
							<td>
								@if(empty($data['customer_id']) || $data['customer_id']==0)
									Lead
								@else
									Reference
								@endif
							</td>
							<td>
								{{ date('d M ,Y H:i',strtotime($data['appointment_date'])) }}
							</td>
							<td>
								<a  data-ref="{{ $data['reference_id'] }}" class="assign btn btn-success" data-status="9">Accepted</a> / <a  data-ref="{{ $data['reference_id'] }}" class="assign btn btn-danger" data-status="10">Rejected</a>
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
			<div class ="pull-right" style="margin-bottom:20px">
				
			</div>
		</div>
		<form action="{{ route('fo-update') }}" method="post" id="frmforemarks" name="frmforemarks" role="search">
  			<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
  			<input type="hidden" name="id" id="id" value="">
  			<input type="hidden" name="type" value="lead">
  			<input type="hidden" name="call_status" value="4">
  			<input type="hidden" name="call_sub_status" value="" id="call_sub_status">
  			<input type="hidden" name="verification_interaction" value="" id="verification_interaction">
		</form>
	</section>
</section>
@endsection
@section('page_level_script_bottom')
	
	<script src="{{URL::asset('public/assets/bootbox/bootbox.min.js')}}" type="text/javascript"></script>
	
	<script>
		jQuery(document).ready(function() { 
			$('#loading').hide();
			App.init(); 
			FormComponents.init();
	   		FormValidation.init();
	   		$('.pagination li a').addClass('clk');
	   		
	   	});

	   	$('.assign').click(function(){
	   		var ref = $(this).data('ref');
			$('#id').val(ref);
			$('#call_sub_status').val($(this).data('status'));
			$('#verification_interaction').val($(this).html());
			bootbox.confirm("Are you sure, You want to "+$(this).html()+" this Lead", function(result) {
				if(result == true){
					document.frmforemarks.submit();
					return false;
				}
			});
		});

		$('#call_sub_status').change(function(){
			$('#verification_interaction').html('');
			if($('#call_sub_status option:selected').val()!=""){
				$('#verification_interaction').html($('#call_sub_status option:selected').html());
			}
		});
	</script>
@endsection
