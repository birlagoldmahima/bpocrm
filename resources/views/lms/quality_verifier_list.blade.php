@extends('layout.master')
@section('page_title')
Field Assign 
@endsection
@section('breadcrumb')
<li>
    <a href="#">Field Assign </a>
</li>
@endsection
@section('page_level_style_top')

@endsection
@section('content')

<header class="page-header">
    <h2>Dashboard </h2>
    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="#">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span>Lead</span></li>
            <li><span>Field Assign </span></li>
        </ol>

        <a class="sidebar-right-toggle" data-open="#"><i class="fa fa-chevron-left"></i></a>
    </div>
</header>
<section role="main" class="content-body">
    <section class="panel">
        <header class="panel-heading">
            <div class="row">
                <h2 class="panel-title col-md-5">Field Assign</h2>
                @if(Session::has('flash_message'))
                <div class="alert {{ Session::get('flash_alert') }}">
                    {!! Session::get('flash_message') !!}
                </div>
                @endif
                @if($errors->any())
                <div class="alert alert-danger text-center">
                    @foreach($errors->all() as $error)
                    <p>{{ $error }}</p>
                    @endforeach
                </div>
                @endif
            </div>
        </header>
        <div class="panel-body">

            <form class="navbar-form" method="post" name="frm" role="search" action="">

                <div class="table-scrollable">
                    <table class="table table-striped table-hover table-bordered">
                        <thead>
                            <tr>
                                <th>
                                    #
                                </th>
                                <th>
                                    ID
                                </th>
                                <th>
                                    Lead Name
                                </th>
                                <th>
                                    Mobile
                                </th>
                                <th>
                                    Email
                                </th>
                                <th>
                                    Appointment Date
                                </th>
                                <th>
                                    Type
                                </th>
                                <th>
                                    Assigned
                                </th>
                                <th>
                                    Quality
                                </th>
                                <th>
                                    Action
                                </th>
                            </tr>
                        </thead>

                        <tbody>
                            @foreach($results as $key=>$data)

                            <tr>
                                <td>
                                    {{ $key+1}}
                                </td>
                                <td>
                                    {{ $data->reference_id}}
                                </td>
                                <td>
                                    {{$data->name}}
                                </td>  
                                <td>
                                    {{$data->mobile}} 
                                </td>
                                <td>
                                    {{ $data->email}}
                                </td>
                                <td>
                                    {{ date('d-m-Y H:i',strtotime($data->appointment_date))}}
                                </td>
                                <td>
                                    @if(empty($data->customer_id) || $data->customer_id==0)
                                    Lead
                                    @else
                                    Reference
                                    @endif
                                </td>
                                <td>
                                    @if($data->stage_id==3)
                                    Send By TL
                                    @elseif($data->stage_id==9)
                                    Send By VOC
                                    @endif

                                </td>
                                <td>
                                    {{ $data->qualityname }}	
                                </td>
                                <td>
                                    @if(Auth::user()->role=="Verifier")
                                   <a href="{{ route('quality-view',[$data->reference_id])}}">View</a>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>

            </form>

        </div>
        <div class="container">
            <div class="modal fade" id="myModal" role="dialog">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-body" id="alt">
                            <p id="para"></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="wide" tabindex="-1" role="basic" aria-hidden="true">
            <form action=""  id="pincodecsv" class="form-horizontal" method="post" enctype="multipart/form-data">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-body">
                            <div class="form-group">
                                <label class="control-label col-md-3">Choose CSV
                                    <span class="required">*</span>
                                </label>
                                <div class="col-md-9">
                                    <div class="fileupload fileupload-new" data-provides="fileupload">
                                        <div class="input-group">
                                            <span class=" default">
                                                <input type="file" name="import_file" class="default form-control"/>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="cancel" class="btn default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn blue">Upload</button>
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </section>
</section>
@endsection
@section('page_level_script_bottom')
<script src="{{URL::asset('public/assets/vendor/jquery-validation/jquery.validate.js')}}"></script>
<script src="{{URL::asset('public/assets/javascripts/forms/form-validation.js')}}"></script>
<script src="{{URL::asset('public/assets/javascripts/app.js')}}"></script>
<script>

jQuery(document).ready(function () {
    $('#loading').hide();
    App.init();
    TableEditable.init();
    $('.pagination li a').addClass('clk');
});
</script>
@endsection