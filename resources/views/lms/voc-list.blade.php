@extends('layout.master')
@section('page_title')
    Voc List
@endsection
@section('page_level_style_top')

	<header class="page-header">
        <h2>Voc List</h2>
        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li>
                    <a href="#">
                        <i class="fa fa-home"></i>
                    </a>
                </li>
                <li><span>Voc List</span></li>
                <li><span>Voc List</span></li>
            </ol>

            <a class="sidebar-right-toggle" data-open="#"><i class="fa fa-chevron-left"></i></a>
        </div>
    </header>
@endsection
@section('content')

<section role="main" class="content-body">
	<section class="panel">
		<header class="panel-heading">
			<h2 class="panel-title">VOC List</h2>
		</header>
		<div class="panel-body">
			<table class="table table-bordered table-striped mb-none">
				<thead>
					<tr>
						<th>#</th>
						<th>ID</th>
						<th>Lead Name</th>
						<th>Mobile</th>
						<th>Type</th>
						<th>Callback Date</th>
						<th>Source Stage</th>
						<th>Current Stage</th>
						<th>Attepmt</th>
						<th>Count</th>
						<th>Call Status(Other)</th>
						<th>Last Attempt Date</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<?php 
						$count=1 ;
						$array = array_merge_recursive(config('custom.Met'),config('custom.Notmet'),['voc'=>'Followup (Met)']);
						$vocStatus = config('custom.VocStatus');
					?>
					@foreach($results as $key=>$data)
						<tr>
							<td>
								{{ $key+1  }}
							</td>
							<td>
								{{ $data->reference_id}}
							</td>
							<td>
								{{$data->name}}
							</td>
							<td>
								{{$data->mobile}}
							</td>
							<td>
								@if(empty($data->customer_id) || $data->customer_id==0)
									Lead
								@else
									Reference
								@endif
							</td>

							<td>
								{{ $data->callback }}
							</td>
							<td>

								{{ (isset($remark_status[$data->last_status_id])? $remark_status[$data->last_status_id] : "" ).' - '.(isset($remark_sub_status[$data->last_status_sub_id]) ? $remark_sub_status[$data->last_status_sub_id] : "") }}
							</td>
							<td>
								
							</td>
							<td>
								{{ $data->countcall }}
							</td>
							<td>
								{{ $data->metcount }}
							</td>
							<td>
								@if($data->call_status_other !=0)
									{{ $vocStatus[$data->call_status_other] }}
								@else 
									-
								@endif
							</td>
							<td>
								@if($data->date != null)
									{{ date('d-m-Y',strtotime($data->date) )  }}
								@else 
								-
								@endif
							</td>
							<td>
								<a  href="#" data-toggle="modal" data-lead="{{$data->reference_id }}" data-target="#remarks" class="responsedata">Response</a> 
								@if($data->metcount<3)
									/ <a href="#" data-fo="{{ $key }}" data-ref="{{ $data->reference_id }}" class="assign">Action</a>
								@endif  
								@if($data->countcall>=5 || $data->metcount==3)
									/ <a href="#" class="not_interested"  data-id="{{$data->reference_id}}" > Mark NI</a>
								@endif
							</td>

						</tr>
						
						<?php $count++;?>
					@endforeach
				</tbody>
			</table>
			<div class ="pull-right" style="margin-bottom:20px">
				
			</div>
		</div>
		<div class="modal fade" id="appointment" tabindex="-1" role="basic" aria-hidden="true"  data-keyboard="false" data-backdrop="static">
			<div class="modal-dialog">
				<form action="{{ route('voc-update') }}" method="post" id="vocupdate" name="vocupdate" role="search">
					<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
					<input type="hidden" name="id" id="id" value="">
					<input type="hidden" name="type" value="lead">
					<input type="hidden" name="status_name" id="status_name" value="">
					<div class="modal-content">
						<div class="modal-header">
							<h4 class="modal-title">Voc Remarks for Lead ID <span id="showlead"></span></h4>
						</div>
						<div class="modal-body">
							<span id="appointfed"></span>
							<div class="form-body">
								<div class="form-group">
									<div class="row">
										<label class="col-md-4 control-label">Call Status:</label>
										<div class="col-md-6">	
											<select class="form-control" name="call_status" id="call_status">
												@foreach($call_status as $key=>$val)
													<option value="{{$val['id']}}">{{$val['name']}}</option>
												@endforeach
											</select>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row">
										<label class="control-label col-md-4">Contact:</label>
										<div class="col-md-8">
											<div class="radio-list" data-error-container="#form_2_membership_error">
												<label class="radio-inline">
													<input type="radio" name="contact" value="contact" class="meet"/>Contactable
												</label>
												<label class="radio-inline">
													<input type="radio" name="contact" value="noncontact" class="meet"/>Not Contactable
												</label>
											</div>
											<div id="form_2_membership_error"></div>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row">
										<label class="col-md-4 control-label">Call Sub Status:</label>
										<div class="col-md-6">	
											<select class="form-control" name="call_sub_status" id="call_sub_status" >
												<option value=""> --- Select Call Sub Status ---</option>
											</select>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row">
										<label class="col-md-4 control-label">Remarks:</label>
										<div class="col-md-6">	
											<textarea type="textarea" rows="4" name="verification_interaction" id="verification_interaction" class="form-control" value="" placeholder="Please Enter Verification Interaction " style="resize:vertical;" ></textarea>	
										</div>
									</div>
								</div>
								<div class="form-group" style="display:none;"  id="datetime">
									<div class="row">
										<label class="col-md-4 control-label">Callback Datetime:</label>
										<div class="col-md-6">	
											<input type="text" name="appointment_date" id="appointmentdate" class="form-control" value="" readonly>	
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<button type="submit" class="btn green">Save</button>
							<button type="button" class="btn default" data-dismiss="modal">Close</button>
						</div>
					</div>
				</form>
			</div>	
		</div>
		<div class="modal fade" id="remarks" tabindex="-1" role="basic" aria-hidden="true" data-keyboard="false" data-backdrop="static">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title text-purple"><b>Interaction</b></h4>
					</div>
					<div class="modal-body" >
						<div class="row form-group">
							<div class="col-md-12" style='overflow:auto;height:400px;'>
								<h4>Verifier Interaction</h4>
								<div class="table-scrollable">
									<table class="table table-striped table-hover table-bordered">
										<thead>
											<tr>
												<th>#</th>
												<th>Created Date</th>
												<th>Logged By</th>
												<th>Call Status</th>
												<th>Call Sub Status</th>
												<th>Call Back</th>
												<th>Remarks</th>
											</tr>
										</thead>
										<tbody id="verfierinteraction">
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn default" data-dismiss="modal" id="closebtn">Close</button>
					</div>
				</div>
			</div>
		</div>
	</section>
</section>
@endsection

@section('page_level_script_bottom')
	<script type="text/javascript" src="{{URL::to('assets/plugins/select2/select2.min.js')}}"></script>
	<script type="text/javascript" src="{{URL::to('assets/plugins/data-tables/jquery.dataTables.js')}}"></script>
	<script type="text/javascript" src="{{URL::to('assets/plugins/data-tables/DT_bootstrap.js')}}"></script>
	<script src="{{URL::asset('public/assets/javascripts/form-validation.js')}}"></script>
    <script src="{{URL::asset('public/assets/javascripts/app.js')}}"></script>
	<script src="{{URL::to('assets/scripts/table-editable.js')}}"></script>
	<script src="{{URL::to('assets/plugins/bootbox/bootbox.min.js')}}" type="text/javascript"></script>
	<script type="text/javascript" src="{{URL::asset('public/assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
	<script>
		jQuery(document).ready(function() { 
			$('#loading').hide();
			App.init(); 
			FormComponents.init();
	   		FormValidation.init();
	   		$('.pagination li a').addClass('clk');
	   		var start = new Date("{!! date('D M d Y H:i:s',strtotime('+5 hours +30 minutes', strtotime(date('Y-m-d H:i:s')))) !!}");
			var end = new Date("{!! date('D M d Y H:i:s',strtotime('+6 day', strtotime(date('Y-m-d H:i:s')))) !!}");
			end.setHours(23);
	   		$('#appointmentdate').datetimepicker({
        		format: "dd-mm-yyyy hh:ii:ss",
        		startDate: start,
        		endDate: end,
        		autoclose: true,
			});
			$('#from_date,#to_date').datepicker({
    			format: "dd-mm-yyyy",
    			autoclose: true,
    		});
    	});

	   	$('.assign').click(function(){
			var ref = $(this).data('ref');
			$('#id').val(ref);
			$('#showlead').html(ref);
			$('#vocupdate')[0].reset();
			$('#call_sub_status').trigger('change');
			$('#appointment').modal('toggle');
		});

		$('#call_sub_status').change(function(){
			var value = $('#call_sub_status option:selected').val();
			$('#status_name').val($('#call_sub_status option:selected').html())
			$('#datetime').hide();
			$('#datetime :input').val('');
			if(value==12 || value==14 || value==24 || value==25 || value==26 || value==28){
				$('#datetime').show();
			}
		});

		$('.responsedata').click(function(){
			var leadid=$(this).data('lead');
			$.ajax({
				url: "{{ URL::to('rm-rejected-reason') }}/"+$(this).data('lead'),
				//data: {'id':$(this).data('lead')}, 
				type: 'get',
				async:true,
				cache: false,
				beforeSend:function(){	
					$('#loading').show();
				},
				success:function(response){
					/*var append="";
					$.each(response['interaction'],function(index,item){
	      				append+='<tr><td>'+(index+1)+'</td>';
	      				append+='<td>'+(item['created_at'])+'</td>';
	      				append+='<td>'+(item['rmname'])+'</td>';
	      				append+='<td>'+(item['call_status'])+'</td>';
	      				append+='<td>'+(item['call_sub_status'])+'</td>';
	      				append+='<td>'+(item['remarks'])+'</td></tr>';
					});
					$('#interaction').append('<tr>'+append+'</tr>');*/
					$('#verfierinteraction').html('');
					var append="";
					$.each(response['verifier'],function(index,item){
						var calldate = item['callback_date'];
						if(item['callback_date']==null){
							calldate="-";
						}
	      				append+='<tr><td>'+(index+1)+'</td>';
	      				append+='<td>'+(item['created_at'])+'</td>';
	      				append+='<td>'+(item['rmname'])+'</td>';
	      				append+='<td>'+(item['call_status'])+'</td>';
	      				append+='<td>'+(item['call_sub_status'])+'</td>';
	      				append+='<td>'+(calldate)+'</td>';
	      				append+='<td>'+(item['remarks'])+'</td></tr>';
					});
					$('#verfierinteraction').append('<tr>'+append+'</tr>');
				},
				error:function(){
					alert("Server is Busy!!");
				},
				complete:function (data) {
	    			$('#loading').hide();
	    		}
			});
   		});

		var call_sub_status = {!! json_encode($call_sub_status) !!};
		$('.meet').change(function(){
			$('#call_sub_status option').not(":first").remove();
			var meet = $(this).val();
			$.each(call_sub_status,function(index,item){ 
				if(item.code==meet && item.id!=27){
		   			$("#call_sub_status").append(
		        		$("<option></option>").text(item.name).val(item.id)
		      		);
		   		}
	    	});
	    });

	    $('.excel').click(function(){
			window.location.href="{{ route('voc-excel') }}?"+$('#frm').serialize();
		})
	</script>
@endsection
