@extends('layout.master')
@section('page_title')
Sale List 
@endsection
@section('breadcrumb')
<li>
    <a href="#">Sale List </a>
</li>
@endsection
@section('page_level_style_top')

@endsection
@section('content')
<header class="page-header">
    <h2>Dashboard </h2>
    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="#">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span>Lead</span></li>
            <li><span>Sale List </span></li>
        </ol>

        <a class="sidebar-right-toggle" data-open="#"><i class="fa fa-chevron-left"></i></a>
    </div>
</header>
<section role="main" class="content-body">
    <section class="panel">
        <header class="panel-heading">
            <div class="row">
                <h2 class="panel-title col-md-5">Sale List</h2>
                @if(Session::has('flash_message'))
                <div class="alert {{ Session::get('flash_alert') }}">
                    {!! Session::get('flash_message') !!}
                </div>
                @endif
                @if($errors->any())
                <div class="alert alert-danger text-center">
                    @foreach($errors->all() as $error)
                    <p>{{ $error }}</p>
                    @endforeach
                </div>
                @endif
            </div>
        </header>
        <div class="panel-body">
            <div class="table-scrollable">
                <table class="table table-striped table-hover table-bordered">
                    <thead>
                        <tr>
                            <th>
                                ID
                            </th>
                            <th>
                                Lead Name
                            </th>
                            <th>
                                Plan Size
                            </th>
                            <th>
                                Form No
                            </th>
                            <th>
                                Ecs
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($results as $key=>$data)
                        <tr>
                            <td>
                                {{ $data['reference_id']}}
                            </td>
                            <td>
                                {{$data['name']}}
                            </td>
                            <td>
                                {{ $data['plan_size']  }}
                            </td>
                            <td>
                                {{ $data['form_no'] }}
                            </td>
                            <td>
                                {{ ucfirst($data['ecs_option']) }}
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </section>
</section>
@endsection
@section('page_level_script_bottom')
<script src="{{URL::asset('public/assets/vendor/jquery-validation/jquery.validate.js')}}"></script>
<script src="{{URL::asset('public/assets/javascripts/forms/form-validation.js')}}"></script>
<script src="{{URL::asset('public/assets/javascripts/app.js')}}"></script>
<script>
jQuery(document).ready(function () {
    $('#loading').hide();
    App.init();
    TableEditable.init();
    $('.pagination li a').addClass('clk');
});
</script>
@endsection
