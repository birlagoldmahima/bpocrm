@extends('layout.master')
@section('page_title')
@endsection
@section('breadcrumb')
<li><a href="#">Verifier Lead </a></li>
@endsection
@section('page_level_style_top')
@endsection
@section('content')

<header class="page-header">
    <h2>Verifier Lead List</h2>
    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="#">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span>Lead</span></li>
            <li><span>Verifier Lead List</span></li>
        </ol>
        <a class="sidebar-right-toggle" data-open="#"><i class="fa fa-chevron-left"></i></a>
    </div>
</header>
<section role="main" class="content-body">
    <section class="panel">
        <header class="panel-heading">
            <div class="row">
                <h2 class="panel-title col-md-5">Verifier Lead</h2>
                @if($errors->any())
                <div class="alert alert-danger text-center col-md-3">
                    @foreach($errors->all() as $error)
                    <p>{{ $error }}</p>
                    @endforeach
                </div>
                @endif
            </div>
        </header>
        <div class="panel-body">
            <table class="table table-striped table-hover table-bordered">
                <thead>
                    <tr id="tbl">
                        <th> Id	</th>
                        <th> Name </th>
                        <th> Entry Date </th>
                        <th> Type </th>
                        <th> Quality </th>
                        <th> Appointment Date </th>
                        <th> Assigned By </th>
                        <th> Actions </th>
                    </tr>
                </thead>
                <tbody id="tbl">
                    <?php $count = 1;
                    ?>
                    @foreach($results as $keychk=>$data)
                    <tr>
                        <td>
                            {{$data['reference_id']}}
                        </td>
                        <td>
                            {{$data['name']}}
                        </td>

                        <td>
                            {{date("d-m-Y",strtotime($data['created']))}}
                        </td>											
                        <td>
                            @if(empty($data['customer_id']) || $data['customer_id']==0)
                            Lead
                            @else
                            Reference
                            @endif
                        </td>
                        <td>
                            <select name="quality" class="form-control" id="sel{{ $keychk }}">
                                <option value="">-- Select --</option>	
                                @foreach($rm as $key=>$value)
                                <option value="{{ $key }}">{{ $value }}</option>
                                @endforeach
                            </select>
                        </td>
                        <td>
                            {{ date('d-m-Y H:i',strtotime($data['appointment_date']))}}
                        </td>
                        <td>
                            @if($data['stage_id']==8)
                            Assign by VOC
                            @else
                            Assign by TL
                            @endif
                        </td>
                        <td>
                            <a  class="btn assign" data-id="{{ $data['reference_id'] }}" data-key="{{ $keychk }}" data-ref="{{ $data['reference_id'] }}">Assign</a>
                        </td>
                    </tr>
                    <?php $count++; ?>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class ="pull-right" style="margin-bottom:20px">
            {!! $results->appends(['q'=>$q,'search_option'=>$search_option,'sort'=>$sort,'sorttype'=>$sorttype])->render() !!}
        </div>
    </section>
</section>
@endsection
@section('page_level_script_bottom')
<script src="{{URL::asset('public/assets/vendor/jquery-validation/jquery.validate.js')}}"></script>
<script src="{{URL::asset('public/assets/javascripts/forms/form-validation.js')}}"></script>
<script src="{{URL::asset('public/assets/javascripts/app.js')}}"></script>

<script>
jQuery(document).ready(function () {
   
    $('#loading').hide();
    App.init();

});

$('.assign').click(function () {

    var rm = $('#sel' + $(this).data('key')).val();
    if (rm == "") {
        alert("Select Quality!!!");
        return false;
    }
    var ref = $(this).data('ref');
    $.ajax({
        url: "{{ route('assign-quality')}}",
        data: {'ref_id': ref, 'quality_id': rm},
        type: 'get',
        dataType: 'json',
        async: "true",
        success: function (response) {
            alert('fdfdgdfgfdg')

            window.location.reload(true);
        },
    });
});
</script>
@endsection

