@extends('layout.master')
@section('page_title')
	Not Interested Voc List
@endsection
@section('page_level_style_top')

	<header class="page-header">
        <h2>Not Interested Voc List</h2>
        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li>
                    <a href="#">
                        <i class="fa fa-home"></i>
                    </a>
                </li>
                <li><span>Not Interested Voc List</span></li>
                <li><span>Not Interested Voc List</span></li>
            </ol>

            <a class="sidebar-right-toggle" data-open="#"><i class="fa fa-chevron-left"></i></a>
        </div>
    </header>
@endsection
@section('content')

<section role="main" class="content-body">
	<section class="panel">
		<header class="panel-heading">
			<h2 class="panel-title">VOC List</h2>
		</header>
		<div class="panel-body">
			<form  method="post"  action="">
			<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
			<input type="hidden" name="currentpage" value="{{ !empty($page)?$page:"1" }}">
			<table class="table table-bordered table-striped mb-none">
				<thead>
					<tr>
						<th>ID</th>
						<th>Lead Name</th>
						<th>Mobile</th>
						<th>Email</th>
						<th>RM</th>
						<th>Appointment Date</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<?php $count=1 ;
					$array = array_merge_recursive(config('custom.Met'),config('custom.Notmet'));?>
					@foreach($results as $key=>$data)
						<tr>
							<td>
								{{ $data['reference_id']}}
							</td>
							<td>
								{{$data['name']}}
							</td>
							<td>
								{{$data['mobile']}}
							</td>
							<td>
								{{ $data['email']}}
							</td>
							<td>
								{{ $data['managername']  }}
							</td>
							<td>
								{{ $data['tlname'] }}
							</td>
							<td>
								<a data-toggle="modal" data-lead="{{$data['reference_id']}}" data-target="#remarks" class="btn btn-primary responsedata">Response</a>
							</td>

							
						</tr>
						<?php $count++;?>
					@endforeach
				</tbody>
			</table>
			<div class ="pull-right" style="margin-bottom:20px">
				
			</div>
		</div>
		<div class="modal fade" id="remarks" tabindex="-1" role="basic" aria-hidden="true" data-keyboard="false" data-backdrop="static">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title text-purple"><b>Interaction</b></h4>
					</div>
					<div class="modal-body" >
						<div class="row form-group">
							<div class="col-md-12" style='overflow:auto;height:500px;'>
								<h4>Interaction</h4>
								<div class="table-scrollable">
									<table class="table table-striped table-hover table-bordered">
										<thead>
											<tr>
												<th>#</th>
												<th>Created Date</th>
												<th>Logged By</th>
												<th>Call Status</th>
												<th>Call Sub Status</th>
												<th>Remarks</th>
											</tr>
										</thead>
										<tbody id="interaction">
										</tbody>
									</table>
								</div>
								<h4>Verifier Interaction</h4>
								<div class="table-scrollable">
									<table class="table table-striped table-hover table-bordered">
										<thead>
											<tr>
												<th>#</th>
												<th>Created Date</th>
												<th>Logged By</th>
												<th>Call Status</th>
												<th>Call Sub Status</th>
												<th>Remarks</th>
											</tr>
										</thead>
										<tbody id="verfierinteraction">
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn default" data-dismiss="modal" id="closebtn">Close</button>
					</div>
				</div>
			</div>
		</div>
	</section>
</section>
@endsection

@section('page_level_script_bottom')
	<script type="text/javascript" src="{{URL::to('assets/plugins/select2/select2.min.js')}}"></script>
	<script type="text/javascript" src="{{URL::to('assets/plugins/data-tables/jquery.dataTables.js')}}"></script>
	<script type="text/javascript" src="{{URL::to('assets/plugins/data-tables/DT_bootstrap.js')}}"></script>
	<script src="{{URL::asset('public/assets/javascripts/form-validation.js')}}"></script>
    <script src="{{URL::asset('public/assets/javascripts/app.js')}}"></script>
	<script src="{{URL::to('assets/scripts/table-editable.js')}}"></script>
	<script src="{{URL::to('assets/plugins/bootbox/bootbox.min.js')}}" type="text/javascript"></script>
	<script type="text/javascript" src="{{URL::asset('public/assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
	<script>
		jQuery(document).ready(function() { 
			$('#loading').hide();
			App.init(); 
		});

		$('.responsedata').click(function(){
			var leadid=$(this).data('lead');
			$.ajax({
				url: "{{ URL::to('rm-rejected-reason') }}/"+$(this).data('lead'),
				//data: {'id':$(this).data('lead')}, 
				type: 'get',
				async:true,
				cache: false,
				beforeSend:function(){	
					$('#loading').show();
				},
				success:function(response){
					var append="";
					$('#interaction,#verfierinteraction').html('');
					$.each(response['interaction'],function(index,item){
	      				append+='<tr><td>'+(index+1)+'</td>';
	      				append+='<td>'+(item['created_at'])+'</td>';
	      				append+='<td>'+(item['rmname'])+'</td>';
	      				append+='<td>'+(item['call_status'])+'</td>';
	      				append+='<td>'+(item['call_sub_status'])+'</td>';
	      				append+='<td>'+(item['remarks'])+'</td></tr>';
					});
					$('#interaction').append('<tr>'+append+'</tr>');
					var append="";
					$.each(response['verifier'],function(index,item){
	      				append+='<tr><td>'+(index+1)+'</td>';
	      				append+='<td>'+(item['created_at'])+'</td>';
	      				append+='<td>'+(item['rmname'])+'</td>';
	      				append+='<td>'+(item['call_status'])+'</td>';
	      				append+='<td>'+(item['call_sub_status'])+'</td>';
	      				append+='<td>'+(item['remarks'])+'</td></tr>';
					});
					$('#verfierinteraction').append('<tr>'+append+'</tr>');
				},
				error:function(){
					alert("Server is Busy!!");
				},
				complete:function (data) {
	    			$('#loading').hide();
	    		}
			});
   		})


	</script>
@endsection
