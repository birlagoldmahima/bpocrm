@extends('layout.master')
@section('page_title')

@endsection
@section('breadcrumb')
<li><a href="#">TL Verifier Lead List</a></li>
@endsection
@section('page_level_style_top')
<link rel="stylesheet" type="text/css" href="http://cherishgold.com/crm/assets/plugins/bootstrap-datetimepicker/css/datetimepicker.css">
@endsection
@section('content')

<header class="page-header">
    <h2>TL Verifier Lead List</h2>
    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="#">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span>Lead</span></li>
            <li><span>TL Verifier Lead List</span></li>
        </ol>

        <a class="sidebar-right-toggle" data-open="#"><i class="fa fa-chevron-left"></i></a>
    </div>
</header>
<section role="main" class="content-body">
    <section class="panel">
        <header class="panel-heading">
            <div class="row">
                <h2 class="panel-title col-md-5">TL Verifier Lead List</h2>
                @if($errors->any())
                <div class="alert alert-danger text-center col-md-3">
                    @foreach($errors->all() as $error)
                    <p>{{ $error }}</p>
                    @endforeach
                </div>
                @endif
            </div>
        </header>
        <div class="panel-body">

            <form method="get" name="frm" id="frm">
                <div class="col-md-12" style="margin-bottom:10px;">
                    <div class="form-group">
                        <div class="col-md-2" style="margin-top:6px;">
                            <select name="datewise" class="form-control">
                                <option value="app" {{ (isset($datewise) && $datewise=="app") ? "selected" : "" }}>Appointment</option>
                                <option value="tlver" {{ (isset($datewise) && $datewise=="tlver") ? "selected" : "" }}>Entry Date</option>
                            </select>
                        </div>
                        <div class="col-md-2">
                            <input type="text" name="fromdate" class="form-control" id="fromdate" value="{{isset($fromdate)?$fromdate:""}}" placeholder="From Date" readonly>
                        </div>
                        <div class="col-md-2">
                            <input type="text" name="todate" class="form-control" id="todate" value="{{isset($todate)?$todate:""}}" placeholder="To Date" readonly>
                        </div>
                        <div class="col-md-2">
                            <button type="submit" class="btn green">Submit</button>
                            <a href="" class="btn red">Clear</a>
                        </div>
                    </div>	
                </div>
            </form>

            <div class="tab-content">
                <div class="table-scrollable">
                    <table class="table table-striped table-hover table-bordered">
                        <thead>
                            <tr id="tbl">
                                <th>
                                    #
                                </th>
                                <th>
                                    Id
                                </th>
                                <th>
                                    Name
                                </th>
                                <th>
                                    Entry Date
                                </th>

                                <th>
                                    Relationship Manager
                                </th>
                                <th>
                                    TL Name
                                </th>
                                <th>
                                    Source
                                </th>
                                <th>
                                    Appointment Datetime
                                </th>
                                <th>
                                    Actions
                                </th>
                            </tr>
                        </thead>
                        <tbody id="tbl">
                            @foreach($results as $key=>$data)
                            <tr>
                                <td>
                                    {{ $key+1 }}
                                </td>
                                <td>
                                    <a href="{{ URL::route('tl-edit-verifier',[$data->reference_id ])}}">{{$data->reference_id }}</a>
                                </td>
                                <td>
                                    {{$data->name }}
                                </td>

                                <td>
                                    {{date("d-m-Y",strtotime($data->created ))}}
                                </td>

                                <td>
                                    @if(!empty($data->manager_name )) 
                                    {{$data->manager_name  }}
                                    @else
                                    -
                                    @endif
                                </td>
                                <td>
                                    @if(!empty($data->tlname )) 
                                    {{$data->tlname  }}
                                    @else
                                    -
                                    @endif
                                </td>
                                <td>
                                    @if(!empty($data->lead_source ))
                                    {{ config('custom.ReferenceLeadSource.'.$data->lead_source ) }}({{$data->lead_source }})
                                    @if(!empty($data->lead_source_info ))
                                    ({{$data->lead_source_info }})
                                    @endif
                                    @endif
                                </td>
                                <td>
                                    {{ date('d M, Y H:i',strtotime($data->appointment_date )) }}
                                </td>
                                <td>
                                    <a href="{{ URL::route('tl-edit-verifier',[$data->reference_id ])}}">Verify</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class ="pull-right" style="margin-bottom:20px">
                {!! $results->render() !!}
            </div>

        </div>
    </section>
</section>
@endsection
@section('page_level_script_bottom')
<script type="text/javascript" src="{{URL::to('assets/plugins/select2/select2.min.js')}}"></script>
<script type="text/javascript" src="http://cherishgold.com/crm/assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>

<script type="text/javascript" src="{{URL::to('assets/plugins/data-tables/DT_bootstrap.js')}}"></script>
<script src="{{URL::to('assets/plugins/bootbox/bootbox.min.js')}}" type="text/javascript"></script>
<script src="{{URL::to('assets/scripts/app.js')}}"></script>

<script>

jQuery(document).ready(function () {
    $('#loading').hide();
    App.init();
    FormComponents.init();
    FormValidation.init();
    $(".quality").attr("style", "display:none");
    $('#state').trigger('change');
    $('#city').val("{{ isset($decode['city'])? key($decode['city']) :"" }}");

});

var d = new Date("{!! date('D M d Y H:i:s',strtotime('+5 hours +30 minutes', strtotime(date('Y-m-d H:i:s')))) !!}");
$('#fromdate').datetimepicker({
    format: "dd-mm-yyyy hh:ii:ss",
    //startDate: d,
});

$('#todate').datetimepicker({
    format: "dd-mm-yyyy hh:ii:ss",
    //startDate: d,
});
</script>
@endsection