
@extends('layout.master')
@section('page_title')
User Profile 
@endsection
@section('breadcrumb')
<li>
    <a href="#">User Profile </a>
</li>
@endsection
@section('content')


    <!-- end: page -->

<section role="main" class="content-body">
    <header class="page-header">
        <h2>User Profile</h2>

     
        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li>
                    <a href="#">
                        <i class="fa fa-home"></i>
                    </a>
                </li>
                <li><span>User</span></li>
                <li><span>User List</span></li>
            </ol>

            <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
        </div>
    </header>

    <!-- start: page -->
    <section class="panel">
      
        <div class="panel-body">

            <table class="table table-bordered table-striped mb-none" id="datatable-editable">
	            <tbody>
					<tr > <th colspan="2" style='align:center'>My Profile</th> </tr>
					@if(count($data)>0)
                    <tr> <th>Name :</th> <td>{{$data['name']}}</td> </tr>
                    <tr> <th>Email :</th> <td>{{$data['email']}}</td> </tr>
                    <tr> <th>Mobile :</th> <td>{{$data['mobile']}}</td> </tr>
                    <tr> <th>Role :</th> <td>{{$data['role']}}</td> </tr>
                    <tr> <th>Reporting Manager :</th> <td>{{$data['remanager']}}</td> </tr>
                    @endif
                </tbody>
             </table>
        </div>
    </section>
    <!-- end: page -->
</section>


@endsection
@section('page_level_script_bottom')
<!-- Vendor -->
<!-- Examples -->
        <script src="{{URL::asset('public/assets/javascripts/tables/examples.datatables.default.js')}}"></script>
        <script src="{{URL::asset('public/assets/javascripts/tables/examples.datatables.row.with.details.js')}}"></script>
        <script src="{{URL::asset('public/assets/javascripts/tables/examples.datatables.tabletools.js')}}"></script>
@endsection			