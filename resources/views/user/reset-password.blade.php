<!DOCTYPE html>
<html class="fixed" id="mainhtml">
    <head>
        <meta charset="UTF-8">

        <title>Dashboard | Cherish Gold - BPO-CRM</title>
        <meta name="keywords" content="Cherish Gold | Birla Gold" />
        <meta name="description" content="Cherish Gold | BPO-CRM">
        <meta name="author" content="Sukhsagar">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <link href="{{URL::asset('public/assets/stylesheets/googleapis.css')}}" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="{{URL::asset('public/assets/vendor/bootstrap/css/bootstrap.css')}}"/>
        <link rel="stylesheet" href="{{URL::asset('public/assets/vendor/font-awesome/css/font-awesome.css')}}"/>
        <link rel="stylesheet" href="{{URL::asset('public/assets/vendor/jquery-ui/css/ui-lightness/jquery-ui-1.10.4.custom.css')}}" />
        
        <link rel="stylesheet" href="{{URL::asset('public/assets/stylesheets/theme.css')}}"/>
        <link rel="stylesheet" href="{{URL::asset('public/assets/stylesheets/skins/default.css')}}" />
        <link rel="stylesheet" href="{{URL::asset('public/assets/stylesheets/theme-custom.css')}}">
        <div class="loading" id="loading"> 
            <img class="loading-image" id="loading-image" src="{{URL::asset('public/assets/images/ajax-loading.gif')}}" alt="Loading..." />
        </div>
    </head>
    <body>
       <section class="body-sign">
    <div align="center" class="center-sign">
        <a class="logo">
            <img src="{{URL::asset('public/assets/images/logo.png')}}" height="54" alt="Cherish Gold BPO-CRM" />
        </a>
        <div class="panel panel-sign">
            <div class="panel-body">
                @if(Session::has('msg'))
                <div class="alert alert-danger text-cente">
                    <span>
                        <h5> {{Session::get('msg')}}</h5>
                    </span>
                </div>  
                @endif
                <form class="login-form" action="{{ route('reset-password-post') }}" id="changepasswords" method="post" name="changepasswords">
                    <input type="hidden" name="_token" value="{{ Session::token() }}">
                    <h3 class="form-title">Reset Password</h3>
                    <div class="form-group">
                        <label class="control-label pull-left">Current Password</label>
                        <div class="input-group input-group-icon">
                            <input class="form-control placeholder-no-fix" type="text"  autocomplete="off" placeholder="Current Password" name="currentpassword"/>
                            <span class="input-group-addon">
                                <span class="icon icon-lg">
                                    <i class="fa fa-lock"></i>
                                </span>
                            </span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label pull-left">New Password</label>
                        <div class="input-group input-group-icon">
                            <input class="form-control placeholder-no-fix" type="text"  autocomplete="off" placeholder="New Password" name="newpassword"/ id="newpassword">
                                   <span class="input-group-addon">
                                <span class="icon icon-lg">
                                    <i class="fa fa-lock"></i>
                                </span>
                            </span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label pull-left">Repeat New Password</label>
                        <div class="input-group input-group-icon">
                            <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Repeat New Password" name="passwordconfirmation" id="passwordconfirmation"/>
                            <span class="input-group-addon">
                                <span class="icon icon-lg">
                                    <i class="fa fa-lock"></i>
                                </span>
                            </span>
                        </div>
                    </div>
                    <div class="form-actions">
                        <label class="checkbox">
                            <button type="submit" class="btn green pull-right sbmt">
                                Reset Password <i class="m-icon-swapright m-icon-white"></i>
                            </button>
                    </div>
                </form>
            </div>
        </div>
</section>
        <script src="{{URL::asset('public/assets/javascripts/jquery.min.js')}}"></script>
        <script src="{{URL::asset('public/assets/vendor/bootstrap/js/bootstrap.js')}}"></script>
        <script src="{{URL::asset('public/assets/vendor/jquery-validation/jquery.validate.js')}}"></script>
        <script src="{{URL::asset('public/assets/javascripts/forms/form-validation.js')}}"></script>
        <script src="{{URL::asset('public/assets/javascripts/app.js')}}"></script>
        <script>
            jQuery(document).ready(function () {
                App.init();
                FormValidation.init();
                 $('#loading').hide();
            });

            function formsubmit(form){
                $('#loading').show();
                form.submit();
            }
        </script>
    </body>
</html> 


