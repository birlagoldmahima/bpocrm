@extends('layout.master')
@section('page_title')
User List 
@endsection
@section('page_level_style_top')
<header class="page-header">
    <h2>User List</h2>
    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="#">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span>User</span></li>
            <li><span>User List</span></li>
        </ol>
        <a class="sidebar-right-toggle" data-open="#"><i class="fa fa-chevron-left"></i></a>
    </div>
</header>
@endsection
@section('content')
<section role="main" class="content-body">
    <section class="panel">
        <header class="panel-heading">
            <div class="row">
                <h2 class="panel-title col-md-4">User List</h2>
                <div class="alert alert-success col-md-5 message" style="padding:7px;margin-bottom: -8px;display:none;"></div>
            </div>
        </header>
        <div class="panel-body">
            <div class="col-md-12">
                <form class="form-horizontal" id="frm">
                    <table class="table"> 
                        <tbody>
                            <tr>
                                <td>
                                    <input type="text" name="q" class="form-control" id="q" placeholder="Search" value="{{isset($q)?$q:""}}">
                                </td>
                                <td>
                                    <select class="form-control" name="search_option" id="search_option">
                                        <option value="">Select</option>
                                        <option value="ID" <?php if ($search_option == 'ID') echo 'selected'; ?> >ID</option>
                                        <option value="Name" <?php if ($search_option == 'Name') echo 'selected'; ?>>Name</option>
                                        <option value="Email" <?php if ($search_option == 'Email') echo 'selected'; ?>>Email</option>                           
                                        <option value="Mobile" <?php if ($search_option == 'Mobile') echo 'selected'; ?>>Mobile</option>
                                        <option value="Role" <?php if ($search_option == 'Role') echo 'selected'; ?>>Role</option>
                                    </select> 
                                </td>
                                <td>
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                    <a href="{{ route('userlist') }}" class="btn btn-primary">Clear</a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </form>
            </div>
            <table class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Mobile</th>
                        <th>Role</th>
                        <th>Reporting Manager</th>

                        <th >Action</th>

                    </tr>
                </thead>
                <tbody>
                    <?php $count = 1; ?>
                    @if (count($data) > 0)
                    @foreach ($data as $key => $val)
                    <tr class="gradeX tr"  data-id="{{$count}}">
                        <td> <?php echo $val->id; ?></td>
                        <td>    
                            <?php echo empty($val->name) ? "-" : $val->name; ?>
                        </td>
                        <td>
                            <?php echo $val->email; ?> 
                        </td>
                        <td>
                            <?php echo $val->mobile; ?> 
                        </td>
                        <td>
                            <?php echo $val->role; ?>  
                        </td>
                        <td>
                            <?php echo $val->remanager; ?>  
                        </td>
                        <td width="40%"> 
                            <?php if ($val->status == 'inactive') { ?>
                                    {{ucfirst( $val->status) }}
                                <?php } else { ?>
                                    <a id="{{ $val->id }}" data-value="{{$key }}" class="btn green clk  btn-primary">In Active  </a> &nbsp;  <a id="{{ $val->id }}" data-value="{{$key }}" class="resetpass btn green btn-primary">Reset Password</a> &nbsp; <a data-id="{{ $val->id }}" data-role="{{ $val->role }}" data-reporting="{{ $val->reporting_manager }}" class="chnagereporting btn green btn-primary">Change Reporting</a>
                                <?php } ?>
                        </td>
                    </tr>
                    <?php $count++; ?>
                    @endforeach
                    @else
                    <tr>
                        <td colspan="8" align="center">
                            Not Data Found
                        </td>
                    </tr>
                    @endif
                </tbody>
            </table>
            <div class ="pull-right" style="margin-bottom:20px">
                {!! $data->appends(['q'=>$q,'search_option'=>$search_option])->render() !!}
            </div>
        </div>
    </section>
</section>
<div class="modal fade" id="changereport" tabindex="-1" role="basic" aria-hidden="true"  data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Change Reporting of User ID <span id="userid"></span></h4>
                </div>
                <div class="modal-body">
                    <div class="form-body">
                        <div class="form-group">
                            <div class="row">
                                <label class="col-md-2 control-label">Reporting:</label>
                                <div class="col-md-10">
                                    <select class="form-control" name="reporting" id="reporting">
                                        <option value="">Select Reporting</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn green" id="changereporting">Change</button>
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('page_level_script_bottom')
<script src="{{URL::to('/public/assets/javascripts/app.js')}}"></script>
<script src="{{URL::to('/public/assets/bootbox/bootbox.min.js')}}"></script>
<script>
    jQuery(document).ready(function () {
        $('#loading').hide();
    });

    $('.clk').click(function () {
        var id = $(this).attr('id');
        var trvalue = $(this).attr('data-value');
        bootbox.confirm("Are you sure, You want to Inactive User Id "+id, function (result) {
            if (result == true) {
                $.ajax({
                    url: "{{route('updateuserstatus')}}",
                    data: {'id': id, '_token': "{{ csrf_token() }}"},
                    type: 'post',
                    cache: false,
                    clearForm: false,
                    success: function (response) {                    
                        if(response==1){
                            $('.message').show().html("Id "+id+" Mark as Inactive!!!");
                            window.scrollTo(0,0);
                            setTimeout(function(){window.location.reload();},800);
                        }else{
                            alert("Something Went Wrong!!!! \n Please try after sometimes!!!");
                        }
                    }
                });
            }
        });
    });

    $('.resetpass').click(function () {
        var id = $(this).attr('id');
        bootbox.confirm("Are you sure, You want to Reset Password of User ID "+id, function (result) {
            if (result == true) {
                $.ajax({
                    url: "{{ route('resetpass')}}",
                    data: {'id': id, '_token':"{{ csrf_token() }}"},
                    type: 'post',
                    cache: false,
                    clearForm: false,
                    success: function (response){
                        if(response==1){
                            alert('Your Password is Reset now. \n Your Password is 123456');
                        }else{
                            alert("Something Went Wrong \n Try after Sometimes");
                        }
                    }
                });
            }
        });
    });

    $('#frm').submit(function(){
        $('#loading').show();
        var search = $('#q').val()=="" ? $('#q').focus() : true;
        var option = $('#search_option option:selected').val()=="" ? $('#search_option').focus() : true;
        if(search==true && option==true){
            return true;
        }
        $('#loading').hide();
        return false;
    });

    $('.chnagereporting').click(function(){
        var role = $(this).data('role');
        var rolepresent = $(this).data('reporting');
        $("#reporting option").not(':first').remove();
        var id = $(this).data('id');
        $('#userid').html(id);
        if (role) {
            $.ajax({
                url: "{{route('get-reporting')}}",
                data: {'role': role, '_token': "{{ csrf_token() }}"},
                type: 'post',
                async: true,
                cache: false,
                clearForm: false,
                beforeSend:function(){  
                  $('#loading').show();
                },
                success: function (response) {
                    $.each(response, function (idx, obj) {
                        if(rolepresent!=idx){
                            $("#reporting").append(
                                $("<option></option>").text(obj).val(idx)
                            );
                        }
                    });
                },
                error:function(){
                    alert("Server is Busy!!");
                },
                complete:function (data) {
                    $('#loading').hide();
                }
            });
        }
        $('#changereport').modal('toggle');
    });
    
    $('#changereporting').click(function(){
        var value = $("#reporting option:selected").val();
        if(value==""){
            alert("Select Reporting!!!");
            return false;
        }
        var id = $('#userid').html();
        $.ajax({
            url: "{{route('changereporting')}}",
            data: {'id':id,'reporting':value},
            type: 'get',
            async: true,
            cache: false,
            clearForm: false,
            beforeSend:function(){  
              $('#loading').show();
            },
            success: function (response) {
                $('.message').show().html("Reporting of User ID "+id+" Change Successfully!!!");
                window.scrollTo(0,0);
                setTimeout(function(){window.location.reload();},800);
            },
            error:function(){
                alert("Server is Busy!!");
            },
            complete:function (data) {
                $('#loading').hide();
            }
        });
    });
</script>

@endsection         