@extends('layout.master')
@section('page_title')
    Add User 
@endsection
@section('page_level_style_top')
    <header class="page-header">
        <h2>Add User</h2>
        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li>
                    <a href="#">
                        <i class="fa fa-home"></i>
                    </a>
                </li>
                <li><span>Add User</span></li>
            </ol>
            <a class="sidebar-right-toggle" data-open="#"><i class="fa fa-chevron-left"></i></a>
        </div>
    </header>
@endsection
@section('content')
    <section role="main" class="content-body">
        <section class="panel">
            <header class="panel-heading">
                <h2 class="panel-title">Add User</h2>
            </header>
            <div class="panel-body">
                <form id="form" action="{{ url('add_user') }}" class="form-horizontal form-bordered" method="post" name="form">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-group">
                        <label class="col-sm-3 control-label"> Name <span class="required">*</span></label>
                        <div class="col-sm-6">
                            <input type="text" name="name" class="form-control" placeholder="Please Enter Name" required/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Email <span class="required">*</span></label>
                        <div class="col-sm-6">                              
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-envelope"></i>
                                </span>
                                <input type="email" name="email" class="form-control" placeholder="Please Enter Email" autocomplete="off" required id="email"/>
                            </div>
                            <span style="color:red;">(Used For login)</span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Password <span class="required">*</span></label>
                        <div class="col-sm-6">
                            <input type="password" name="password" class="form-control" placeholder="Please Enter Password" required />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Mobile <span class="required">*</span></label>
                        <div class="col-sm-6">
                            <input type="text" name="mobile" class="form-control" placeholder="Please Enter Mobile" required/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="inputSuccess">Role</label>                            
                        <div class="col-sm-6">
                            <select class="form-control " name="role" required="" id="role">
                                <option value=""> -- Select Role -- </option>
                                <?php
                                foreach (config('custom.ROLES') as $value) {
                                    ?>
                                    <option value="{{ $value }}">{{ $value }}</option>
                                <?php }
                                ?>
                            </select>                               
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Reporting Manager </label>
                        <div class="col-sm-6">                                
                            <select class="form-control" name="reporting_manager" required id="reporting_manager">
                                <option value=""> -- Select Reporting Manager -- </option>
                            </select>

                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12" align="center">
                            <button class="btn btn-primary" id="submit" type="submit">Submit</button>
                            <a href="{{ url('user_list') }}" class="btn btn-default">Cancel </a>
                        </div>
                    </div>
                </form>
            </div>
        </section>
    </section>
@endsection
@section('page_level_script_bottom')
    <script src="{{URL::asset('public/assets/vendor/jquery-validation/jquery.validate.js')}}"></script>
    <script src="{{URL::asset('public/assets/javascripts/forms/form-validation.js')}}"></script>
    <script src="{{URL::asset('public/assets/javascripts/app.js')}}"></script>
    <script>
    jQuery(document).ready(function () {
        App.init();
        //FormComponents.init();
        FormValidation.init();
        $('#loading').hide();
    });

    $("#role").change(function () {
        var role = $("#role").val();
        $("#reporting_manager option").not(':first').remove();
        if (role) {
            $.ajax({
                url: "{{route('get-reporting')}}",
                data: {'role': role, '_token': "{{ csrf_token() }}"},
                type: 'post',
                async: true,
                cache: false,
                clearForm: false,
                beforeSend:function(){  
                  $('#loading').show();
                },
                success: function (response) {
                    $.each(response, function (idx, obj) {
                        $("#reporting_manager").append(
                            $("<option></option>").text(obj).val(idx)
                        );
                    });
                },
                error:function(){
                    alert("Server is Busy!!");
                },
                complete:function (data) {
                    $('#loading').hide();
                }
            });
        }
    });
    $('#submit').click(function(e){
        if($('#form').valid()==false){
            return false;
        }
        var name=$("#email").val();
        var flag = false;
        $('#loading').show();
        $.ajax({
            url: "{{route('username-check')}}",
            data: {'name': name, '_token': "{{ csrf_token() }}"},
            type: 'post',
            async: false,
            cache: false,
            clearForm: false,
            success: function (response) {
                if(response==1){
                    alert("User Name Already Exist!!!!");
                    $('#loading').hide();
                }else{
                    flag = true;
                }
            },
            error:function(){
                alert("Server is Busy!!");
            },
            complete:function (data) {
                
            }
        });
        return flag;
    });
</script>

@endsection
