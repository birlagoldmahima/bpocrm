@extends('layout.master')
@section('page_title')
    User List 
@endsection
@section('page_level_style_top')
    <header class="page-header">
        <h2>User List</h2>
        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li>
                    <a href="#">
                        <i class="fa fa-home"></i>
                    </a>
                </li>
                <li><span>User</span></li>
                <li><span>User List</span></li>
            </ol>
            <a class="sidebar-right-toggle" data-open="#"><i class="fa fa-chevron-left"></i></a>
        </div>
    </header>
@endsection
@section('content')
    <section role="main" class="content-body">
        <section class="panel">
            <header class="panel-heading">
                <h2 class="panel-title">User List</h2>
            </header>
            <div class="panel-body">
                <div class="col-md-12">
                    <form class="form-horizontal">
                        <table class="table"> 
                            <tbody>
                                <tr>
                                    <td>
                                        <input type="text" name="q" class="form-control" id="exampleInputUsername2" placeholder="Search" value="{{isset($q)?$q:""}}">
                                    </td>
                                    <td>
                                        <select class="form-control" name="search_option" required="" >
                                            <option value="">Select</option>
                                            <option value="ID" <?php if ($search_option == 'ID') echo 'selected'; ?> >ID</option>
                                            <option value="Name" <?php if ($search_option == 'Name') echo 'selected'; ?>>Name</option>
                                            <option value="Email" <?php if ($search_option == 'Email') echo 'selected'; ?>>Email</option>                           
                                            <option value="Mobile" <?php if ($search_option == 'Mobile') echo 'selected'; ?>>Mobile</option>
                							<option value="Role" <?php if ($search_option == 'Role') echo 'selected'; ?>>Role</option>
    						            </select> 
                                    </td>
                                    <td>
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                        <a href="{{ route('user_list') }}" class="btn btn-primary">Clear</a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </form>
                </div>
                <table class="table table-bordered table-striped mb-none" id="datatable-editable">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th >Mobile</th>
                            <th >Role</th>
                            <th >Reporting Manager</th>
                            <th >Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if (count($data) > 0)
                            @foreach ($data as $key => $val)
                                <tr class="gradeX">
                                    <td> <?php echo $val->id; ?></td>
                                    <td>    
                                        <?php echo empty($val->name) ? "-" : $val->name; ?>
                                    </td>
                                    <td>
                                        <?php echo $val->email; ?> 
                                    </td>
                                    <td class="center hidden-phone">
                                        <?php echo $val->mobile; ?> 
                                    </td>
                                    <td class="center hidden-phone">
                                        <?php echo $val->role; ?>  
                                    </td>
                                    <td class="center hidden-phone">
                                        <?php echo $val->remanager; ?>  
                                    </td>
                                    <td class="center hidden-phone">
                                        <?php echo ucfirst($val->status); ?>  
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="8" align="center">
                                    Not Data Found
                                </td>
                            </tr>
                        @endif
                    </tbody>
                </table>
                <div class ="pull-right" style="margin-bottom:20px">
                    {!! $data->appends(['q'=>$q,'search_option'=>$search_option])->render() !!}
                </div>
            </div>
        </section>
    </section>
@endsection
@section('page_level_script_bottom')
    <script>
        jQuery(document).ready(function () {
            $('#loading').hide();
        });
    </script>
@endsection 		