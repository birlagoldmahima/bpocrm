@extends('layout.master')
@section('page_title')
Edit User
@endsection
@section('')
<li>
    <a href="#">Edit User </a>
</li>
@endsection
@section('content')
<?php // print_r($role);
//die;
?>
<div class="row">
    <div class="loading" id="loading" style="display:none">
        <img class="loading-image" id="loading-image" src="{{URL::to('public/img/ajax-loading.gif')}}" alt="Loading..." />
    </div>
    <div class="col-md-12">

        <div class="portlet box litgreen">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-reorder"></i> Edit User
                </div>
            </div>
            <div class="portlet-body">
                @if(Session::has('flash_message'))
                <div class="alert alert-success text-cente ">
                    {{ Session::get('flash_message') }}
                </div>
                @endif
                @if($errors->any())
                <div class="alert alert-danger text-cente">
                    @foreach($errors->all() as $error)
                    <p>{{ $error }}</p>
                    @endforeach
                </div>
                @endif
                <form id="form" action="{{ url('update_user') }}" class="form-horizontal" method="post">
                    <input type="hidden" name="id" value="{{$data['data']['id']}}">
                    <section class="panel">
                        <div class="panel-body">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="form-group">
                                <label class="col-sm-3 control-label"> Name <span class="required">*</span></label>
                                <div class="col-sm-6">
                                    <input type="text" name="name" class="form-control" value="{{$data['data']['name']}}" required/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Email <span class="required">*</span></label>
                                <div class="col-sm-6">
                                    <input type="text" name="email" class="form-control"  value="{{$data['data']['email']}}" required/>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Mobile <span class="required">*</span></label>
                                <div class="col-sm-6">
                                    <input type="text" name="mobile" class="form-control"  value="{{$data['data']['mobile']}}" required/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Role <span class="required">*</span></label>
                                <div class="col-sm-6">
                                    <div class="col-sm-12 input-group">
                                        <select class="form-control input-sm" name="role" required="">
                                            <option value=""> -- Select Role -- </option>                                          
                                            
                                            <?php foreach($role['ROLES'] as $key=> $val)
                                           {
                                               ?>
                                            <option value="<?php echo $val ?>" <?php if ($data['data']['role'] == $val ) echo 'selected' ; ?>><?php echo $val ?></option>
                                           <?php } ?>
                                          
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Reporting Manager <span class="required">*</span></label>
                                <div class="col-sm-6">
                                    <div class="col-sm-12 input-group">
                                        <select class="form-control input-sm" name="rmanager" required="">
                                            <option value=""> -- Reporting Manager -- </option>
                                            <?php foreach($reportingm as $key=> $value)
                                           {
                                               ?>
                                            <option value="<?php echo $value['id'] ?>" <?php if ($data['data']['reporting_manager'] ==  $value['id'] ) echo 'selected' ; ?>><?php echo $value['name']."  ".$value['role']; ?></option>
                                           <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Status </label>
                                <div class="col-sm-6">
                                    <div class="col-sm-12 input-group">
                                        <select class="form-control input-sm" name="status" required="">
                                            <option value=""> -- Select Status -- </option>
                                            <option value="active" @if($data['data']['status'] == "active") selected @endif >Active</option>
                                            <option value="inactive" @if($data['data']['status'] == "inactive") selected @endif >Inactive</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-9">

                                </div>
                            </div>
                        </div>
                        <footer class="panel-footer">
                            <div class="row">
                                <div class="col-sm-12" align="center">
                                    <button class="btn btn-primary">Submit</button>
                                    <a href="{{ url('user_list') }}" class="btn btn-default">Cancel </a>
                                </div>
                            </div>
                        </footer>
                    </section>
                </form>

            </div>
            <div class ="pull-right" style="margin-bottom:20px">

            </div>
        </div>

        @endsection
        @section('page_level_script_bottom')
        @endsection			