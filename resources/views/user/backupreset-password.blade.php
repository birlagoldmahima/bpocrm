<!doctype html>
<html class="fixed">
	<head>
		<meta charset="UTF-8">
		<meta name="keywords" content="Cherish Gold | Birla Gold" />
		<meta name="description" content="Cherish Gold | BPO-CRM">
		<meta name="author" content="Sukhsagar">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">
		<link rel="stylesheet" href="{{URL::asset('public/assets/vendor/bootstrap/css/bootstrap.css')}}" />
		<link rel="stylesheet" href="{{URL::asset('public/assets/vendor/font-awesome/css/font-awesome.css')}}" />
		<link rel="stylesheet" href="{{URL::asset('public/assets/vendor/magnific-popup/magnific-popup.css')}}" />
		<link rel="stylesheet" href="{{URL::asset('public/assets/vendor/bootstrap-datepicker/css/datepicker3.css')}}" />
		<link rel="stylesheet" href="{{URL::asset('public/assets/stylesheets/theme.css')}}" />
		<link rel="stylesheet" href="{{URL::asset('public/assets/stylesheets/skins/default.css')}}" />
		<link rel="stylesheet" href="{{URL::asset('public/assets/stylesheets/theme-custom.css')}}">
		<script src="{{URL::asset('public/assets/vendor/modernizr/modernizr.js')}}"></script>
	</head>
	<body>
		<section class="body-sign">
			<div align="center" class="center-sign">
				<a class="logo">
					<img src="{{URL::asset('public/assets/images/logo.png')}}" height="54" alt="Cherish Gold BPO-CRM" />
				</a>
				<div class="panel panel-sign">
					<div class="panel-body">
						@if(Session::has('msg'))
							<div class="alert alert-danger text-cente">
								<span>
									<h5> {{Session::get('msg')}}</h5>
								</span>
							</div>  
						@endif
						<form class="login-form" action="{{ route('reset-password-post') }}" id="changepasswords" method="post">
							<input type="hidden" name="_token" value="{{ Session::token() }}">
							<h3 class="form-title">Reset Password</h3>
							<div class="form-group">
								<label class="control-label pull-left">Current Password</label>
								<div class="input-group input-group-icon">
									<input class="form-control placeholder-no-fix" type="text"  autocomplete="off" placeholder="Current Password" name="currentpassword"/>
									<span class="input-group-addon">
										<span class="icon icon-lg">
											<i class="fa fa-lock"></i>
										</span>
									</span>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label pull-left">New Password</label>
								<div class="input-group input-group-icon">
									<input class="form-control placeholder-no-fix" type="text"  autocomplete="off" placeholder="New Password" name="newpassword"/ id="newpassword">
									<span class="input-group-addon">
										<span class="icon icon-lg">
											<i class="fa fa-lock"></i>
										</span>
									</span>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label pull-left">Repeat New Password</label>
								<div class="input-group input-group-icon">
									<input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Repeat New Password" name="passwordconfirmation" id="passwordconfirmation"/>
									<span class="input-group-addon">
										<span class="icon icon-lg">
											<i class="fa fa-lock"></i>
										</span>
									</span>
								</div>
							</div>
							<div class="form-actions">
								<label class="checkbox">
								<button type="submit" class="btn green pull-right sbmt">
								Reset Password <i class="m-icon-swapright m-icon-white"></i>
								</button>
							</div>
						</form>
					</div>
				</div>
		</section>
		<script src="{{URL::asset('public/assets/vendor/jquery/jquery.js')}}"></script>
		<script src="{{URL::asset('public/assets/vendor/jquery-browser-mobile/jquery.browser.mobile.js')}}"></script>
		<script src="{{URL::asset('public/assets/vendor/bootstrap/js/bootstrap.js')}}"></script>
		<script src="{{URL::asset('public/assets/vendor/nanoscroller/nanoscroller.js')}}"></script>
		<script src="{{URL::asset('public/assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
		<script src="{{URL::asset('public/assets/vendor/magnific-popup/magnific-popup.js')}}"></script>
		<script src="{{URL::asset('public/assets/vendor/jquery-placeholder/jquery.placeholder.js')}}"></script>
		<script src="{{URL::asset('public/assets/javascripts/theme.js')}}"></script>
		<script src="{{URL::asset('public/assets/javascripts/theme.custom.js')}}"></script>
		<script src="{{URL::asset('public/assets/javascripts/theme.init.js')}}"></script>
		<script type="text/javascript">
			$('#changepasswords').submit(function(){
				if($('#newpassword').val()!=$('#passwordconfirmation').val()){
					alert("New Password && Repeat Password Does Not Match");
					return false;
				}
				//if()
				return true;
			});
		</script>
	</body>
</html>