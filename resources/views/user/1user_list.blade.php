@extends('layout.master')
    @section('page_title')
        User List 
        @endsection
@section('breadcrumb')
<li>
    <a href="#">User List </a>
</li>
@endsection
@section('content')
<section role="main" class="content-body">
    <header class="page-header">
        <h2>User List</h2>
        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li>
                    <a href="#">
                        <i class="fa fa-home"></i>
                    </a>
                </li>
                <li><span>User</span></li>
                <li><span>User List</span></li>
            </ol>
            <a class="sidebar-right-toggle" data-open="#"><i class="fa fa-chevron-left"></i></a>
        </div>
    </header>
    <section class="panel">
        <header class="panel-heading">
            <h2 class="panel-title">User List</h2>
        </header>
        <div class="panel-body">
            <table class="table table-bordered table-striped mb-none" id="datatable-editable">

                <tr>
                <form class="form-horizontal">
                    <div class="form-group col-md-3">
                        <label class="sr-only" for="exampleInputUsername2">Username</label>
                        <input type="text" name="q" class="form-control" id="exampleInputUsername2" placeholder="Search" value="{{isset($q)?$q:""}}">
                    </div>

                    <div class="form-group col-md-3">
                       <select class="form-control" name="search_option" required="" >
                            <option value="">Select</option>
                            <option value="ID" <?php if ($search_option == 'ID') echo 'selected'; ?> >ID</option>
                            <option value="Name" <?php if ($search_option == 'Name') echo 'selected'; ?>>Name</option>
                            <option value="Email" <?php if ($search_option == 'Email') echo 'selected'; ?>>Email</option>                           
                            <option value="Mobile" <?php if ($search_option == 'Mobile') echo 'selected'; ?>>Mobile</option>
							<option value="Role" <?php if ($search_option == 'Role') echo 'selected'; ?>>Role</option>
						</select> 
  
                    </div>
                    <div class="form-group col-md-3">
                        <button type="submit" class="btn btn-primary">Submit</button>
                        <a href="{{ route('user_list') }}" class="btn btn-primary">Clear</a>
                    </div>
                </form>
                <!-- <a class="btn btn-primary" href="{{ URL::route('user_list',['q'=>$q,'search_option'=>$search_option,'flag'=>1])}}">Download Report</a> -->
               
                </tr>
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th class="hidden-phone">Mobile</th>
                        <th class="hidden-phone">Role</th>
                        <th class="hidden-phone">Reporting Manager</th>
                        <th class="hidden-phone">Status</th>

                        <!-- <th class="hidden-phone"> Action</th> -->
                    </tr>
                </thead>
                <tbody>


<?php
if (count($data) > 0) {
    foreach ($data as $key => $val) {
        ?>
                            <tr class="gradeX">

                                <td>
                            <?php echo $val->id; ?>
                                </td>
                                <td>
                                    <?php echo empty($val->name) ? "-" : $val->name; ?>
                                </td>
                                <td>
                                    <?php echo $val->email; ?> 
                                </td>
                                <td class="center hidden-phone">
                                    <?php echo $val->mobile; ?> 
                                </td>
                                <td class="center hidden-phone">
                                    <?php echo $val->role; ?>  
                                </td>
                                <td class="center hidden-phone">
                                    <?php echo $val->remanager; ?>  
                                </td>
                                <td class="center hidden-phone">
                                    <?php echo ucfirst($val->status); ?>  
                                </td>
                                <!-- <td class="center hidden-phone"> 
                                    {{--@if(in_array(Auth::user()->role,['Relationship Manager','Customer Service','Trainee']))--}}
                                    <!-- <a href="{{ URL::to('edit_user',$val->id)}}" class="btn btn-primary">Edit</a>  -->
                                    {{--@endif--}}
                                <!-- </td> -->
    <?php
    }
} else {
    ?>

                        </tr>
                        <tr>
                            <td colspan="8" align="center">
                                Not Data Found
                            </td>
                        </tr>
<?php } ?>
                </tbody>
            </table>
            <div class ="pull-right" style="margin-bottom:20px">
                             {!! $data->appends(['q'=>$q,'search_option'=>$search_option])->render() !!}

                    </div>
        </div>
    </section>
    <!-- end: page -->
</section>


@endsection
@section('page_level_script_bottom')
<!-- Vendor -->
<!-- Examples -->

<script src="{{URL::asset('public/assets/javascripts/tables/examples.datatables.default.js')}}"></script>
<script src="{{URL::asset('public/assets/javascripts/tables/examples.datatables.row.with.details.js')}}"></script>
<script src="{{URL::asset('public/assets/javascripts/tables/examples.datatables.tabletools.js')}}"></script>

        <script src="{{URL::asset('public/assets/javascripts/tables/examples.datatables.default.js')}}"></script>
        <script src="{{URL::asset('public/assets/javascripts/tables/examples.datatables.row.with.details.js')}}"></script>
        <script src="{{URL::asset('public/assets/javascripts/tables/examples.datatables.tabletools.js')}}"></script>

@endsection			