var FormValidation = function () {
	var lead = function() {
		var form3 = $('#form');
        var error3 = $('.alert-danger', form3);
        var success3 = $('.alert-success', form3);
        $.validator.addMethod("multiple1000",function(value, element) {
            return value % 1000 == 0
        },"Amount Shold be multiple of 1000");
		form3.validate({
			errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
				name: {
                    required: true
                },
                mobile: {
                    required: true,
                    number: true,
                    minlength: 10,
                    maxlength: 10,
                },
                state:{
                    required: true, 
                },
                city:{
                    required: true, 
                },
                address:{
                    required:true,
                },
                landmark:{
                    required:true,  
                },
                emi_size:{
                    number: true,
                    multiple1000:true,
                },                    
            },
			errorPlacement: function (error, element) {
				if (element.parent(".input-group").size() > 0) {
                    error.insertAfter(element.parent(".input-group"));
                } else if (element.attr("data-error-container")) { 
                    error.appendTo(element.attr("data-error-container"));
                } else if (element.parents('.radio-list').size() > 0) { 
                    error.appendTo(element.parents('.radio-list').attr("data-error-container"));
                } else if (element.parents('.radio-inline').size() > 0) { 
                    error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
                } else if (element.parents('.checkbox-list').size() > 0) {
                    error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
                } else if (element.parents('.checkbox-inline').size() > 0) { 
                    error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
                } else {
                    error.insertAfter(element);
                }
            },

            invalidHandler: function (event, validator) {
                success3.hide();
                error3.show();
                App.scrollTo(error3, -200);
            },

            highlight: function (element){
               $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function (element) { 
                $(element).closest('.form-group').removeClass('has-error');
            },

            success: function (label) {
                label.closest('.form-group').removeClass('has-error');
            },
            submitHandler: function (form) {
                formsubmit(form);
            }
        });
    }
	   
    var customerInteraction = function() {
        var form3 = $('#customer-interaction');
        var error3 = $('.alert-danger', form3);
        var success3 = $('.alert-success', form3);
        form3.validate({
            errorElement: 'span', 
            errorClass: 'help-block',
            focusInvalid: false, 
            ignore: "",
            rules: {
                interaction: {
                    required: true
                },
                callsubstatus: {
                    required: true
                },
                call_sub_status_other: {
                    required: true
                },
                callstatus: {
                    required: true
                },
                callinitiator: {
                    required: true
                },
                nextdatetime:{
                    checkdatetime:true,
                }
            },
            messages: { 
                interaction: {
                    required: "Please enter a brief summary of the call"
                },
                callinitiator: {
                    required: "Please select one Call Initiator Option"
                },
                callstatus: {
                    required: "Please select one call Status"
                },
                callsubstatus: {
                    required: "Please select one call Sub Status"
                }
            },
            errorPlacement: function (error, element) {
                if (element.parent(".input-group").size() > 0) {
                    error.insertAfter(element.parent(".input-group"));
                } else if (element.attr("data-error-container")) { 
                    error.appendTo(element.attr("data-error-container"));
                } else if (element.parents('.radio-list').size() > 0) { 
                    error.appendTo(element.parents('.radio-list').attr("data-error-container"));
                } else if (element.parents('.radio-inline').size() > 0) { 
                    error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
                } else if (element.parents('.checkbox-list').size() > 0) {
                    error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
                } else if (element.parents('.checkbox-inline').size() > 0) { 
                    error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
                } else {
                    error.insertAfter(element);
                }
            },

            invalidHandler: function (event, validator) {
                success3.hide();
                error3.show();
                App.scrollTo(error3, -200);
            },

            highlight: function (element) {
               $(element).closest('.form-group').addClass('has-error');
            },

            unhighlight: function (element){
                $(element).closest('.form-group').removeClass('has-error');
            },

            success: function (label) {
                label.closest('.form-group').removeClass('has-error');
            },

            submitHandler: function (form) {
                postInteraction();
            }
        });
    }

    var changerequestvalidation = function() {
            var form200 = $('#changerequest');
            var error200 = $('.alert-danger', form200);
            var success200 = $('.alert-success', form200);
            
            $.validator.addMethod("multiple1000",function(value, element) {
                return value % 1000 == 0
            },"Amount Shold be multiple of 1000");

            form200.validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-block', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: ":hidden",
                rules: {
                    contact_email: {
                        required: true,
                        email: true
                    },
                    applicant_name: {
                        required: true
                    },
                    contact_mobile: {
                        number: true,
                        required: true,
                        minlength: 10
                    },
                    applicant_dob:{
                        required: true
                    },
                    applicant_gender:{
                        required: true  
                    },
                    mailing_address:{
                        required: true
                    },
                    mailing_landmark:{
                        required: true  
                    },
                    mailing_state:{
                        required: true  
                    },
                    mailing_city:{
                        required: true   
                    },
                    mailing_district:{
                        required: true  
                    },
                    mailing_pincode:{
                        required: true  
                    },
                    amount:{
                        required:true,
                        digits: true,
                        multiple1000:true
                    },
                    fulfilment:{
                      required:true  
                    },
                    login_email:{
                        required:true,
                        email:true,
                    }
                },
                messages:{
                   
                },

                errorPlacement: function (error, element) { // render error placement for each input type
                   
                    if (element.parent(".input-group").size() > 0) {
                        error.insertAfter(element.parent(".input-group"));
                    } else if (element.attr("data-error-container")) { 
                        error.appendTo(element.attr("data-error-container"));
                    } else if (element.parents('.radio-list').size() > 0) { 
                        error.appendTo(element.parents('.radio-list').attr("data-error-container"));
                    } else if (element.parents('.radio-inline').size() > 0) { 
                        error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
                    } else if (element.parents('.checkbox-list').size() > 0) {
                        error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
                    } else if (element.parents('.checkbox-inline').size() > 0) { 
                        error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
                    } else {
                        error.insertAfter(element); // for other inputs, just perform default behavior
                    }
                },

                invalidHandler: function (event, validator) { //display error alert on form submit   
                    success200.hide();
                    //error200.show();
                    App.scrollTo(error200, -200);
                },

                highlight: function (element) { // hightlight error inputs
                   $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
                },

                success: function (label) {
                    label
                        .closest('.form-group').removeClass('has-error'); // set success class to the control group
                },

                submitHandler: function (form) {
                    //form.submit();
                    validatedata();
                }

            });
            $('.select2me', form200).change(function () {
                form200.validate().element($(this));
            });
    }

    var resetpass = function () {
        var form3 = $('#changepasswords');
        var error3 = $('.alert-danger', form3);
        var success3 = $('.alert-success', form3);
        $.validator.addMethod("multiple1000", function (value, element) {
            return value % 1000 == 0
        }, "Amount Shold be multiple of 1000");
        form3.validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                currentpassword: {
                    required: true
                },

                newpassword: {
                    minlength: 6,
                    required: true
                },
                passwordconfirmation: {
                    minlength: 6,
                    equalTo: "#newpassword",
                    required: true
                },


            },
            errorPlacement: function (error, element) {
                if (element.parent(".input-group").size() > 0) {
                    error.insertAfter(element.parent(".input-group"));
                } else if (element.attr("data-error-container")) {
                    error.appendTo(element.attr("data-error-container"));
                } else if (element.parents('.radio-list').size() > 0) {
                    error.appendTo(element.parents('.radio-list').attr("data-error-container"));
                } else if (element.parents('.radio-inline').size() > 0) {
                    error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
                } else if (element.parents('.checkbox-list').size() > 0) {
                    error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
                } else if (element.parents('.checkbox-inline').size() > 0) {
                    error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
                } else {
                    error.insertAfter(element);
                }
            },

            invalidHandler: function (event, validator) {
                success3.hide();
                error3.show();
                App.scrollTo(error3, -200);
            },

            highlight: function (element) {
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function (element) {
                $(element).closest('.form-group').removeClass('has-error');
            },

            success: function (label) {
                label.closest('.form-group').removeClass('has-error');
            },
            submitHandler: function (form) {
                formsubmit(form);
            }
        });
    }


    var loginform = function () {
        var form3 = $('#loginform');
        var error3 = $('.alert-danger', form3);
        var success3 = $('.alert-success', form3);
        $.validator.addMethod("multiple1000", function (value, element) {
            return value % 1000 == 0
        }, "Amount Shold be multiple of 1000");
        form3.validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                email: {
                    required: true,
                    email:true,
                },               
                password: {
                    required: true,
                },

            },
            messages: { // custom messages for radio buttons and checkboxes
                email: {
                    required: "Please Enter Email !!!"
                },
                password: {
                    required: "Please Enter password !!!"
                }
            },
            errorPlacement: function (error, element) {
                if (element.parent(".input-group").size() > 0) {
                    error.insertAfter(element.parent(".input-group"));
                } else if (element.attr("data-error-container")) {
                    error.appendTo(element.attr("data-error-container"));
                } else if (element.parents('.radio-list').size() > 0) {
                    error.appendTo(element.parents('.radio-list').attr("data-error-container"));
                } else if (element.parents('.radio-inline').size() > 0) {
                    error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
                } else if (element.parents('.checkbox-list').size() > 0) {
                    error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
                } else if (element.parents('.checkbox-inline').size() > 0) {
                    error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
                } else {
                    error.insertAfter(element);
                }
            },

            invalidHandler: function (event, validator) {
                success3.hide();
                error3.show();
                App.scrollTo(error3, -200);
            },

            highlight: function (element) {
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function (element) {
                $(element).closest('.form-group').removeClass('has-error');
            },

            success: function (label) {
                label.closest('.form-group').removeClass('has-error');
            },
            submitHandler: function (form) {
              
                formsubmit(form);
            }
        });
    }


	return {
        init: function () {
			lead();
            customerInteraction();
            changerequestvalidation();
            resetpass();
            loginform();
        }
    };
}();